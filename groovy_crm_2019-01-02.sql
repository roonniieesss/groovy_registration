# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.3.10-MariaDB)
# Database: groovy_crm
# Generation Time: 2019-01-02 02:06:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acl_gudang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_gudang`;

CREATE TABLE `acl_gudang` (
  `acl_gudang_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `kadaluarsa` datetime DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`acl_gudang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table acl_perusahaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_perusahaan`;

CREATE TABLE `acl_perusahaan` (
  `acl_perusahaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `perusahaan_id` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`acl_perusahaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table akun
# ------------------------------------------------------------

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_akun` varchar(45) DEFAULT NULL,
  `kelompok` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`akun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table alamat_pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `alamat_pelanggan`;

CREATE TABLE `alamat_pelanggan` (
  `alamat_pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) NOT NULL,
  `negara_id` int(11) DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `alamat` varchar(450) DEFAULT NULL,
  `kode_pos` varchar(45) DEFAULT NULL,
  `nama_penerima` varchar(45) DEFAULT NULL,
  `alamat_utama` tinyint(1) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`alamat_pelanggan_id`),
  KEY `fk_alamat_pelanggan_pelanggan1_idx` (`pelanggan_id`),
  KEY `fk_alamat_pelanggan_kota1_idx` (`kota_id`),
  KEY `fk_alamat_pelanggan_negara1_idx` (`negara_id`),
  CONSTRAINT `fk_alamat_pelanggan_kota1` FOREIGN KEY (`kota_id`) REFERENCES `kota` (`kota_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alamat_pelanggan_negara1` FOREIGN KEY (`negara_id`) REFERENCES `negara` (`negara_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alamat_pelanggan_pelanggan1` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table alasan_batal_inquiry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `alasan_batal_inquiry`;

CREATE TABLE `alasan_batal_inquiry` (
  `alasan_batal_inquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `alasan_batal_inquiry` varchar(160) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`alasan_batal_inquiry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table alat_produksi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `alat_produksi`;

CREATE TABLE `alat_produksi` (
  `alat_produksi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_alat` varchar(45) DEFAULT NULL,
  `merek` varchar(45) DEFAULT NULL,
  `tipe` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`alat_produksi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table apkir_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `apkir_aset`;

CREATE TABLE `apkir_aset` (
  `apkir_aset_id` int(11) NOT NULL,
  `nama_penanggung_jawab` varchar(45) DEFAULT NULL,
  `berita_acara_apkir` varchar(450) DEFAULT NULL,
  `status` enum('PENGAJUAN','APPROVED','REJECTED') DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`apkir_aset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table apkir_aset_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `apkir_aset_detail`;

CREATE TABLE `apkir_aset_detail` (
  `apkir_aset_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `apkir_aset_id` int(11) NOT NULL,
  `aset_id` int(11) NOT NULL,
  `penempatan_aset_id` int(11) DEFAULT NULL COMMENT 'Row penempatan aset diisi ketika apkir dilaksanakan',
  `alasan_apkir` enum('RUSAK','HILANG','DICURI') DEFAULT NULL,
  `keterangan_alasan_apkir` varchar(450) DEFAULT NULL,
  `sudah_diapkir` tinyint(4) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`apkir_aset_detail_id`),
  KEY `fk_pengeluaran_aset_detail_pengeluaran_aset1_idx` (`apkir_aset_id`),
  KEY `fk_apkir_aset_detail_penempatan_aset1_idx` (`penempatan_aset_id`),
  KEY `fk_apkir_aset_detail_aset1_idx` (`aset_id`),
  CONSTRAINT `fk_apkir_aset_detail_aset1` FOREIGN KEY (`aset_id`) REFERENCES `aset` (`aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_apkir_aset_detail_penempatan_aset1` FOREIGN KEY (`penempatan_aset_id`) REFERENCES `penempatan_aset` (`penempatan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pengeluaran_aset_detail_pengeluaran_aset1` FOREIGN KEY (`apkir_aset_id`) REFERENCES `apkir_aset` (`apkir_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table artikel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artikel`;

CREATE TABLE `artikel` (
  `artikel_id` int(11) NOT NULL AUTO_INCREMENT,
  `waktu_terbit` datetime DEFAULT NULL,
  `waktu_akhir_tayang` datetime DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`artikel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table artikel_konten
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artikel_konten`;

CREATE TABLE `artikel_konten` (
  `artikel_konten_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_id` int(11) NOT NULL,
  `kode_bahasa` char(5) NOT NULL,
  `judul` varchar(300) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `ringkasan` varchar(450) DEFAULT NULL,
  `tag` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`artikel_konten_id`),
  KEY `fk_artikel_konten_artikel_idx` (`artikel_id`),
  CONSTRAINT `fk_artikel_konten_artikel` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aset`;

CREATE TABLE `aset` (
  `aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_aset_id` int(11) NOT NULL,
  `pengadaan_aset_detail_id` int(11) DEFAULT NULL,
  `penerimaan_aset_id` int(11) DEFAULT NULL,
  `no_aset` varchar(45) DEFAULT NULL,
  `tanggal_perolehan` date DEFAULT NULL,
  `harga_perolehan` double DEFAULT NULL,
  `penyusutan_tahunan` double DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `habis_garansi` date DEFAULT NULL,
  `aset_dipakai` tinyint(4) DEFAULT NULL COMMENT 'Aset dipakai ini untuk membedakan saat inventarisasi aset mana yang memang dipakai atau hanya sekedar di simpan. Gunanya saat pengadaan, perusahaan dapat mencari aset-aset yang menganggur agar bisa dipakai di tempat lain.',
  `qty` int(11) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`aset_id`),
  KEY `fk_aset_jenis_aset1_idx` (`jenis_aset_id`),
  KEY `fk_aset_pengadaan_aset_detail1_idx` (`pengadaan_aset_detail_id`),
  KEY `fk_aset_penerimaan_aset1_idx` (`penerimaan_aset_id`),
  CONSTRAINT `fk_aset_jenis_aset1` FOREIGN KEY (`jenis_aset_id`) REFERENCES `jenis_aset` (`jenis_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_aset_penerimaan_aset1` FOREIGN KEY (`penerimaan_aset_id`) REFERENCES `penerimaan_aset` (`penerimaan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_aset_pengadaan_aset_detail1` FOREIGN KEY (`pengadaan_aset_detail_id`) REFERENCES `pengadaan_aset_detail` (`pengadaan_aset_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bahasa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bahasa`;

CREATE TABLE `bahasa` (
  `bahasa_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_bahasa` char(5) DEFAULT NULL,
  `nama_bahasa` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`bahasa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table barang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_barang_id` int(11) NOT NULL,
  `kemasan_id` int(11) DEFAULT NULL,
  `no_identifikasi` varchar(45) DEFAULT NULL,
  `kondisi` enum('BARU','BEKAS','RUSAK','DIBONGKAR') DEFAULT NULL,
  `catatan` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`barang_id`),
  KEY `fk_barang_jenis_barang1_idx` (`jenis_barang_id`),
  KEY `fk_barang_kemasan1_idx` (`kemasan_id`),
  CONSTRAINT `fk_barang_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_barang_kemasan1` FOREIGN KEY (`kemasan_id`) REFERENCES `kemasan` (`kemasan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table batal_inquiry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `batal_inquiry`;

CREATE TABLE `batal_inquiry` (
  `inquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `alasan_batal_inquiry_id` int(11) DEFAULT NULL,
  `keterangan_alasan` text DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  KEY `fk_batal_inquiry_inquiry1_idx` (`inquiry_id`),
  KEY `fk_batal_inquiry_alasan_batal_inquiry1_idx` (`alasan_batal_inquiry_id`),
  CONSTRAINT `fk_batal_inquiry_alasan_batal_inquiry1` FOREIGN KEY (`alasan_batal_inquiry_id`) REFERENCES `alasan_batal_inquiry` (`alasan_batal_inquiry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_batal_inquiry_inquiry1` FOREIGN KEY (`inquiry_id`) REFERENCES `inquiry` (`inquiry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table buku_besar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `buku_besar`;

CREATE TABLE `buku_besar` (
  `buku_besar_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_keuangan_id` int(11) NOT NULL,
  `akun_id` int(11) NOT NULL,
  `kurs` char(3) DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`buku_besar_id`),
  KEY `fk_buku_besar_akun_jurnal1_idx` (`akun_id`),
  KEY `fk_buku_besar_transaksi_keuangan1_idx` (`transaksi_keuangan_id`),
  CONSTRAINT `fk_buku_besar_akun_jurnal1` FOREIGN KEY (`akun_id`) REFERENCES `akun` (`akun_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_buku_besar_transaksi_keuangan1` FOREIGN KEY (`transaksi_keuangan_id`) REFERENCES `transaksi_keuangan` (`transaksi_keuangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table delivery_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `delivery_order`;

CREATE TABLE `delivery_order` (
  `delivery_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `gudang_id` int(11) NOT NULL,
  `jadwal_pengiriman_id` int(11) NOT NULL,
  `no_surat_jalan` varchar(45) DEFAULT NULL,
  `rencana_tanggal_kirim` date DEFAULT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `tanggal_diterima` date DEFAULT NULL,
  `nama_supir` varchar(45) DEFAULT NULL,
  `telp_supir` varchar(45) DEFAULT NULL,
  `nama_penerima` varchar(45) DEFAULT NULL,
  `alamat_penerima` varchar(45) DEFAULT NULL,
  `telp_penerima` varchar(45) DEFAULT NULL,
  `fax_penerima` varchar(45) DEFAULT NULL,
  `metode_kirim` enum('SENDIRI','EKSPEDISI','TAKE_AWAY') DEFAULT NULL,
  `catatan` varchar(450) DEFAULT NULL,
  `catatan_gudang` varchar(450) DEFAULT NULL,
  `status` enum('PENDING','MUAT','PERJALANAN','TERKIRIM','DIKEMBALIKAN','BATAL') DEFAULT NULL,
  `keterangan_status` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`delivery_order_id`),
  KEY `fk_delivery_order_gudang1_idx` (`gudang_id`),
  KEY `idx_no_surat_jalan` (`no_surat_jalan`),
  CONSTRAINT `fk_delivery_order_gudang1` FOREIGN KEY (`gudang_id`) REFERENCES `gudang` (`gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table delivery_order_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `delivery_order_detail`;

CREATE TABLE `delivery_order_detail` (
  `delivery_order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_order_id` int(11) NOT NULL,
  `sales_order_item_id` int(11) NOT NULL,
  `qty_kirim` double DEFAULT NULL,
  `status` enum('PENDING','TERKIRIM','BATAL') DEFAULT NULL,
  `alasan_pembatalan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`delivery_order_detail_id`),
  KEY `fk_delivery_order_detail_delivery_order1_idx` (`delivery_order_id`),
  KEY `fk_delivery_order_detail_sales_order_item1_idx` (`sales_order_item_id`),
  CONSTRAINT `fk_delivery_order_detail_delivery_order1` FOREIGN KEY (`delivery_order_id`) REFERENCES `delivery_order` (`delivery_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_order_detail_sales_order_item1` FOREIGN KEY (`sales_order_item_id`) REFERENCES `sales_order_item` (`sales_order_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table deposit_pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `deposit_pelanggan`;

CREATE TABLE `deposit_pelanggan` (
  `deposit_pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) NOT NULL,
  `transaksi_keuangan_id` int(11) NOT NULL,
  `kurs` char(3) DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`deposit_pelanggan_id`),
  KEY `fk_deposit_pelanggan_transaksi_keuangan1_idx` (`transaksi_keuangan_id`),
  CONSTRAINT `fk_deposit_pelanggan_transaksi_keuangan1` FOREIGN KEY (`transaksi_keuangan_id`) REFERENCES `transaksi_keuangan` (`transaksi_keuangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table gudang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gudang`;

CREATE TABLE `gudang` (
  `gudang_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `nama_gudang` varchar(45) DEFAULT NULL,
  `nama_pic` varchar(45) DEFAULT NULL,
  `alamat` varchar(45) DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `kapasitas_kirim_harian` int(11) DEFAULT NULL,
  `kapasitas_do_harian` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`gudang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table hasil_pecah_kemasan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hasil_pecah_kemasan`;

CREATE TABLE `hasil_pecah_kemasan` (
  `hasil_pecah_kemasan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pecah_kemasan_detail_id` int(11) NOT NULL,
  `transaksi_barang_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`hasil_pecah_kemasan_id`),
  KEY `fk_hasil_pecah_kemasan_pecah_kemasan_detail1_idx` (`pecah_kemasan_detail_id`),
  CONSTRAINT `fk_hasil_pecah_kemasan_pecah_kemasan_detail1` FOREIGN KEY (`pecah_kemasan_detail_id`) REFERENCES `pecah_kemasan_detail` (`pecah_kemasan_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table inquiry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inquiry`;

CREATE TABLE `inquiry` (
  `inquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) NOT NULL,
  `no_inquiry` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`inquiry_id`),
  KEY `fk_inquiry_pelanggan1_idx` (`pelanggan_id`),
  CONSTRAINT `fk_inquiry_pelanggan1` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table inquiry_komentar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inquiry_komentar`;

CREATE TABLE `inquiry_komentar` (
  `inquiry_komentar_id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_id` int(11) NOT NULL,
  `komentar` varchar(45) DEFAULT NULL,
  `penulis` enum('PELANGGAN','STAFF','BOT') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`inquiry_komentar_id`),
  KEY `fk_inquiry_komentar_inquiry1_idx` (`inquiry_id`),
  CONSTRAINT `fk_inquiry_komentar_inquiry1` FOREIGN KEY (`inquiry_id`) REFERENCES `inquiry` (`inquiry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table inquiry_produk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inquiry_produk`;

CREATE TABLE `inquiry_produk` (
  `inquiry_produk_id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `catatan_pesanan` text DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`inquiry_produk_id`),
  KEY `fk_inquiry_produk_inquiry1_idx` (`inquiry_id`),
  KEY `fk_inquiry_produk_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_inquiry_produk_inquiry1` FOREIGN KEY (`inquiry_id`) REFERENCES `inquiry` (`inquiry_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inquiry_produk_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table inquiry_produk_pet_transport
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inquiry_produk_pet_transport`;

CREATE TABLE `inquiry_produk_pet_transport` (
  `inquiry_produk_pet_transport_id` int(11) NOT NULL AUTO_INCREMENT,
  `inquiry_produk_id` int(11) NOT NULL,
  `asal_kota_id` int(11) NOT NULL,
  `tujuan_kota_id` int(11) NOT NULL,
  `moda_transportasi_id` int(11) NOT NULL,
  `ras_hewan_id` int(11) NOT NULL,
  `nama_hewan` varchar(45) DEFAULT NULL,
  `jenis_kelamin` enum('JANTAN','BETINA') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`inquiry_produk_pet_transport_id`),
  KEY `fk_inquiry_produk_pet_transport_inquiry_produk1_idx` (`inquiry_produk_id`),
  KEY `fk_inquiry_produk_pet_transport_kota1_idx` (`asal_kota_id`),
  KEY `fk_inquiry_produk_pet_transport_kota2_idx` (`tujuan_kota_id`),
  KEY `fk_inquiry_produk_pet_transport_moda_transportasi1_idx` (`moda_transportasi_id`),
  KEY `fk_inquiry_produk_pet_transport_ras_hewan1_idx` (`ras_hewan_id`),
  CONSTRAINT `fk_inquiry_produk_pet_transport_inquiry_produk1` FOREIGN KEY (`inquiry_produk_id`) REFERENCES `inquiry_produk` (`inquiry_produk_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inquiry_produk_pet_transport_kota1` FOREIGN KEY (`asal_kota_id`) REFERENCES `kota` (`kota_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inquiry_produk_pet_transport_kota2` FOREIGN KEY (`tujuan_kota_id`) REFERENCES `kota` (`kota_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inquiry_produk_pet_transport_moda_transportasi1` FOREIGN KEY (`moda_transportasi_id`) REFERENCES `moda_transportasi` (`moda_transportasi_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inquiry_produk_pet_transport_ras_hewan1` FOREIGN KEY (`ras_hewan_id`) REFERENCES `ras_hewan` (`ras_hewan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `pelanggan_id` int(11) NOT NULL,
  `no_invoice` varchar(45) DEFAULT NULL,
  `keterangan_pembayaran` varchar(450) DEFAULT NULL,
  `kurs` char(3) DEFAULT NULL,
  `total_ppn` double DEFAULT NULL,
  `bea_materai` double DEFAULT NULL,
  `total_tagihan` double DEFAULT NULL,
  `total_terbayar` double DEFAULT NULL,
  `tanggal_jatuh_tempo` date DEFAULT NULL,
  `status` enum('PIUTANG','LUNAS','VOID') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `idx_no_invoice` (`no_invoice`),
  KEY `fk_invoice_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_invoice_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invoice_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoice_detail`;

CREATE TABLE `invoice_detail` (
  `invoice_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) NOT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`invoice_detail_id`),
  KEY `fk_invoice_detail_invoice1_idx` (`invoice_id`),
  KEY `fk_invoice_detail_sales_order1_idx` (`sales_order_id`),
  CONSTRAINT `fk_invoice_detail_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_detail_sales_order1` FOREIGN KEY (`sales_order_id`) REFERENCES `sales_order` (`sales_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_aset`;

CREATE TABLE `jenis_aset` (
  `jenis_aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_aset_id` int(11) NOT NULL,
  `merek_id` int(11) NOT NULL,
  `nama_jenis_aset` varchar(45) DEFAULT NULL,
  `spesifikasi` varchar(45) DEFAULT NULL,
  `jenis_identifikasi` enum('TIDAK_ADA','BARCODE') DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_aset_id`),
  KEY `fk_jenis_aset_kategori_aset1_idx` (`kategori_aset_id`),
  KEY `fk_jenis_aset_merek1_idx` (`merek_id`),
  CONSTRAINT `fk_jenis_aset_kategori_aset1` FOREIGN KEY (`kategori_aset_id`) REFERENCES `kategori_aset` (`kategori_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jenis_aset_merek1` FOREIGN KEY (`merek_id`) REFERENCES `merek` (`merek_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_barang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_barang`;

CREATE TABLE `jenis_barang` (
  `jenis_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_barang_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `kode_barang` varchar(10) DEFAULT NULL,
  `kode_barcode` varchar(45) DEFAULT NULL,
  `nama_jenis_barang` varchar(45) DEFAULT NULL,
  `spesifikasi` varchar(45) DEFAULT NULL,
  `warna` varchar(45) DEFAULT NULL,
  `ukuran` varchar(45) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `batas_bawah_harga` double DEFAULT NULL,
  `klasifikasi_barang` enum('FISIK','NON_FISIK') DEFAULT NULL,
  `jenis_identifikasi` enum('IMEI','ID','SERIAL','LAINNYA','TIDAK_ADA') DEFAULT NULL,
  `dijual` tinyint(1) DEFAULT NULL,
  `pcf_stok` int(11) DEFAULT NULL,
  `pcf_stok_booked` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_barang_id`),
  KEY `fk_jenis_barang_kategori_barang1_idx` (`kategori_barang_id`),
  KEY `fk_jenis_barang_satuan1_idx` (`satuan_id`),
  CONSTRAINT `fk_jenis_barang_kategori_barang1` FOREIGN KEY (`kategori_barang_id`) REFERENCES `kategori_barang` (`kategori_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jenis_barang_satuan1` FOREIGN KEY (`satuan_id`) REFERENCES `satuan` (`satuan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_dokumen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_dokumen`;

CREATE TABLE `jenis_dokumen` (
  `jenis_dokumen_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_dokumen` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_dokumen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_hewan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_hewan`;

CREATE TABLE `jenis_hewan` (
  `jenis_hewan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_hewan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_hewan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `jenis_hewan` WRITE;
/*!40000 ALTER TABLE `jenis_hewan` DISABLE KEYS */;

INSERT INTO `jenis_hewan` (`jenis_hewan_id`, `nama_jenis_hewan`, `__active`, `__created`, `__updated`, `__username`)
VALUES
	(1,'herder',1,NULL,NULL,NULL);

/*!40000 ALTER TABLE `jenis_hewan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jenis_kontak_pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_kontak_pelanggan`;

CREATE TABLE `jenis_kontak_pelanggan` (
  `jenis_kontak_pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_kontak_pelanggan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_kontak_pelanggan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_layanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_layanan`;

CREATE TABLE `jenis_layanan` (
  `jenis_layanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_barang_id` int(11) NOT NULL,
  `kategori_layanan_id` int(11) NOT NULL,
  `pelaksana` enum('DRIVER','SURVEYOR','EXIM','ADMIN') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_layanan_id`),
  KEY `fk_jenis_layanan_kategori_layanan1_idx` (`kategori_layanan_id`),
  KEY `fk_jenis_layanan_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_jenis_layanan_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jenis_layanan_kategori_layanan1` FOREIGN KEY (`kategori_layanan_id`) REFERENCES `kategori_layanan` (`kategori_layanan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_segmen_pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_segmen_pelanggan`;

CREATE TABLE `jenis_segmen_pelanggan` (
  `jenis_segmen_pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_segmen_pelanggan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_segmen_pelanggan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table jenis_vaksin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_vaksin`;

CREATE TABLE `jenis_vaksin` (
  `jenis_vaksin_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_vaksin` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenis_vaksin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kategori_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori_aset`;

CREATE TABLE `kategori_aset` (
  `kategori_aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_aset` varchar(45) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kategori_aset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kategori_barang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori_barang`;

CREATE TABLE `kategori_barang` (
  `kategori_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_barang` varchar(45) DEFAULT NULL,
  `dapat_dijual` tinyint(1) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kategori_barang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kategori_layanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori_layanan`;

CREATE TABLE `kategori_layanan` (
  `kategori_layanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_kategori_layanan_id` int(11) DEFAULT NULL,
  `nama_kategori_layanan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kategori_layanan_id`),
  KEY `fk_kategori_layanan_kategori_layanan1_idx` (`parent_kategori_layanan_id`),
  CONSTRAINT `fk_kategori_layanan_kategori_layanan1` FOREIGN KEY (`parent_kategori_layanan_id`) REFERENCES `kategori_layanan` (`kategori_layanan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kemasan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kemasan`;

CREATE TABLE `kemasan` (
  `kemasan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kemasan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kemasan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kontak_pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kontak_pelanggan`;

CREATE TABLE `kontak_pelanggan` (
  `kontak_pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) NOT NULL,
  `jenis_kontak_pelanggan_id` int(11) NOT NULL,
  `kontak` varchar(45) DEFAULT NULL,
  `kontak_utama` tinyint(1) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kontak_pelanggan_id`),
  KEY `fk_kontak_pelanggan_pelanggan1_idx` (`pelanggan_id`),
  KEY `fk_kontak_pelanggan_jenis_kontak_pelanggan1_idx` (`jenis_kontak_pelanggan_id`),
  CONSTRAINT `fk_kontak_pelanggan_jenis_kontak_pelanggan1` FOREIGN KEY (`jenis_kontak_pelanggan_id`) REFERENCES `jenis_kontak_pelanggan` (`jenis_kontak_pelanggan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_kontak_pelanggan_pelanggan1` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kota
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kota`;

CREATE TABLE `kota` (
  `kota_id` int(11) NOT NULL AUTO_INCREMENT,
  `propinsi_id` int(11) NOT NULL,
  `nama_kota` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kota_id`),
  KEY `fk_kota_propinsi1_idx` (`propinsi_id`),
  CONSTRAINT `fk_kota_propinsi1` FOREIGN KEY (`propinsi_id`) REFERENCES `propinsi` (`propinsi_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `kota` WRITE;
/*!40000 ALTER TABLE `kota` DISABLE KEYS */;

INSERT INTO `kota` (`kota_id`, `propinsi_id`, `nama_kota`, `__active`, `__created`, `__updated`, `__username`)
VALUES
	(7,1,'Jakarta Timur',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),
	(9,1,'Jakarta Selatan',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),
	(11,1,'Jakarta Timur',1,'2018-12-23 12:15:05','0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `kota` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kupon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kupon`;

CREATE TABLE `kupon` (
  `kupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_kupon` varchar(45) DEFAULT NULL,
  `kuota` int(11) DEFAULT NULL,
  `kuota_per_pelanggan` int(11) DEFAULT NULL,
  `tanggal_habis_berlaku` date DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  `tipe_nilai` enum('TETAP','PERSEN') DEFAULT NULL,
  `nilai_diskon_maks` double DEFAULT NULL,
  `min_pembelian` double DEFAULT NULL,
  `pembatasan_jenis_barang` enum('TIDAK_ADA','BLACKLIST','WHITELIST') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table lokasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lokasi`;

CREATE TABLE `lokasi` (
  `lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `nama_lokasi` varchar(45) DEFAULT NULL,
  `jenis_lokasi` enum('LUAR_RUANG','BANGUNAN') DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `hak_akses` enum('PUBLIC','PRIVATE') DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`lokasi_id`),
  KEY `fk_lokasi_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_lokasi_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table merek
# ------------------------------------------------------------

DROP TABLE IF EXISTS `merek`;

CREATE TABLE `merek` (
  `merek_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merek` varchar(45) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`merek_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table metode_pembayaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `metode_pembayaran`;

CREATE TABLE `metode_pembayaran` (
  `metode_pembayaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_id` int(11) NOT NULL,
  `nama_metode_pembayaran` varchar(45) DEFAULT NULL,
  `nama_bank` varchar(45) DEFAULT NULL,
  `nomor_rekening` varchar(45) DEFAULT NULL,
  `pemilik_rekening` varchar(45) DEFAULT NULL,
  `kurs` char(3) DEFAULT NULL,
  `jenis` enum('TUNAI','TRANSFER','CEK','DEPOSIT') DEFAULT NULL,
  `pembayaran_invoice` tinyint(1) DEFAULT NULL,
  `pembayaran_tagihan` tinyint(1) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`metode_pembayaran_id`),
  KEY `fk_metode_pembayaran_akun1_idx` (`akun_id`),
  CONSTRAINT `fk_metode_pembayaran_akun1` FOREIGN KEY (`akun_id`) REFERENCES `akun` (`akun_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table moda_transportasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moda_transportasi`;

CREATE TABLE `moda_transportasi` (
  `moda_transportasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_moda_transportasi` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`moda_transportasi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table negara
# ------------------------------------------------------------

DROP TABLE IF EXISTS `negara`;

CREATE TABLE `negara` (
  `negara_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_negara` char(2) DEFAULT NULL,
  `kode_bahasa` char(5) DEFAULT NULL,
  `nama_negara` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT 1,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`negara_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `negara` WRITE;
/*!40000 ALTER TABLE `negara` DISABLE KEYS */;

INSERT INTO `negara` (`negara_id`, `kode_negara`, `kode_bahasa`, `nama_negara`, `__active`, `__created`, `__updated`, `__username`)
VALUES
	(1,'ID','ID','Indonesia',NULL,NULL,NULL,NULL),
	(2,'US','EN','United States',1,NULL,NULL,NULL);

/*!40000 ALTER TABLE `negara` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paket_layanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket_layanan`;

CREATE TABLE `paket_layanan` (
  `paket_layanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_layanan_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `nama_paket_layanan` varchar(45) DEFAULT NULL,
  `dapat_dicustom` tinyint(1) DEFAULT NULL,
  `rumus_pembulatan_custom` enum('ROUND','CEIL','FLOOR') DEFAULT NULL,
  `kelipatan_pembulatan_custom` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  `paket_layanan_id1` int(11) NOT NULL,
  PRIMARY KEY (`paket_layanan_id`),
  KEY `fk_paket_layanan_kategori_layanan1_idx` (`kategori_layanan_id`),
  KEY `fk_paket_layanan_jenis_barang1_idx` (`jenis_barang_id`),
  KEY `fk_paket_layanan_paket_layanan1_idx` (`paket_layanan_id1`),
  CONSTRAINT `fk_paket_layanan_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_paket_layanan_kategori_layanan1` FOREIGN KEY (`kategori_layanan_id`) REFERENCES `kategori_layanan` (`kategori_layanan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_paket_layanan_paket_layanan1` FOREIGN KEY (`paket_layanan_id1`) REFERENCES `paket_layanan` (`paket_layanan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table paket_layanan_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket_layanan_detail`;

CREATE TABLE `paket_layanan_detail` (
  `paket_layanan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_layanan_id` int(11) NOT NULL,
  `jenis_layanan_id` int(11) NOT NULL,
  `group_pilihan` varchar(45) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `persentase_harga_custom` float DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`paket_layanan_detail_id`),
  KEY `fk_paket_layanan_detail_paket_layanan1_idx` (`paket_layanan_id`),
  KEY `fk_paket_layanan_detail_jenis_layanan1_idx` (`jenis_layanan_id`),
  CONSTRAINT `fk_paket_layanan_detail_jenis_layanan1` FOREIGN KEY (`jenis_layanan_id`) REFERENCES `jenis_layanan` (`jenis_layanan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_paket_layanan_detail_paket_layanan1` FOREIGN KEY (`paket_layanan_id`) REFERENCES `paket_layanan` (`paket_layanan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pecah_kemasan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pecah_kemasan`;

CREATE TABLE `pecah_kemasan` (
  `pecah_kemasan_id` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi_bongkar` varchar(45) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pecah_kemasan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pecah_kemasan_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pecah_kemasan_detail`;

CREATE TABLE `pecah_kemasan_detail` (
  `pecah_kemasan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pecah_kemasan_id` int(11) NOT NULL,
  `transaksi_barang_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pecah_kemasan_detail_id`),
  KEY `fk_pecah_kemasan_detail_pecah_kemasan1_idx` (`pecah_kemasan_id`),
  CONSTRAINT `fk_pecah_kemasan_detail_pecah_kemasan1` FOREIGN KEY (`pecah_kemasan_id`) REFERENCES `pecah_kemasan` (`pecah_kemasan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pelanggan`;

CREATE TABLE `pelanggan` (
  `pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(45) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('PRIA','WANITA') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pelanggan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pembatasan_jenis_barang_kupon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pembatasan_jenis_barang_kupon`;

CREATE TABLE `pembatasan_jenis_barang_kupon` (
  `pembatasan_jenis_barang_kupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `kupon_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pembatasan_jenis_barang_kupon_id`),
  KEY `fk_pembatasan_jenis_barang_kupon_kupon1_idx` (`kupon_id`),
  KEY `fk_pembatasan_jenis_barang_kupon_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_pembatasan_jenis_barang_kupon_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembatasan_jenis_barang_kupon_kupon1` FOREIGN KEY (`kupon_id`) REFERENCES `kupon` (`kupon_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peminjaman_barang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peminjaman_barang`;

CREATE TABLE `peminjaman_barang` (
  `peminjaman_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_peminjaman_barang` varchar(45) DEFAULT NULL,
  `tanggal_pinjam` date DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `nama_peminjam` varchar(45) DEFAULT NULL,
  `alamat_peminjam` varchar(45) DEFAULT NULL,
  `keperluan` varchar(45) DEFAULT NULL,
  `telp_peminjam` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peminjaman_barang_id`),
  KEY `idx_no_peminjaman_barang` (`no_peminjaman_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peminjaman_barang_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peminjaman_barang_detail`;

CREATE TABLE `peminjaman_barang_detail` (
  `peminjaman_barang_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `peminjaman_barang_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `transaksi_barang_keluar` int(11) NOT NULL,
  `transaksi_barang_kembali` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peminjaman_barang_detail_id`),
  KEY `fk_peminjaman_barang_detail_peminjaman_barang1_idx` (`peminjaman_barang_id`),
  KEY `fk_peminjaman_barang_detail_barang1_idx` (`barang_id`),
  CONSTRAINT `fk_peminjaman_barang_detail_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peminjaman_barang_detail_peminjaman_barang1` FOREIGN KEY (`peminjaman_barang_id`) REFERENCES `peminjaman_barang` (`peminjaman_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table penempatan_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `penempatan_aset`;

CREATE TABLE `penempatan_aset` (
  `penempatan_aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `aset_id` int(11) NOT NULL,
  `ruangan_id` int(11) NOT NULL,
  `jenis_dokumen_masuk` enum('PENGADAAN','PINDAH','PERBAIKAN') DEFAULT NULL,
  `waktu_masuk` timestamp NULL DEFAULT NULL,
  `jenis_dokumen_keluar` enum('PINDAH','APKIR','PERBAIKAN') DEFAULT NULL,
  `waktu_keluar` timestamp NULL DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`penempatan_aset_id`),
  KEY `fk_penempatan_aset_aset1_idx` (`aset_id`),
  KEY `fk_pergerakan_aset_ruangan1_idx` (`ruangan_id`),
  KEY `fk_penempatan_aset_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_penempatan_aset_aset1` FOREIGN KEY (`aset_id`) REFERENCES `aset` (`aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_penempatan_aset_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pergerakan_aset_ruangan1` FOREIGN KEY (`ruangan_id`) REFERENCES `ruangan` (`ruangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table penerimaan_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `penerimaan_aset`;

CREATE TABLE `penerimaan_aset` (
  `penerimaan_aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_surat_jalan` varchar(45) DEFAULT NULL,
  `tanggal_surat_jalan` date DEFAULT NULL,
  `nama_supir` varchar(45) DEFAULT NULL,
  `keterangan_penerimaan_aset` varchar(450) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`penerimaan_aset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table penerimaan_barang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `penerimaan_barang`;

CREATE TABLE `penerimaan_barang` (
  `penerimaan_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `no_surat_jalan` varchar(45) DEFAULT NULL,
  `tanggal_surat_jalan` date DEFAULT NULL,
  `tanggal_terima` date DEFAULT NULL,
  `nama_supir` varchar(45) DEFAULT NULL,
  `telp_supir` varchar(45) DEFAULT NULL,
  `catatan_gudang` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`penerimaan_barang_id`),
  KEY `fk_penerimaan_barang_gudang1_idx` (`gudang_id`),
  CONSTRAINT `fk_penerimaan_barang_gudang1` FOREIGN KEY (`gudang_id`) REFERENCES `gudang` (`gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table penerimaan_barang_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `penerimaan_barang_detail`;

CREATE TABLE `penerimaan_barang_detail` (
  `penerimaan_barang_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `penerimaan_barang_id` int(11) NOT NULL,
  `transaksi_barang_id` int(11) NOT NULL,
  `purchase_order_detail_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`penerimaan_barang_detail_id`),
  KEY `fk_penerimaan_barang_detail_penerimaan_barang1_idx` (`penerimaan_barang_id`),
  CONSTRAINT `fk_penerimaan_barang_detail_penerimaan_barang1` FOREIGN KEY (`penerimaan_barang_id`) REFERENCES `penerimaan_barang` (`penerimaan_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pengadaan_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pengadaan_aset`;

CREATE TABLE `pengadaan_aset` (
  `pengadaan_aset_id` int(11) NOT NULL,
  `perusahaan_id` int(11) NOT NULL,
  `ruangan_id` int(11) NOT NULL,
  `divisi_pengusul` varchar(45) DEFAULT NULL,
  `kontak_pengusul` varchar(45) DEFAULT NULL,
  `rencana_tanggal_penempatan` date DEFAULT NULL,
  `keperluan` varchar(45) DEFAULT NULL,
  `status` enum('PENGAJUAN','APPROVED','REJECTED') DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pengadaan_aset_id`),
  KEY `fk_pengadaan_aset_ruangan1_idx` (`ruangan_id`),
  KEY `fk_pengadaan_aset_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_pengadaan_aset_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pengadaan_aset_ruangan1` FOREIGN KEY (`ruangan_id`) REFERENCES `ruangan` (`ruangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pengadaan_aset_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pengadaan_aset_detail`;

CREATE TABLE `pengadaan_aset_detail` (
  `pengadaan_aset_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengadaan_aset_id` int(11) NOT NULL,
  `jenis_aset_id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pengadaan_aset_detail_id`),
  KEY `fk_permintaan_aset_detail_permintaan_aset1_idx` (`pengadaan_aset_id`),
  KEY `fk_permintaan_aset_detail_jenis_aset1_idx` (`jenis_aset_id`),
  CONSTRAINT `fk_permintaan_aset_detail_jenis_aset1` FOREIGN KEY (`jenis_aset_id`) REFERENCES `jenis_aset` (`jenis_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_permintaan_aset_detail_permintaan_aset1` FOREIGN KEY (`pengadaan_aset_id`) REFERENCES `pengadaan_aset` (`pengadaan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table penggunaan_barang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `penggunaan_barang`;

CREATE TABLE `penggunaan_barang` (
  `penggunaan_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_order_detail_id` int(11) NOT NULL,
  `transaksi_barang_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`penggunaan_barang_id`),
  KEY `fk_penggunaan_barang_work_order_detail1_idx` (`work_order_detail_id`),
  CONSTRAINT `fk_penggunaan_barang_work_order_detail1` FOREIGN KEY (`work_order_detail_id`) REFERENCES `work_order_detail` (`work_order_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peraturan_exim
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peraturan_exim`;

CREATE TABLE `peraturan_exim` (
  `peraturan_exim_id` int(11) NOT NULL AUTO_INCREMENT,
  `negara_id` int(11) NOT NULL,
  `arah` enum('EXPORT','IMPORT') DEFAULT NULL,
  `tanggal_berlaku` date DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peraturan_exim_id`),
  KEY `fk_peraturan_exim_negara1_idx` (`negara_id`),
  CONSTRAINT `fk_peraturan_exim_negara1` FOREIGN KEY (`negara_id`) REFERENCES `negara` (`negara_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peraturan_exim_attachment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peraturan_exim_attachment`;

CREATE TABLE `peraturan_exim_attachment` (
  `peraturan_exim_attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `peraturan_exim_id` int(11) NOT NULL,
  `media_type` enum('IMAGE','VIDEO') DEFAULT NULL,
  `media_filename` varchar(1024) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peraturan_exim_attachment_id`),
  KEY `fk_peraturan_exim_attachment_peraturan_exim1_idx` (`peraturan_exim_id`),
  CONSTRAINT `fk_peraturan_exim_attachment_peraturan_exim1` FOREIGN KEY (`peraturan_exim_id`) REFERENCES `peraturan_exim` (`peraturan_exim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peraturan_exim_jenis_dokumen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peraturan_exim_jenis_dokumen`;

CREATE TABLE `peraturan_exim_jenis_dokumen` (
  `peraturan_exim_jenis_dokumen_id` int(11) NOT NULL AUTO_INCREMENT,
  `peraturan_exim_ras_hewan_id` int(11) NOT NULL,
  `jenis_dokumen_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peraturan_exim_jenis_dokumen_id`),
  KEY `fk_peraturan_exim_jenis_dokumen_peraturan_exim_ras_hewan1_idx` (`peraturan_exim_ras_hewan_id`),
  KEY `fk_peraturan_exim_jenis_dokumen_jenis_dokumen1_idx` (`jenis_dokumen_id`),
  CONSTRAINT `fk_peraturan_exim_jenis_dokumen_jenis_dokumen1` FOREIGN KEY (`jenis_dokumen_id`) REFERENCES `jenis_dokumen` (`jenis_dokumen_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peraturan_exim_jenis_dokumen_peraturan_exim_ras_hewan1` FOREIGN KEY (`peraturan_exim_ras_hewan_id`) REFERENCES `peraturan_exim_ras_hewan` (`peraturan_exim_ras_hewan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peraturan_exim_konten
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peraturan_exim_konten`;

CREATE TABLE `peraturan_exim_konten` (
  `peraturan_exim_konten_id` int(11) NOT NULL AUTO_INCREMENT,
  `peraturan_exim_id` int(11) NOT NULL,
  `kode_bahasa` char(5) DEFAULT NULL,
  `media_type` enum('IMAGE','VIDEO') DEFAULT NULL,
  `media_filename` varchar(1024) DEFAULT NULL,
  `isi_peraturan` text DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peraturan_exim_konten_id`),
  KEY `fk_peraturan_exim_konten_peraturan_exim1_idx` (`peraturan_exim_id`),
  CONSTRAINT `fk_peraturan_exim_konten_peraturan_exim1` FOREIGN KEY (`peraturan_exim_id`) REFERENCES `peraturan_exim` (`peraturan_exim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peraturan_exim_ras_hewan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peraturan_exim_ras_hewan`;

CREATE TABLE `peraturan_exim_ras_hewan` (
  `peraturan_exim_ras_hewan_id` int(11) NOT NULL AUTO_INCREMENT,
  `peraturan_exim_id` int(11) NOT NULL,
  `ras_hewan_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peraturan_exim_ras_hewan_id`),
  KEY `fk_peraturan_exim_ras_hewan_peraturan_exim1_idx` (`peraturan_exim_id`),
  KEY `fk_peraturan_exim_ras_hewan_ras_hewan1_idx` (`ras_hewan_id`),
  CONSTRAINT `fk_peraturan_exim_ras_hewan_peraturan_exim1` FOREIGN KEY (`peraturan_exim_id`) REFERENCES `peraturan_exim` (`peraturan_exim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peraturan_exim_ras_hewan_ras_hewan1` FOREIGN KEY (`ras_hewan_id`) REFERENCES `ras_hewan` (`ras_hewan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table peraturan_exim_vaksin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peraturan_exim_vaksin`;

CREATE TABLE `peraturan_exim_vaksin` (
  `peraturan_exim_vaksin_id` int(11) NOT NULL AUTO_INCREMENT,
  `peraturan_exim_ras_hewan_id` int(11) NOT NULL,
  `jenis_vaksin_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`peraturan_exim_vaksin_id`),
  KEY `fk_peraturan_exim_vaksin_peraturan_exim_ras_hewan1_idx` (`peraturan_exim_ras_hewan_id`),
  KEY `fk_peraturan_exim_vaksin_jenis_vaksin1_idx` (`jenis_vaksin_id`),
  CONSTRAINT `fk_peraturan_exim_vaksin_jenis_vaksin1` FOREIGN KEY (`jenis_vaksin_id`) REFERENCES `jenis_vaksin` (`jenis_vaksin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peraturan_exim_vaksin_peraturan_exim_ras_hewan1` FOREIGN KEY (`peraturan_exim_ras_hewan_id`) REFERENCES `peraturan_exim_ras_hewan` (`peraturan_exim_ras_hewan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table perbaikan_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perbaikan_aset`;

CREATE TABLE `perbaikan_aset` (
  `perbaikan_aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `aset_id` int(11) NOT NULL,
  `aset_id_pengganti` int(11) DEFAULT NULL,
  `penempatan_aset_keluar` int(11) DEFAULT NULL,
  `penempatan_aset_masuk` int(11) DEFAULT NULL COMMENT 'diisi dengan Penempatan Aset saat barang kembali dari service',
  `pelaksana_perbaikan` varchar(45) DEFAULT NULL,
  `kontak_pelaksana_perbaikan` varchar(45) DEFAULT NULL,
  `tanggal_perbaikan` varchar(45) DEFAULT NULL,
  `tanggal_selesai` varchar(45) DEFAULT NULL,
  `alasan_perbaikan` varchar(450) DEFAULT NULL,
  `catatan_hasil_perbaikan` varchar(450) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`perbaikan_aset_id`),
  KEY `fk_perbaikan_aset_aset1_idx` (`aset_id`),
  KEY `fk_perbaikan_aset_penempatan_aset1_idx` (`penempatan_aset_keluar`),
  KEY `fk_perbaikan_aset_penempatan_aset2_idx` (`penempatan_aset_masuk`),
  KEY `fk_perbaikan_aset_aset2_idx` (`aset_id_pengganti`),
  CONSTRAINT `fk_perbaikan_aset_aset1` FOREIGN KEY (`aset_id`) REFERENCES `aset` (`aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_perbaikan_aset_aset2` FOREIGN KEY (`aset_id_pengganti`) REFERENCES `aset` (`aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_perbaikan_aset_penempatan_aset1` FOREIGN KEY (`penempatan_aset_keluar`) REFERENCES `penempatan_aset` (`penempatan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_perbaikan_aset_penempatan_aset2` FOREIGN KEY (`penempatan_aset_masuk`) REFERENCES `penempatan_aset` (`penempatan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table perusahaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `perusahaan`;

CREATE TABLE `perusahaan` (
  `perusahaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(45) DEFAULT NULL,
  `alamat_perusahaan` varchar(160) DEFAULT NULL,
  `telp_perusahaan` varchar(45) DEFAULT NULL,
  `fax_perusahaan` varchar(45) DEFAULT NULL,
  `npwp` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`perusahaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pindah_aset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pindah_aset`;

CREATE TABLE `pindah_aset` (
  `pindah_aset_id` int(11) NOT NULL AUTO_INCREMENT,
  `keperluan` varchar(450) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pindah_aset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pindah_aset_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pindah_aset_detail`;

CREATE TABLE `pindah_aset_detail` (
  `pindah_aset_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_aset_id` int(11) NOT NULL,
  `penempatan_aset_asal` int(11) DEFAULT NULL,
  `penempatan_aset_tujuan` int(11) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pindah_aset_detail_id`),
  KEY `fk_pindah_aset_detail_pindah_aset1_idx` (`pindah_aset_id`),
  KEY `fk_pindah_aset_detail_penempatan_aset1_idx` (`penempatan_aset_asal`),
  KEY `fk_pindah_aset_detail_penempatan_aset2_idx` (`penempatan_aset_tujuan`),
  CONSTRAINT `fk_pindah_aset_detail_penempatan_aset1` FOREIGN KEY (`penempatan_aset_asal`) REFERENCES `penempatan_aset` (`penempatan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pindah_aset_detail_penempatan_aset2` FOREIGN KEY (`penempatan_aset_tujuan`) REFERENCES `penempatan_aset` (`penempatan_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pindah_aset_detail_pindah_aset1` FOREIGN KEY (`pindah_aset_id`) REFERENCES `pindah_aset` (`pindah_aset_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pindah_gudang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pindah_gudang`;

CREATE TABLE `pindah_gudang` (
  `pindah_gudang_id` int(11) NOT NULL AUTO_INCREMENT,
  `gudang_asal` int(11) NOT NULL,
  `gudang_tujuan` int(11) DEFAULT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `tanggal_terima` date DEFAULT NULL,
  `catatan_pengirim` varchar(450) DEFAULT NULL,
  `catatan_penerima` varchar(450) DEFAULT NULL,
  `jenis` enum('INTERNAL','ANTAR_PERUSAHAAN') DEFAULT NULL,
  `status` enum('PERJALANAN','DITERIMA','DIKEMBALIKAN') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pindah_gudang_id`),
  KEY `fk_pindah_gudang_gudang1_idx` (`gudang_asal`),
  KEY `fk_pindah_gudang_gudang2_idx` (`gudang_tujuan`),
  CONSTRAINT `fk_pindah_gudang_gudang1` FOREIGN KEY (`gudang_asal`) REFERENCES `gudang` (`gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pindah_gudang_gudang2` FOREIGN KEY (`gudang_tujuan`) REFERENCES `gudang` (`gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pindah_gudang_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pindah_gudang_detail`;

CREATE TABLE `pindah_gudang_detail` (
  `pindah_gudang_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `pindah_gudang_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `transaksi_barang_keluar` int(11) NOT NULL,
  `transaksi_barang_masuk` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pindah_gudang_detail_id`),
  KEY `fk_pindah_gudang_detail_pindah_gudang1_idx` (`pindah_gudang_id`),
  KEY `fk_pindah_gudang_detail_barang1_idx` (`barang_id`),
  CONSTRAINT `fk_pindah_gudang_detail_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pindah_gudang_detail_pindah_gudang1` FOREIGN KEY (`pindah_gudang_id`) REFERENCES `pindah_gudang` (`pindah_gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table propinsi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `propinsi`;

CREATE TABLE `propinsi` (
  `propinsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `negara_id` int(11) NOT NULL,
  `nama_propinsi` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`propinsi_id`),
  KEY `fk_propinsi_negara1_idx` (`negara_id`),
  CONSTRAINT `fk_propinsi_negara1` FOREIGN KEY (`negara_id`) REFERENCES `negara` (`negara_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `propinsi` WRITE;
/*!40000 ALTER TABLE `propinsi` DISABLE KEYS */;

INSERT INTO `propinsi` (`propinsi_id`, `negara_id`, `nama_propinsi`, `__active`, `__created`, `__updated`, `__username`)
VALUES
	(1,1,'DKI Jakarta',1,NULL,NULL,NULL),
	(2,1,'Banten',1,NULL,NULL,NULL),
	(3,1,'Jawa Tengah',1,NULL,NULL,NULL),
	(4,1,'Banten',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0'),
	(5,2,'Toronto',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0');

/*!40000 ALTER TABLE `propinsi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table purchase_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase_order`;

CREATE TABLE `purchase_order` (
  `purchase_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `no_purchase_order` varchar(45) DEFAULT NULL,
  `permintaan_tanggal_kirim` date DEFAULT NULL,
  `nama_penerima` varchar(45) DEFAULT NULL,
  `alamat_penerima` varchar(450) DEFAULT NULL,
  `telp_penerima` varchar(45) DEFAULT NULL,
  `fax_penerima` varchar(45) DEFAULT NULL,
  `kurs` char(3) DEFAULT NULL,
  `nilai_tukar` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `biaya_lainnya` double DEFAULT NULL,
  `total_pembelian` double DEFAULT NULL,
  `no_so_supplier` varchar(45) DEFAULT NULL,
  `tanggal_so_supplier` date DEFAULT NULL,
  `lampiran_so` varchar(45) DEFAULT NULL,
  `term_pembayaran` varchar(45) DEFAULT NULL,
  `catatan` varchar(450) DEFAULT NULL,
  `status` enum('PENDING','APPROVED','DITERIMA','BATAL') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`purchase_order_id`),
  KEY `fk_purchase_order_supplier1_idx` (`supplier_id`),
  KEY `fk_purchase_order_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_purchase_order_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_order_supplier1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table purchase_order_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchase_order_detail`;

CREATE TABLE `purchase_order_detail` (
  `purchase_order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `gudang_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga_satuan` double DEFAULT NULL,
  `satuan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`purchase_order_detail_id`),
  KEY `fk_purchase_order_detail_purchase_order1_idx` (`purchase_order_id`),
  CONSTRAINT `fk_purchase_order_detail_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`purchase_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ras_hewan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ras_hewan`;

CREATE TABLE `ras_hewan` (
  `ras_hewan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_hewan_id` int(11) NOT NULL,
  `nama_ras_hewan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ras_hewan_id`),
  KEY `fk_ras_hewan_jenis_hewan1_idx` (`jenis_hewan_id`),
  CONSTRAINT `fk_ras_hewan_jenis_hewan1` FOREIGN KEY (`jenis_hewan_id`) REFERENCES `jenis_hewan` (`jenis_hewan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ras_hewan` WRITE;
/*!40000 ALTER TABLE `ras_hewan` DISABLE KEYS */;

INSERT INTO `ras_hewan` (`ras_hewan_id`, `jenis_hewan_id`, `nama_ras_hewan`, `__active`, `__created`, `__updated`, `__username`)
VALUES
	(2,1,'Anjing',1,NULL,NULL,NULL),
	(4,1,'Kucing',NULL,NULL,NULL,NULL),
	(5,1,'Kucing 1',NULL,NULL,NULL,NULL),
	(6,1,'Kucing 2',NULL,NULL,NULL,NULL),
	(7,1,'Kucing 3',NULL,NULL,NULL,NULL),
	(8,1,'Kucing 4',NULL,NULL,NULL,NULL),
	(10,1,'Kucing 6',NULL,NULL,NULL,NULL),
	(11,1,'Kucing 7',NULL,NULL,NULL,NULL),
	(12,1,'Kucing 8',NULL,NULL,NULL,NULL),
	(13,1,'Kucing 9',NULL,NULL,NULL,NULL),
	(14,1,'Anjing 1',NULL,NULL,NULL,NULL),
	(15,1,'Anjing 2',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `ras_hewan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table retur_pembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur_pembelian`;

CREATE TABLE `retur_pembelian` (
  `retur_pembelian_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_keuangan_id` int(11) DEFAULT NULL,
  `no_retur_pembelian` varchar(45) DEFAULT NULL,
  `jenis_retur` enum('TUKAR','DIUANGKAN') DEFAULT NULL,
  `status_barang` enum('BARU','BEKAS','RUSAK') DEFAULT NULL,
  `alasan_pengembalian` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`retur_pembelian_id`),
  KEY `fk_retur_pembelian_transaksi_keuangan1_idx` (`transaksi_keuangan_id`),
  KEY `idx_no_retur_pembelian` (`no_retur_pembelian`),
  CONSTRAINT `fk_retur_pembelian_transaksi_keuangan1` FOREIGN KEY (`transaksi_keuangan_id`) REFERENCES `transaksi_keuangan` (`transaksi_keuangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table retur_pembelian_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur_pembelian_detail`;

CREATE TABLE `retur_pembelian_detail` (
  `retur_pembelian_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `retur_pembelian_id` int(11) NOT NULL,
  `penerimaan_barang_detail_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`retur_pembelian_detail_id`),
  KEY `fk_retur_pembelian_detail_retur_pembelian1_idx` (`retur_pembelian_id`),
  KEY `fk_retur_pembelian_detail_penerimaan_barang_detail1_idx` (`penerimaan_barang_detail_id`),
  CONSTRAINT `fk_retur_pembelian_detail_penerimaan_barang_detail1` FOREIGN KEY (`penerimaan_barang_detail_id`) REFERENCES `penerimaan_barang_detail` (`penerimaan_barang_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_retur_pembelian_detail_retur_pembelian1` FOREIGN KEY (`retur_pembelian_id`) REFERENCES `retur_pembelian` (`retur_pembelian_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table retur_penjualan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur_penjualan`;

CREATE TABLE `retur_penjualan` (
  `retur_penjualan_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_keuangan_id` int(11) DEFAULT NULL,
  `no_retur_penjualan` varchar(45) DEFAULT NULL,
  `jenis_retur` enum('TUKAR','DIUANGKAN') DEFAULT NULL,
  `status_barang` enum('BARU','BEKAS','RUSAK') DEFAULT NULL,
  `alasan_pengembalian` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`retur_penjualan_id`),
  KEY `fk_retur_penjualan_transaksi_keuangan1_idx` (`transaksi_keuangan_id`),
  KEY `idx_no_retur_penjualan` (`no_retur_penjualan`),
  CONSTRAINT `fk_retur_penjualan_transaksi_keuangan1` FOREIGN KEY (`transaksi_keuangan_id`) REFERENCES `transaksi_keuangan` (`transaksi_keuangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table retur_penjualan_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur_penjualan_detail`;

CREATE TABLE `retur_penjualan_detail` (
  `retur_penjualan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `retur_penjualan_id` int(11) NOT NULL,
  `satuan_barang_delivery_order_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`retur_penjualan_detail_id`),
  KEY `fk_retur_penjualan_detail_retur_penjualan1_idx` (`retur_penjualan_id`),
  KEY `fk_retur_penjualan_detail_satuan_barang_delivery_order1_idx` (`satuan_barang_delivery_order_id`),
  CONSTRAINT `fk_retur_penjualan_detail_retur_penjualan1` FOREIGN KEY (`retur_penjualan_id`) REFERENCES `retur_penjualan` (`retur_penjualan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_retur_penjualan_detail_satuan_barang_delivery_order1` FOREIGN KEY (`satuan_barang_delivery_order_id`) REFERENCES `satuan_barang_delivery_order` (`satuan_barang_delivery_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ruangan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ruangan`;

CREATE TABLE `ruangan` (
  `ruangan_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_lokasi_id` int(11) NOT NULL,
  `nama_ruangan` varchar(45) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ruangan_id`),
  KEY `fk_ruangan_sub_lokasi1_idx` (`sub_lokasi_id`),
  CONSTRAINT `fk_ruangan_sub_lokasi1` FOREIGN KEY (`sub_lokasi_id`) REFERENCES `sub_lokasi` (`sub_lokasi_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sales_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_order`;

CREATE TABLE `sales_order` (
  `sales_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `pelanggan_id` int(11) NOT NULL,
  `skema_harga_id` int(11) DEFAULT NULL,
  `no_sales_order` varchar(45) DEFAULT NULL,
  `permintaan_awal_kirim` date DEFAULT NULL,
  `permintaan_akhir_kirim` date DEFAULT NULL,
  `jumlah_pengiriman` int(11) DEFAULT NULL,
  `default_jumlah_pengiriman` int(11) DEFAULT NULL,
  `nama_penerima` varchar(45) DEFAULT NULL,
  `alamat_penerima` varchar(160) DEFAULT NULL,
  `telp_penerima` varchar(45) DEFAULT NULL,
  `fax_penerima` varchar(45) DEFAULT NULL,
  `no_po_pelanggan` varchar(45) DEFAULT NULL,
  `tanggal_po_pelanggan` date DEFAULT NULL,
  `lampiran_po` char(18) DEFAULT NULL,
  `kurs` char(3) DEFAULT NULL,
  `total_penjualan` double DEFAULT NULL,
  `total_ppn` double DEFAULT NULL,
  `catatan_penjualan` varchar(450) DEFAULT NULL,
  `status` enum('PENAWARAN','PENDING','APPROVED','PROSES','REJECTED','VOID') DEFAULT NULL,
  `alasan_batal` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sales_order_id`),
  KEY `idx_no_sales_order` (`no_sales_order`),
  KEY `fk_sales_order_perusahaan1_idx` (`perusahaan_id`),
  KEY `fk_sales_order_skema_harga1_idx` (`skema_harga_id`),
  CONSTRAINT `fk_sales_order_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_order_skema_harga1` FOREIGN KEY (`skema_harga_id`) REFERENCES `skema_harga` (`skema_harga_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sales_order_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_order_detail`;

CREATE TABLE `sales_order_detail` (
  `sales_order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga_satuan` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sales_order_detail_id`),
  KEY `fk_sales_order_detail_sales_order1_idx` (`sales_order_id`),
  KEY `fk_sales_order_detail_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_sales_order_detail_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_order_detail_sales_order1` FOREIGN KEY (`sales_order_id`) REFERENCES `sales_order` (`sales_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sales_order_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_order_item`;

CREATE TABLE `sales_order_item` (
  `sales_order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_detail_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sales_order_item_id`),
  KEY `fk_sales_order_item_sales_order_detail1_idx` (`sales_order_detail_id`),
  KEY `fk_sales_order_item_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_sales_order_item_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sales_order_item_sales_order_detail1` FOREIGN KEY (`sales_order_detail_id`) REFERENCES `sales_order_detail` (`sales_order_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table satuan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `satuan`;

CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satuan` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table satuan_barang_delivery_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `satuan_barang_delivery_order`;

CREATE TABLE `satuan_barang_delivery_order` (
  `satuan_barang_delivery_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_order_detail_id` int(11) NOT NULL,
  `transaksi_barang_id` int(11) NOT NULL,
  `diretur` tinyint(1) NOT NULL DEFAULT 0,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`satuan_barang_delivery_order_id`),
  KEY `fk_satuan_barang_delivery_order_delivery_order_detail1_idx` (`delivery_order_detail_id`),
  CONSTRAINT `fk_satuan_barang_delivery_order_delivery_order_detail1` FOREIGN KEY (`delivery_order_detail_id`) REFERENCES `delivery_order_detail` (`delivery_order_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table satuan_barang_transfer_stock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `satuan_barang_transfer_stock`;

CREATE TABLE `satuan_barang_transfer_stock` (
  `satuan_barang_transfer_stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_stock_detail_id` int(11) NOT NULL,
  `transaksi_barang_keluar` int(11) NOT NULL,
  `transaksi_barang_masuk` int(11) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`satuan_barang_transfer_stock_id`),
  KEY `fk_satuan_barang_transfer_stock_transfer_stock_detail1_idx` (`transfer_stock_detail_id`),
  CONSTRAINT `fk_satuan_barang_transfer_stock_transfer_stock_detail1` FOREIGN KEY (`transfer_stock_detail_id`) REFERENCES `transfer_stock_detail` (`transfer_stock_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table segmen_pelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `segmen_pelanggan`;

CREATE TABLE `segmen_pelanggan` (
  `segmen_pelanggan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) NOT NULL,
  `jenis_segmen_pelanggan_id` int(11) NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`segmen_pelanggan_id`),
  KEY `fk_segmen_pelanggan_pelanggan1_idx` (`pelanggan_id`),
  KEY `fk_segmen_pelanggan_jenis_segmen_pelanggan1_idx` (`jenis_segmen_pelanggan_id`),
  CONSTRAINT `fk_segmen_pelanggan_jenis_segmen_pelanggan1` FOREIGN KEY (`jenis_segmen_pelanggan_id`) REFERENCES `jenis_segmen_pelanggan` (`jenis_segmen_pelanggan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_segmen_pelanggan_pelanggan1` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggan` (`pelanggan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table skema_harga
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skema_harga`;

CREATE TABLE `skema_harga` (
  `skema_harga_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  `tanggal_berlaku` date DEFAULT NULL,
  `tanggal_habis_berlaku` date DEFAULT NULL,
  `no_kontrak` varchar(45) DEFAULT NULL,
  `tanggal_kontrak` date DEFAULT NULL,
  `pejabat_pembuat_komitmen` varchar(45) DEFAULT NULL,
  `diskon_global` float DEFAULT NULL,
  `keterangan` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`skema_harga_id`),
  KEY `fk_skema_harga_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_skema_harga_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table skema_harga_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `skema_harga_detail`;

CREATE TABLE `skema_harga_detail` (
  `skema_harga_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `skema_harga_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `harga` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`skema_harga_detail_id`),
  KEY `fk_skema_harga_detail_skema_harga1_idx` (`skema_harga_id`),
  KEY `fk_skema_harga_detail_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_skema_harga_detail_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_skema_harga_detail_skema_harga1` FOREIGN KEY (`skema_harga_id`) REFERENCES `skema_harga` (`skema_harga_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sub_lokasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sub_lokasi`;

CREATE TABLE `sub_lokasi` (
  `sub_lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi_id` int(11) NOT NULL,
  `nama_sub_lokasi` varchar(45) DEFAULT NULL,
  `filename_denah` char(20) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `__active` tinyint(4) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`sub_lokasi_id`),
  KEY `fk_sub_lokasi_lokasi1_idx` (`lokasi_id`),
  CONSTRAINT `fk_sub_lokasi_lokasi1` FOREIGN KEY (`lokasi_id`) REFERENCES `lokasi` (`lokasi_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table supplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(45) DEFAULT NULL,
  `alamat_supplier` varchar(160) DEFAULT NULL,
  `npwp_supplier` varchar(45) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sys_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_config`;

CREATE TABLE `sys_config` (
  `group` varchar(20) NOT NULL,
  `key` varchar(32) NOT NULL,
  `description` varchar(450) DEFAULT NULL,
  `default_value` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`group`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sys_config_perusahaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_config_perusahaan`;

CREATE TABLE `sys_config_perusahaan` (
  `perusahaan_id` int(11) NOT NULL,
  `group` varchar(20) NOT NULL,
  `key` varchar(32) NOT NULL,
  `value` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`group`,`key`,`perusahaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sys_running_number
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_running_number`;

CREATE TABLE `sys_running_number` (
  `perusahaan_id` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `format` varchar(128) DEFAULT NULL,
  `next_id` int(11) DEFAULT NULL,
  `period` int(11) DEFAULT NULL,
  `last_retrieve` timestamp NULL DEFAULT NULL,
  `zero_pad_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`key`,`perusahaan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table tagihan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tagihan`;

CREATE TABLE `tagihan` (
  `tagihan_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `no_invoice_supplier` varchar(45) DEFAULT NULL,
  `tanggal_invoice_supplier` date DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `total_tagihan` double DEFAULT NULL,
  `kurs` char(3) DEFAULT NULL,
  `status` enum('HUTANG','LUNAS','VOID') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tagihan_id`),
  KEY `fk_tagihan_purchase_order1_idx` (`purchase_order_id`),
  CONSTRAINT `fk_tagihan_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`purchase_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transaksi_invoice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transaksi_invoice`;

CREATE TABLE `transaksi_invoice` (
  `transaksi_invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_keuangan_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `jenis` enum('TUNAI','TRANSFER','CEK','DEPOSIT','PIUTANG') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`transaksi_invoice_id`),
  KEY `fk_pembayaran_invoice_invoice1_idx` (`invoice_id`),
  KEY `fk_pembayaran_invoice_transaksi_keuangan1_idx` (`transaksi_keuangan_id`),
  CONSTRAINT `fk_pembayaran_invoice_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`invoice_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembayaran_invoice_transaksi_keuangan1` FOREIGN KEY (`transaksi_keuangan_id`) REFERENCES `transaksi_keuangan` (`transaksi_keuangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transaksi_keuangan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transaksi_keuangan`;

CREATE TABLE `transaksi_keuangan` (
  `transaksi_keuangan_id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) NOT NULL,
  `tanggal_buku` date DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `no_cek` varchar(45) DEFAULT NULL,
  `tanggal_terima_cek` date DEFAULT NULL,
  `tanggal_cair_cek` date DEFAULT NULL,
  `status` enum('PENDING','APPROVED','REJECTED') DEFAULT NULL,
  `keterangan_status` varchar(160) DEFAULT NULL,
  `jenis` enum('OTOMATIS','MANUAL') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`transaksi_keuangan_id`),
  KEY `fk_transaksi_keuangan_perusahaan1_idx` (`perusahaan_id`),
  CONSTRAINT `fk_transaksi_keuangan_perusahaan1` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transaksi_tagihan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transaksi_tagihan`;

CREATE TABLE `transaksi_tagihan` (
  `transaksi_tagihan_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_keuangan_id` int(11) NOT NULL,
  `tagihan_id` int(11) NOT NULL,
  `term` varchar(45) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `net_pembayaran` double DEFAULT NULL,
  `catatan_pembayaran` varchar(450) DEFAULT NULL,
  `jenis` enum('TUNAI','TRANSFER','CEK','DEPOSIT','HUTANG') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`transaksi_tagihan_id`),
  KEY `fk_transaksi_tagihan_transaksi_keuangan1_idx` (`transaksi_keuangan_id`),
  KEY `fk_transaksi_tagihan_tagihan1_idx` (`tagihan_id`),
  CONSTRAINT `fk_transaksi_tagihan_tagihan1` FOREIGN KEY (`tagihan_id`) REFERENCES `tagihan` (`tagihan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaksi_tagihan_transaksi_keuangan1` FOREIGN KEY (`transaksi_keuangan_id`) REFERENCES `transaksi_keuangan` (`transaksi_keuangan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transfer_stock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transfer_stock`;

CREATE TABLE `transfer_stock` (
  `transfer_stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `gudang_asal` int(11) NOT NULL,
  `gudang_tujuan` int(11) NOT NULL,
  `tanggal_transfer` date DEFAULT NULL,
  `keperluan` varchar(450) DEFAULT NULL,
  `catatan_pengiriman` varchar(450) DEFAULT NULL,
  `catatan_penerimaan` varchar(450) DEFAULT NULL,
  `status` enum('PROSES','TERKIRIM','DITERIMA','BATAL') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`transfer_stock_id`),
  KEY `fk_transfer_stock_gudang1_idx` (`gudang_asal`),
  KEY `fk_transfer_stock_gudang2_idx` (`gudang_tujuan`),
  CONSTRAINT `fk_transfer_stock_gudang1` FOREIGN KEY (`gudang_asal`) REFERENCES `gudang` (`gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transfer_stock_gudang2` FOREIGN KEY (`gudang_tujuan`) REFERENCES `gudang` (`gudang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transfer_stock_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transfer_stock_detail`;

CREATE TABLE `transfer_stock_detail` (
  `transfer_stock_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_stock_id` int(11) NOT NULL,
  `jenis_barang_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`transfer_stock_detail_id`),
  KEY `fk_transfer_stock_detail_transfer_stock1_idx` (`transfer_stock_id`),
  KEY `fk_transfer_stock_detail_jenis_barang1_idx` (`jenis_barang_id`),
  CONSTRAINT `fk_transfer_stock_detail_jenis_barang1` FOREIGN KEY (`jenis_barang_id`) REFERENCES `jenis_barang` (`jenis_barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transfer_stock_detail_transfer_stock1` FOREIGN KEY (`transfer_stock_id`) REFERENCES `transfer_stock` (`transfer_stock_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `nama_users` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','BANNED') DEFAULT NULL,
  `role` enum('ADMIN','SALES','INVENTORY','FINANCE','ACCOUNTING','LAB','RADIOLOGI','DOKTER') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table work_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `work_order`;

CREATE TABLE `work_order` (
  `work_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `analis_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` enum('PROSES','SELESAI','BATAL') DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`work_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table work_order_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `work_order_detail`;

CREATE TABLE `work_order_detail` (
  `work_order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_order_id` int(11) NOT NULL,
  `sales_order_item_id` int(11) NOT NULL,
  `alat_produksi_id` int(11) DEFAULT NULL,
  `jenis_sample` varchar(45) DEFAULT NULL,
  `waktu_ambil_sample` date DEFAULT NULL,
  `barang_id` int(11) DEFAULT NULL,
  `status` enum('PROSES','SELESAI','BATAL') DEFAULT NULL,
  `alasan_batal` varchar(450) DEFAULT NULL,
  `__active` tinyint(1) DEFAULT NULL,
  `__created` datetime DEFAULT NULL,
  `__updated` datetime DEFAULT NULL,
  `__username` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`work_order_detail_id`),
  KEY `fk_work_order_detail_work_order1_idx` (`work_order_id`),
  KEY `fk_work_order_detail_alat_produksi1_idx` (`alat_produksi_id`),
  KEY `fk_work_order_detail_sales_order_item1_idx` (`sales_order_item_id`),
  CONSTRAINT `fk_work_order_detail_alat_produksi1` FOREIGN KEY (`alat_produksi_id`) REFERENCES `alat_produksi` (`alat_produksi_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_order_detail_sales_order_item1` FOREIGN KEY (`sales_order_item_id`) REFERENCES `sales_order_item` (`sales_order_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_order_detail_work_order1` FOREIGN KEY (`work_order_id`) REFERENCES `work_order` (`work_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
