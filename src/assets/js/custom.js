jQuery(function($){ // wait until the DOM is ready
    //Bootstrap datepicker plugin
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
    });
});
