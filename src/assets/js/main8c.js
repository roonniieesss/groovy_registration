$(document).ready(function() {

    // BUTTON NEXT STEP 1
    $('#btn_destination_details').on('click', function(e){
       var err_msg_kota_asal,err_msg_kota_tujuan,err_msg_moda_transportasi = '';

       if($.trim($('#kota-asal-sel').val()) == 0)
       {
          err_msg_kota_asal = 'This field is required';
          $('#err_msg_kota_asal').text(err_msg_kota_asal);
          $('#kota-asal-sel').addClass('has-error');
       }
       else
       {
          err_msg_kota_asal = '';
          $('#err_msg_kota_asal').text(err_msg_kota_asal);
          $('#kota-asal-sel').removeClass('has-error');
       }

       if($.trim($('#kota-tujuan-sel').val()).length == 0)
       {
          err_msg_kota_tujuan = 'This field is required';
          $('#err_msg_kota_tujuan').text(err_msg_kota_tujuan);
          $('#kota-tujuan-sel').addClass('has-error');
       }
       else
       {
          err_msg_kota_tujuan = '';
          $('#err_msg_kota_tujuan').text(err_msg_kota_tujuan);
          $('#kota-tujuan-sel').removeClass('has-error');
       }
       
       if($.trim($('#moda-transportasi-sel').val()).length == 0)
       {
          err_msg_moda_transportasi = 'This field is required';
          $('#err_msg_moda_transportasi').text(err_msg_moda_transportasi);
          $('#moda-transportasi-sel').addClass('has-error');
       }
       else
       {
          err_msg_moda_transportasi = '';
          $('#err_msg_moda_transportasi').text(err_msg_moda_transportasi);
          $('#moda-transportasi-sel').removeClass('has-error');
       }

       if(err_msg_kota_asal != '' || err_msg_kota_tujuan != '' || err_msg_moda_transportasi != '')
       {
          // return false;
          $('#list_destination').removeClass('active active_tab1');
          $('#list_destination').removeAttr('href data-toggle');
          $('#destination_details').removeClass('active');
          $('#list_destination').addClass('inactive_tab1');
          $('#list_pet_information').removeClass('inactive_tab1');
          $('#list_pet_information').addClass('active_tab1 active');
          $('#list_pet_information').attr('href', '#pet_information_details');
          $('#list_pet_information').attr('data-toggle', 'tab');
          $('#pet_information_details').addClass('active in');
          $('#pet_information_details').find('input').val('').focus();
       }
       else
       {
          $('#list_destination').removeClass('active active_tab1');
          $('#list_destination').removeAttr('href data-toggle');
          $('#destination_details').removeClass('active');
          $('#list_destination').addClass('inactive_tab1');
          $('#list_pet_information').removeClass('inactive_tab1');
          $('#list_pet_information').addClass('active_tab1 active');
          $('#list_pet_information').attr('href', '#pet_information_details');
          $('#list_pet_information').attr('data-toggle', 'tab');
          $('#pet_information_details').addClass('active in');
          $('#pet_information_details').find('input').val('').focus();
       }
    });

    $('#previous_btn_pet_details').click(function(){
       $('#list_pet_information').removeClass('active active_tab1');
       $('#list_pet_information').removeAttr('href data-toggle');
       $('#pet_information_details').removeClass('active in');
       $('#list_pet_information').addClass('inactive_tab1');
       $('#list_destination').removeClass('inactive_tab1');
       $('#list_destination').addClass('active_tab1 active');
       $('#list_destination').attr('href', '#destination_details');
       $('#list_destination').attr('data-toggle', 'tab');
       $('#destination_details').addClass('active in');
    });

    DEMO ONLY //
    $('#activate-step-2').on('click', function(e) {
        // $('ul.setup-panel li:eq(1)').removeClass('disabled');
        // $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        // $(this).remove();
    });

    
});

