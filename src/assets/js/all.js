$(document).ready(function(){

    //DUPLICATE HEWAN
    $(document).on( "click", "a#addhewan", function() {
        var confirm = window.confirm('Yakin menambah data hewan??');            
        if(confirm == true){                
            var form_hewan = $('.copyHewan').html();            
            $(form_hewan).insertBefore('.hewan-baru');
            // $('.master-hewan').addClass('hidden');

            //ENABLED DATE PICKER CLONE
            $('.date').datetimepicker({
                format : "YYYY-MM-DD",  
                showTodayButton : true,
                allowInputToggle : true,
                widgetPositioning : {
                    horizontal: 'left',
                    vertical: 'bottom'
                },
            });
        }
    });

    //SELECT JENIS HEWAN
    // $("#ras_hewan_id").change(function(){
    $(document).on( "change", "#ras_hewan_id", function() {

        var rule_pet_korean_jindo = $('#rule_pet_korean_jindo').html();
        var rule_pet_siberian_husky = $('#rule_pet_siberian_husky').html();
        var rule_pet_persian = $('#rule_pet_persian').html();

        if($(this).val() == "1"){
            
            $(rule_pet_korean_jindo).insertBefore('.show-rule-hewan');
            $('.cari_rule').find('.master-hewan').addClass('hidden');

        } else if($(this).val() == "2"){               
            
            $(rule_pet_siberian_husky).insertBefore('.show-rule-hewan');
            $('.cari_rule').find('.master-hewan').addClass('hidden');     

        } else if($(this).val() == "3"){
             
            $(rule_pet_persian).insertBefore('.show-rule-hewan');

        }

    });

    //MASUKAN JENIS MASKAPAI
    $("#moda_transportasi_id").change(function(){            
        if($(this).val() == "1"){                
            $('.maskapai').removeClass('hidden');
        }

    });
    
    $(function () {
        // Bootstrap datepicker plugin
        $('.datetime').datetimepicker({
            format : "YYYY-MM-DD HH:mm:ss",
        });

        $('.date').datetimepicker({
            format : "YYYY-MM-DD",
            showTodayButton : true,
            allowInputToggle : true,
            widgetPositioning : {
                horizontal: 'left',
                vertical: 'bottom'
            },
        });            
    });

});