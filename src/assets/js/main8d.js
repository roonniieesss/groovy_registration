$(document).ready(function() {
    
    //TOMBOL ATAS
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });    
    $('ul.setup-panel li.active a').trigger('click');
    
    //TOMBOL PREVIOUSE 1
    var prevnavListItems = $('#previous_1'),
        allWells = $('.setup-content');

    prevnavListItems.click(function(e)
    {
      
        e.preventDefault();
        var head = $('div#kepala');
        var $target = $($(this).attr('href')),
            $item = head.find('ul.setup-panel li a:first');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    //TOMBOL PREVIOUS 2
    var prevnavListItems2 = $('#previous_2'),
        allWells = $('.setup-content');

    prevnavListItems2.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    // TOMBOL NEXT 1
    $('#activate-step-2').on('click', function(e) {        

        var err_msg_kota_asal,
            err_msg_kota_tujuan,
            err_msg_moda_transportasi,
            err_msg_jenis_layanan,
            err_msg_maskapai = '';
            
        if($.trim($('#asal_kota').val()) == 0)
        {
           err_msg_kota_asal = 'This field is required';
           $('#err_msg_kota_asal').text(err_msg_kota_asal);
           $('#asal_kota').addClass('has-error');
        }
        else
        {
           err_msg_kota_asal = '';
           $('#err_msg_kota_asal').text(err_msg_kota_asal);
           $('#asal_kota').removeClass('has-error');
        }

        if($.trim($('#tujuan_kota').val()).length == 0)
        {
           err_msg_kota_tujuan = 'This field is required';
           $('#err_msg_kota_tujuan').text(err_msg_kota_tujuan);
           $('#tujuan_kota').addClass('has-error');
        }
        else
        {
           err_msg_kota_tujuan = '';
           $('#err_msg_kota_tujuan').text(err_msg_kota_tujuan);
           $('#tujuan_kota').removeClass('has-error');
        }
        
        if($.trim($('#moda_transportasi_id').val()).length == 0)
        {
           err_msg_moda_transportasi = 'This field is required';
           $('#err_msg_moda_transportasi').text(err_msg_moda_transportasi);
           $('#moda_transportasi_id').addClass('has-error');
        }
        else
        {
           err_msg_moda_transportasi = '';
           $('#err_msg_moda_transportasi').text(err_msg_moda_transportasi);
           $('#moda_transportasi_id').removeClass('has-error');
        }

        if($.trim($('#jenis_barang_id').val()).length == 0)
        {
           err_msg_jenis_layanan = 'This field is required';
           $('#err_msg_jenis_layanan').text(err_msg_jenis_layanan);
           $('#jenis_barang_id').addClass('has-error');
        }
        else
        {
           err_msg_jenis_layanan = '';
           $('#err_msg_jenis_layanan').text(err_msg_jenis_layanan);
           $('#jenis_barang_id').removeClass('has-error');
        }

        if($.trim($('#maskapai').val()).length == 0)
        {
           err_msg_maskapai = 'This field is required';
           $('#err_msg_maskapai').text(err_msg_maskapai);
           $('#maskapai').addClass('has-error');
        }
        else
        {
           err_msg_maskapai = '';
           $('#err_msg_maskapai').text(err_msg_maskapai);
           $('#maskapai').removeClass('has-error');
        }

        if(err_msg_kota_asal != '' || err_msg_kota_tujuan != '' || err_msg_moda_transportasi != '' 
         || err_msg_jenis_layanan != '' || err_msg_maskapai != '')
        {
           // return false;
           $('ul.setup-panel li:eq(1)').removeClass('disabled');
           $('ul.setup-panel li a[href="#step-2"]').trigger('click');
           $('#pet_information_details').find('input').val('').focus();
        }
        else
        {
            $('ul.setup-panel li:eq(1)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-2"]').trigger('click');
            // $(this).remove();
            $('#pet_information_details').find('input').val('').focus();
        }
    });    


    

    // TOMBOL NEXT 2
    $('#activate-step-3').on('click', function(e){
      var err_msg_nama_hewan,
          err_msg_dob_hewan,
          err_msg_warna_hewan,
          err_msg_jenis_kelamin_hewan,
          err_msg_berat_hewan,
          err_msg_microchip_hewan,
          err_msg_jenis_hewan,          
          err_msg_ras_hewan = '';
      
      if($.trim($('#nama_hewan').val()).length == 0)
      {
         err_msg_nama_hewan = 'This field is required';
         $('#err_msg_nama_hewan').text(err_msg_nama_hewan);
         $('#nama_hewan').addClass('has-error');
      }
      else
      {
         err_msg_nama_hewan = '';
         $('#err_msg_nama_hewan').text(err_msg_nama_hewan);
         $('#nama_hewan').removeClass('has-error');
      }

      if($.trim($('#dob').val()).length == 0)
      {
         err_msg_dob_hewan = 'This field is required';
         $('#err_msg_dob_hewan').text(err_msg_dob_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_dob_hewan = '';
         $('#err_msg_dob_hewan').text(err_msg_dob_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }

      if($.trim($('#warna_hewan').val()).length == 0)
      {
         err_msg_warna_hewan = 'This field is required';
         $('#err_msg_warna_hewan').text(err_msg_warna_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_warna_hewan = '';
         $('#err_msg_warna_hewan').text(err_msg_warna_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }

      if($.trim($('#jenis_kelamin_hewan').val()).length == 0)
      {
         err_msg_jenis_kelamin_hewan = 'This field is required';
         $('#err_msg_jenis_kelamin_hewan').text(err_msg_jenis_kelamin_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_jenis_kelamin_hewan = '';
         $('#err_msg_jenis_kelamin_hewan').text(err_msg_jenis_kelamin_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }

      if($.trim($('#berat_hewan').val()).length == 0)
      {
         err_msg_berat_hewan = 'This field is required';
         $('#err_msg_berat_hewan').text(err_msg_berat_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_berat_hewan = '';
         $('#err_msg_berat_hewan').text(err_msg_berat_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }

      if($.trim($('#microchip').val()).length == 0)
      {
         err_msg_microchip_hewan = 'This field is required';
         $('#err_msg_microchip_hewan').text(err_msg_microchip_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_microchip_hewan = '';
         $('#err_msg_microchip_hewan').text(err_msg_microchip_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }

      if($.trim($('#jenis_hewan_sel').val()).length == 0)
      {
         err_msg_jenis_hewan = 'This field is required';
         $('#err_msg_jenis_hewan').text(err_msg_jenis_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_jenis_hewan = '';
         $('#err_msg_jenis_hewan').text(err_msg_jenis_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }    

      if($.trim($('#ras_hewan_sel').val()).length == 0)
      {
         err_msg_ras_hewan = 'This field is required';
         $('#err_msg_ras_hewan').text(err_msg_ras_hewan);
         $('#ras_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_ras_hewan = '';
         $('#err_msg_ras_hewan').text(err_msg_ras_hewan);
         $('#ras_hewan_sel').removeClass('has-error');
      }
      
      if(err_msg_nama_hewan != '')
      {
         // return false; 
         $('ul.setup-panel li:eq(2)').removeClass('disabled');
         $('ul.setup-panel li a[href="#step-3"]').trigger('click');                        
      }
      else
      {          
         $('ul.setup-panel li:eq(2)').removeClass('disabled');
         $('ul.setup-panel li a[href="#step-3"]').trigger('click');        
      }

    });

    // REGISTER
   $('#register').on('click', function(e){

   })
   
});

