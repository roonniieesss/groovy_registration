$(document).ready(function(){
   // elemen hide

   $("#rule-pet-herder").hide();
   $("#rule-pet-anggora").hide();

   $('#btn_destination_details').click(function(){
      var err_msg_kota_asal,err_msg_kota_tujuan,err_msg_moda_transportasi = '';

      if($.trim($('#kota-asal-sel').val()) == 0)
      {
         err_msg_kota_asal = 'This field is required';
         $('#err_msg_kota_asal').text(err_msg_kota_asal);
         $('#kota-asal-sel').addClass('has-error');
      }
      else
      {
         err_msg_kota_asal = '';
         $('#err_msg_kota_asal').text(err_msg_kota_asal);
         $('#kota-asal-sel').removeClass('has-error');
      }

      if($.trim($('#kota-tujuan-sel').val()).length == 0)
      {
         err_msg_kota_tujuan = 'This field is required';
         $('#err_msg_kota_tujuan').text(err_msg_kota_tujuan);
         $('#kota-tujuan-sel').addClass('has-error');
      }
      else
      {
         err_msg_kota_tujuan = '';
         $('#err_msg_kota_tujuan').text(err_msg_kota_tujuan);
         $('#kota-tujuan-sel').removeClass('has-error');
      }
      
      if($.trim($('#moda-transportasi-sel').val()).length == 0)
      {
         err_msg_moda_transportasi = 'This field is required';
         $('#err_msg_moda_transportasi').text(err_msg_moda_transportasi);
         $('#moda-transportasi-sel').addClass('has-error');
      }
      else
      {
         err_msg_moda_transportasi = '';
         $('#err_msg_moda_transportasi').text(err_msg_moda_transportasi);
         $('#moda-transportasi-sel').removeClass('has-error');
      }

      if(err_msg_kota_asal != '' || err_msg_kota_tujuan != '' || err_msg_moda_transportasi != '')
      {
         // return false;
         $('#list_destination').removeClass('active active_tab1');
         $('#list_destination').removeAttr('href data-toggle');
         $('#destination_details').removeClass('active');
         $('#list_destination').addClass('inactive_tab1');
         $('#list_pet_information').removeClass('inactive_tab1');
         $('#list_pet_information').addClass('active_tab1 active');
         $('#list_pet_information').attr('href', '#pet_information_details');
         $('#list_pet_information').attr('data-toggle', 'tab');
         $('#pet_information_details').addClass('active in');
         $('#qty').focus();
      }
      else
      {
         $('#list_destination').removeClass('active active_tab1');
         $('#list_destination').removeAttr('href data-toggle');
         $('#destination_details').removeClass('active');
         $('#list_destination').addClass('inactive_tab1');
         $('#list_pet_information').removeClass('inactive_tab1');
         $('#list_pet_information').addClass('active_tab1 active');
         $('#list_pet_information').attr('href', '#pet_information_details');
         $('#list_pet_information').attr('data-toggle', 'tab');
         $('#pet_information_details').addClass('active in');
         $('#qty').focus();
      }
   });

   $('#previous_btn_pet_details').click(function(){
      $('#list_pet_information').removeClass('active active_tab1');
      $('#list_pet_information').removeAttr('href data-toggle');
      $('#pet_information_details').removeClass('active in');
      $('#list_pet_information').addClass('inactive_tab1');
      $('#list_destination').removeClass('inactive_tab1');
      $('#list_destination').addClass('active_tab1 active');
      $('#list_destination').attr('href', '#destination_details');
      $('#list_destination').attr('data-toggle', 'tab');
      $('#destination_details').addClass('active in');
   });

   $('#btn_pet_details').click(function(){
      var err_msg_qty_hewan,err_msg_nama_hewan,err_msg_jenis_hewan = '';
      var a = $('#qty').val();
      // alert(a);
      for (var i = 0; i <= a.length; i++) {

      if($.trim($('#qty').val()).length == 0)
      {
         err_msg_qty_hewan = 'This field is required';
         $('#err_msg_qty_hewan').text(err_msg_qty_hewan);
         $('#qty').addClass('has-error');
      }
      else
      {
         err_msg_qty_hewan = '';
         $('#err_msg_qty_hewan').text(err_msg_qty_hewan);
         $('#qty').removeClass('has-error');
      }

      if($.trim($('#nama_hewan').val()).length == 0)
      {
         err_msg_nama_hewan = 'This field is required';
         $('#err_msg_nama_hewan').text(err_msg_nama_hewan);
         $('#nama_hewan').addClass('has-error');
      }
      else
      {
         err_msg_nama_hewan = '';
         $('#err_msg_nama_hewan').text(err_msg_nama_hewan);
         $('#nama_hewan').removeClass('has-error');
      }

      if($.trim($('#jenis_hewan_sel').val()).length == 0)
      {
         err_msg_jenis_hewan = 'This field is required';
         $('#err_msg_jenis_hewan').text(err_msg_jenis_hewan);
         $('#jenis_hewan_sel').addClass('has-error');
      }
      else
      {
         err_msg_jenis_hewan = '';
         $('#err_msg_jenis_hewan').text(err_msg_jenis_hewan);
         $('#jenis_hewan_sel').removeClass('has-error');
      }

      if($.trim($('#catatan_pesanan').val()).length == 0)
      {
         err_msg_catatan_pesanan = 'This field is required';
         $('#err_msg_catatan_pesanan').text(err_msg_catatan_pesanan);
         $('#catatan_pesanan').addClass('has-error');
      }
      else
      {
         err_msg_catatan_pesanan = '';
         $('#err_msg_catatan_pesanan').text(err_msg_catatan_pesanan);
         $('#catatan_pesananan').removeClass('has-error');
      }

      if($.trim($('#jenis_kelamin_pet').val()).length == 0)
      {
         err_msg_jenis_kelamin_pet = 'This field is required';
         $('#err_msg_jenis_kelamin_pet').text(err_msg_jenis_kelamin_pet);
         $('#jenis_kelamin_pet').addClass('has-error');
      }
      else
      {
         err_msg_jenis_kelamin_pet = '';
         $('#err_msg_jenis_kelamin_pet').text(err_msg_jenis_kelamin_pet);
         $('#jenis_kelamin_pet').removeClass('has-error');
      }
      }
      if(err_msg_qty_hewan != '' || err_msg_nama_hewan != '' || err_msg_jenis_hewan != '')
      {
         // return false;
         $('#list_pet_information').removeClass('active active_tab1');
         $('#list_pet_information').removeAttr('href data-toggle');
         $('#pet_information_details').removeClass('active');
         $('#list_pet_information').addClass('inactive_tab1');
         $('#list_customer_information').removeClass('inactive_tab1');
         $('#list_customer_information').addClass('active_tab1 active');
         $('#list_customer_information').attr('href', '#pet_information_details');
         $('#list_customer_information').attr('data-toggle', 'tab');
         $('#pelanggan_information_details').addClass('active in');
      }
      else
      {
         $('#list_pet_information').removeClass('active active_tab1');
         $('#list_pet_information').removeAttr('href data-toggle');
         $('#pet_information_details').removeClass('active');
         $('#list_pet_information').addClass('inactive_tab1');
         $('#list_customer_information').removeClass('inactive_tab1');
         $('#list_customer_information').addClass('active_tab1 active');
         $('#list_customer_information').attr('href', '#pet_information_details');
         $('#list_customer_information').attr('data-toggle', 'tab');
         $('#pelanggan_information_details').addClass('active in');
         $('#nama_pelanggan').focus();
      }

   });

   $('#previous_btn_cust_details').click(function(){
      $('#list_customer_information').removeClass('active active_tab1');
      $('#list_customer_information').removeAttr('href data-toggle');
      $('#pelanggan_information_details').removeClass('active in');
      $('#list_customer_information').addClass('inactive_tab1');
      $('#list_pet_information').removeClass('inactive_tab1');
      $('#list_pet_information').addClass('active_tab1 active');
      $('#list_pet_information').attr('href', '#pet_information_details');
      $('#list_pet_information').attr('data-toggle', 'tab');
      $('#pet_information_details').addClass('active in');
   });

   $('#register').click(function(){
      var err_msg_nama_pelanggan,err_msg_tgl_lahir,err_msg_jenis_kelamin,err_msg_kontak,err_msg_jenis_kontak = '';

      if($.trim($('#nama_pelanggan').val()).length == 0)
      {
         err_msg_nama_pelanggan = 'This field is required';
         $('#err_msg_nama_pelanggan').text(err_msg_nama_pelanggan);
         $('#nama_pelanggan').addClass('has-error');
      }
      else
      {
         err_msg_nama_pelanggan = '';
         $('#err_msg_nama_pelanggan').text(err_msg_nama_pelanggan);
         $('#qty').removeClass('has-error');
      }

   });

});