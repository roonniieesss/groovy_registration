<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('gudang_model');
    }

    public function index()
    {
        $data['page'] = 'gudang/gudang_list';
        $data['title'] = 'List Gudang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->gudang_model->json();
    }

    public function read($id)
    {
        $row = $this->gudang_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'nama_gudang' => $row->nama_gudang,
				'nama_pic' => $row->nama_pic,
				'alamat' => $row->alamat,
				'telepon' => $row->telepon,
				'fax' => $row->fax,
				'kapasitas_kirim_harian' => $row->kapasitas_kirim_harian,
				'kapasitas_do_harian' => $row->kapasitas_do_harian,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'gudang/gudang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('gudang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('gudang/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'nama_gudang' => set_value('nama_gudang'),
		    'nama_pic' => set_value('nama_pic'),
		    'alamat' => set_value('alamat'),
		    'telepon' => set_value('telepon'),
		    'fax' => set_value('fax'),
		    'kapasitas_kirim_harian' => set_value('kapasitas_kirim_harian'),
		    'kapasitas_do_harian' => set_value('kapasitas_do_harian'),
		);
        $data['page'] = 'gudang/gudang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('gudang/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'nama_gudang' => post('nama_gudang',true),
				'nama_pic' => post('nama_pic',true),
				'alamat' => post('alamat',true),
				'telepon' => post('telepon',true),
				'fax' => post('fax',true),
				'kapasitas_kirim_harian' => post('kapasitas_kirim_harian',true),
				'kapasitas_do_harian' => post('kapasitas_do_harian',true),
		    );

            $this->gudang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('gudang'));
        }
    }

    public function update($id)
    {
        $row = $this->gudang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('gudang/update-action'),
				'gudang_id' => set_value('gudang_id', $row->gudang_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'nama_gudang' => set_value('nama_gudang', $row->nama_gudang),
				'nama_pic' => set_value('nama_pic', $row->nama_pic),
				'alamat' => set_value('alamat', $row->alamat),
				'telepon' => set_value('telepon', $row->telepon),
				'fax' => set_value('fax', $row->fax),
				'kapasitas_kirim_harian' => set_value('kapasitas_kirim_harian', $row->kapasitas_kirim_harian),
				'kapasitas_do_harian' => set_value('kapasitas_do_harian', $row->kapasitas_do_harian),
			    );

            $data['page'] = 'gudang/gudang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('gudang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('gudang/update/'.post('gudang_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'nama_gudang' => post('nama_gudang',true),
				'nama_pic' => post('nama_pic',true),
				'alamat' => post('alamat',true),
				'telepon' => post('telepon',true),
				'fax' => post('fax',true),
				'kapasitas_kirim_harian' => post('kapasitas_kirim_harian',true),
				'kapasitas_do_harian' => post('kapasitas_do_harian',true),
		    );

            $this->gudang_model->update($data, post('gudang_id', true));

            success('Update Record Success');
            redirect(base_url('gudang/update/'.post('gudang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->gudang_model->get($id);

        if ($row) {
            $this->gudang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('gudang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('gudang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_gudang', 'nama gudang', 'trim|required');
		$this->form_validation->set_rules('nama_pic', 'nama pic', 'trim|required');
		$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
		$this->form_validation->set_rules('telepon', 'telepon', 'trim|required');
		$this->form_validation->set_rules('fax', 'fax', 'trim|required');
		$this->form_validation->set_rules('kapasitas_kirim_harian', 'kapasitas kirim harian', 'trim|required|numeric');
		$this->form_validation->set_rules('kapasitas_do_harian', 'kapasitas do harian', 'trim|required|numeric');

		$this->form_validation->set_rules('gudang_id', 'gudang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file gudang.php */
/* Location: ./application/controllers/gudang.php */
/* Please DO NOT modify this information : */