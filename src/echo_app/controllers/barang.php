<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('barang_model');
    }

    public function index()
    {
        $data['page'] = 'barang/barang_list';
        $data['title'] = 'List Barang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->barang_model->json();
    }

    public function read($id)
    {
        $row = $this->barang_model->get($id);
        if ($row) {
            $data = array(
				'jenis_barang_id' => $row->jenis_barang_id,
				'kemasan_id' => $row->kemasan_id,
				'no_identifikasi' => $row->no_identifikasi,
				'kondisi' => $row->kondisi,
				'catatan' => $row->catatan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'barang/barang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('barang/create-action'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'kemasan_id' => set_value('kemasan_id'),
		    'no_identifikasi' => set_value('no_identifikasi'),
		    'kondisi' => set_value('kondisi'),
		    'catatan' => set_value('catatan'),
		);
        $data['page'] = 'barang/barang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('barang/create'));
        } else {
            $data = array(
				'jenis_barang_id' => post('jenis_barang_id',true),
				'kemasan_id' => post('kemasan_id',true),
				'no_identifikasi' => post('no_identifikasi',true),
				'kondisi' => post('kondisi',true),
				'catatan' => post('catatan',true),
		    );

            $this->barang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('barang'));
        }
    }

    public function update($id)
    {
        $row = $this->barang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('barang/update-action'),
				'barang_id' => set_value('barang_id', $row->barang_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'kemasan_id' => set_value('kemasan_id', $row->kemasan_id),
				'no_identifikasi' => set_value('no_identifikasi', $row->no_identifikasi),
				'kondisi' => set_value('kondisi', $row->kondisi),
				'catatan' => set_value('catatan', $row->catatan),
			    );

            $data['page'] = 'barang/barang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('barang/update/'.post('barang_id', true)));
        } else {
            $data = array(
				'jenis_barang_id' => post('jenis_barang_id',true),
				'kemasan_id' => post('kemasan_id',true),
				'no_identifikasi' => post('no_identifikasi',true),
				'kondisi' => post('kondisi',true),
				'catatan' => post('catatan',true),
		    );

            $this->barang_model->update($data, post('barang_id', true));

            success('Update Record Success');
            redirect(base_url('barang/update/'.post('barang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->barang_model->get($id);

        if ($row) {
            $this->barang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('barang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('barang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('kemasan_id', 'kemasan', 'trim|required|numeric');
		$this->form_validation->set_rules('no_identifikasi', 'no entifikasi', 'trim|required');
		$this->form_validation->set_rules('kondisi', 'kondisi', 'trim|required');
		$this->form_validation->set_rules('catatan', 'catatan', 'trim|required');

		$this->form_validation->set_rules('barang_id', 'barang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file barang.php */
/* Location: ./application/controllers/barang.php */
/* Please DO NOT modify this information : */