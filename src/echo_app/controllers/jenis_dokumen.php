<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_dokumen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_dokumen_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-dokumen/jenis_dokumen_list';
        $data['title'] = 'List Jenis Dokumen';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_dokumen_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_dokumen_model->get($id);
        if ($row) {
            $data = array(
				'nama_jenis_dokumen' => $row->nama_jenis_dokumen,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-dokumen/jenis_dokumen_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-dokumen'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-dokumen/create-action'),
		    'nama_jenis_dokumen' => set_value('nama_jenis_dokumen'),
		);
        $data['page'] = 'jenis-dokumen/jenis_dokumen_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-dokumen/create'));
        } else {
            $data = array(
				'nama_jenis_dokumen' => post('nama_jenis_dokumen',true),
		    );

            $this->jenis_dokumen_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-dokumen'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_dokumen_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-dokumen/update-action'),
				'jenis_dokumen_id' => set_value('jenis_dokumen_id', $row->jenis_dokumen_id),
				'nama_jenis_dokumen' => set_value('nama_jenis_dokumen', $row->nama_jenis_dokumen),
			    );

            $data['page'] = 'jenis-dokumen/jenis_dokumen_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-dokumen'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-dokumen/update/'.post('jenis_dokumen_id', true)));
        } else {
            $data = array(
				'nama_jenis_dokumen' => post('nama_jenis_dokumen',true),
		    );

            $this->jenis_dokumen_model->update($data, post('jenis_dokumen_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-dokumen/update/'.post('jenis_dokumen_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_dokumen_model->get($id);

        if ($row) {
            $this->jenis_dokumen_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-dokumen'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-dokumen'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_jenis_dokumen', 'nama jenis dokumen', 'trim|required');

		$this->form_validation->set_rules('jenis_dokumen_id', 'jenis_dokumen_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_dokumen.php */
/* Location: ./application/controllers/jenis_dokumen.php */
/* Please DO NOT modify this information : */