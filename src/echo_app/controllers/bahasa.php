<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class bahasa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('bahasa_model');
    }

    public function index()
    {
        $data['page'] = 'bahasa/bahasa_list';
        $data['title'] = 'List Bahasa';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->bahasa_model->json();
    }

    public function read($id)
    {
        $row = $this->bahasa_model->get($id);
        if ($row) {
            $data = array(
				'kode_bahasa' => $row->kode_bahasa,
				'nama_bahasa' => $row->nama_bahasa,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'bahasa/bahasa_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('bahasa'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('bahasa/create-action'),
		    'kode_bahasa' => set_value('kode_bahasa'),
		    'nama_bahasa' => set_value('nama_bahasa'),
		);
        $data['page'] = 'bahasa/bahasa_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('bahasa/create'));
        } else {
            $data = array(
				'kode_bahasa' => post('kode_bahasa',true),
				'nama_bahasa' => post('nama_bahasa',true),
		    );

            $this->bahasa_model->insert($data);

            success('Create Record Success');
            redirect(base_url('bahasa'));
        }
    }

    public function update($id)
    {
        $row = $this->bahasa_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('bahasa/update-action'),
				'bahasa_id' => set_value('bahasa_id', $row->bahasa_id),
				'kode_bahasa' => set_value('kode_bahasa', $row->kode_bahasa),
				'nama_bahasa' => set_value('nama_bahasa', $row->nama_bahasa),
			    );

            $data['page'] = 'bahasa/bahasa_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('bahasa'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('bahasa/update/'.post('bahasa_id', true)));
        } else {
            $data = array(
				'kode_bahasa' => post('kode_bahasa',true),
				'nama_bahasa' => post('nama_bahasa',true),
		    );

            $this->bahasa_model->update($data, post('bahasa_id', true));

            success('Update Record Success');
            redirect(base_url('bahasa/update/'.post('bahasa_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->bahasa_model->get($id);

        if ($row) {
            $this->bahasa_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('bahasa'));
        } else {

            warning('Record Not Found');
            redirect(base_url('bahasa'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('kode_bahasa', 'kode bahasa', 'trim|required');
		$this->form_validation->set_rules('nama_bahasa', 'nama bahasa', 'trim|required');

		$this->form_validation->set_rules('bahasa_id', 'bahasa_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file bahasa.php */
/* Location: ./application/controllers/bahasa.php */
/* Please DO NOT modify this information : */