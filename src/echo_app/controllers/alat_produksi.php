<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class alat_produksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('alat_produksi_model');
    }

    public function index()
    {
        $data['page'] = 'alat-produksi/alat_produksi_list';
        $data['title'] = 'List Alat Produksi';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->alat_produksi_model->json();
    }

    public function read($id)
    {
        $row = $this->alat_produksi_model->get($id);
        if ($row) {
            $data = array(
				'nama_alat' => $row->nama_alat,
				'merek' => $row->merek,
				'tipe' => $row->tipe,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'alat-produksi/alat_produksi_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('alat-produksi'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('alat-produksi/create-action'),
		    'nama_alat' => set_value('nama_alat'),
		    'merek' => set_value('merek'),
		    'tipe' => set_value('tipe'),
		);
        $data['page'] = 'alat-produksi/alat_produksi_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('alat-produksi/create'));
        } else {
            $data = array(
				'nama_alat' => post('nama_alat',true),
				'merek' => post('merek',true),
				'tipe' => post('tipe',true),
		    );

            $this->alat_produksi_model->insert($data);

            success('Create Record Success');
            redirect(base_url('alat-produksi'));
        }
    }

    public function update($id)
    {
        $row = $this->alat_produksi_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('alat-produksi/update-action'),
				'alat_produksi_id' => set_value('alat_produksi_id', $row->alat_produksi_id),
				'nama_alat' => set_value('nama_alat', $row->nama_alat),
				'merek' => set_value('merek', $row->merek),
				'tipe' => set_value('tipe', $row->tipe),
			    );

            $data['page'] = 'alat-produksi/alat_produksi_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('alat-produksi'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('alat-produksi/update/'.post('alat_produksi_id', true)));
        } else {
            $data = array(
				'nama_alat' => post('nama_alat',true),
				'merek' => post('merek',true),
				'tipe' => post('tipe',true),
		    );

            $this->alat_produksi_model->update($data, post('alat_produksi_id', true));

            success('Update Record Success');
            redirect(base_url('alat-produksi/update/'.post('alat_produksi_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->alat_produksi_model->get($id);

        if ($row) {
            $this->alat_produksi_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('alat-produksi'));
        } else {

            warning('Record Not Found');
            redirect(base_url('alat-produksi'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_alat', 'nama alat', 'trim|required');
		$this->form_validation->set_rules('merek', 'merek', 'trim|required');
		$this->form_validation->set_rules('tipe', 'tipe', 'trim|required');

		$this->form_validation->set_rules('alat_produksi_id', 'alat_produksi_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file alat_produksi.php */
/* Location: ./application/controllers/alat_produksi.php */
/* Please DO NOT modify this information : */