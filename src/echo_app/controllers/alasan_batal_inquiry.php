<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class alasan_batal_inquiry extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('alasan_batal_inquiry_model');
    }

    public function index()
    {
        $data['page'] = 'alasan-batal-inquiry/alasan_batal_inquiry_list';
        $data['title'] = 'List Alasan Batal Inquiry';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->alasan_batal_inquiry_model->json();
    }

    public function read($id)
    {
        $row = $this->alasan_batal_inquiry_model->get($id);
        if ($row) {
            $data = array(
				'alasan_batal_inquiry' => $row->alasan_batal_inquiry,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'alasan-batal-inquiry/alasan_batal_inquiry_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('alasan-batal-inquiry'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('alasan-batal-inquiry/create-action'),
		    'alasan_batal_inquiry' => set_value('alasan_batal_inquiry'),
		);
        $data['page'] = 'alasan-batal-inquiry/alasan_batal_inquiry_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('alasan-batal-inquiry/create'));
        } else {
            $data = array(
				'alasan_batal_inquiry' => post('alasan_batal_inquiry',true),
		    );

            $this->alasan_batal_inquiry_model->insert($data);

            success('Create Record Success');
            redirect(base_url('alasan-batal-inquiry'));
        }
    }

    public function update($id)
    {
        $row = $this->alasan_batal_inquiry_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('alasan-batal-inquiry/update-action'),
				'alasan_batal_inquiry_id' => set_value('alasan_batal_inquiry_id', $row->alasan_batal_inquiry_id),
				'alasan_batal_inquiry' => set_value('alasan_batal_inquiry', $row->alasan_batal_inquiry),
			    );

            $data['page'] = 'alasan-batal-inquiry/alasan_batal_inquiry_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('alasan-batal-inquiry'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('alasan-batal-inquiry/update/'.post('alasan_batal_inquiry_id', true)));
        } else {
            $data = array(
				'alasan_batal_inquiry' => post('alasan_batal_inquiry',true),
		    );

            $this->alasan_batal_inquiry_model->update($data, post('alasan_batal_inquiry_id', true));

            success('Update Record Success');
            redirect(base_url('alasan-batal-inquiry/update/'.post('alasan_batal_inquiry_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->alasan_batal_inquiry_model->get($id);

        if ($row) {
            $this->alasan_batal_inquiry_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('alasan-batal-inquiry'));
        } else {

            warning('Record Not Found');
            redirect(base_url('alasan-batal-inquiry'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('alasan_batal_inquiry', 'alasan batal inquiry', 'trim|required');

		$this->form_validation->set_rules('alasan_batal_inquiry_id', 'alasan_batal_inquiry_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file alasan_batal_inquiry.php */
/* Location: ./application/controllers/alasan_batal_inquiry.php */
/* Please DO NOT modify this information : */