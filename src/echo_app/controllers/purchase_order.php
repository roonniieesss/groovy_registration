<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class purchase_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('purchase_order_model');
    }

    public function index()
    {
        $data['page'] = 'purchase-order/purchase_order_list';
        $data['title'] = 'List Purchase Order';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->purchase_order_model->json();
    }

    public function read($id)
    {
        $row = $this->purchase_order_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'supplier_id' => $row->supplier_id,
				'no_purchase_order' => $row->no_purchase_order,
				'permintaan_tanggal_kirim' => $row->permintaan_tanggal_kirim,
				'nama_penerima' => $row->nama_penerima,
				'alamat_penerima' => $row->alamat_penerima,
				'telp_penerima' => $row->telp_penerima,
				'fax_penerima' => $row->fax_penerima,
				'kurs' => $row->kurs,
				'nilai_tukar' => $row->nilai_tukar,
				'diskon' => $row->diskon,
				'biaya_lainnya' => $row->biaya_lainnya,
				'total_pembelian' => $row->total_pembelian,
				'no_so_supplier' => $row->no_so_supplier,
				'tanggal_so_supplier' => $row->tanggal_so_supplier,
				'lampiran_so' => $row->lampiran_so,
				'term_pembayaran' => $row->term_pembayaran,
				'catatan' => $row->catatan,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'purchase-order/purchase_order_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('purchase-order'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('purchase-order/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'supplier_id' => set_value('supplier_id'),
		    'no_purchase_order' => set_value('no_purchase_order'),
		    'permintaan_tanggal_kirim' => set_value('permintaan_tanggal_kirim'),
		    'nama_penerima' => set_value('nama_penerima'),
		    'alamat_penerima' => set_value('alamat_penerima'),
		    'telp_penerima' => set_value('telp_penerima'),
		    'fax_penerima' => set_value('fax_penerima'),
		    'kurs' => set_value('kurs'),
		    'nilai_tukar' => set_value('nilai_tukar'),
		    'diskon' => set_value('diskon'),
		    'biaya_lainnya' => set_value('biaya_lainnya'),
		    'total_pembelian' => set_value('total_pembelian'),
		    'no_so_supplier' => set_value('no_so_supplier'),
		    'tanggal_so_supplier' => set_value('tanggal_so_supplier'),
		    'lampiran_so' => set_value('lampiran_so'),
		    'term_pembayaran' => set_value('term_pembayaran'),
		    'catatan' => set_value('catatan'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'purchase-order/purchase_order_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('purchase-order/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'supplier_id' => post('supplier_id',true),
				'no_purchase_order' => post('no_purchase_order',true),
				'permintaan_tanggal_kirim' => post('permintaan_tanggal_kirim',true),
				'nama_penerima' => post('nama_penerima',true),
				'alamat_penerima' => post('alamat_penerima',true),
				'telp_penerima' => post('telp_penerima',true),
				'fax_penerima' => post('fax_penerima',true),
				'kurs' => post('kurs',true),
				'nilai_tukar' => post('nilai_tukar',true),
				'diskon' => post('diskon',true),
				'biaya_lainnya' => post('biaya_lainnya',true),
				'total_pembelian' => post('total_pembelian',true),
				'no_so_supplier' => post('no_so_supplier',true),
				'tanggal_so_supplier' => post('tanggal_so_supplier',true),
				'lampiran_so' => post('lampiran_so',true),
				'term_pembayaran' => post('term_pembayaran',true),
				'catatan' => post('catatan',true),
				'status' => post('status',true),
		    );

            $this->purchase_order_model->insert($data);

            success('Create Record Success');
            redirect(base_url('purchase-order'));
        }
    }

    public function update($id)
    {
        $row = $this->purchase_order_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('purchase-order/update-action'),
				'purchase_order_id' => set_value('purchase_order_id', $row->purchase_order_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'supplier_id' => set_value('supplier_id', $row->supplier_id),
				'no_purchase_order' => set_value('no_purchase_order', $row->no_purchase_order),
				'permintaan_tanggal_kirim' => set_value('permintaan_tanggal_kirim', $row->permintaan_tanggal_kirim),
				'nama_penerima' => set_value('nama_penerima', $row->nama_penerima),
				'alamat_penerima' => set_value('alamat_penerima', $row->alamat_penerima),
				'telp_penerima' => set_value('telp_penerima', $row->telp_penerima),
				'fax_penerima' => set_value('fax_penerima', $row->fax_penerima),
				'kurs' => set_value('kurs', $row->kurs),
				'nilai_tukar' => set_value('nilai_tukar', $row->nilai_tukar),
				'diskon' => set_value('diskon', $row->diskon),
				'biaya_lainnya' => set_value('biaya_lainnya', $row->biaya_lainnya),
				'total_pembelian' => set_value('total_pembelian', $row->total_pembelian),
				'no_so_supplier' => set_value('no_so_supplier', $row->no_so_supplier),
				'tanggal_so_supplier' => set_value('tanggal_so_supplier', $row->tanggal_so_supplier),
				'lampiran_so' => set_value('lampiran_so', $row->lampiran_so),
				'term_pembayaran' => set_value('term_pembayaran', $row->term_pembayaran),
				'catatan' => set_value('catatan', $row->catatan),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'purchase-order/purchase_order_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('purchase-order'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('purchase-order/update/'.post('purchase_order_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'supplier_id' => post('supplier_id',true),
				'no_purchase_order' => post('no_purchase_order',true),
				'permintaan_tanggal_kirim' => post('permintaan_tanggal_kirim',true),
				'nama_penerima' => post('nama_penerima',true),
				'alamat_penerima' => post('alamat_penerima',true),
				'telp_penerima' => post('telp_penerima',true),
				'fax_penerima' => post('fax_penerima',true),
				'kurs' => post('kurs',true),
				'nilai_tukar' => post('nilai_tukar',true),
				'diskon' => post('diskon',true),
				'biaya_lainnya' => post('biaya_lainnya',true),
				'total_pembelian' => post('total_pembelian',true),
				'no_so_supplier' => post('no_so_supplier',true),
				'tanggal_so_supplier' => post('tanggal_so_supplier',true),
				'lampiran_so' => post('lampiran_so',true),
				'term_pembayaran' => post('term_pembayaran',true),
				'catatan' => post('catatan',true),
				'status' => post('status',true),
		    );

            $this->purchase_order_model->update($data, post('purchase_order_id', true));

            success('Update Record Success');
            redirect(base_url('purchase-order/update/'.post('purchase_order_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->purchase_order_model->get($id);

        if ($row) {
            $this->purchase_order_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('purchase-order'));
        } else {

            warning('Record Not Found');
            redirect(base_url('purchase-order'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('supplier_id', 'supplier', 'trim|required|numeric');
		$this->form_validation->set_rules('no_purchase_order', 'no purchase order', 'trim|required');
		$this->form_validation->set_rules('permintaan_tanggal_kirim', 'permintaan tanggal kirim', 'trim|required');
		$this->form_validation->set_rules('nama_penerima', 'nama penerima', 'trim|required');
		$this->form_validation->set_rules('alamat_penerima', 'alamat penerima', 'trim|required');
		$this->form_validation->set_rules('telp_penerima', 'telp penerima', 'trim|required');
		$this->form_validation->set_rules('fax_penerima', 'fax penerima', 'trim|required');
		$this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
		$this->form_validation->set_rules('nilai_tukar', 'nilai tukar', 'trim|required|numeric');
		$this->form_validation->set_rules('diskon', 'diskon', 'trim|required|numeric');
		$this->form_validation->set_rules('biaya_lainnya', 'biaya lainnya', 'trim|required|numeric');
		$this->form_validation->set_rules('total_pembelian', 'total pembelian', 'trim|required|numeric');
		$this->form_validation->set_rules('no_so_supplier', 'no so supplier', 'trim|required');
		$this->form_validation->set_rules('tanggal_so_supplier', 'tanggal so supplier', 'trim|required');
		$this->form_validation->set_rules('lampiran_so', 'lampiran so', 'trim|required');
		$this->form_validation->set_rules('term_pembayaran', 'term pembayaran', 'trim|required');
		$this->form_validation->set_rules('catatan', 'catatan', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('purchase_order_id', 'purchase_order_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file purchase_order.php */
/* Location: ./application/controllers/purchase_order.php */
/* Please DO NOT modify this information : */