<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sales_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sales_order_model');
    }

    public function index()
    {
        $data['page']  = 'sales-order/sales_order_list';
        $data['title'] = 'List Sales Order';
        view('template', $data);
    }

    public function read($id)
    {
        $row = $this->sales_order_model->get($id);
        if ($row) {
            $data = array(
                'perusahaan_id'             => $row->perusahaan_id,
                'pelanggan_id'              => $row->pelanggan_id,
                'skema_harga_id'            => $row->skema_harga_id,
                'no_sales_order'            => $row->no_sales_order,
                'permintaan_awal_kirim'     => $row->permintaan_awal_kirim,
                'permintaan_akhir_kirim'    => $row->permintaan_akhir_kirim,
                'jumlah_pengiriman'         => $row->jumlah_pengiriman,
                'default_jumlah_pengiriman' => $row->default_jumlah_pengiriman,
                'nama_penerima'             => $row->nama_penerima,
                'alamat_penerima'           => $row->alamat_penerima,
                'telp_penerima'             => $row->telp_penerima,
                'fax_penerima'              => $row->fax_penerima,
                'no_po_pelanggan'           => $row->no_po_pelanggan,
                'tanggal_po_pelanggan'      => $row->tanggal_po_pelanggan,
                'lampiran_po'               => $row->lampiran_po,
                'kurs'                      => $row->kurs,
                'total_penjualan'           => $row->total_penjualan,
                'total_ppn'                 => $row->total_ppn,
                'catatan_penjualan'         => $row->catatan_penjualan,
                'status'                    => $row->status,
                'alasan_batal'              => $row->alasan_batal,
                '__active'                  => $row->__active,
                '__created'                 => $row->__created,
                '__updated'                 => $row->__updated,
                '__username'                => $row->__username,
            );
            $data['page'] = 'sales-order/sales_order_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sales-order'));
        }
    }

    public function create_from_inq($inquiry_no)
    {
        model('inquiry_model');
        model('inquiry_produk_model');

        $data_inq = $this->inquiry_model->get($inquiry_no);
		$order_detail = $this->inquiry_produk_model->with('jenis_barang')->where('inquiry_id',$data_inq->inquiry_id)->get();
		
		$order_detail->jenis_barang->harga_satuan = $order_detail->jenis_barang->harga;
		$order_detail->jenis_barang->qty = 1;

		//add qty
		$data_inq->qty = $order_detail->qty;

        $data = array(
            'inquiry'                   => $data_inq,
            'order_detail'              => [$order_detail->jenis_barang],
            'button'                    => 'Create',
            'action'                    => base_url('sales-order/create-action'),
            'perusahaan_id'             => set_value('perusahaan_id'),
            'pelanggan_id'              => set_value('pelanggan_id'),
            'skema_harga_id'            => set_value('skema_harga_id'),
            'no_sales_order'            => set_value('no_sales_order'),
            'permintaan_awal_kirim'     => set_value('permintaan_awal_kirim'),
            'permintaan_akhir_kirim'    => set_value('permintaan_akhir_kirim'),
            'jumlah_pengiriman'         => set_value('jumlah_pengiriman'),
            'default_jumlah_pengiriman' => set_value('default_jumlah_pengiriman'),
            'nama_penerima'             => set_value('nama_penerima'),
            'alamat_penerima'           => set_value('alamat_penerima'),
            'telp_penerima'             => set_value('telp_penerima'),
            'fax_penerima'              => set_value('fax_penerima'),
            'no_po_pelanggan'           => set_value('no_po_pelanggan'),
            'tanggal_po_pelanggan'      => set_value('tanggal_po_pelanggan'),
            'lampiran_po'               => set_value('lampiran_po'),
            'kurs'                      => set_value('kurs'),
            'total_penjualan'           => set_value('total_penjualan'),
            'total_ppn'                 => set_value('total_ppn'),
            'catatan_penjualan'         => set_value('catatan_penjualan'),
            'status'                    => set_value('status'),
            'alasan_batal'              => set_value('alasan_batal'),
        );
        $data['page'] = 'sales-order/sales_order_form';
        view('template', $data);

    }

    public function create()
    {
        $data = array(
            'button'                    => 'Create',
            'action'                    => base_url('sales-order/create-action'),
            'perusahaan_id'             => set_value('perusahaan_id'),
            'pelanggan_id'              => set_value('pelanggan_id'),
            'skema_harga_id'            => set_value('skema_harga_id'),
            'no_sales_order'            => set_value('no_sales_order'),
            'permintaan_awal_kirim'     => set_value('permintaan_awal_kirim'),
            'permintaan_akhir_kirim'    => set_value('permintaan_akhir_kirim'),
            'jumlah_pengiriman'         => set_value('jumlah_pengiriman'),
            'default_jumlah_pengiriman' => set_value('default_jumlah_pengiriman'),
            'nama_penerima'             => set_value('nama_penerima'),
            'alamat_penerima'           => set_value('alamat_penerima'),
            'telp_penerima'             => set_value('telp_penerima'),
            'fax_penerima'              => set_value('fax_penerima'),
            'no_po_pelanggan'           => set_value('no_po_pelanggan'),
            'tanggal_po_pelanggan'      => set_value('tanggal_po_pelanggan'),
            'lampiran_po'               => set_value('lampiran_po'),
            'kurs'                      => set_value('kurs'),
            'total_penjualan'           => set_value('total_penjualan'),
            'total_ppn'                 => set_value('total_ppn'),
            'catatan_penjualan'         => set_value('catatan_penjualan'),
            'status'                    => set_value('status'),
            'alasan_batal'              => set_value('alasan_batal'),
        );
        $data['page'] = 'sales-order/sales_order_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sales-order/create'));
        } else {
            $data = array(
                'perusahaan_id'             => post('perusahaan_id', true),
                'pelanggan_id'              => post('pelanggan_id', true),
                'skema_harga_id'            => post('skema_harga_id', true),
                'no_sales_order'            => post('no_sales_order', true),
                'permintaan_awal_kirim'     => post('permintaan_awal_kirim', true),
                'permintaan_akhir_kirim'    => post('permintaan_akhir_kirim', true),
                'jumlah_pengiriman'         => post('jumlah_pengiriman', true),
                'default_jumlah_pengiriman' => post('default_jumlah_pengiriman', true),
                'nama_penerima'             => post('nama_penerima', true),
                'alamat_penerima'           => post('alamat_penerima', true),
                'telp_penerima'             => post('telp_penerima', true),
                'fax_penerima'              => post('fax_penerima', true),
                'no_po_pelanggan'           => post('no_po_pelanggan', true),
                'tanggal_po_pelanggan'      => post('tanggal_po_pelanggan', true),
                'lampiran_po'               => post('lampiran_po', true),
                'kurs'                      => post('kurs', true),
                'total_penjualan'           => post('total_penjualan', true),
                'total_ppn'                 => post('total_ppn', true),
                'catatan_penjualan'         => post('catatan_penjualan', true),
                'status'                    => post('status', true),
                'alasan_batal'              => post('alasan_batal', true),
            );
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->sales_order_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sales-order'));
        }
    }

    public function update($id)
    {
        $row = $this->sales_order_model->get($id);

        if ($row) {
            $data = array(
                'button'                    => 'Update',
                'action'                    => base_url('sales-order/update-action'),
                'sales_order_id'            => set_value('sales_order_id', $row->sales_order_id),
                'perusahaan_id'             => set_value('perusahaan_id', $row->perusahaan_id),
                'pelanggan_id'              => set_value('pelanggan_id', $row->pelanggan_id),
                'skema_harga_id'            => set_value('skema_harga_id', $row->skema_harga_id),
                'no_sales_order'            => set_value('no_sales_order', $row->no_sales_order),
                'permintaan_awal_kirim'     => set_value('permintaan_awal_kirim', $row->permintaan_awal_kirim),
                'permintaan_akhir_kirim'    => set_value('permintaan_akhir_kirim', $row->permintaan_akhir_kirim),
                'jumlah_pengiriman'         => set_value('jumlah_pengiriman', $row->jumlah_pengiriman),
                'default_jumlah_pengiriman' => set_value('default_jumlah_pengiriman', $row->default_jumlah_pengiriman),
                'nama_penerima'             => set_value('nama_penerima', $row->nama_penerima),
                'alamat_penerima'           => set_value('alamat_penerima', $row->alamat_penerima),
                'telp_penerima'             => set_value('telp_penerima', $row->telp_penerima),
                'fax_penerima'              => set_value('fax_penerima', $row->fax_penerima),
                'no_po_pelanggan'           => set_value('no_po_pelanggan', $row->no_po_pelanggan),
                'tanggal_po_pelanggan'      => set_value('tanggal_po_pelanggan', $row->tanggal_po_pelanggan),
                'lampiran_po'               => set_value('lampiran_po', $row->lampiran_po),
                'kurs'                      => set_value('kurs', $row->kurs),
                'total_penjualan'           => set_value('total_penjualan', $row->total_penjualan),
                'total_ppn'                 => set_value('total_ppn', $row->total_ppn),
                'catatan_penjualan'         => set_value('catatan_penjualan', $row->catatan_penjualan),
                'status'                    => set_value('status', $row->status),
                'alasan_batal'              => set_value('alasan_batal', $row->alasan_batal),
            );

            $data['page'] = 'sales-order/sales_order_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sales-order'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sales-order/update/' . post('sales_order_id', true)));
        } else {
            $data = array(
                'perusahaan_id'             => post('perusahaan_id', true),
                'pelanggan_id'              => post('pelanggan_id', true),
                'skema_harga_id'            => post('skema_harga_id', true),
                'no_sales_order'            => post('no_sales_order', true),
                'permintaan_awal_kirim'     => post('permintaan_awal_kirim', true),
                'permintaan_akhir_kirim'    => post('permintaan_akhir_kirim', true),
                'jumlah_pengiriman'         => post('jumlah_pengiriman', true),
                'default_jumlah_pengiriman' => post('default_jumlah_pengiriman', true),
                'nama_penerima'             => post('nama_penerima', true),
                'alamat_penerima'           => post('alamat_penerima', true),
                'telp_penerima'             => post('telp_penerima', true),
                'fax_penerima'              => post('fax_penerima', true),
                'no_po_pelanggan'           => post('no_po_pelanggan', true),
                'tanggal_po_pelanggan'      => post('tanggal_po_pelanggan', true),
                'lampiran_po'               => post('lampiran_po', true),
                'kurs'                      => post('kurs', true),
                'total_penjualan'           => post('total_penjualan', true),
                'total_ppn'                 => post('total_ppn', true),
                'catatan_penjualan'         => post('catatan_penjualan', true),
                'status'                    => post('status', true),
                'alasan_batal'              => post('alasan_batal', true),
            );

            $this->sales_order_model->update($data, post('sales_order_id', true));

            success('Update Record Success');
            redirect(base_url('sales-order/update/' . post('sales_order_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sales_order_model->get($id);

        if ($row) {
            $this->sales_order_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sales-order'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sales-order'));
        }
    }

    public function _rules()
    {
        // $this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
        // $this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
        // $this->form_validation->set_rules('skema_harga_id', 'skema harga', 'trim|required|numeric');
        // $this->form_validation->set_rules('no_sales_order', 'no sales order', 'trim|required');
        // $this->form_validation->set_rules('permintaan_awal_kirim', 'permintaan awal kirim', 'trim|required');
        // $this->form_validation->set_rules('permintaan_akhir_kirim', 'permintaan akhir kirim', 'trim|required');
        // $this->form_validation->set_rules('jumlah_pengiriman', 'jumlah pengiriman', 'trim|required|numeric');
        // $this->form_validation->set_rules('default_jumlah_pengiriman', 'default jumlah pengiriman', 'trim|required|numeric');
        // $this->form_validation->set_rules('nama_penerima', 'nama penerima', 'trim|required');
        // $this->form_validation->set_rules('alamat_penerima', 'alamat penerima', 'trim|required');
        // $this->form_validation->set_rules('telp_penerima', 'telp penerima', 'trim|required');
        // $this->form_validation->set_rules('fax_penerima', 'fax penerima', 'trim|required');
        // $this->form_validation->set_rules('no_po_pelanggan', 'no po pelanggan', 'trim|required');
        // $this->form_validation->set_rules('tanggal_po_pelanggan', 'tanggal po pelanggan', 'trim|required');
        // $this->form_validation->set_rules('lampiran_po', 'lampiran po', 'trim|required');
        // $this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
        // $this->form_validation->set_rules('total_penjualan', 'total penjualan', 'trim|required|numeric');
        // $this->form_validation->set_rules('total_ppn', 'total ppn', 'trim|required|numeric');
        // $this->form_validation->set_rules('catatan_penjualan', 'catatan penjualan', 'trim|required');
        // $this->form_validation->set_rules('status', 'status', 'trim|required');
        // $this->form_validation->set_rules('alasan_batal', 'alasan batal', 'trim|required');

        // $this->form_validation->set_rules('sales_order_id', 'sales_order_id', 'trim');
        // $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    /**
     * Ajax Base
     */

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->sales_order_model->json();
    }

}

/* End of file sales_order.php */
/* Location: ./application/controllers/sales_order.php */
/* Please DO NOT modify this information : */
