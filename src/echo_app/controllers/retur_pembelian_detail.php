<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class retur_pembelian_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('retur_pembelian_detail_model');
    }

    public function index()
    {
        $data['page'] = 'retur-pembelian-detail/retur_pembelian_detail_list';
        $data['title'] = 'List Retur Pembelian Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->retur_pembelian_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->retur_pembelian_detail_model->get($id);
        if ($row) {
            $data = array(
				'retur_pembelian_id' => $row->retur_pembelian_id,
				'penerimaan_barang_detail_id' => $row->penerimaan_barang_detail_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'retur-pembelian-detail/retur_pembelian_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('retur-pembelian-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('retur-pembelian-detail/create-action'),
		    'retur_pembelian_id' => set_value('retur_pembelian_id'),
		    'penerimaan_barang_detail_id' => set_value('penerimaan_barang_detail_id'),
		);
        $data['page'] = 'retur-pembelian-detail/retur_pembelian_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-pembelian-detail/create'));
        } else {
            $data = array(
				'retur_pembelian_id' => post('retur_pembelian_id',true),
				'penerimaan_barang_detail_id' => post('penerimaan_barang_detail_id',true),
		    );

            $this->retur_pembelian_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('retur-pembelian-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->retur_pembelian_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('retur-pembelian-detail/update-action'),
				'retur_pembelian_detail_id' => set_value('retur_pembelian_detail_id', $row->retur_pembelian_detail_id),
				'retur_pembelian_id' => set_value('retur_pembelian_id', $row->retur_pembelian_id),
				'penerimaan_barang_detail_id' => set_value('penerimaan_barang_detail_id', $row->penerimaan_barang_detail_id),
			    );

            $data['page'] = 'retur-pembelian-detail/retur_pembelian_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('retur-pembelian-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-pembelian-detail/update/'.post('retur_pembelian_detail_id', true)));
        } else {
            $data = array(
				'retur_pembelian_id' => post('retur_pembelian_id',true),
				'penerimaan_barang_detail_id' => post('penerimaan_barang_detail_id',true),
		    );

            $this->retur_pembelian_detail_model->update($data, post('retur_pembelian_detail_id', true));

            success('Update Record Success');
            redirect(base_url('retur-pembelian-detail/update/'.post('retur_pembelian_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->retur_pembelian_detail_model->get($id);

        if ($row) {
            $this->retur_pembelian_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('retur-pembelian-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('retur-pembelian-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('retur_pembelian_id', 'retur pembelian', 'trim|required|numeric');
		$this->form_validation->set_rules('penerimaan_barang_detail_id', 'penerimaan barang detail', 'trim|required|numeric');

		$this->form_validation->set_rules('retur_pembelian_detail_id', 'retur_pembelian_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file retur_pembelian_detail.php */
/* Location: ./application/controllers/retur_pembelian_detail.php */
/* Please DO NOT modify this information : */