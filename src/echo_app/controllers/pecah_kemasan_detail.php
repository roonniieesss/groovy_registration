<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pecah_kemasan_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pecah_kemasan_detail_model');
    }

    public function index()
    {
        $data['page'] = 'pecah-kemasan-detail/pecah_kemasan_detail_list';
        $data['title'] = 'List Pecah Kemasan Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pecah_kemasan_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->pecah_kemasan_detail_model->get($id);
        if ($row) {
            $data = array(
				'pecah_kemasan_id' => $row->pecah_kemasan_id,
				'transaksi_barang_id' => $row->transaksi_barang_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pecah-kemasan-detail/pecah_kemasan_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pecah-kemasan-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pecah-kemasan-detail/create-action'),
		    'pecah_kemasan_id' => set_value('pecah_kemasan_id'),
		    'transaksi_barang_id' => set_value('transaksi_barang_id'),
		);
        $data['page'] = 'pecah-kemasan-detail/pecah_kemasan_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pecah-kemasan-detail/create'));
        } else {
            $data = array(
				'pecah_kemasan_id' => post('pecah_kemasan_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
		    );

            $this->pecah_kemasan_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pecah-kemasan-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->pecah_kemasan_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pecah-kemasan-detail/update-action'),
				'pecah_kemasan_detail_id' => set_value('pecah_kemasan_detail_id', $row->pecah_kemasan_detail_id),
				'pecah_kemasan_id' => set_value('pecah_kemasan_id', $row->pecah_kemasan_id),
				'transaksi_barang_id' => set_value('transaksi_barang_id', $row->transaksi_barang_id),
			    );

            $data['page'] = 'pecah-kemasan-detail/pecah_kemasan_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pecah-kemasan-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pecah-kemasan-detail/update/'.post('pecah_kemasan_detail_id', true)));
        } else {
            $data = array(
				'pecah_kemasan_id' => post('pecah_kemasan_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
		    );

            $this->pecah_kemasan_detail_model->update($data, post('pecah_kemasan_detail_id', true));

            success('Update Record Success');
            redirect(base_url('pecah-kemasan-detail/update/'.post('pecah_kemasan_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pecah_kemasan_detail_model->get($id);

        if ($row) {
            $this->pecah_kemasan_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pecah-kemasan-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pecah-kemasan-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pecah_kemasan_id', 'pecah kemasan', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_id', 'transaksi barang', 'trim|required|numeric');

		$this->form_validation->set_rules('pecah_kemasan_detail_id', 'pecah_kemasan_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pecah_kemasan_detail.php */
/* Location: ./application/controllers/pecah_kemasan_detail.php */
/* Please DO NOT modify this information : */