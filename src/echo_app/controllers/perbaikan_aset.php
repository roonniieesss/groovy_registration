<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class perbaikan_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('perbaikan_aset_model');
    }

    public function index()
    {
        $data['page'] = 'perbaikan-aset/perbaikan_aset_list';
        $data['title'] = 'List Perbaikan Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->perbaikan_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->perbaikan_aset_model->get($id);
        if ($row) {
            $data = array(
				'aset_id' => $row->aset_id,
				'aset_id_pengganti' => $row->aset_id_pengganti,
				'penempatan_aset_keluar' => $row->penempatan_aset_keluar,
				'penempatan_aset_masuk' => $row->penempatan_aset_masuk,
				'pelaksana_perbaikan' => $row->pelaksana_perbaikan,
				'kontak_pelaksana_perbaikan' => $row->kontak_pelaksana_perbaikan,
				'tanggal_perbaikan' => $row->tanggal_perbaikan,
				'tanggal_selesai' => $row->tanggal_selesai,
				'alasan_perbaikan' => $row->alasan_perbaikan,
				'catatan_hasil_perbaikan' => $row->catatan_hasil_perbaikan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'perbaikan-aset/perbaikan_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('perbaikan-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('perbaikan-aset/create-action'),
		    'aset_id' => set_value('aset_id'),
		    'aset_id_pengganti' => set_value('aset_id_pengganti'),
		    'penempatan_aset_keluar' => set_value('penempatan_aset_keluar'),
		    'penempatan_aset_masuk' => set_value('penempatan_aset_masuk'),
		    'pelaksana_perbaikan' => set_value('pelaksana_perbaikan'),
		    'kontak_pelaksana_perbaikan' => set_value('kontak_pelaksana_perbaikan'),
		    'tanggal_perbaikan' => set_value('tanggal_perbaikan'),
		    'tanggal_selesai' => set_value('tanggal_selesai'),
		    'alasan_perbaikan' => set_value('alasan_perbaikan'),
		    'catatan_hasil_perbaikan' => set_value('catatan_hasil_perbaikan'),
		);
        $data['page'] = 'perbaikan-aset/perbaikan_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('perbaikan-aset/create'));
        } else {
            $data = array(
				'aset_id' => post('aset_id',true),
				'aset_id_pengganti' => post('aset_id_pengganti',true),
				'penempatan_aset_keluar' => post('penempatan_aset_keluar',true),
				'penempatan_aset_masuk' => post('penempatan_aset_masuk',true),
				'pelaksana_perbaikan' => post('pelaksana_perbaikan',true),
				'kontak_pelaksana_perbaikan' => post('kontak_pelaksana_perbaikan',true),
				'tanggal_perbaikan' => post('tanggal_perbaikan',true),
				'tanggal_selesai' => post('tanggal_selesai',true),
				'alasan_perbaikan' => post('alasan_perbaikan',true),
				'catatan_hasil_perbaikan' => post('catatan_hasil_perbaikan',true),
		    );

            $this->perbaikan_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('perbaikan-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->perbaikan_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('perbaikan-aset/update-action'),
				'perbaikan_aset_id' => set_value('perbaikan_aset_id', $row->perbaikan_aset_id),
				'aset_id' => set_value('aset_id', $row->aset_id),
				'aset_id_pengganti' => set_value('aset_id_pengganti', $row->aset_id_pengganti),
				'penempatan_aset_keluar' => set_value('penempatan_aset_keluar', $row->penempatan_aset_keluar),
				'penempatan_aset_masuk' => set_value('penempatan_aset_masuk', $row->penempatan_aset_masuk),
				'pelaksana_perbaikan' => set_value('pelaksana_perbaikan', $row->pelaksana_perbaikan),
				'kontak_pelaksana_perbaikan' => set_value('kontak_pelaksana_perbaikan', $row->kontak_pelaksana_perbaikan),
				'tanggal_perbaikan' => set_value('tanggal_perbaikan', $row->tanggal_perbaikan),
				'tanggal_selesai' => set_value('tanggal_selesai', $row->tanggal_selesai),
				'alasan_perbaikan' => set_value('alasan_perbaikan', $row->alasan_perbaikan),
				'catatan_hasil_perbaikan' => set_value('catatan_hasil_perbaikan', $row->catatan_hasil_perbaikan),
			    );

            $data['page'] = 'perbaikan-aset/perbaikan_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('perbaikan-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('perbaikan-aset/update/'.post('perbaikan_aset_id', true)));
        } else {
            $data = array(
				'aset_id' => post('aset_id',true),
				'aset_id_pengganti' => post('aset_id_pengganti',true),
				'penempatan_aset_keluar' => post('penempatan_aset_keluar',true),
				'penempatan_aset_masuk' => post('penempatan_aset_masuk',true),
				'pelaksana_perbaikan' => post('pelaksana_perbaikan',true),
				'kontak_pelaksana_perbaikan' => post('kontak_pelaksana_perbaikan',true),
				'tanggal_perbaikan' => post('tanggal_perbaikan',true),
				'tanggal_selesai' => post('tanggal_selesai',true),
				'alasan_perbaikan' => post('alasan_perbaikan',true),
				'catatan_hasil_perbaikan' => post('catatan_hasil_perbaikan',true),
		    );

            $this->perbaikan_aset_model->update($data, post('perbaikan_aset_id', true));

            success('Update Record Success');
            redirect(base_url('perbaikan-aset/update/'.post('perbaikan_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->perbaikan_aset_model->get($id);

        if ($row) {
            $this->perbaikan_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('perbaikan-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('perbaikan-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('aset_id', 'aset', 'trim|required|numeric');
		$this->form_validation->set_rules('aset_id_pengganti', 'aset  pengganti', 'trim|required|numeric');
		$this->form_validation->set_rules('penempatan_aset_keluar', 'penempatan aset keluar', 'trim|required|numeric');
		$this->form_validation->set_rules('penempatan_aset_masuk', 'penempatan aset masuk', 'trim|required|numeric');
		$this->form_validation->set_rules('pelaksana_perbaikan', 'pelaksana perbaikan', 'trim|required');
		$this->form_validation->set_rules('kontak_pelaksana_perbaikan', 'kontak pelaksana perbaikan', 'trim|required');
		$this->form_validation->set_rules('tanggal_perbaikan', 'tanggal perbaikan', 'trim|required');
		$this->form_validation->set_rules('tanggal_selesai', 'tanggal selesai', 'trim|required');
		$this->form_validation->set_rules('alasan_perbaikan', 'alasan perbaikan', 'trim|required');
		$this->form_validation->set_rules('catatan_hasil_perbaikan', 'catatan hasil perbaikan', 'trim|required');

		$this->form_validation->set_rules('perbaikan_aset_id', 'perbaikan_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file perbaikan_aset.php */
/* Location: ./application/controllers/perbaikan_aset.php */
/* Please DO NOT modify this information : */