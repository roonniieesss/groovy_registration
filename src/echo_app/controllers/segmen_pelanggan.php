<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class segmen_pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('segmen_pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'segmen-pelanggan/segmen_pelanggan_list';
        $data['title'] = 'List Segmen Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->segmen_pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->segmen_pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'pelanggan_id' => $row->pelanggan_id,
				'jenis_segmen_pelanggan_id' => $row->jenis_segmen_pelanggan_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'segmen-pelanggan/segmen_pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('segmen-pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('segmen-pelanggan/create-action'),
		    'pelanggan_id' => set_value('pelanggan_id'),
		    'jenis_segmen_pelanggan_id' => set_value('jenis_segmen_pelanggan_id'),
		);
        $data['page'] = 'segmen-pelanggan/segmen_pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('segmen-pelanggan/create'));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'jenis_segmen_pelanggan_id' => post('jenis_segmen_pelanggan_id',true),
		    );

            $this->segmen_pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('segmen-pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->segmen_pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('segmen-pelanggan/update-action'),
				'segmen_pelanggan_id' => set_value('segmen_pelanggan_id', $row->segmen_pelanggan_id),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'jenis_segmen_pelanggan_id' => set_value('jenis_segmen_pelanggan_id', $row->jenis_segmen_pelanggan_id),
			    );

            $data['page'] = 'segmen-pelanggan/segmen_pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('segmen-pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('segmen-pelanggan/update/'.post('segmen_pelanggan_id', true)));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'jenis_segmen_pelanggan_id' => post('jenis_segmen_pelanggan_id',true),
		    );

            $this->segmen_pelanggan_model->update($data, post('segmen_pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('segmen-pelanggan/update/'.post('segmen_pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->segmen_pelanggan_model->get($id);

        if ($row) {
            $this->segmen_pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('segmen-pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('segmen-pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_segmen_pelanggan_id', 'jenis segmen pelanggan', 'trim|required|numeric');

		$this->form_validation->set_rules('segmen_pelanggan_id', 'segmen_pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file segmen_pelanggan.php */
/* Location: ./application/controllers/segmen_pelanggan.php */
/* Please DO NOT modify this information : */