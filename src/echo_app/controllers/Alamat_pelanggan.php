<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class alamat_pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('alamat_pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'alamat-pelanggan/alamat_pelanggan_list';
        $data['title'] = 'List Alamat Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->alamat_pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->alamat_pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'pelanggan_id' => $row->pelanggan_id,
				'negara_id' => $row->negara_id,
				'kota_id' => $row->kota_id,
				'label' => $row->label,
				'alamat' => $row->alamat,
				'kode_pos' => $row->kode_pos,
				'nama_penerima' => $row->nama_penerima,
				'alamat_utama' => $row->alamat_utama,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'alamat-pelanggan/alamat_pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('alamat-pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('alamat-pelanggan/create-action'),
		    'pelanggan_id' => set_value('pelanggan_id'),
		    'negara_id' => set_value('negara_id'),
		    'kota_id' => set_value('kota_id'),
		    'label' => set_value('label'),
		    'alamat' => set_value('alamat'),
		    'kode_pos' => set_value('kode_pos'),
		    'nama_penerima' => set_value('nama_penerima'),
		    'alamat_utama' => set_value('alamat_utama'),
		);
        $data['page'] = 'alamat-pelanggan/alamat_pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('alamat-pelanggan/create'));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'negara_id' => post('negara_id',true),
				'kota_id' => post('kota_id',true),
				'label' => post('label',true),
				'alamat' => post('alamat',true),
				'kode_pos' => post('kode_pos',true),
				'nama_penerima' => post('nama_penerima',true),
				'alamat_utama' => post('alamat_utama',true),
		    );

            $this->alamat_pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('alamat-pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->alamat_pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('alamat-pelanggan/update-action'),
				'alamat_pelanggan_id' => set_value('alamat_pelanggan_id', $row->alamat_pelanggan_id),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'negara_id' => set_value('negara_id', $row->negara_id),
				'kota_id' => set_value('kota_id', $row->kota_id),
				'label' => set_value('label', $row->label),
				'alamat' => set_value('alamat', $row->alamat),
				'kode_pos' => set_value('kode_pos', $row->kode_pos),
				'nama_penerima' => set_value('nama_penerima', $row->nama_penerima),
				'alamat_utama' => set_value('alamat_utama', $row->alamat_utama),
			    );

            $data['page'] = 'alamat-pelanggan/alamat_pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('alamat-pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('alamat-pelanggan/update/'.post('alamat_pelanggan_id', true)));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'negara_id' => post('negara_id',true),
				'kota_id' => post('kota_id',true),
				'label' => post('label',true),
				'alamat' => post('alamat',true),
				'kode_pos' => post('kode_pos',true),
				'nama_penerima' => post('nama_penerima',true),
				'alamat_utama' => post('alamat_utama',true),
		    );

            $this->alamat_pelanggan_model->update($data, post('alamat_pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('alamat-pelanggan/update/'.post('alamat_pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->alamat_pelanggan_model->get($id);

        if ($row) {
            $this->alamat_pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('alamat-pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('alamat-pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('negara_id', 'negara', 'trim|required|numeric');
		$this->form_validation->set_rules('kota_id', 'kota', 'trim|required|numeric');
		$this->form_validation->set_rules('label', 'label', 'trim|required');
		$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
		$this->form_validation->set_rules('kode_pos', 'kode pos', 'trim|required');
		$this->form_validation->set_rules('nama_penerima', 'nama penerima', 'trim|required');
		$this->form_validation->set_rules('alamat_utama', 'alamat utama', 'trim|required');

		$this->form_validation->set_rules('alamat_pelanggan_id', 'alamat_pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file alamat_pelanggan.php */
/* Location: ./application/controllers/alamat_pelanggan.php */
/* Please DO NOT modify this information : */