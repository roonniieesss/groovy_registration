<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peraturan_exim_attachment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peraturan_exim_attachment_model');
    }

    public function index()
    {
        $data['page'] = 'peraturan-exim-attachment/peraturan_exim_attachment_list';
        $data['title'] = 'List Peraturan Exim Attachment';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peraturan_exim_attachment_model->json();
    }

    public function read($id)
    {
        $row = $this->peraturan_exim_attachment_model->get($id);
        if ($row) {
            $data = array(
				'peraturan_exim_id' => $row->peraturan_exim_id,
				'media_type' => $row->media_type,
				'media_filename' => $row->media_filename,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peraturan-exim-attachment/peraturan_exim_attachment_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-attachment'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peraturan-exim-attachment/create-action'),
		    'peraturan_exim_id' => set_value('peraturan_exim_id'),
		    'media_type' => set_value('media_type'),
		    'media_filename' => set_value('media_filename'),
		);
        $data['page'] = 'peraturan-exim-attachment/peraturan_exim_attachment_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-attachment/create'));
        } else {
            $data = array(
				'peraturan_exim_id' => post('peraturan_exim_id',true),
				'media_type' => post('media_type',true),
				'media_filename' => post('media_filename',true),
		    );

            $this->peraturan_exim_attachment_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peraturan-exim-attachment'));
        }
    }

    public function update($id)
    {
        $row = $this->peraturan_exim_attachment_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peraturan-exim-attachment/update-action'),
				'peraturan_exim_attachment_id' => set_value('peraturan_exim_attachment_id', $row->peraturan_exim_attachment_id),
				'peraturan_exim_id' => set_value('peraturan_exim_id', $row->peraturan_exim_id),
				'media_type' => set_value('media_type', $row->media_type),
				'media_filename' => set_value('media_filename', $row->media_filename),
			    );

            $data['page'] = 'peraturan-exim-attachment/peraturan_exim_attachment_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-attachment'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-attachment/update/'.post('peraturan_exim_attachment_id', true)));
        } else {
            $data = array(
				'peraturan_exim_id' => post('peraturan_exim_id',true),
				'media_type' => post('media_type',true),
				'media_filename' => post('media_filename',true),
		    );

            $this->peraturan_exim_attachment_model->update($data, post('peraturan_exim_attachment_id', true));

            success('Update Record Success');
            redirect(base_url('peraturan-exim-attachment/update/'.post('peraturan_exim_attachment_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peraturan_exim_attachment_model->get($id);

        if ($row) {
            $this->peraturan_exim_attachment_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peraturan-exim-attachment'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peraturan-exim-attachment'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('peraturan_exim_id', 'peraturan exim', 'trim|required|numeric');
		$this->form_validation->set_rules('media_type', 'media type', 'trim|required');
		$this->form_validation->set_rules('media_filename', 'media filename', 'trim|required');

		$this->form_validation->set_rules('peraturan_exim_attachment_id', 'peraturan_exim_attachment_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peraturan_exim_attachment.php */
/* Location: ./application/controllers/peraturan_exim_attachment.php */
/* Please DO NOT modify this information : */