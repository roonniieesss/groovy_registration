<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class dashboard extends CI_Controller
{	
	function index(){		
		$data['page']  = 'dashboard/index.php';
        $data['title'] = 'Dashboard';
        // dd($data);
        view('template', $data);
	}
}