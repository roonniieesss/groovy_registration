<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class acl_gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('acl_gudang_model');
    }

    public function index()
    {
        $data['page'] = 'acl-gudang/acl_gudang_list';
        $data['title'] = 'List Acl Gudang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->acl_gudang_model->json();
    }

    public function read($id)
    {
        $row = $this->acl_gudang_model->get($id);
        if ($row) {
            $data = array(
				'users_id' => $row->users_id,
				'gudang_id' => $row->gudang_id,
				'kadaluarsa' => $row->kadaluarsa,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'acl-gudang/acl_gudang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('acl-gudang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('acl-gudang/create-action'),
		    'users_id' => set_value('users_id'),
		    'gudang_id' => set_value('gudang_id'),
		    'kadaluarsa' => set_value('kadaluarsa'),
		);
        $data['page'] = 'acl-gudang/acl_gudang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('acl-gudang/create'));
        } else {
            $data = array(
				'users_id' => post('users_id',true),
				'gudang_id' => post('gudang_id',true),
				'kadaluarsa' => post('kadaluarsa',true),
		    );

            $this->acl_gudang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('acl-gudang'));
        }
    }

    public function update($id)
    {
        $row = $this->acl_gudang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('acl-gudang/update-action'),
				'acl_gudang_id' => set_value('acl_gudang_id', $row->acl_gudang_id),
				'users_id' => set_value('users_id', $row->users_id),
				'gudang_id' => set_value('gudang_id', $row->gudang_id),
				'kadaluarsa' => set_value('kadaluarsa', $row->kadaluarsa),
			    );

            $data['page'] = 'acl-gudang/acl_gudang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('acl-gudang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('acl-gudang/update/'.post('acl_gudang_id', true)));
        } else {
            $data = array(
				'users_id' => post('users_id',true),
				'gudang_id' => post('gudang_id',true),
				'kadaluarsa' => post('kadaluarsa',true),
		    );

            $this->acl_gudang_model->update($data, post('acl_gudang_id', true));

            success('Update Record Success');
            redirect(base_url('acl-gudang/update/'.post('acl_gudang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->acl_gudang_model->get($id);

        if ($row) {
            $this->acl_gudang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('acl-gudang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('acl-gudang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('users_id', 'users', 'trim|required|numeric');
		$this->form_validation->set_rules('gudang_id', 'gudang', 'trim|required|numeric');
		$this->form_validation->set_rules('kadaluarsa', 'kadaluarsa', 'trim|required');

		$this->form_validation->set_rules('acl_gudang_id', 'acl_gudang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file acl_gudang.php */
/* Location: ./application/controllers/acl_gudang.php */
/* Please DO NOT modify this information : */