<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_aset_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-aset/jenis_aset_list';
        $data['title'] = 'List Jenis Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_aset_model->get($id);
        if ($row) {
            $data = array(
				'kategori_aset_id' => $row->kategori_aset_id,
				'merek_id' => $row->merek_id,
				'nama_jenis_aset' => $row->nama_jenis_aset,
				'spesifikasi' => $row->spesifikasi,
				'jenis_identifikasi' => $row->jenis_identifikasi,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-aset/jenis_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-aset/create-action'),
		    'kategori_aset_id' => set_value('kategori_aset_id'),
		    'merek_id' => set_value('merek_id'),
		    'nama_jenis_aset' => set_value('nama_jenis_aset'),
		    'spesifikasi' => set_value('spesifikasi'),
		    'jenis_identifikasi' => set_value('jenis_identifikasi'),
		);
        $data['page'] = 'jenis-aset/jenis_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-aset/create'));
        } else {
            $data = array(
				'kategori_aset_id' => post('kategori_aset_id',true),
				'merek_id' => post('merek_id',true),
				'nama_jenis_aset' => post('nama_jenis_aset',true),
				'spesifikasi' => post('spesifikasi',true),
				'jenis_identifikasi' => post('jenis_identifikasi',true),
		    );

            $this->jenis_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-aset/update-action'),
				'jenis_aset_id' => set_value('jenis_aset_id', $row->jenis_aset_id),
				'kategori_aset_id' => set_value('kategori_aset_id', $row->kategori_aset_id),
				'merek_id' => set_value('merek_id', $row->merek_id),
				'nama_jenis_aset' => set_value('nama_jenis_aset', $row->nama_jenis_aset),
				'spesifikasi' => set_value('spesifikasi', $row->spesifikasi),
				'jenis_identifikasi' => set_value('jenis_identifikasi', $row->jenis_identifikasi),
			    );

            $data['page'] = 'jenis-aset/jenis_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-aset/update/'.post('jenis_aset_id', true)));
        } else {
            $data = array(
				'kategori_aset_id' => post('kategori_aset_id',true),
				'merek_id' => post('merek_id',true),
				'nama_jenis_aset' => post('nama_jenis_aset',true),
				'spesifikasi' => post('spesifikasi',true),
				'jenis_identifikasi' => post('jenis_identifikasi',true),
		    );

            $this->jenis_aset_model->update($data, post('jenis_aset_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-aset/update/'.post('jenis_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_aset_model->get($id);

        if ($row) {
            $this->jenis_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('kategori_aset_id', 'kategori aset', 'trim|required|numeric');
		$this->form_validation->set_rules('merek_id', 'merek', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_jenis_aset', 'nama jenis aset', 'trim|required');
		$this->form_validation->set_rules('spesifikasi', 'spesifikasi', 'trim|required');
		$this->form_validation->set_rules('jenis_identifikasi', 'jenis entifikasi', 'trim|required');

		$this->form_validation->set_rules('jenis_aset_id', 'jenis_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_aset.php */
/* Location: ./application/controllers/jenis_aset.php */
/* Please DO NOT modify this information : */