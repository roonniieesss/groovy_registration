<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class delivery_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('delivery_order_model');
    }

    public function index()
    {
        $data['page'] = 'delivery-order/delivery_order_list';
        $data['title'] = 'List Delivery Order';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->delivery_order_model->json();
    }

    public function read($id)
    {
        $row = $this->delivery_order_model->get($id);
        if ($row) {
            $data = array(
				'gudang_id' => $row->gudang_id,
				'jadwal_pengiriman_id' => $row->jadwal_pengiriman_id,
				'no_surat_jalan' => $row->no_surat_jalan,
				'rencana_tanggal_kirim' => $row->rencana_tanggal_kirim,
				'tanggal_kirim' => $row->tanggal_kirim,
				'tanggal_diterima' => $row->tanggal_diterima,
				'nama_supir' => $row->nama_supir,
				'telp_supir' => $row->telp_supir,
				'nama_penerima' => $row->nama_penerima,
				'alamat_penerima' => $row->alamat_penerima,
				'telp_penerima' => $row->telp_penerima,
				'fax_penerima' => $row->fax_penerima,
				'metode_kirim' => $row->metode_kirim,
				'catatan' => $row->catatan,
				'catatan_gudang' => $row->catatan_gudang,
				'status' => $row->status,
				'keterangan_status' => $row->keterangan_status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'delivery-order/delivery_order_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('delivery-order'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('delivery-order/create-action'),
		    'gudang_id' => set_value('gudang_id'),
		    'jadwal_pengiriman_id' => set_value('jadwal_pengiriman_id'),
		    'no_surat_jalan' => set_value('no_surat_jalan'),
		    'rencana_tanggal_kirim' => set_value('rencana_tanggal_kirim'),
		    'tanggal_kirim' => set_value('tanggal_kirim'),
		    'tanggal_diterima' => set_value('tanggal_diterima'),
		    'nama_supir' => set_value('nama_supir'),
		    'telp_supir' => set_value('telp_supir'),
		    'nama_penerima' => set_value('nama_penerima'),
		    'alamat_penerima' => set_value('alamat_penerima'),
		    'telp_penerima' => set_value('telp_penerima'),
		    'fax_penerima' => set_value('fax_penerima'),
		    'metode_kirim' => set_value('metode_kirim'),
		    'catatan' => set_value('catatan'),
		    'catatan_gudang' => set_value('catatan_gudang'),
		    'status' => set_value('status'),
		    'keterangan_status' => set_value('keterangan_status'),
		);
        $data['page'] = 'delivery-order/delivery_order_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('delivery-order/create'));
        } else {
            $data = array(
				'gudang_id' => post('gudang_id',true),
				'jadwal_pengiriman_id' => post('jadwal_pengiriman_id',true),
				'no_surat_jalan' => post('no_surat_jalan',true),
				'rencana_tanggal_kirim' => post('rencana_tanggal_kirim',true),
				'tanggal_kirim' => post('tanggal_kirim',true),
				'tanggal_diterima' => post('tanggal_diterima',true),
				'nama_supir' => post('nama_supir',true),
				'telp_supir' => post('telp_supir',true),
				'nama_penerima' => post('nama_penerima',true),
				'alamat_penerima' => post('alamat_penerima',true),
				'telp_penerima' => post('telp_penerima',true),
				'fax_penerima' => post('fax_penerima',true),
				'metode_kirim' => post('metode_kirim',true),
				'catatan' => post('catatan',true),
				'catatan_gudang' => post('catatan_gudang',true),
				'status' => post('status',true),
				'keterangan_status' => post('keterangan_status',true),
		    );

            $this->delivery_order_model->insert($data);

            success('Create Record Success');
            redirect(base_url('delivery-order'));
        }
    }

    public function update($id)
    {
        $row = $this->delivery_order_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('delivery-order/update-action'),
				'delivery_order_id' => set_value('delivery_order_id', $row->delivery_order_id),
				'gudang_id' => set_value('gudang_id', $row->gudang_id),
				'jadwal_pengiriman_id' => set_value('jadwal_pengiriman_id', $row->jadwal_pengiriman_id),
				'no_surat_jalan' => set_value('no_surat_jalan', $row->no_surat_jalan),
				'rencana_tanggal_kirim' => set_value('rencana_tanggal_kirim', $row->rencana_tanggal_kirim),
				'tanggal_kirim' => set_value('tanggal_kirim', $row->tanggal_kirim),
				'tanggal_diterima' => set_value('tanggal_diterima', $row->tanggal_diterima),
				'nama_supir' => set_value('nama_supir', $row->nama_supir),
				'telp_supir' => set_value('telp_supir', $row->telp_supir),
				'nama_penerima' => set_value('nama_penerima', $row->nama_penerima),
				'alamat_penerima' => set_value('alamat_penerima', $row->alamat_penerima),
				'telp_penerima' => set_value('telp_penerima', $row->telp_penerima),
				'fax_penerima' => set_value('fax_penerima', $row->fax_penerima),
				'metode_kirim' => set_value('metode_kirim', $row->metode_kirim),
				'catatan' => set_value('catatan', $row->catatan),
				'catatan_gudang' => set_value('catatan_gudang', $row->catatan_gudang),
				'status' => set_value('status', $row->status),
				'keterangan_status' => set_value('keterangan_status', $row->keterangan_status),
			    );

            $data['page'] = 'delivery-order/delivery_order_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('delivery-order'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('delivery-order/update/'.post('delivery_order_id', true)));
        } else {
            $data = array(
				'gudang_id' => post('gudang_id',true),
				'jadwal_pengiriman_id' => post('jadwal_pengiriman_id',true),
				'no_surat_jalan' => post('no_surat_jalan',true),
				'rencana_tanggal_kirim' => post('rencana_tanggal_kirim',true),
				'tanggal_kirim' => post('tanggal_kirim',true),
				'tanggal_diterima' => post('tanggal_diterima',true),
				'nama_supir' => post('nama_supir',true),
				'telp_supir' => post('telp_supir',true),
				'nama_penerima' => post('nama_penerima',true),
				'alamat_penerima' => post('alamat_penerima',true),
				'telp_penerima' => post('telp_penerima',true),
				'fax_penerima' => post('fax_penerima',true),
				'metode_kirim' => post('metode_kirim',true),
				'catatan' => post('catatan',true),
				'catatan_gudang' => post('catatan_gudang',true),
				'status' => post('status',true),
				'keterangan_status' => post('keterangan_status',true),
		    );

            $this->delivery_order_model->update($data, post('delivery_order_id', true));

            success('Update Record Success');
            redirect(base_url('delivery-order/update/'.post('delivery_order_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->delivery_order_model->get($id);

        if ($row) {
            $this->delivery_order_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('delivery-order'));
        } else {

            warning('Record Not Found');
            redirect(base_url('delivery-order'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('gudang_id', 'gudang', 'trim|required|numeric');
		$this->form_validation->set_rules('jadwal_pengiriman_id', 'jadwal pengiriman', 'trim|required|numeric');
		$this->form_validation->set_rules('no_surat_jalan', 'no surat jalan', 'trim|required');
		$this->form_validation->set_rules('rencana_tanggal_kirim', 'rencana tanggal kirim', 'trim|required');
		$this->form_validation->set_rules('tanggal_kirim', 'tanggal kirim', 'trim|required');
		$this->form_validation->set_rules('tanggal_diterima', 'tanggal diterima', 'trim|required');
		$this->form_validation->set_rules('nama_supir', 'nama supir', 'trim|required');
		$this->form_validation->set_rules('telp_supir', 'telp supir', 'trim|required');
		$this->form_validation->set_rules('nama_penerima', 'nama penerima', 'trim|required');
		$this->form_validation->set_rules('alamat_penerima', 'alamat penerima', 'trim|required');
		$this->form_validation->set_rules('telp_penerima', 'telp penerima', 'trim|required');
		$this->form_validation->set_rules('fax_penerima', 'fax penerima', 'trim|required');
		$this->form_validation->set_rules('metode_kirim', 'metode kirim', 'trim|required');
		$this->form_validation->set_rules('catatan', 'catatan', 'trim|required');
		$this->form_validation->set_rules('catatan_gudang', 'catatan gudang', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('keterangan_status', 'keterangan status', 'trim|required');

		$this->form_validation->set_rules('delivery_order_id', 'delivery_order_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file delivery_order.php */
/* Location: ./application/controllers/delivery_order.php */
/* Please DO NOT modify this information : */