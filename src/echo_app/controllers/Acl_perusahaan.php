<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class acl_perusahaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('acl_perusahaan_model');
    }

    public function index()
    {
        $data['page'] = 'acl-perusahaan/acl_perusahaan_list';
        $data['title'] = 'List Acl Perusahaan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->acl_perusahaan_model->json();
    }

    public function read($id)
    {
        $row = $this->acl_perusahaan_model->get($id);
        if ($row) {
            $data = array(
				'users_id' => $row->users_id,
				'perusahaan_id' => $row->perusahaan_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'acl-perusahaan/acl_perusahaan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('acl-perusahaan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('acl-perusahaan/create-action'),
		    'users_id' => set_value('users_id'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		);
        $data['page'] = 'acl-perusahaan/acl_perusahaan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('acl-perusahaan/create'));
        } else {
            $data = array(
				'users_id' => post('users_id',true),
				'perusahaan_id' => post('perusahaan_id',true),
		    );

            $this->acl_perusahaan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('acl-perusahaan'));
        }
    }

    public function update($id)
    {
        $row = $this->acl_perusahaan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('acl-perusahaan/update-action'),
				'acl_perusahaan_id' => set_value('acl_perusahaan_id', $row->acl_perusahaan_id),
				'users_id' => set_value('users_id', $row->users_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
			    );

            $data['page'] = 'acl-perusahaan/acl_perusahaan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('acl-perusahaan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('acl-perusahaan/update/'.post('acl_perusahaan_id', true)));
        } else {
            $data = array(
				'users_id' => post('users_id',true),
				'perusahaan_id' => post('perusahaan_id',true),
		    );

            $this->acl_perusahaan_model->update($data, post('acl_perusahaan_id', true));

            success('Update Record Success');
            redirect(base_url('acl-perusahaan/update/'.post('acl_perusahaan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->acl_perusahaan_model->get($id);

        if ($row) {
            $this->acl_perusahaan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('acl-perusahaan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('acl-perusahaan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('users_id', 'users', 'trim|required|numeric');
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');

		$this->form_validation->set_rules('acl_perusahaan_id', 'acl_perusahaan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file acl_perusahaan.php */
/* Location: ./application/controllers/acl_perusahaan.php */
/* Please DO NOT modify this information : */