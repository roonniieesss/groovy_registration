<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class satuan_barang_transfer_stock extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('satuan_barang_transfer_stock_model');
    }

    public function index()
    {
        $data['page'] = 'satuan-barang-transfer-stock/satuan_barang_transfer_stock_list';
        $data['title'] = 'List Satuan Barang Transfer Stock';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->satuan_barang_transfer_stock_model->json();
    }

    public function read($id)
    {
        $row = $this->satuan_barang_transfer_stock_model->get($id);
        if ($row) {
            $data = array(
				'transfer_stock_detail_id' => $row->transfer_stock_detail_id,
				'transaksi_barang_keluar' => $row->transaksi_barang_keluar,
				'transaksi_barang_masuk' => $row->transaksi_barang_masuk,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'satuan-barang-transfer-stock/satuan_barang_transfer_stock_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('satuan-barang-transfer-stock'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('satuan-barang-transfer-stock/create-action'),
		    'transfer_stock_detail_id' => set_value('transfer_stock_detail_id'),
		    'transaksi_barang_keluar' => set_value('transaksi_barang_keluar'),
		    'transaksi_barang_masuk' => set_value('transaksi_barang_masuk'),
		);
        $data['page'] = 'satuan-barang-transfer-stock/satuan_barang_transfer_stock_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('satuan-barang-transfer-stock/create'));
        } else {
            $data = array(
				'transfer_stock_detail_id' => post('transfer_stock_detail_id',true),
				'transaksi_barang_keluar' => post('transaksi_barang_keluar',true),
				'transaksi_barang_masuk' => post('transaksi_barang_masuk',true),
		    );

            $this->satuan_barang_transfer_stock_model->insert($data);

            success('Create Record Success');
            redirect(base_url('satuan-barang-transfer-stock'));
        }
    }

    public function update($id)
    {
        $row = $this->satuan_barang_transfer_stock_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('satuan-barang-transfer-stock/update-action'),
				'satuan_barang_transfer_stock_id' => set_value('satuan_barang_transfer_stock_id', $row->satuan_barang_transfer_stock_id),
				'transfer_stock_detail_id' => set_value('transfer_stock_detail_id', $row->transfer_stock_detail_id),
				'transaksi_barang_keluar' => set_value('transaksi_barang_keluar', $row->transaksi_barang_keluar),
				'transaksi_barang_masuk' => set_value('transaksi_barang_masuk', $row->transaksi_barang_masuk),
			    );

            $data['page'] = 'satuan-barang-transfer-stock/satuan_barang_transfer_stock_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('satuan-barang-transfer-stock'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('satuan-barang-transfer-stock/update/'.post('satuan_barang_transfer_stock_id', true)));
        } else {
            $data = array(
				'transfer_stock_detail_id' => post('transfer_stock_detail_id',true),
				'transaksi_barang_keluar' => post('transaksi_barang_keluar',true),
				'transaksi_barang_masuk' => post('transaksi_barang_masuk',true),
		    );

            $this->satuan_barang_transfer_stock_model->update($data, post('satuan_barang_transfer_stock_id', true));

            success('Update Record Success');
            redirect(base_url('satuan-barang-transfer-stock/update/'.post('satuan_barang_transfer_stock_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->satuan_barang_transfer_stock_model->get($id);

        if ($row) {
            $this->satuan_barang_transfer_stock_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('satuan-barang-transfer-stock'));
        } else {

            warning('Record Not Found');
            redirect(base_url('satuan-barang-transfer-stock'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transfer_stock_detail_id', 'transfer stock detail', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_keluar', 'transaksi barang keluar', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_masuk', 'transaksi barang masuk', 'trim|required|numeric');

		$this->form_validation->set_rules('satuan_barang_transfer_stock_id', 'satuan_barang_transfer_stock_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file satuan_barang_transfer_stock.php */
/* Location: ./application/controllers/satuan_barang_transfer_stock.php */
/* Please DO NOT modify this information : */