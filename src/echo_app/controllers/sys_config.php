<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sys_config extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sys_config_model');
    }

    public function index()
    {
        $data['page'] = 'sys-config/sys_config_list';
        $data['title'] = 'List Sys Config';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->sys_config_model->json();
    }

    public function read($id)
    {
        $row = $this->sys_config_model->get($id);
        if ($row) {
            $data = array(
				'description' => $row->description,
				'default_value' => $row->default_value,
		    );
            $data['page'] = 'sys-config/sys_config_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sys-config'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('sys-config/create-action'),
		    'description' => set_value('description'),
		    'default_value' => set_value('default_value'),
		);
        $data['page'] = 'sys-config/sys_config_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sys-config/create'));
        } else {
            $data = array(
				'description' => post('description',true),
				'default_value' => post('default_value',true),
		    );

            $this->sys_config_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sys-config'));
        }
    }

    public function update($id)
    {
        $row = $this->sys_config_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('sys-config/update-action'),
				'group' => set_value('group', $row->group),
				'description' => set_value('description', $row->description),
				'default_value' => set_value('default_value', $row->default_value),
			    );

            $data['page'] = 'sys-config/sys_config_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sys-config'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sys-config/update/'.post('group', true)));
        } else {
            $data = array(
				'description' => post('description',true),
				'default_value' => post('default_value',true),
		    );

            $this->sys_config_model->update($data, post('group', true));

            success('Update Record Success');
            redirect(base_url('sys-config/update/'.post('group', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sys_config_model->get($id);

        if ($row) {
            $this->sys_config_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sys-config'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sys-config'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('description', 'description', 'trim|required');
		$this->form_validation->set_rules('default_value', 'default value', 'trim|required');

		$this->form_validation->set_rules('group', 'group', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file sys_config.php */
/* Location: ./application/controllers/sys_config.php */
/* Please DO NOT modify this information : */