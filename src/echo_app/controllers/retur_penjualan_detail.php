<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class retur_penjualan_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('retur_penjualan_detail_model');
    }

    public function index()
    {
        $data['page'] = 'retur-penjualan-detail/retur_penjualan_detail_list';
        $data['title'] = 'List Retur Penjualan Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->retur_penjualan_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->retur_penjualan_detail_model->get($id);
        if ($row) {
            $data = array(
				'retur_penjualan_id' => $row->retur_penjualan_id,
				'satuan_barang_delivery_order_id' => $row->satuan_barang_delivery_order_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'retur-penjualan-detail/retur_penjualan_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('retur-penjualan-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('retur-penjualan-detail/create-action'),
		    'retur_penjualan_id' => set_value('retur_penjualan_id'),
		    'satuan_barang_delivery_order_id' => set_value('satuan_barang_delivery_order_id'),
		);
        $data['page'] = 'retur-penjualan-detail/retur_penjualan_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-penjualan-detail/create'));
        } else {
            $data = array(
				'retur_penjualan_id' => post('retur_penjualan_id',true),
				'satuan_barang_delivery_order_id' => post('satuan_barang_delivery_order_id',true),
		    );

            $this->retur_penjualan_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('retur-penjualan-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->retur_penjualan_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('retur-penjualan-detail/update-action'),
				'retur_penjualan_detail_id' => set_value('retur_penjualan_detail_id', $row->retur_penjualan_detail_id),
				'retur_penjualan_id' => set_value('retur_penjualan_id', $row->retur_penjualan_id),
				'satuan_barang_delivery_order_id' => set_value('satuan_barang_delivery_order_id', $row->satuan_barang_delivery_order_id),
			    );

            $data['page'] = 'retur-penjualan-detail/retur_penjualan_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('retur-penjualan-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-penjualan-detail/update/'.post('retur_penjualan_detail_id', true)));
        } else {
            $data = array(
				'retur_penjualan_id' => post('retur_penjualan_id',true),
				'satuan_barang_delivery_order_id' => post('satuan_barang_delivery_order_id',true),
		    );

            $this->retur_penjualan_detail_model->update($data, post('retur_penjualan_detail_id', true));

            success('Update Record Success');
            redirect(base_url('retur-penjualan-detail/update/'.post('retur_penjualan_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->retur_penjualan_detail_model->get($id);

        if ($row) {
            $this->retur_penjualan_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('retur-penjualan-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('retur-penjualan-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('retur_penjualan_id', 'retur penjualan', 'trim|required|numeric');
		$this->form_validation->set_rules('satuan_barang_delivery_order_id', 'satuan barang delivery order', 'trim|required|numeric');

		$this->form_validation->set_rules('retur_penjualan_detail_id', 'retur_penjualan_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file retur_penjualan_detail.php */
/* Location: ./application/controllers/retur_penjualan_detail.php */
/* Please DO NOT modify this information : */