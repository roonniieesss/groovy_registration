<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pengadaan_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pengadaan_aset_model');
    }

    public function index()
    {
        $data['page'] = 'pengadaan-aset/pengadaan_aset_list';
        $data['title'] = 'List Pengadaan Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pengadaan_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->pengadaan_aset_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'ruangan_id' => $row->ruangan_id,
				'divisi_pengusul' => $row->divisi_pengusul,
				'kontak_pengusul' => $row->kontak_pengusul,
				'rencana_tanggal_penempatan' => $row->rencana_tanggal_penempatan,
				'keperluan' => $row->keperluan,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pengadaan-aset/pengadaan_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pengadaan-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pengadaan-aset/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'ruangan_id' => set_value('ruangan_id'),
		    'divisi_pengusul' => set_value('divisi_pengusul'),
		    'kontak_pengusul' => set_value('kontak_pengusul'),
		    'rencana_tanggal_penempatan' => set_value('rencana_tanggal_penempatan'),
		    'keperluan' => set_value('keperluan'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'pengadaan-aset/pengadaan_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pengadaan-aset/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'ruangan_id' => post('ruangan_id',true),
				'divisi_pengusul' => post('divisi_pengusul',true),
				'kontak_pengusul' => post('kontak_pengusul',true),
				'rencana_tanggal_penempatan' => post('rencana_tanggal_penempatan',true),
				'keperluan' => post('keperluan',true),
				'status' => post('status',true),
		    );

            $this->pengadaan_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pengadaan-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->pengadaan_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pengadaan-aset/update-action'),
				'pengadaan_aset_id' => set_value('pengadaan_aset_id', $row->pengadaan_aset_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'ruangan_id' => set_value('ruangan_id', $row->ruangan_id),
				'divisi_pengusul' => set_value('divisi_pengusul', $row->divisi_pengusul),
				'kontak_pengusul' => set_value('kontak_pengusul', $row->kontak_pengusul),
				'rencana_tanggal_penempatan' => set_value('rencana_tanggal_penempatan', $row->rencana_tanggal_penempatan),
				'keperluan' => set_value('keperluan', $row->keperluan),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'pengadaan-aset/pengadaan_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pengadaan-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pengadaan-aset/update/'.post('pengadaan_aset_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'ruangan_id' => post('ruangan_id',true),
				'divisi_pengusul' => post('divisi_pengusul',true),
				'kontak_pengusul' => post('kontak_pengusul',true),
				'rencana_tanggal_penempatan' => post('rencana_tanggal_penempatan',true),
				'keperluan' => post('keperluan',true),
				'status' => post('status',true),
		    );

            $this->pengadaan_aset_model->update($data, post('pengadaan_aset_id', true));

            success('Update Record Success');
            redirect(base_url('pengadaan-aset/update/'.post('pengadaan_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pengadaan_aset_model->get($id);

        if ($row) {
            $this->pengadaan_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pengadaan-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pengadaan-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('ruangan_id', 'ruangan', 'trim|required|numeric');
		$this->form_validation->set_rules('divisi_pengusul', 'divisi pengusul', 'trim|required');
		$this->form_validation->set_rules('kontak_pengusul', 'kontak pengusul', 'trim|required');
		$this->form_validation->set_rules('rencana_tanggal_penempatan', 'rencana tanggal penempatan', 'trim|required');
		$this->form_validation->set_rules('keperluan', 'keperluan', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('pengadaan_aset_id', 'pengadaan_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pengadaan_aset.php */
/* Location: ./application/controllers/pengadaan_aset.php */
/* Please DO NOT modify this information : */