<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class penempatan_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('penempatan_aset_model');
    }

    public function index()
    {
        $data['page'] = 'penempatan-aset/penempatan_aset_list';
        $data['title'] = 'List Penempatan Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->penempatan_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->penempatan_aset_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'aset_id' => $row->aset_id,
				'ruangan_id' => $row->ruangan_id,
				'jenis_dokumen_masuk' => $row->jenis_dokumen_masuk,
				'waktu_masuk' => $row->waktu_masuk,
				'jenis_dokumen_keluar' => $row->jenis_dokumen_keluar,
				'waktu_keluar' => $row->waktu_keluar,
				'longitude' => $row->longitude,
				'latitude' => $row->latitude,
				'label' => $row->label,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'penempatan-aset/penempatan_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('penempatan-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('penempatan-aset/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'aset_id' => set_value('aset_id'),
		    'ruangan_id' => set_value('ruangan_id'),
		    'jenis_dokumen_masuk' => set_value('jenis_dokumen_masuk'),
		    'waktu_masuk' => set_value('waktu_masuk'),
		    'jenis_dokumen_keluar' => set_value('jenis_dokumen_keluar'),
		    'waktu_keluar' => set_value('waktu_keluar'),
		    'longitude' => set_value('longitude'),
		    'latitude' => set_value('latitude'),
		    'label' => set_value('label'),
		);
        $data['page'] = 'penempatan-aset/penempatan_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penempatan-aset/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'aset_id' => post('aset_id',true),
				'ruangan_id' => post('ruangan_id',true),
				'jenis_dokumen_masuk' => post('jenis_dokumen_masuk',true),
				'waktu_masuk' => post('waktu_masuk',true),
				'jenis_dokumen_keluar' => post('jenis_dokumen_keluar',true),
				'waktu_keluar' => post('waktu_keluar',true),
				'longitude' => post('longitude',true),
				'latitude' => post('latitude',true),
				'label' => post('label',true),
		    );

            $this->penempatan_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('penempatan-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->penempatan_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('penempatan-aset/update-action'),
				'penempatan_aset_id' => set_value('penempatan_aset_id', $row->penempatan_aset_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'aset_id' => set_value('aset_id', $row->aset_id),
				'ruangan_id' => set_value('ruangan_id', $row->ruangan_id),
				'jenis_dokumen_masuk' => set_value('jenis_dokumen_masuk', $row->jenis_dokumen_masuk),
				'waktu_masuk' => set_value('waktu_masuk', $row->waktu_masuk),
				'jenis_dokumen_keluar' => set_value('jenis_dokumen_keluar', $row->jenis_dokumen_keluar),
				'waktu_keluar' => set_value('waktu_keluar', $row->waktu_keluar),
				'longitude' => set_value('longitude', $row->longitude),
				'latitude' => set_value('latitude', $row->latitude),
				'label' => set_value('label', $row->label),
			    );

            $data['page'] = 'penempatan-aset/penempatan_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('penempatan-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penempatan-aset/update/'.post('penempatan_aset_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'aset_id' => post('aset_id',true),
				'ruangan_id' => post('ruangan_id',true),
				'jenis_dokumen_masuk' => post('jenis_dokumen_masuk',true),
				'waktu_masuk' => post('waktu_masuk',true),
				'jenis_dokumen_keluar' => post('jenis_dokumen_keluar',true),
				'waktu_keluar' => post('waktu_keluar',true),
				'longitude' => post('longitude',true),
				'latitude' => post('latitude',true),
				'label' => post('label',true),
		    );

            $this->penempatan_aset_model->update($data, post('penempatan_aset_id', true));

            success('Update Record Success');
            redirect(base_url('penempatan-aset/update/'.post('penempatan_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->penempatan_aset_model->get($id);

        if ($row) {
            $this->penempatan_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('penempatan-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('penempatan-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('aset_id', 'aset', 'trim|required|numeric');
		$this->form_validation->set_rules('ruangan_id', 'ruangan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_dokumen_masuk', 'jenis dokumen masuk', 'trim|required');
		$this->form_validation->set_rules('waktu_masuk', 'waktu masuk', 'trim|required');
		$this->form_validation->set_rules('jenis_dokumen_keluar', 'jenis dokumen keluar', 'trim|required');
		$this->form_validation->set_rules('waktu_keluar', 'waktu keluar', 'trim|required');
		$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
		$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
		$this->form_validation->set_rules('label', 'label', 'trim|required');

		$this->form_validation->set_rules('penempatan_aset_id', 'penempatan_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file penempatan_aset.php */
/* Location: ./application/controllers/penempatan_aset.php */
/* Please DO NOT modify this information : */