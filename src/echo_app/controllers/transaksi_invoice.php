<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class transaksi_invoice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('transaksi_invoice_model');
    }

    public function index()
    {
        $data['page'] = 'transaksi-invoice/transaksi_invoice_list';
        $data['title'] = 'List Transaksi Invoice';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->transaksi_invoice_model->json();
    }

    public function read($id)
    {
        $row = $this->transaksi_invoice_model->get($id);
        if ($row) {
            $data = array(
				'transaksi_keuangan_id' => $row->transaksi_keuangan_id,
				'invoice_id' => $row->invoice_id,
				'jenis' => $row->jenis,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'transaksi-invoice/transaksi_invoice_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('transaksi-invoice'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('transaksi-invoice/create-action'),
		    'transaksi_keuangan_id' => set_value('transaksi_keuangan_id'),
		    'invoice_id' => set_value('invoice_id'),
		    'jenis' => set_value('jenis'),
		);
        $data['page'] = 'transaksi-invoice/transaksi_invoice_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transaksi-invoice/create'));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'invoice_id' => post('invoice_id',true),
				'jenis' => post('jenis',true),
		    );

            $this->transaksi_invoice_model->insert($data);

            success('Create Record Success');
            redirect(base_url('transaksi-invoice'));
        }
    }

    public function update($id)
    {
        $row = $this->transaksi_invoice_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('transaksi-invoice/update-action'),
				'transaksi_invoice_id' => set_value('transaksi_invoice_id', $row->transaksi_invoice_id),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'invoice_id' => set_value('invoice_id', $row->invoice_id),
				'jenis' => set_value('jenis', $row->jenis),
			    );

            $data['page'] = 'transaksi-invoice/transaksi_invoice_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('transaksi-invoice'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transaksi-invoice/update/'.post('transaksi_invoice_id', true)));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'invoice_id' => post('invoice_id',true),
				'jenis' => post('jenis',true),
		    );

            $this->transaksi_invoice_model->update($data, post('transaksi_invoice_id', true));

            success('Update Record Success');
            redirect(base_url('transaksi-invoice/update/'.post('transaksi_invoice_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->transaksi_invoice_model->get($id);

        if ($row) {
            $this->transaksi_invoice_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('transaksi-invoice'));
        } else {

            warning('Record Not Found');
            redirect(base_url('transaksi-invoice'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi keuangan', 'trim|required|numeric');
		$this->form_validation->set_rules('invoice_id', 'invoice', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required');

		$this->form_validation->set_rules('transaksi_invoice_id', 'transaksi_invoice_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file transaksi_invoice.php */
/* Location: ./application/controllers/transaksi_invoice.php */
/* Please DO NOT modify this information : */