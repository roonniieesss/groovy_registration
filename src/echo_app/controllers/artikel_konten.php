<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class artikel_konten extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('artikel_konten_model');
    }

    public function index()
    {
        $data['page'] = 'artikel-konten/artikel_konten_list';
        $data['title'] = 'List Artikel Konten';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->artikel_konten_model->json();
    }

    public function read($id)
    {
        $row = $this->artikel_konten_model->get($id);
        if ($row) {
            $data = array(
				'artikel_id' => $row->artikel_id,
				'kode_bahasa' => $row->kode_bahasa,
				'judul' => $row->judul,
				'isi' => $row->isi,
				'ringkasan' => $row->ringkasan,
				'tag' => $row->tag,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'artikel-konten/artikel_konten_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('artikel-konten'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('artikel-konten/create-action'),
		    'artikel_id' => set_value('artikel_id'),
		    'kode_bahasa' => set_value('kode_bahasa'),
		    'judul' => set_value('judul'),
		    'isi' => set_value('isi'),
		    'ringkasan' => set_value('ringkasan'),
		    'tag' => set_value('tag'),
		);
        $data['page'] = 'artikel-konten/artikel_konten_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('artikel-konten/create'));
        } else {
            $data = array(
				'artikel_id' => post('artikel_id',true),
				'kode_bahasa' => post('kode_bahasa',true),
				'judul' => post('judul',true),
				'isi' => post('isi',true),
				'ringkasan' => post('ringkasan',true),
				'tag' => post('tag',true),
		    );

            $this->artikel_konten_model->insert($data);

            success('Create Record Success');
            redirect(base_url('artikel-konten'));
        }
    }

    public function update($id)
    {
        $row = $this->artikel_konten_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('artikel-konten/update-action'),
				'artikel_konten_id' => set_value('artikel_konten_id', $row->artikel_konten_id),
				'artikel_id' => set_value('artikel_id', $row->artikel_id),
				'kode_bahasa' => set_value('kode_bahasa', $row->kode_bahasa),
				'judul' => set_value('judul', $row->judul),
				'isi' => set_value('isi', $row->isi),
				'ringkasan' => set_value('ringkasan', $row->ringkasan),
				'tag' => set_value('tag', $row->tag),
			    );

            $data['page'] = 'artikel-konten/artikel_konten_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('artikel-konten'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('artikel-konten/update/'.post('artikel_konten_id', true)));
        } else {
            $data = array(
				'artikel_id' => post('artikel_id',true),
				'kode_bahasa' => post('kode_bahasa',true),
				'judul' => post('judul',true),
				'isi' => post('isi',true),
				'ringkasan' => post('ringkasan',true),
				'tag' => post('tag',true),
		    );

            $this->artikel_konten_model->update($data, post('artikel_konten_id', true));

            success('Update Record Success');
            redirect(base_url('artikel-konten/update/'.post('artikel_konten_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->artikel_konten_model->get($id);

        if ($row) {
            $this->artikel_konten_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('artikel-konten'));
        } else {

            warning('Record Not Found');
            redirect(base_url('artikel-konten'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('artikel_id', 'artikel', 'trim|required|numeric');
		$this->form_validation->set_rules('kode_bahasa', 'kode bahasa', 'trim|required');
		$this->form_validation->set_rules('judul', 'judul', 'trim|required');
		$this->form_validation->set_rules('isi', 'isi', 'trim|required');
		$this->form_validation->set_rules('ringkasan', 'ringkasan', 'trim|required');
		$this->form_validation->set_rules('tag', 'tag', 'trim|required');

		$this->form_validation->set_rules('artikel_konten_id', 'artikel_konten_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file artikel_konten.php */
/* Location: ./application/controllers/artikel_konten.php */
/* Please DO NOT modify this information : */