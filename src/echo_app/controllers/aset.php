<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('aset_model');
    }

    public function index()
    {
        $data['page'] = 'aset/aset_list';
        $data['title'] = 'List Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->aset_model->json();
    }

    public function read($id)
    {
        $row = $this->aset_model->get($id);
        if ($row) {
            $data = array(
				'jenis_aset_id' => $row->jenis_aset_id,
				'pengadaan_aset_detail_id' => $row->pengadaan_aset_detail_id,
				'penerimaan_aset_id' => $row->penerimaan_aset_id,
				'no_aset' => $row->no_aset,
				'tanggal_perolehan' => $row->tanggal_perolehan,
				'harga_perolehan' => $row->harga_perolehan,
				'penyusutan_tahunan' => $row->penyusutan_tahunan,
				'serial_number' => $row->serial_number,
				'habis_garansi' => $row->habis_garansi,
				'aset_dipakai' => $row->aset_dipakai,
				'qty' => $row->qty,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'aset/aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('aset/create-action'),
		    'jenis_aset_id' => set_value('jenis_aset_id'),
		    'pengadaan_aset_detail_id' => set_value('pengadaan_aset_detail_id'),
		    'penerimaan_aset_id' => set_value('penerimaan_aset_id'),
		    'no_aset' => set_value('no_aset'),
		    'tanggal_perolehan' => set_value('tanggal_perolehan'),
		    'harga_perolehan' => set_value('harga_perolehan'),
		    'penyusutan_tahunan' => set_value('penyusutan_tahunan'),
		    'serial_number' => set_value('serial_number'),
		    'habis_garansi' => set_value('habis_garansi'),
		    'aset_dipakai' => set_value('aset_dipakai'),
		    'qty' => set_value('qty'),
		);
        $data['page'] = 'aset/aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('aset/create'));
        } else {
            $data = array(
				'jenis_aset_id' => post('jenis_aset_id',true),
				'pengadaan_aset_detail_id' => post('pengadaan_aset_detail_id',true),
				'penerimaan_aset_id' => post('penerimaan_aset_id',true),
				'no_aset' => post('no_aset',true),
				'tanggal_perolehan' => post('tanggal_perolehan',true),
				'harga_perolehan' => post('harga_perolehan',true),
				'penyusutan_tahunan' => post('penyusutan_tahunan',true),
				'serial_number' => post('serial_number',true),
				'habis_garansi' => post('habis_garansi',true),
				'aset_dipakai' => post('aset_dipakai',true),
				'qty' => post('qty',true),
		    );

            $this->aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('aset'));
        }
    }

    public function update($id)
    {
        $row = $this->aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('aset/update-action'),
				'aset_id' => set_value('aset_id', $row->aset_id),
				'jenis_aset_id' => set_value('jenis_aset_id', $row->jenis_aset_id),
				'pengadaan_aset_detail_id' => set_value('pengadaan_aset_detail_id', $row->pengadaan_aset_detail_id),
				'penerimaan_aset_id' => set_value('penerimaan_aset_id', $row->penerimaan_aset_id),
				'no_aset' => set_value('no_aset', $row->no_aset),
				'tanggal_perolehan' => set_value('tanggal_perolehan', $row->tanggal_perolehan),
				'harga_perolehan' => set_value('harga_perolehan', $row->harga_perolehan),
				'penyusutan_tahunan' => set_value('penyusutan_tahunan', $row->penyusutan_tahunan),
				'serial_number' => set_value('serial_number', $row->serial_number),
				'habis_garansi' => set_value('habis_garansi', $row->habis_garansi),
				'aset_dipakai' => set_value('aset_dipakai', $row->aset_dipakai),
				'qty' => set_value('qty', $row->qty),
			    );

            $data['page'] = 'aset/aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('aset/update/'.post('aset_id', true)));
        } else {
            $data = array(
				'jenis_aset_id' => post('jenis_aset_id',true),
				'pengadaan_aset_detail_id' => post('pengadaan_aset_detail_id',true),
				'penerimaan_aset_id' => post('penerimaan_aset_id',true),
				'no_aset' => post('no_aset',true),
				'tanggal_perolehan' => post('tanggal_perolehan',true),
				'harga_perolehan' => post('harga_perolehan',true),
				'penyusutan_tahunan' => post('penyusutan_tahunan',true),
				'serial_number' => post('serial_number',true),
				'habis_garansi' => post('habis_garansi',true),
				'aset_dipakai' => post('aset_dipakai',true),
				'qty' => post('qty',true),
		    );

            $this->aset_model->update($data, post('aset_id', true));

            success('Update Record Success');
            redirect(base_url('aset/update/'.post('aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->aset_model->get($id);

        if ($row) {
            $this->aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('jenis_aset_id', 'jenis aset', 'trim|required|numeric');
		$this->form_validation->set_rules('pengadaan_aset_detail_id', 'pengadaan aset detail', 'trim|required|numeric');
		$this->form_validation->set_rules('penerimaan_aset_id', 'penerimaan aset', 'trim|required|numeric');
		$this->form_validation->set_rules('no_aset', 'no aset', 'trim|required');
		$this->form_validation->set_rules('tanggal_perolehan', 'tanggal perolehan', 'trim|required');
		$this->form_validation->set_rules('harga_perolehan', 'harga perolehan', 'trim|required|numeric');
		$this->form_validation->set_rules('penyusutan_tahunan', 'penyusutan tahunan', 'trim|required|numeric');
		$this->form_validation->set_rules('serial_number', 'serial number', 'trim|required');
		$this->form_validation->set_rules('habis_garansi', 'habis garansi', 'trim|required');
		$this->form_validation->set_rules('aset_dipakai', 'aset dipakai', 'trim|required');
		$this->form_validation->set_rules('qty', 'qty', 'trim|required|numeric');

		$this->form_validation->set_rules('aset_id', 'aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file aset.php */
/* Location: ./application/controllers/aset.php */
/* Please DO NOT modify this information : */