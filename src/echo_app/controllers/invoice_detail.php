<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class invoice_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('invoice_detail_model');
    }

    public function index()
    {
        $data['page'] = 'invoice-detail/invoice_detail_list';
        $data['title'] = 'List Invoice Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->invoice_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->invoice_detail_model->get($id);
        if ($row) {
            $data = array(
				'sales_order_id' => $row->sales_order_id,
				'invoice_id' => $row->invoice_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'invoice-detail/invoice_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('invoice-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('invoice-detail/create-action'),
		    'sales_order_id' => set_value('sales_order_id'),
		    'invoice_id' => set_value('invoice_id'),
		);
        $data['page'] = 'invoice-detail/invoice_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('invoice-detail/create'));
        } else {
            $data = array(
				'sales_order_id' => post('sales_order_id',true),
				'invoice_id' => post('invoice_id',true),
		    );

            $this->invoice_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('invoice-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->invoice_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('invoice-detail/update-action'),
				'invoice_detail_id' => set_value('invoice_detail_id', $row->invoice_detail_id),
				'sales_order_id' => set_value('sales_order_id', $row->sales_order_id),
				'invoice_id' => set_value('invoice_id', $row->invoice_id),
			    );

            $data['page'] = 'invoice-detail/invoice_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('invoice-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('invoice-detail/update/'.post('invoice_detail_id', true)));
        } else {
            $data = array(
				'sales_order_id' => post('sales_order_id',true),
				'invoice_id' => post('invoice_id',true),
		    );

            $this->invoice_detail_model->update($data, post('invoice_detail_id', true));

            success('Update Record Success');
            redirect(base_url('invoice-detail/update/'.post('invoice_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->invoice_detail_model->get($id);

        if ($row) {
            $this->invoice_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('invoice-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('invoice-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('sales_order_id', 'sales order', 'trim|required|numeric');
		$this->form_validation->set_rules('invoice_id', 'invoice', 'trim|required|numeric');

		$this->form_validation->set_rules('invoice_detail_id', 'invoice_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file invoice_detail.php */
/* Location: ./application/controllers/invoice_detail.php */
/* Please DO NOT modify this information : */