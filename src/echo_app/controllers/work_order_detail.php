<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class work_order_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('work_order_detail_model');
    }

    public function index()
    {
        $data['page'] = 'work-order-detail/work_order_detail_list';
        $data['title'] = 'List Work Order Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->work_order_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->work_order_detail_model->get($id);
        if ($row) {
            $data = array(
				'work_order_id' => $row->work_order_id,
				'sales_order_item_id' => $row->sales_order_item_id,
				'alat_produksi_id' => $row->alat_produksi_id,
				'jenis_sample' => $row->jenis_sample,
				'waktu_ambil_sample' => $row->waktu_ambil_sample,
				'barang_id' => $row->barang_id,
				'status' => $row->status,
				'alasan_batal' => $row->alasan_batal,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'work-order-detail/work_order_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('work-order-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('work-order-detail/create-action'),
		    'work_order_id' => set_value('work_order_id'),
		    'sales_order_item_id' => set_value('sales_order_item_id'),
		    'alat_produksi_id' => set_value('alat_produksi_id'),
		    'jenis_sample' => set_value('jenis_sample'),
		    'waktu_ambil_sample' => set_value('waktu_ambil_sample'),
		    'barang_id' => set_value('barang_id'),
		    'status' => set_value('status'),
		    'alasan_batal' => set_value('alasan_batal'),
		);
        $data['page'] = 'work-order-detail/work_order_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('work-order-detail/create'));
        } else {
            $data = array(
				'work_order_id' => post('work_order_id',true),
				'sales_order_item_id' => post('sales_order_item_id',true),
				'alat_produksi_id' => post('alat_produksi_id',true),
				'jenis_sample' => post('jenis_sample',true),
				'waktu_ambil_sample' => post('waktu_ambil_sample',true),
				'barang_id' => post('barang_id',true),
				'status' => post('status',true),
				'alasan_batal' => post('alasan_batal',true),
		    );

            $this->work_order_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('work-order-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->work_order_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('work-order-detail/update-action'),
				'work_order_detail_id' => set_value('work_order_detail_id', $row->work_order_detail_id),
				'work_order_id' => set_value('work_order_id', $row->work_order_id),
				'sales_order_item_id' => set_value('sales_order_item_id', $row->sales_order_item_id),
				'alat_produksi_id' => set_value('alat_produksi_id', $row->alat_produksi_id),
				'jenis_sample' => set_value('jenis_sample', $row->jenis_sample),
				'waktu_ambil_sample' => set_value('waktu_ambil_sample', $row->waktu_ambil_sample),
				'barang_id' => set_value('barang_id', $row->barang_id),
				'status' => set_value('status', $row->status),
				'alasan_batal' => set_value('alasan_batal', $row->alasan_batal),
			    );

            $data['page'] = 'work-order-detail/work_order_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('work-order-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('work-order-detail/update/'.post('work_order_detail_id', true)));
        } else {
            $data = array(
				'work_order_id' => post('work_order_id',true),
				'sales_order_item_id' => post('sales_order_item_id',true),
				'alat_produksi_id' => post('alat_produksi_id',true),
				'jenis_sample' => post('jenis_sample',true),
				'waktu_ambil_sample' => post('waktu_ambil_sample',true),
				'barang_id' => post('barang_id',true),
				'status' => post('status',true),
				'alasan_batal' => post('alasan_batal',true),
		    );

            $this->work_order_detail_model->update($data, post('work_order_detail_id', true));

            success('Update Record Success');
            redirect(base_url('work-order-detail/update/'.post('work_order_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->work_order_detail_model->get($id);

        if ($row) {
            $this->work_order_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('work-order-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('work-order-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('work_order_id', 'work order', 'trim|required|numeric');
		$this->form_validation->set_rules('sales_order_item_id', 'sales order item', 'trim|required|numeric');
		$this->form_validation->set_rules('alat_produksi_id', 'alat produksi', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_sample', 'jenis sample', 'trim|required');
		$this->form_validation->set_rules('waktu_ambil_sample', 'waktu ambil sample', 'trim|required');
		$this->form_validation->set_rules('barang_id', 'barang', 'trim|required|numeric');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('alasan_batal', 'alasan batal', 'trim|required');

		$this->form_validation->set_rules('work_order_detail_id', 'work_order_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file work_order_detail.php */
/* Location: ./application/controllers/work_order_detail.php */
/* Please DO NOT modify this information : */