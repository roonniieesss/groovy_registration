<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pecah_kemasan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pecah_kemasan_model');
    }

    public function index()
    {
        $data['page'] = 'pecah-kemasan/pecah_kemasan_list';
        $data['title'] = 'List Pecah Kemasan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pecah_kemasan_model->json();
    }

    public function read($id)
    {
        $row = $this->pecah_kemasan_model->get($id);
        if ($row) {
            $data = array(
				'lokasi_bongkar' => $row->lokasi_bongkar,
				'keterangan' => $row->keterangan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pecah-kemasan/pecah_kemasan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pecah-kemasan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pecah-kemasan/create-action'),
		    'lokasi_bongkar' => set_value('lokasi_bongkar'),
		    'keterangan' => set_value('keterangan'),
		);
        $data['page'] = 'pecah-kemasan/pecah_kemasan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pecah-kemasan/create'));
        } else {
            $data = array(
				'lokasi_bongkar' => post('lokasi_bongkar',true),
				'keterangan' => post('keterangan',true),
		    );

            $this->pecah_kemasan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pecah-kemasan'));
        }
    }

    public function update($id)
    {
        $row = $this->pecah_kemasan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pecah-kemasan/update-action'),
				'pecah_kemasan_id' => set_value('pecah_kemasan_id', $row->pecah_kemasan_id),
				'lokasi_bongkar' => set_value('lokasi_bongkar', $row->lokasi_bongkar),
				'keterangan' => set_value('keterangan', $row->keterangan),
			    );

            $data['page'] = 'pecah-kemasan/pecah_kemasan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pecah-kemasan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pecah-kemasan/update/'.post('pecah_kemasan_id', true)));
        } else {
            $data = array(
				'lokasi_bongkar' => post('lokasi_bongkar',true),
				'keterangan' => post('keterangan',true),
		    );

            $this->pecah_kemasan_model->update($data, post('pecah_kemasan_id', true));

            success('Update Record Success');
            redirect(base_url('pecah-kemasan/update/'.post('pecah_kemasan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pecah_kemasan_model->get($id);

        if ($row) {
            $this->pecah_kemasan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pecah-kemasan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pecah-kemasan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('lokasi_bongkar', 'lokasi bongkar', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

		$this->form_validation->set_rules('pecah_kemasan_id', 'pecah_kemasan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pecah_kemasan.php */
/* Location: ./application/controllers/pecah_kemasan.php */
/* Please DO NOT modify this information : */