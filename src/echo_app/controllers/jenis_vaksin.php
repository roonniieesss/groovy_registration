<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_vaksin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_vaksin_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-vaksin/jenis_vaksin_list';
        $data['title'] = 'List Jenis Vaksin';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_vaksin_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_vaksin_model->get($id);
        if ($row) {
            $data = array(
				'nama_jenis_vaksin' => $row->nama_jenis_vaksin,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-vaksin/jenis_vaksin_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-vaksin'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-vaksin/create-action'),
		    'nama_jenis_vaksin' => set_value('nama_jenis_vaksin'),
		);
        $data['page'] = 'jenis-vaksin/jenis_vaksin_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-vaksin/create'));
        } else {
            $data = array(
				'nama_jenis_vaksin' => post('nama_jenis_vaksin',true),
		    );

            $this->jenis_vaksin_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-vaksin'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_vaksin_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-vaksin/update-action'),
				'jenis_vaksin_id' => set_value('jenis_vaksin_id', $row->jenis_vaksin_id),
				'nama_jenis_vaksin' => set_value('nama_jenis_vaksin', $row->nama_jenis_vaksin),
			    );

            $data['page'] = 'jenis-vaksin/jenis_vaksin_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-vaksin'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-vaksin/update/'.post('jenis_vaksin_id', true)));
        } else {
            $data = array(
				'nama_jenis_vaksin' => post('nama_jenis_vaksin',true),
		    );

            $this->jenis_vaksin_model->update($data, post('jenis_vaksin_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-vaksin/update/'.post('jenis_vaksin_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_vaksin_model->get($id);

        if ($row) {
            $this->jenis_vaksin_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-vaksin'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-vaksin'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_jenis_vaksin', 'nama jenis vaksin', 'trim|required');

		$this->form_validation->set_rules('jenis_vaksin_id', 'jenis_vaksin_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_vaksin.php */
/* Location: ./application/controllers/jenis_vaksin.php */
/* Please DO NOT modify this information : */