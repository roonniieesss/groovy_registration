<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pembatasan_jenis_barang_kupon extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pembatasan_jenis_barang_kupon_model');
    }

    public function index()
    {
        $data['page'] = 'pembatasan-jenis-barang-kupon/pembatasan_jenis_barang_kupon_list';
        $data['title'] = 'List Pembatasan Jenis Barang Kupon';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pembatasan_jenis_barang_kupon_model->json();
    }

    public function read($id)
    {
        $row = $this->pembatasan_jenis_barang_kupon_model->get($id);
        if ($row) {
            $data = array(
				'kupon_id' => $row->kupon_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pembatasan-jenis-barang-kupon/pembatasan_jenis_barang_kupon_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pembatasan-jenis-barang-kupon'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pembatasan-jenis-barang-kupon/create-action'),
		    'kupon_id' => set_value('kupon_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		);
        $data['page'] = 'pembatasan-jenis-barang-kupon/pembatasan_jenis_barang_kupon_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pembatasan-jenis-barang-kupon/create'));
        } else {
            $data = array(
				'kupon_id' => post('kupon_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
		    );

            $this->pembatasan_jenis_barang_kupon_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pembatasan-jenis-barang-kupon'));
        }
    }

    public function update($id)
    {
        $row = $this->pembatasan_jenis_barang_kupon_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pembatasan-jenis-barang-kupon/update-action'),
				'pembatasan_jenis_barang_kupon_id' => set_value('pembatasan_jenis_barang_kupon_id', $row->pembatasan_jenis_barang_kupon_id),
				'kupon_id' => set_value('kupon_id', $row->kupon_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
			    );

            $data['page'] = 'pembatasan-jenis-barang-kupon/pembatasan_jenis_barang_kupon_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pembatasan-jenis-barang-kupon'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pembatasan-jenis-barang-kupon/update/'.post('pembatasan_jenis_barang_kupon_id', true)));
        } else {
            $data = array(
				'kupon_id' => post('kupon_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
		    );

            $this->pembatasan_jenis_barang_kupon_model->update($data, post('pembatasan_jenis_barang_kupon_id', true));

            success('Update Record Success');
            redirect(base_url('pembatasan-jenis-barang-kupon/update/'.post('pembatasan_jenis_barang_kupon_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pembatasan_jenis_barang_kupon_model->get($id);

        if ($row) {
            $this->pembatasan_jenis_barang_kupon_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pembatasan-jenis-barang-kupon'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pembatasan-jenis-barang-kupon'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('kupon_id', 'kupon', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');

		$this->form_validation->set_rules('pembatasan_jenis_barang_kupon_id', 'pembatasan_jenis_barang_kupon_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pembatasan_jenis_barang_kupon.php */
/* Location: ./application/controllers/pembatasan_jenis_barang_kupon.php */
/* Please DO NOT modify this information : */