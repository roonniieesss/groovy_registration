<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ras_hewan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('ras_hewan_model');
    }

    public function index()
    {
        $data['page'] = 'ras-hewan/ras_hewan_list';
        $data['title'] = 'List Ras Hewan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->ras_hewan_model->json();
    }

    public function read($id)
    {
        $row = $this->ras_hewan_model->get($id);
        if ($row) {
            $data = array(
				'jenis_hewan_id' => $row->jenis_hewan_id,
				'nama_ras_hewan' => $row->nama_ras_hewan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'ras-hewan/ras_hewan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('ras-hewan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('ras-hewan/create-action'),
		    'jenis_hewan_id' => set_value('jenis_hewan_id'),
		    'nama_ras_hewan' => set_value('nama_ras_hewan'),
		);
        $data['page'] = 'ras-hewan/ras_hewan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('ras-hewan/create'));
        } else {
            $data = array(
				'jenis_hewan_id' => post('jenis_hewan_id',true),
				'nama_ras_hewan' => post('nama_ras_hewan',true),
		    );

            $this->ras_hewan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('ras-hewan'));
        }
    }

    public function update($id)
    {
        $row = $this->ras_hewan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('ras-hewan/update-action'),
				'ras_hewan_id' => set_value('ras_hewan_id', $row->ras_hewan_id),
				'jenis_hewan_id' => set_value('jenis_hewan_id', $row->jenis_hewan_id),
				'nama_ras_hewan' => set_value('nama_ras_hewan', $row->nama_ras_hewan),
			    );

            $data['page'] = 'ras-hewan/ras_hewan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('ras-hewan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('ras-hewan/update/'.post('ras_hewan_id', true)));
        } else {
            $data = array(
				'jenis_hewan_id' => post('jenis_hewan_id',true),
				'nama_ras_hewan' => post('nama_ras_hewan',true),
		    );

            $this->ras_hewan_model->update($data, post('ras_hewan_id', true));

            success('Update Record Success');
            redirect(base_url('ras-hewan/update/'.post('ras_hewan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->ras_hewan_model->get($id);

        if ($row) {
            $this->ras_hewan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('ras-hewan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('ras-hewan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('jenis_hewan_id', 'jenis hewan', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_ras_hewan', 'nama ras hewan', 'trim|required');

		$this->form_validation->set_rules('ras_hewan_id', 'ras_hewan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file ras_hewan.php */
/* Location: ./application/controllers/ras_hewan.php */
/* Please DO NOT modify this information : */