<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class transaksi_tagihan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('transaksi_tagihan_model');
    }

    public function index()
    {
        $data['page'] = 'transaksi-tagihan/transaksi_tagihan_list';
        $data['title'] = 'List Transaksi Tagihan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->transaksi_tagihan_model->json();
    }

    public function read($id)
    {
        $row = $this->transaksi_tagihan_model->get($id);
        if ($row) {
            $data = array(
				'transaksi_keuangan_id' => $row->transaksi_keuangan_id,
				'tagihan_id' => $row->tagihan_id,
				'term' => $row->term,
				'rate' => $row->rate,
				'net_pembayaran' => $row->net_pembayaran,
				'catatan_pembayaran' => $row->catatan_pembayaran,
				'jenis' => $row->jenis,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'transaksi-tagihan/transaksi_tagihan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('transaksi-tagihan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('transaksi-tagihan/create-action'),
		    'transaksi_keuangan_id' => set_value('transaksi_keuangan_id'),
		    'tagihan_id' => set_value('tagihan_id'),
		    'term' => set_value('term'),
		    'rate' => set_value('rate'),
		    'net_pembayaran' => set_value('net_pembayaran'),
		    'catatan_pembayaran' => set_value('catatan_pembayaran'),
		    'jenis' => set_value('jenis'),
		);
        $data['page'] = 'transaksi-tagihan/transaksi_tagihan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transaksi-tagihan/create'));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'tagihan_id' => post('tagihan_id',true),
				'term' => post('term',true),
				'rate' => post('rate',true),
				'net_pembayaran' => post('net_pembayaran',true),
				'catatan_pembayaran' => post('catatan_pembayaran',true),
				'jenis' => post('jenis',true),
		    );

            $this->transaksi_tagihan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('transaksi-tagihan'));
        }
    }

    public function update($id)
    {
        $row = $this->transaksi_tagihan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('transaksi-tagihan/update-action'),
				'transaksi_tagihan_id' => set_value('transaksi_tagihan_id', $row->transaksi_tagihan_id),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'tagihan_id' => set_value('tagihan_id', $row->tagihan_id),
				'term' => set_value('term', $row->term),
				'rate' => set_value('rate', $row->rate),
				'net_pembayaran' => set_value('net_pembayaran', $row->net_pembayaran),
				'catatan_pembayaran' => set_value('catatan_pembayaran', $row->catatan_pembayaran),
				'jenis' => set_value('jenis', $row->jenis),
			    );

            $data['page'] = 'transaksi-tagihan/transaksi_tagihan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('transaksi-tagihan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transaksi-tagihan/update/'.post('transaksi_tagihan_id', true)));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'tagihan_id' => post('tagihan_id',true),
				'term' => post('term',true),
				'rate' => post('rate',true),
				'net_pembayaran' => post('net_pembayaran',true),
				'catatan_pembayaran' => post('catatan_pembayaran',true),
				'jenis' => post('jenis',true),
		    );

            $this->transaksi_tagihan_model->update($data, post('transaksi_tagihan_id', true));

            success('Update Record Success');
            redirect(base_url('transaksi-tagihan/update/'.post('transaksi_tagihan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->transaksi_tagihan_model->get($id);

        if ($row) {
            $this->transaksi_tagihan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('transaksi-tagihan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('transaksi-tagihan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi keuangan', 'trim|required|numeric');
		$this->form_validation->set_rules('tagihan_id', 'tagihan', 'trim|required|numeric');
		$this->form_validation->set_rules('term', 'term', 'trim|required');
		$this->form_validation->set_rules('rate', 'rate', 'trim|required|numeric');
		$this->form_validation->set_rules('net_pembayaran', 'net pembayaran', 'trim|required|numeric');
		$this->form_validation->set_rules('catatan_pembayaran', 'catatan pembayaran', 'trim|required');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required');

		$this->form_validation->set_rules('transaksi_tagihan_id', 'transaksi_tagihan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file transaksi_tagihan.php */
/* Location: ./application/controllers/transaksi_tagihan.php */
/* Please DO NOT modify this information : */