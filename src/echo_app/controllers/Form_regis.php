<?php
class form_regis extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        model('form_regis_model');
        model('inquiry_model');
        // $this->output->enable_profiler(TRUE); 
    }

    public function index()
    {                      
        $data = array(                          
                'action'       => base_url('form_regis/create_action'),
                'pelanggan_id' => set_value('pelanggan_id'),
                'no_inquiry'   => set_value('no_inquiry'),

                // 'kota' => $this->form_regis_model->get_kota()->result(),
                // 'layanan' => $this->form_regis_model->get_jenis_layanan()->result(),
                // 'transportasi' => $this->form_regis_model->get_transportasi()->result(),
                // 'jenis_hewan' =>  $this->form_regis_model->get_jenis_hewan()->result(),
                // 'ras_hewan'  =>   $this->form_regis_model->get_ras_hewan()->result(),
                // 'pelanggan' =>  $this->form_regis_model->get_jenis_pelanggan()->result(),
                // 'id' => $this->form_regis_model->get_id(),
                // 'kontak' => $this->form_regis_model->get_jns_kontak()->result(),
                // 'segmen' => $this->form_regis_model->get_jns_segmen()->result(),
                // 'inquiry_id' => $this->form_regis_model->get_inquiry_id(),
                // 'kontak_pel_id' => $this->form_regis_model->get_kontak_pel_id(),
                // 'no_inquiry' => $this->form_regis_model->get_no_inquiry(),
                // 'inquiry_produk_id' => $this->form_regis_model->get_inquiry_produk_id(),
                // 'jenis_barang_id' => $this->form_regis_model->get_jns_brg_id(),
                // 'inquiry_produk_pet_transport_id' => $this->form_regis_model->get_inquiry_produk_pet_transport_id(),
                // 'alamat_pelanggan_id' => $this->form_regis_model->get_alamat_pelanggan_id()
            );

        $data['page'] = 'pages/form-regis/form_registration8d';
        view('pages/form-regis/form_registration8d',$data);
    }

    //klo pake ajax
    function get_data_jenis_hewan(){
        $data = $this->form_regis_model->get_jenis_hewan()->result();
        // var_dump($data);exit();
        echo json_encode($data);
    }

    // get data ras hewan
    function get_ras_hewan(){
        $id=$this->input->post('id');
        $data['ras_hewan'] = $this->form_regis_model->get_ras_hewan($id);
        echo json_encode($data['ras_hewan']);
    }

    //klo pake ajax
    function get_negara_id(){
        $id=$this->input->post('id');
        $data = $this->form_regis_model->get_negara_id($id);
        // var_dump($data);exit();
        echo json_encode($data);
    }

    function insert(){

        if (isset($_POST['submit'])) {
            // pelanggan(4) - -
            $data = array(
                'pelanggan_id'=>post('pelanggan_id'),
                'nama_pelanggan'=>post('nama_pelanggan'),
                'tanggal_lahir'=>post('tanggal_lahir'),
                'jenis_kelamin'=>post('jenis_kelamin')
            );
            // kontak pelanggan(5) - -
            $data2 = array(
                'kontak_pelanggan_id'=>post('kontak_pelanggan_id'),
                'pelanggan_id'=>post('pelanggan_id'),
                'kontak'=>post('kontak'),
                'kontak_utama'=>post('kontak_utama'),
                'jenis_kontak_pelanggan_id'=>post('jenis_kontak_pelanggan_id')
            );
            // inquiry(3)  - -
            $data3 = array(
                'inquiry_id'=>post('inquiry_id'),
                'pelanggan_id'=>post('pelanggan_id'),
                'no_inquiry'=>post('no_inquiry')        
            ); 
            // inquiry_produk(5) - -
            $data4 = array(
                'inquiry_produk_id'=>post('inquiry_produk_id'),
                'inquiry_id'=>post('inquiry_id'),
                'jenis_barang_id'=>post('jenis_barang_id'), //blm mengerti
                'qty'=>post('qty'),                
                'catatan_pesanan'=>post('catatan_pesan')    
            );
            // inquiry_produk_pet_transport(8) - -
            $data5 = array(
                'inquiry_produk_pet_transport_id'=>post('inquiry_produk_pet_transport_id'),
                'inquiry_produk_id'=>post('inquiry_produk_id'),
                'asal_kota_id'=>post('kota_asal'),
                'tujuan_kota_id'=>post('kota_tujuan'),
                'moda_transportasi_id'=>post('moda_transportasi'), 
                'ras_hewan_id'=>post('ras_hewan'), 
                'nama_hewan'=>post('nama_hewan'), 
                'jenis_kelamin'=>post('jenis_kelamin_pet')
            );

            // alamat_pelanggan(9)-
            $data6 = array(
                'alamat_pelanggan_id'=>post('alamat_pelanggan_id'),
                'pelanggan_id'=>post('pelanggan_id'),
                'negara_id'=>post('get-negara'),
                'kota_id'=>post('kota_asal_pel'),
                'label'=>post('label'), 
                'alamat'=>post('alamat'), 
                'kode_pos'=>post('kode_pos'), 
                'nama_penerima'=>post('nama_penerima'),
                'alamat_utama'=>'0'
            );
            $semua = array($data,$data2,$data3,$data4,$data5,$data6);
            // dd($semua);
            $this->Form_regis_model->insert($data,$data2,$data3,$data4,$data5,$data6);
            $this->session->set_flashdata('notif','<div class="alert alert-success" role="alert">
                        Terimakasih Data Sedang Diproses
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('form_regis','refresh');
        }else{
            view('pages/form-regis/form_registration8');
        }
    }

    function create_action(){

        if (isset($_POST['submit'])) {
            $this->form_regis_model->insertInquiry();            
            success('Create Record Success');
            redirect(base_url('form_regis'));             
        }else{
            return false;
        }
    }

}