<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class hasil_pecah_kemasan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('hasil_pecah_kemasan_model');
    }

    public function index()
    {
        $data['page'] = 'hasil-pecah-kemasan/hasil_pecah_kemasan_list';
        $data['title'] = 'List Hasil Pecah Kemasan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->hasil_pecah_kemasan_model->json();
    }

    public function read($id)
    {
        $row = $this->hasil_pecah_kemasan_model->get($id);
        if ($row) {
            $data = array(
				'pecah_kemasan_detail_id' => $row->pecah_kemasan_detail_id,
				'transaksi_barang_id' => $row->transaksi_barang_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'hasil-pecah-kemasan/hasil_pecah_kemasan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('hasil-pecah-kemasan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('hasil-pecah-kemasan/create-action'),
		    'pecah_kemasan_detail_id' => set_value('pecah_kemasan_detail_id'),
		    'transaksi_barang_id' => set_value('transaksi_barang_id'),
		);
        $data['page'] = 'hasil-pecah-kemasan/hasil_pecah_kemasan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('hasil-pecah-kemasan/create'));
        } else {
            $data = array(
				'pecah_kemasan_detail_id' => post('pecah_kemasan_detail_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
		    );

            $this->hasil_pecah_kemasan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('hasil-pecah-kemasan'));
        }
    }

    public function update($id)
    {
        $row = $this->hasil_pecah_kemasan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('hasil-pecah-kemasan/update-action'),
				'hasil_pecah_kemasan_id' => set_value('hasil_pecah_kemasan_id', $row->hasil_pecah_kemasan_id),
				'pecah_kemasan_detail_id' => set_value('pecah_kemasan_detail_id', $row->pecah_kemasan_detail_id),
				'transaksi_barang_id' => set_value('transaksi_barang_id', $row->transaksi_barang_id),
			    );

            $data['page'] = 'hasil-pecah-kemasan/hasil_pecah_kemasan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('hasil-pecah-kemasan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('hasil-pecah-kemasan/update/'.post('hasil_pecah_kemasan_id', true)));
        } else {
            $data = array(
				'pecah_kemasan_detail_id' => post('pecah_kemasan_detail_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
		    );

            $this->hasil_pecah_kemasan_model->update($data, post('hasil_pecah_kemasan_id', true));

            success('Update Record Success');
            redirect(base_url('hasil-pecah-kemasan/update/'.post('hasil_pecah_kemasan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->hasil_pecah_kemasan_model->get($id);

        if ($row) {
            $this->hasil_pecah_kemasan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('hasil-pecah-kemasan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('hasil-pecah-kemasan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pecah_kemasan_detail_id', 'pecah kemasan detail', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_id', 'transaksi barang', 'trim|required|numeric');

		$this->form_validation->set_rules('hasil_pecah_kemasan_id', 'hasil_pecah_kemasan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file hasil_pecah_kemasan.php */
/* Location: ./application/controllers/hasil_pecah_kemasan.php */
/* Please DO NOT modify this information : */