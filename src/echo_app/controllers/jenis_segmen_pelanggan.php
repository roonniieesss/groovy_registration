<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_segmen_pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_segmen_pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-segmen-pelanggan/jenis_segmen_pelanggan_list';
        $data['title'] = 'List Jenis Segmen Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_segmen_pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_segmen_pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'jenis_segmen_pelanggan' => $row->jenis_segmen_pelanggan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-segmen-pelanggan/jenis_segmen_pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-segmen-pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-segmen-pelanggan/create-action'),
		    'jenis_segmen_pelanggan' => set_value('jenis_segmen_pelanggan'),
		);
        $data['page'] = 'jenis-segmen-pelanggan/jenis_segmen_pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-segmen-pelanggan/create'));
        } else {
            $data = array(
				'jenis_segmen_pelanggan' => post('jenis_segmen_pelanggan',true),
		    );

            $this->jenis_segmen_pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-segmen-pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_segmen_pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-segmen-pelanggan/update-action'),
				'jenis_segmen_pelanggan_id' => set_value('jenis_segmen_pelanggan_id', $row->jenis_segmen_pelanggan_id),
				'jenis_segmen_pelanggan' => set_value('jenis_segmen_pelanggan', $row->jenis_segmen_pelanggan),
			    );

            $data['page'] = 'jenis-segmen-pelanggan/jenis_segmen_pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-segmen-pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-segmen-pelanggan/update/'.post('jenis_segmen_pelanggan_id', true)));
        } else {
            $data = array(
				'jenis_segmen_pelanggan' => post('jenis_segmen_pelanggan',true),
		    );

            $this->jenis_segmen_pelanggan_model->update($data, post('jenis_segmen_pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-segmen-pelanggan/update/'.post('jenis_segmen_pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_segmen_pelanggan_model->get($id);

        if ($row) {
            $this->jenis_segmen_pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-segmen-pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-segmen-pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('jenis_segmen_pelanggan', 'jenis segmen pelanggan', 'trim|required');

		$this->form_validation->set_rules('jenis_segmen_pelanggan_id', 'jenis_segmen_pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_segmen_pelanggan.php */
/* Location: ./application/controllers/jenis_segmen_pelanggan.php */
/* Please DO NOT modify this information : */