<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class delivery_order_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('delivery_order_detail_model');
    }

    public function index()
    {
        $data['page'] = 'delivery-order-detail/delivery_order_detail_list';
        $data['title'] = 'List Delivery Order Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->delivery_order_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->delivery_order_detail_model->get($id);
        if ($row) {
            $data = array(
				'delivery_order_id' => $row->delivery_order_id,
				'sales_order_item_id' => $row->sales_order_item_id,
				'qty_kirim' => $row->qty_kirim,
				'status' => $row->status,
				'alasan_pembatalan' => $row->alasan_pembatalan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'delivery-order-detail/delivery_order_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('delivery-order-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('delivery-order-detail/create-action'),
		    'delivery_order_id' => set_value('delivery_order_id'),
		    'sales_order_item_id' => set_value('sales_order_item_id'),
		    'qty_kirim' => set_value('qty_kirim'),
		    'status' => set_value('status'),
		    'alasan_pembatalan' => set_value('alasan_pembatalan'),
		);
        $data['page'] = 'delivery-order-detail/delivery_order_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('delivery-order-detail/create'));
        } else {
            $data = array(
				'delivery_order_id' => post('delivery_order_id',true),
				'sales_order_item_id' => post('sales_order_item_id',true),
				'qty_kirim' => post('qty_kirim',true),
				'status' => post('status',true),
				'alasan_pembatalan' => post('alasan_pembatalan',true),
		    );

            $this->delivery_order_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('delivery-order-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->delivery_order_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('delivery-order-detail/update-action'),
				'delivery_order_detail_id' => set_value('delivery_order_detail_id', $row->delivery_order_detail_id),
				'delivery_order_id' => set_value('delivery_order_id', $row->delivery_order_id),
				'sales_order_item_id' => set_value('sales_order_item_id', $row->sales_order_item_id),
				'qty_kirim' => set_value('qty_kirim', $row->qty_kirim),
				'status' => set_value('status', $row->status),
				'alasan_pembatalan' => set_value('alasan_pembatalan', $row->alasan_pembatalan),
			    );

            $data['page'] = 'delivery-order-detail/delivery_order_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('delivery-order-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('delivery-order-detail/update/'.post('delivery_order_detail_id', true)));
        } else {
            $data = array(
				'delivery_order_id' => post('delivery_order_id',true),
				'sales_order_item_id' => post('sales_order_item_id',true),
				'qty_kirim' => post('qty_kirim',true),
				'status' => post('status',true),
				'alasan_pembatalan' => post('alasan_pembatalan',true),
		    );

            $this->delivery_order_detail_model->update($data, post('delivery_order_detail_id', true));

            success('Update Record Success');
            redirect(base_url('delivery-order-detail/update/'.post('delivery_order_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->delivery_order_detail_model->get($id);

        if ($row) {
            $this->delivery_order_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('delivery-order-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('delivery-order-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('delivery_order_id', 'delivery order', 'trim|required|numeric');
		$this->form_validation->set_rules('sales_order_item_id', 'sales order item', 'trim|required|numeric');
		$this->form_validation->set_rules('qty_kirim', 'qty kirim', 'trim|required|numeric');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('alasan_pembatalan', 'alasan pembatalan', 'trim|required');

		$this->form_validation->set_rules('delivery_order_detail_id', 'delivery_order_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file delivery_order_detail.php */
/* Location: ./application/controllers/delivery_order_detail.php */
/* Please DO NOT modify this information : */