<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kontak_pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kontak_pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'kontak-pelanggan/kontak_pelanggan_list';
        $data['title'] = 'List Kontak Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->kontak_pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->kontak_pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'pelanggan_id' => $row->pelanggan_id,
				'jenis_kontak_pelanggan_id' => $row->jenis_kontak_pelanggan_id,
				'kontak' => $row->kontak,
				'kontak_utama' => $row->kontak_utama,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'kontak-pelanggan/kontak_pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kontak-pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('kontak-pelanggan/create-action'),
		    'pelanggan_id' => set_value('pelanggan_id'),
		    'jenis_kontak_pelanggan_id' => set_value('jenis_kontak_pelanggan_id'),
		    'kontak' => set_value('kontak'),
		    'kontak_utama' => set_value('kontak_utama'),
		);
        $data['page'] = 'kontak-pelanggan/kontak_pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kontak-pelanggan/create'));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'jenis_kontak_pelanggan_id' => post('jenis_kontak_pelanggan_id',true),
				'kontak' => post('kontak',true),
				'kontak_utama' => post('kontak_utama',true),
		    );

            $this->kontak_pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kontak-pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->kontak_pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('kontak-pelanggan/update-action'),
				'kontak_pelanggan_id' => set_value('kontak_pelanggan_id', $row->kontak_pelanggan_id),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'jenis_kontak_pelanggan_id' => set_value('jenis_kontak_pelanggan_id', $row->jenis_kontak_pelanggan_id),
				'kontak' => set_value('kontak', $row->kontak),
				'kontak_utama' => set_value('kontak_utama', $row->kontak_utama),
			    );

            $data['page'] = 'kontak-pelanggan/kontak_pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kontak-pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kontak-pelanggan/update/'.post('kontak_pelanggan_id', true)));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'jenis_kontak_pelanggan_id' => post('jenis_kontak_pelanggan_id',true),
				'kontak' => post('kontak',true),
				'kontak_utama' => post('kontak_utama',true),
		    );

            $this->kontak_pelanggan_model->update($data, post('kontak_pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('kontak-pelanggan/update/'.post('kontak_pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kontak_pelanggan_model->get($id);

        if ($row) {
            $this->kontak_pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kontak-pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kontak-pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_kontak_pelanggan_id', 'jenis kontak pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('kontak', 'kontak', 'trim|required');
		$this->form_validation->set_rules('kontak_utama', 'kontak utama', 'trim|required');

		$this->form_validation->set_rules('kontak_pelanggan_id', 'kontak_pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file kontak_pelanggan.php */
/* Location: ./application/controllers/kontak_pelanggan.php */
/* Please DO NOT modify this information : */