<?php
class Coba extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        model('Form_regis_model');
    }

    public function index()
    {        
    	$data['kota'] = $this->Form_regis_model->view_kota()->result();
    	$data['transport'] = $this->Form_regis_model->view_moda_transportasi()->result();  
        $data['hewan'] = $this->Form_regis_model->view_jenis_hewan()->result();
        $data['pelanggan'] = $this->Form_regis_model->view_jenis_pelanggan()->result();
        $data['id'] = $this->Form_regis_model->get_id();
        $data['kontak'] = $this->Form_regis_model->get_jns_kontak()->result();
        $data['segmen'] = $this->Form_regis_model->get_jns_segmen()->result();
        $data['inquiry_id'] = $this->Form_regis_model->get_inquiry_id();
        $data['kontak_pel_id'] = $this->Form_regis_model->get_kontak_pel_id();
        $data['no_inquiry'] = $this->Form_regis_model->get_no_inquiry();
        $data['inquiry_produk_id'] = $this->Form_regis_model->get_inquiry_produk_id();
        $data['jenis_barang_id'] = $this->Form_regis_model->get_jns_brg_id();
    	// dd($data);
        view('pages/form-regis/coba',$data);
    }

    function insert(){

        if (isset($_POST['submit'])) {
            // pelanggan(4) - -
            $data = array(
                'pelanggan_id'=>post('pelanggan_id'),
                'nama_pelanggan'=>post('nama_pelanggan'),
                'tanggal_lahir'=>post('tanggal_lahir'),
                'jenis_kelamin'=>post('jenis_kelamin')
            );
            // kontak pelanggan(5) - -
            $data2 = array(
                'kontak_pelanggan_id'=>post('kontak_pelanggan_id'),
                'pelanggan_id'=>post('pelanggan_id'),
                'kontak'=>post('kontak'),
                'kontak_utama'=>post('kontak_utama'),
                'jenis_kontak_pelanggan_id'=>post('jenis_kontak_pelanggan_id')
            );
            // inquiry(3)  - -
            $data3 = array(
                'inquiry_id'=>post('inquiry_id'),
                'pelanggan_id'=>post('pelanggan_id'),
                'no_inquiry'=>post('no_inquiry')        
            ); 
            // inquiry_produk(5) -
            $data4 = array(
                'inquiry_produk_id'=>post('inquiry_produk_id'),
                'inquiry_id'=>post('inquiry_id'),
                'jenis_barang_id'=>post('jenis_barang_id'),
                'qty'=>post('qty'),                
                'catatan_pesan'=>post('catatan_pesan')    
            );
             // inquiry_produk_pet_transport(8) - 
            $data5 = array(
                'inquiry_produk_pet_transport'=>post('inquiry_produk_pet_transport'),
                'inquiry_produk_id'=>post('inquiry_produk_id'),
                'asal_kota'=>post('asal_kota'),
                'tujuan_kota'=>post('asal_kota'),
                'moda_transportasi'=>post('moda_transportasi'), 
                'ras_hewan'=>post('ras_hewan'), 
                'nama_hewan'=>post('nama_hewan'), 
                'jenis_kelamin'=>post('jenis_kelamin')
            );           
            // dd($data);
            $this->Form_regis_model->insert($data,$data2,$data3);
            $this->session->set_flashdata('notif','<div class="alert alert-danger" role="alert">
                        Terimakasih Data Sedang Diproses
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('coba');
        }else{
            view('pages/form-regis/coba');
        }
    }
}