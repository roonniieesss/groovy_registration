<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class inquiry_produk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('inquiry_produk_model');
    }

    public function index()
    {
        $data['page'] = 'inquiry-produk/inquiry_produk_list';
        $data['title'] = 'List Inquiry Produk';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->inquiry_produk_model->json();
    }

    public function read($id)
    {
        $row = $this->inquiry_produk_model->get($id);
        if ($row) {
            $data = array(
				'inquiry_id' => $row->inquiry_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'qty' => $row->qty,
				'catatan_pesanan' => $row->catatan_pesanan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'inquiry-produk/inquiry_produk_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry-produk'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('inquiry-produk/create-action'),
		    'inquiry_id' => set_value('inquiry_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'qty' => set_value('qty'),
		    'catatan_pesanan' => set_value('catatan_pesanan'),
		);
        $data['page'] = 'inquiry-produk/inquiry_produk_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry-produk/create'));
        } else {
            $data = array(
				'inquiry_id' => post('inquiry_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'qty' => post('qty',true),
				'catatan_pesanan' => post('catatan_pesanan',true),
		    );

            $this->inquiry_produk_model->insert($data);

            success('Create Record Success');
            redirect(base_url('inquiry-produk'));
        }
    }

    public function update($id)
    {
        $row = $this->inquiry_produk_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('inquiry-produk/update-action'),
				'inquiry_produk_id' => set_value('inquiry_produk_id', $row->inquiry_produk_id),
				'inquiry_id' => set_value('inquiry_id', $row->inquiry_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'qty' => set_value('qty', $row->qty),
				'catatan_pesanan' => set_value('catatan_pesanan', $row->catatan_pesanan),
			    );

            $data['page'] = 'inquiry-produk/inquiry_produk_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry-produk'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry-produk/update/'.post('inquiry_produk_id', true)));
        } else {
            $data = array(
				'inquiry_id' => post('inquiry_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'qty' => post('qty',true),
				'catatan_pesanan' => post('catatan_pesanan',true),
		    );

            $this->inquiry_produk_model->update($data, post('inquiry_produk_id', true));

            success('Update Record Success');
            redirect(base_url('inquiry-produk/update/'.post('inquiry_produk_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->inquiry_produk_model->get($id);

        if ($row) {
            $this->inquiry_produk_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('inquiry-produk'));
        } else {

            warning('Record Not Found');
            redirect(base_url('inquiry-produk'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('inquiry_id', 'inquiry', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('qty', 'qty', 'trim|required|numeric');
		$this->form_validation->set_rules('catatan_pesanan', 'catatan pesanan', 'trim|required');

		$this->form_validation->set_rules('inquiry_produk_id', 'inquiry_produk_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file inquiry_produk.php */
/* Location: ./application/controllers/inquiry_produk.php */
/* Please DO NOT modify this information : */