<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sales_order_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sales_order_detail_model');
    }

    public function index()
    {
        $data['page'] = 'sales-order-detail/sales_order_detail_list';
        $data['title'] = 'List Sales Order Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->sales_order_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->sales_order_detail_model->get($id);
        if ($row) {
            $data = array(
				'sales_order_id' => $row->sales_order_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'qty' => $row->qty,
				'harga_satuan' => $row->harga_satuan,
				'diskon' => $row->diskon,
				'harga_jual' => $row->harga_jual,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'sales-order-detail/sales_order_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sales-order-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('sales-order-detail/create-action'),
		    'sales_order_id' => set_value('sales_order_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'qty' => set_value('qty'),
		    'harga_satuan' => set_value('harga_satuan'),
		    'diskon' => set_value('diskon'),
		    'harga_jual' => set_value('harga_jual'),
		);
        $data['page'] = 'sales-order-detail/sales_order_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sales-order-detail/create'));
        } else {
            $data = array(
				'sales_order_id' => post('sales_order_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'qty' => post('qty',true),
				'harga_satuan' => post('harga_satuan',true),
				'diskon' => post('diskon',true),
				'harga_jual' => post('harga_jual',true),
		    );

            $this->sales_order_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sales-order-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->sales_order_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('sales-order-detail/update-action'),
				'sales_order_detail_id' => set_value('sales_order_detail_id', $row->sales_order_detail_id),
				'sales_order_id' => set_value('sales_order_id', $row->sales_order_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'qty' => set_value('qty', $row->qty),
				'harga_satuan' => set_value('harga_satuan', $row->harga_satuan),
				'diskon' => set_value('diskon', $row->diskon),
				'harga_jual' => set_value('harga_jual', $row->harga_jual),
			    );

            $data['page'] = 'sales-order-detail/sales_order_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sales-order-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sales-order-detail/update/'.post('sales_order_detail_id', true)));
        } else {
            $data = array(
				'sales_order_id' => post('sales_order_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'qty' => post('qty',true),
				'harga_satuan' => post('harga_satuan',true),
				'diskon' => post('diskon',true),
				'harga_jual' => post('harga_jual',true),
		    );

            $this->sales_order_detail_model->update($data, post('sales_order_detail_id', true));

            success('Update Record Success');
            redirect(base_url('sales-order-detail/update/'.post('sales_order_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sales_order_detail_model->get($id);

        if ($row) {
            $this->sales_order_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sales-order-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sales-order-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('sales_order_id', 'sales order', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('qty', 'qty', 'trim|required|numeric');
		$this->form_validation->set_rules('harga_satuan', 'harga satuan', 'trim|required|numeric');
		$this->form_validation->set_rules('diskon', 'diskon', 'trim|required|numeric');
		$this->form_validation->set_rules('harga_jual', 'harga jual', 'trim|required|numeric');

		$this->form_validation->set_rules('sales_order_detail_id', 'sales_order_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file sales_order_detail.php */
/* Location: ./application/controllers/sales_order_detail.php */
/* Please DO NOT modify this information : */