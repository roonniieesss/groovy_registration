<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peraturan_exim_vaksin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peraturan_exim_vaksin_model');
    }

    public function index()
    {
        $data['page'] = 'peraturan-exim-vaksin/peraturan_exim_vaksin_list';
        $data['title'] = 'List Peraturan Exim Vaksin';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peraturan_exim_vaksin_model->json();
    }

    public function read($id)
    {
        $row = $this->peraturan_exim_vaksin_model->get($id);
        if ($row) {
            $data = array(
				'peraturan_exim_ras_hewan_id' => $row->peraturan_exim_ras_hewan_id,
				'jenis_vaksin_id' => $row->jenis_vaksin_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peraturan-exim-vaksin/peraturan_exim_vaksin_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-vaksin'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peraturan-exim-vaksin/create-action'),
		    'peraturan_exim_ras_hewan_id' => set_value('peraturan_exim_ras_hewan_id'),
		    'jenis_vaksin_id' => set_value('jenis_vaksin_id'),
		);
        $data['page'] = 'peraturan-exim-vaksin/peraturan_exim_vaksin_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-vaksin/create'));
        } else {
            $data = array(
				'peraturan_exim_ras_hewan_id' => post('peraturan_exim_ras_hewan_id',true),
				'jenis_vaksin_id' => post('jenis_vaksin_id',true),
		    );

            $this->peraturan_exim_vaksin_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peraturan-exim-vaksin'));
        }
    }

    public function update($id)
    {
        $row = $this->peraturan_exim_vaksin_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peraturan-exim-vaksin/update-action'),
				'peraturan_exim_vaksin_id' => set_value('peraturan_exim_vaksin_id', $row->peraturan_exim_vaksin_id),
				'peraturan_exim_ras_hewan_id' => set_value('peraturan_exim_ras_hewan_id', $row->peraturan_exim_ras_hewan_id),
				'jenis_vaksin_id' => set_value('jenis_vaksin_id', $row->jenis_vaksin_id),
			    );

            $data['page'] = 'peraturan-exim-vaksin/peraturan_exim_vaksin_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-vaksin'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-vaksin/update/'.post('peraturan_exim_vaksin_id', true)));
        } else {
            $data = array(
				'peraturan_exim_ras_hewan_id' => post('peraturan_exim_ras_hewan_id',true),
				'jenis_vaksin_id' => post('jenis_vaksin_id',true),
		    );

            $this->peraturan_exim_vaksin_model->update($data, post('peraturan_exim_vaksin_id', true));

            success('Update Record Success');
            redirect(base_url('peraturan-exim-vaksin/update/'.post('peraturan_exim_vaksin_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peraturan_exim_vaksin_model->get($id);

        if ($row) {
            $this->peraturan_exim_vaksin_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peraturan-exim-vaksin'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peraturan-exim-vaksin'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('peraturan_exim_ras_hewan_id', 'peraturan exim ras hewan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_vaksin_id', 'jenis vaksin', 'trim|required|numeric');

		$this->form_validation->set_rules('peraturan_exim_vaksin_id', 'peraturan_exim_vaksin_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peraturan_exim_vaksin.php */
/* Location: ./application/controllers/peraturan_exim_vaksin.php */
/* Please DO NOT modify this information : */