<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ruangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('ruangan_model');
    }

    public function index()
    {
        $data['page'] = 'ruangan/ruangan_list';
        $data['title'] = 'List Ruangan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->ruangan_model->json();
    }

    public function read($id)
    {
        $row = $this->ruangan_model->get($id);
        if ($row) {
            $data = array(
				'sub_lokasi_id' => $row->sub_lokasi_id,
				'nama_ruangan' => $row->nama_ruangan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'ruangan/ruangan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('ruangan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('ruangan/create-action'),
		    'sub_lokasi_id' => set_value('sub_lokasi_id'),
		    'nama_ruangan' => set_value('nama_ruangan'),
		);
        $data['page'] = 'ruangan/ruangan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('ruangan/create'));
        } else {
            $data = array(
				'sub_lokasi_id' => post('sub_lokasi_id',true),
				'nama_ruangan' => post('nama_ruangan',true),
		    );

            $this->ruangan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('ruangan'));
        }
    }

    public function update($id)
    {
        $row = $this->ruangan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('ruangan/update-action'),
				'ruangan_id' => set_value('ruangan_id', $row->ruangan_id),
				'sub_lokasi_id' => set_value('sub_lokasi_id', $row->sub_lokasi_id),
				'nama_ruangan' => set_value('nama_ruangan', $row->nama_ruangan),
			    );

            $data['page'] = 'ruangan/ruangan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('ruangan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('ruangan/update/'.post('ruangan_id', true)));
        } else {
            $data = array(
				'sub_lokasi_id' => post('sub_lokasi_id',true),
				'nama_ruangan' => post('nama_ruangan',true),
		    );

            $this->ruangan_model->update($data, post('ruangan_id', true));

            success('Update Record Success');
            redirect(base_url('ruangan/update/'.post('ruangan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->ruangan_model->get($id);

        if ($row) {
            $this->ruangan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('ruangan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('ruangan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('sub_lokasi_id', 'sub lokasi', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_ruangan', 'nama ruangan', 'trim|required');

		$this->form_validation->set_rules('ruangan_id', 'ruangan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file ruangan.php */
/* Location: ./application/controllers/ruangan.php */
/* Please DO NOT modify this information : */