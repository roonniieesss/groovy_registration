<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class negara extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('negara_model');
    }

    public function index()
    {
        $data['page'] = 'negara/negara_list';
        $data['title'] = 'List Negara';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->negara_model->json();
    }

    public function read($id)
    {
        $row = $this->negara_model->get($id);
        if ($row) {
            $data = array(
				'kode_negara' => $row->kode_negara,
				'kode_bahasa' => $row->kode_bahasa,
				'nama_negara' => $row->nama_negara,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'negara/negara_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('negara'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('negara/create-action'),
		    'kode_negara' => set_value('kode_negara'),
		    'kode_bahasa' => set_value('kode_bahasa'),
		    'nama_negara' => set_value('nama_negara'),
		);
        $data['page'] = 'negara/negara_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('negara/create'));
        } else {
            $data = array(
				'kode_negara' => post('kode_negara',true),
				'kode_bahasa' => post('kode_bahasa',true),
				'nama_negara' => post('nama_negara',true),
		    );

            $this->negara_model->insert($data);

            success('Create Record Success');
            redirect(base_url('negara'));
        }
    }

    public function update($id)
    {
        $row = $this->negara_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('negara/update-action'),
				'negara_id' => set_value('negara_id', $row->negara_id),
				'kode_negara' => set_value('kode_negara', $row->kode_negara),
				'kode_bahasa' => set_value('kode_bahasa', $row->kode_bahasa),
				'nama_negara' => set_value('nama_negara', $row->nama_negara),
			    );

            $data['page'] = 'negara/negara_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('negara'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('negara/update/'.post('negara_id', true)));
        } else {
            $data = array(
				'kode_negara' => post('kode_negara',true),
				'kode_bahasa' => post('kode_bahasa',true),
				'nama_negara' => post('nama_negara',true),
		    );

            $this->negara_model->update($data, post('negara_id', true));

            success('Update Record Success');
            redirect(base_url('negara/update/'.post('negara_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->negara_model->get($id);

        if ($row) {
            $this->negara_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('negara'));
        } else {

            warning('Record Not Found');
            redirect(base_url('negara'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('kode_negara', 'kode negara', 'trim|required');
		$this->form_validation->set_rules('kode_bahasa', 'kode bahasa', 'trim|required');
		$this->form_validation->set_rules('nama_negara', 'nama negara', 'trim|required');

		$this->form_validation->set_rules('negara_id', 'negara_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file negara.php */
/* Location: ./application/controllers/negara.php */
/* Please DO NOT modify this information : */