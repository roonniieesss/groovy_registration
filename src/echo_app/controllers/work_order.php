<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class work_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('work_order_model');
    }

    public function index()
    {
        $data['page'] = 'work-order/work_order_list';
        $data['title'] = 'List Work Order';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->work_order_model->json();
    }

    public function read($id)
    {
        $row = $this->work_order_model->get($id);
        if ($row) {
            $data = array(
				'analis_id' => $row->analis_id,
				'tanggal' => $row->tanggal,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'work-order/work_order_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('work-order'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('work-order/create-action'),
		    'analis_id' => set_value('analis_id'),
		    'tanggal' => set_value('tanggal'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'work-order/work_order_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('work-order/create'));
        } else {
            $data = array(
				'analis_id' => post('analis_id',true),
				'tanggal' => post('tanggal',true),
				'status' => post('status',true),
		    );

            $this->work_order_model->insert($data);

            success('Create Record Success');
            redirect(base_url('work-order'));
        }
    }

    public function update($id)
    {
        $row = $this->work_order_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('work-order/update-action'),
				'work_order_id' => set_value('work_order_id', $row->work_order_id),
				'analis_id' => set_value('analis_id', $row->analis_id),
				'tanggal' => set_value('tanggal', $row->tanggal),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'work-order/work_order_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('work-order'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('work-order/update/'.post('work_order_id', true)));
        } else {
            $data = array(
				'analis_id' => post('analis_id',true),
				'tanggal' => post('tanggal',true),
				'status' => post('status',true),
		    );

            $this->work_order_model->update($data, post('work_order_id', true));

            success('Update Record Success');
            redirect(base_url('work-order/update/'.post('work_order_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->work_order_model->get($id);

        if ($row) {
            $this->work_order_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('work-order'));
        } else {

            warning('Record Not Found');
            redirect(base_url('work-order'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('analis_id', 'analis', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('work_order_id', 'work_order_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file work_order.php */
/* Location: ./application/controllers/work_order.php */
/* Please DO NOT modify this information : */