<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peraturan_exim_konten extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peraturan_exim_konten_model');
    }

    public function index()
    {
        $data['page'] = 'peraturan-exim-konten/peraturan_exim_konten_list';
        $data['title'] = 'List Peraturan Exim Konten';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peraturan_exim_konten_model->json();
    }

    public function read($id)
    {
        $row = $this->peraturan_exim_konten_model->get($id);
        if ($row) {
            $data = array(
				'peraturan_exim_id' => $row->peraturan_exim_id,
				'kode_bahasa' => $row->kode_bahasa,
				'media_type' => $row->media_type,
				'media_filename' => $row->media_filename,
				'isi_peraturan' => $row->isi_peraturan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peraturan-exim-konten/peraturan_exim_konten_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-konten'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peraturan-exim-konten/create-action'),
		    'peraturan_exim_id' => set_value('peraturan_exim_id'),
		    'kode_bahasa' => set_value('kode_bahasa'),
		    'media_type' => set_value('media_type'),
		    'media_filename' => set_value('media_filename'),
		    'isi_peraturan' => set_value('isi_peraturan'),
		);
        $data['page'] = 'peraturan-exim-konten/peraturan_exim_konten_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-konten/create'));
        } else {
            $data = array(
				'peraturan_exim_id' => post('peraturan_exim_id',true),
				'kode_bahasa' => post('kode_bahasa',true),
				'media_type' => post('media_type',true),
				'media_filename' => post('media_filename',true),
				'isi_peraturan' => post('isi_peraturan',true),
		    );

            $this->peraturan_exim_konten_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peraturan-exim-konten'));
        }
    }

    public function update($id)
    {
        $row = $this->peraturan_exim_konten_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peraturan-exim-konten/update-action'),
				'peraturan_exim_konten_id' => set_value('peraturan_exim_konten_id', $row->peraturan_exim_konten_id),
				'peraturan_exim_id' => set_value('peraturan_exim_id', $row->peraturan_exim_id),
				'kode_bahasa' => set_value('kode_bahasa', $row->kode_bahasa),
				'media_type' => set_value('media_type', $row->media_type),
				'media_filename' => set_value('media_filename', $row->media_filename),
				'isi_peraturan' => set_value('isi_peraturan', $row->isi_peraturan),
			    );

            $data['page'] = 'peraturan-exim-konten/peraturan_exim_konten_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-konten'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-konten/update/'.post('peraturan_exim_konten_id', true)));
        } else {
            $data = array(
				'peraturan_exim_id' => post('peraturan_exim_id',true),
				'kode_bahasa' => post('kode_bahasa',true),
				'media_type' => post('media_type',true),
				'media_filename' => post('media_filename',true),
				'isi_peraturan' => post('isi_peraturan',true),
		    );

            $this->peraturan_exim_konten_model->update($data, post('peraturan_exim_konten_id', true));

            success('Update Record Success');
            redirect(base_url('peraturan-exim-konten/update/'.post('peraturan_exim_konten_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peraturan_exim_konten_model->get($id);

        if ($row) {
            $this->peraturan_exim_konten_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peraturan-exim-konten'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peraturan-exim-konten'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('peraturan_exim_id', 'peraturan exim', 'trim|required|numeric');
		$this->form_validation->set_rules('kode_bahasa', 'kode bahasa', 'trim|required');
		$this->form_validation->set_rules('media_type', 'media type', 'trim|required');
		$this->form_validation->set_rules('media_filename', 'media filename', 'trim|required');
		$this->form_validation->set_rules('isi_peraturan', 'isi peraturan', 'trim|required');

		$this->form_validation->set_rules('peraturan_exim_konten_id', 'peraturan_exim_konten_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peraturan_exim_konten.php */
/* Location: ./application/controllers/peraturan_exim_konten.php */
/* Please DO NOT modify this information : */