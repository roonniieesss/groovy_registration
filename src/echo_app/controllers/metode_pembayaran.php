<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class metode_pembayaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('metode_pembayaran_model');
    }

    public function index()
    {
        $data['page'] = 'metode-pembayaran/metode_pembayaran_list';
        $data['title'] = 'List Metode Pembayaran';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->metode_pembayaran_model->json();
    }

    public function read($id)
    {
        $row = $this->metode_pembayaran_model->get($id);
        if ($row) {
            $data = array(
				'akun_id' => $row->akun_id,
				'nama_metode_pembayaran' => $row->nama_metode_pembayaran,
				'nama_bank' => $row->nama_bank,
				'nomor_rekening' => $row->nomor_rekening,
				'pemilik_rekening' => $row->pemilik_rekening,
				'kurs' => $row->kurs,
				'jenis' => $row->jenis,
				'pembayaran_invoice' => $row->pembayaran_invoice,
				'pembayaran_tagihan' => $row->pembayaran_tagihan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'metode-pembayaran/metode_pembayaran_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('metode-pembayaran'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('metode-pembayaran/create-action'),
		    'akun_id' => set_value('akun_id'),
		    'nama_metode_pembayaran' => set_value('nama_metode_pembayaran'),
		    'nama_bank' => set_value('nama_bank'),
		    'nomor_rekening' => set_value('nomor_rekening'),
		    'pemilik_rekening' => set_value('pemilik_rekening'),
		    'kurs' => set_value('kurs'),
		    'jenis' => set_value('jenis'),
		    'pembayaran_invoice' => set_value('pembayaran_invoice'),
		    'pembayaran_tagihan' => set_value('pembayaran_tagihan'),
		);
        $data['page'] = 'metode-pembayaran/metode_pembayaran_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('metode-pembayaran/create'));
        } else {
            $data = array(
				'akun_id' => post('akun_id',true),
				'nama_metode_pembayaran' => post('nama_metode_pembayaran',true),
				'nama_bank' => post('nama_bank',true),
				'nomor_rekening' => post('nomor_rekening',true),
				'pemilik_rekening' => post('pemilik_rekening',true),
				'kurs' => post('kurs',true),
				'jenis' => post('jenis',true),
				'pembayaran_invoice' => post('pembayaran_invoice',true),
				'pembayaran_tagihan' => post('pembayaran_tagihan',true),
		    );

            $this->metode_pembayaran_model->insert($data);

            success('Create Record Success');
            redirect(base_url('metode-pembayaran'));
        }
    }

    public function update($id)
    {
        $row = $this->metode_pembayaran_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('metode-pembayaran/update-action'),
				'metode_pembayaran_id' => set_value('metode_pembayaran_id', $row->metode_pembayaran_id),
				'akun_id' => set_value('akun_id', $row->akun_id),
				'nama_metode_pembayaran' => set_value('nama_metode_pembayaran', $row->nama_metode_pembayaran),
				'nama_bank' => set_value('nama_bank', $row->nama_bank),
				'nomor_rekening' => set_value('nomor_rekening', $row->nomor_rekening),
				'pemilik_rekening' => set_value('pemilik_rekening', $row->pemilik_rekening),
				'kurs' => set_value('kurs', $row->kurs),
				'jenis' => set_value('jenis', $row->jenis),
				'pembayaran_invoice' => set_value('pembayaran_invoice', $row->pembayaran_invoice),
				'pembayaran_tagihan' => set_value('pembayaran_tagihan', $row->pembayaran_tagihan),
			    );

            $data['page'] = 'metode-pembayaran/metode_pembayaran_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('metode-pembayaran'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('metode-pembayaran/update/'.post('metode_pembayaran_id', true)));
        } else {
            $data = array(
				'akun_id' => post('akun_id',true),
				'nama_metode_pembayaran' => post('nama_metode_pembayaran',true),
				'nama_bank' => post('nama_bank',true),
				'nomor_rekening' => post('nomor_rekening',true),
				'pemilik_rekening' => post('pemilik_rekening',true),
				'kurs' => post('kurs',true),
				'jenis' => post('jenis',true),
				'pembayaran_invoice' => post('pembayaran_invoice',true),
				'pembayaran_tagihan' => post('pembayaran_tagihan',true),
		    );

            $this->metode_pembayaran_model->update($data, post('metode_pembayaran_id', true));

            success('Update Record Success');
            redirect(base_url('metode-pembayaran/update/'.post('metode_pembayaran_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->metode_pembayaran_model->get($id);

        if ($row) {
            $this->metode_pembayaran_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('metode-pembayaran'));
        } else {

            warning('Record Not Found');
            redirect(base_url('metode-pembayaran'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('akun_id', 'akun', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_metode_pembayaran', 'nama metode pembayaran', 'trim|required');
		$this->form_validation->set_rules('nama_bank', 'nama bank', 'trim|required');
		// $this->form_validation->set_rules('nomor_rekening', 'nomor rekening', 'trim|required');
		// $this->form_validation->set_rules('pemilik_rekening', 'pemilik rekening', 'trim|required');
		// $this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
		// $this->form_validation->set_rules('jenis', 'jenis', 'trim|required');
		// $this->form_validation->set_rules('pembayaran_invoice', 'pembayaran invoice', 'trim|required');
		// $this->form_validation->set_rules('pembayaran_tagihan', 'pembayaran tagihan', 'trim|required');

		$this->form_validation->set_rules('metode_pembayaran_id', 'metode_pembayaran_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file metode_pembayaran.php */
/* Location: ./application/controllers/metode_pembayaran.php */
/* Please DO NOT modify this information : */