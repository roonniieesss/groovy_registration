<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class purchase_order_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('purchase_order_detail_model');
    }

    public function index()
    {
        $data['page'] = 'purchase-order-detail/purchase_order_detail_list';
        $data['title'] = 'List Purchase Order Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->purchase_order_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->purchase_order_detail_model->get($id);
        if ($row) {
            $data = array(
				'purchase_order_id' => $row->purchase_order_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'gudang_id' => $row->gudang_id,
				'qty' => $row->qty,
				'harga_satuan' => $row->harga_satuan,
				'satuan' => $row->satuan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'purchase-order-detail/purchase_order_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('purchase-order-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('purchase-order-detail/create-action'),
		    'purchase_order_id' => set_value('purchase_order_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'gudang_id' => set_value('gudang_id'),
		    'qty' => set_value('qty'),
		    'harga_satuan' => set_value('harga_satuan'),
		    'satuan' => set_value('satuan'),
		);
        $data['page'] = 'purchase-order-detail/purchase_order_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('purchase-order-detail/create'));
        } else {
            $data = array(
				'purchase_order_id' => post('purchase_order_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'gudang_id' => post('gudang_id',true),
				'qty' => post('qty',true),
				'harga_satuan' => post('harga_satuan',true),
				'satuan' => post('satuan',true),
		    );

            $this->purchase_order_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('purchase-order-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->purchase_order_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('purchase-order-detail/update-action'),
				'purchase_order_detail_id' => set_value('purchase_order_detail_id', $row->purchase_order_detail_id),
				'purchase_order_id' => set_value('purchase_order_id', $row->purchase_order_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'gudang_id' => set_value('gudang_id', $row->gudang_id),
				'qty' => set_value('qty', $row->qty),
				'harga_satuan' => set_value('harga_satuan', $row->harga_satuan),
				'satuan' => set_value('satuan', $row->satuan),
			    );

            $data['page'] = 'purchase-order-detail/purchase_order_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('purchase-order-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('purchase-order-detail/update/'.post('purchase_order_detail_id', true)));
        } else {
            $data = array(
				'purchase_order_id' => post('purchase_order_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'gudang_id' => post('gudang_id',true),
				'qty' => post('qty',true),
				'harga_satuan' => post('harga_satuan',true),
				'satuan' => post('satuan',true),
		    );

            $this->purchase_order_detail_model->update($data, post('purchase_order_detail_id', true));

            success('Update Record Success');
            redirect(base_url('purchase-order-detail/update/'.post('purchase_order_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->purchase_order_detail_model->get($id);

        if ($row) {
            $this->purchase_order_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('purchase-order-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('purchase-order-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('purchase_order_id', 'purchase order', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('gudang_id', 'gudang', 'trim|required|numeric');
		$this->form_validation->set_rules('qty', 'qty', 'trim|required|numeric');
		$this->form_validation->set_rules('harga_satuan', 'harga satuan', 'trim|required|numeric');
		$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');

		$this->form_validation->set_rules('purchase_order_detail_id', 'purchase_order_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file purchase_order_detail.php */
/* Location: ./application/controllers/purchase_order_detail.php */
/* Please DO NOT modify this information : */