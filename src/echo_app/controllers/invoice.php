<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class invoice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('invoice_model');
    }

    public function index()
    {
        $data['page'] = 'invoice/invoice_list';
        $data['title'] = 'List Invoice';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->invoice_model->json();
    }

    public function read($id)
    {
        $row = $this->invoice_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'pelanggan_id' => $row->pelanggan_id,
				'no_invoice' => $row->no_invoice,
				'keterangan_pembayaran' => $row->keterangan_pembayaran,
				'kurs' => $row->kurs,
				'total_ppn' => $row->total_ppn,
				'bea_materai' => $row->bea_materai,
				'total_tagihan' => $row->total_tagihan,
				'total_terbayar' => $row->total_terbayar,
				'tanggal_jatuh_tempo' => $row->tanggal_jatuh_tempo,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'invoice/invoice_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('invoice'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('invoice/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'pelanggan_id' => set_value('pelanggan_id'),
		    'no_invoice' => set_value('no_invoice'),
		    'keterangan_pembayaran' => set_value('keterangan_pembayaran'),
		    'kurs' => set_value('kurs'),
		    'total_ppn' => set_value('total_ppn'),
		    'bea_materai' => set_value('bea_materai'),
		    'total_tagihan' => set_value('total_tagihan'),
		    'total_terbayar' => set_value('total_terbayar'),
		    'tanggal_jatuh_tempo' => set_value('tanggal_jatuh_tempo'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'invoice/invoice_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('invoice/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'pelanggan_id' => post('pelanggan_id',true),
				'no_invoice' => post('no_invoice',true),
				'keterangan_pembayaran' => post('keterangan_pembayaran',true),
				'kurs' => post('kurs',true),
				'total_ppn' => post('total_ppn',true),
				'bea_materai' => post('bea_materai',true),
				'total_tagihan' => post('total_tagihan',true),
				'total_terbayar' => post('total_terbayar',true),
				'tanggal_jatuh_tempo' => post('tanggal_jatuh_tempo',true),
				'status' => post('status',true),
		    );

            $this->invoice_model->insert($data);

            success('Create Record Success');
            redirect(base_url('invoice'));
        }
    }

    public function update($id)
    {
        $row = $this->invoice_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('invoice/update-action'),
				'invoice_id' => set_value('invoice_id', $row->invoice_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'no_invoice' => set_value('no_invoice', $row->no_invoice),
				'keterangan_pembayaran' => set_value('keterangan_pembayaran', $row->keterangan_pembayaran),
				'kurs' => set_value('kurs', $row->kurs),
				'total_ppn' => set_value('total_ppn', $row->total_ppn),
				'bea_materai' => set_value('bea_materai', $row->bea_materai),
				'total_tagihan' => set_value('total_tagihan', $row->total_tagihan),
				'total_terbayar' => set_value('total_terbayar', $row->total_terbayar),
				'tanggal_jatuh_tempo' => set_value('tanggal_jatuh_tempo', $row->tanggal_jatuh_tempo),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'invoice/invoice_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('invoice'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('invoice/update/'.post('invoice_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'pelanggan_id' => post('pelanggan_id',true),
				'no_invoice' => post('no_invoice',true),
				'keterangan_pembayaran' => post('keterangan_pembayaran',true),
				'kurs' => post('kurs',true),
				'total_ppn' => post('total_ppn',true),
				'bea_materai' => post('bea_materai',true),
				'total_tagihan' => post('total_tagihan',true),
				'total_terbayar' => post('total_terbayar',true),
				'tanggal_jatuh_tempo' => post('tanggal_jatuh_tempo',true),
				'status' => post('status',true),
		    );

            $this->invoice_model->update($data, post('invoice_id', true));

            success('Update Record Success');
            redirect(base_url('invoice/update/'.post('invoice_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->invoice_model->get($id);

        if ($row) {
            $this->invoice_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('invoice'));
        } else {

            warning('Record Not Found');
            redirect(base_url('invoice'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('no_invoice', 'no invoice', 'trim|required');
		$this->form_validation->set_rules('keterangan_pembayaran', 'keterangan pembayaran', 'trim|required');
		$this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
		$this->form_validation->set_rules('total_ppn', 'total ppn', 'trim|required|numeric');
		$this->form_validation->set_rules('bea_materai', 'bea materai', 'trim|required|numeric');
		$this->form_validation->set_rules('total_tagihan', 'total tagihan', 'trim|required|numeric');
		$this->form_validation->set_rules('total_terbayar', 'total terbayar', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal_jatuh_tempo', 'tanggal jatuh tempo', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('invoice_id', 'invoice_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file invoice.php */
/* Location: ./application/controllers/invoice.php */
/* Please DO NOT modify this information : */