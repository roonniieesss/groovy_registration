<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kategori_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kategori_aset_model');
    }

    public function index()
    {
        $data['page'] = 'kategori-aset/kategori_aset_list';
        $data['title'] = 'List Kategori Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->kategori_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->kategori_aset_model->get($id);
        if ($row) {
            $data = array(
				'nama_kategori_aset' => $row->nama_kategori_aset,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'kategori-aset/kategori_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kategori-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('kategori-aset/create-action'),
		    'nama_kategori_aset' => set_value('nama_kategori_aset'),
		);
        $data['page'] = 'kategori-aset/kategori_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kategori-aset/create'));
        } else {
            $data = array(
				'nama_kategori_aset' => post('nama_kategori_aset',true),
		    );

            $this->kategori_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kategori-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->kategori_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('kategori-aset/update-action'),
				'kategori_aset_id' => set_value('kategori_aset_id', $row->kategori_aset_id),
				'nama_kategori_aset' => set_value('nama_kategori_aset', $row->nama_kategori_aset),
			    );

            $data['page'] = 'kategori-aset/kategori_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kategori-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kategori-aset/update/'.post('kategori_aset_id', true)));
        } else {
            $data = array(
				'nama_kategori_aset' => post('nama_kategori_aset',true),
		    );

            $this->kategori_aset_model->update($data, post('kategori_aset_id', true));

            success('Update Record Success');
            redirect(base_url('kategori-aset/update/'.post('kategori_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kategori_aset_model->get($id);

        if ($row) {
            $this->kategori_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kategori-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kategori-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_kategori_aset', 'nama kategori aset', 'trim|required');

		$this->form_validation->set_rules('kategori_aset_id', 'kategori_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file kategori_aset.php */
/* Location: ./application/controllers/kategori_aset.php */
/* Please DO NOT modify this information : */