<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class skema_harga extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('skema_harga_model');
    }

    public function index()
    {
        $data['page'] = 'skema-harga/skema_harga_list';
        $data['title'] = 'List Skema Harga';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->skema_harga_model->json();
    }

    public function read($id)
    {
        $row = $this->skema_harga_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'pelanggan_id' => $row->pelanggan_id,
				'tanggal_berlaku' => $row->tanggal_berlaku,
				'tanggal_habis_berlaku' => $row->tanggal_habis_berlaku,
				'no_kontrak' => $row->no_kontrak,
				'tanggal_kontrak' => $row->tanggal_kontrak,
				'pejabat_pembuat_komitmen' => $row->pejabat_pembuat_komitmen,
				'diskon_global' => $row->diskon_global,
				'keterangan' => $row->keterangan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'skema-harga/skema_harga_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('skema-harga'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('skema-harga/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'pelanggan_id' => set_value('pelanggan_id'),
		    'tanggal_berlaku' => set_value('tanggal_berlaku'),
		    'tanggal_habis_berlaku' => set_value('tanggal_habis_berlaku'),
		    'no_kontrak' => set_value('no_kontrak'),
		    'tanggal_kontrak' => set_value('tanggal_kontrak'),
		    'pejabat_pembuat_komitmen' => set_value('pejabat_pembuat_komitmen'),
		    'diskon_global' => set_value('diskon_global'),
		    'keterangan' => set_value('keterangan'),
		);
        $data['page'] = 'skema-harga/skema_harga_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('skema-harga/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'pelanggan_id' => post('pelanggan_id',true),
				'tanggal_berlaku' => post('tanggal_berlaku',true),
				'tanggal_habis_berlaku' => post('tanggal_habis_berlaku',true),
				'no_kontrak' => post('no_kontrak',true),
				'tanggal_kontrak' => post('tanggal_kontrak',true),
				'pejabat_pembuat_komitmen' => post('pejabat_pembuat_komitmen',true),
				'diskon_global' => post('diskon_global',true),
				'keterangan' => post('keterangan',true),
		    );

            $this->skema_harga_model->insert($data);

            success('Create Record Success');
            redirect(base_url('skema-harga'));
        }
    }

    public function update($id)
    {
        $row = $this->skema_harga_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('skema-harga/update-action'),
				'skema_harga_id' => set_value('skema_harga_id', $row->skema_harga_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'tanggal_berlaku' => set_value('tanggal_berlaku', $row->tanggal_berlaku),
				'tanggal_habis_berlaku' => set_value('tanggal_habis_berlaku', $row->tanggal_habis_berlaku),
				'no_kontrak' => set_value('no_kontrak', $row->no_kontrak),
				'tanggal_kontrak' => set_value('tanggal_kontrak', $row->tanggal_kontrak),
				'pejabat_pembuat_komitmen' => set_value('pejabat_pembuat_komitmen', $row->pejabat_pembuat_komitmen),
				'diskon_global' => set_value('diskon_global', $row->diskon_global),
				'keterangan' => set_value('keterangan', $row->keterangan),
			    );

            $data['page'] = 'skema-harga/skema_harga_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('skema-harga'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('skema-harga/update/'.post('skema_harga_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'pelanggan_id' => post('pelanggan_id',true),
				'tanggal_berlaku' => post('tanggal_berlaku',true),
				'tanggal_habis_berlaku' => post('tanggal_habis_berlaku',true),
				'no_kontrak' => post('no_kontrak',true),
				'tanggal_kontrak' => post('tanggal_kontrak',true),
				'pejabat_pembuat_komitmen' => post('pejabat_pembuat_komitmen',true),
				'diskon_global' => post('diskon_global',true),
				'keterangan' => post('keterangan',true),
		    );

            $this->skema_harga_model->update($data, post('skema_harga_id', true));

            success('Update Record Success');
            redirect(base_url('skema-harga/update/'.post('skema_harga_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->skema_harga_model->get($id);

        if ($row) {
            $this->skema_harga_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('skema-harga'));
        } else {

            warning('Record Not Found');
            redirect(base_url('skema-harga'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal_berlaku', 'tanggal berlaku', 'trim|required');
		$this->form_validation->set_rules('tanggal_habis_berlaku', 'tanggal habis berlaku', 'trim|required');
		$this->form_validation->set_rules('no_kontrak', 'no kontrak', 'trim|required');
		$this->form_validation->set_rules('tanggal_kontrak', 'tanggal kontrak', 'trim|required');
		$this->form_validation->set_rules('pejabat_pembuat_komitmen', 'pejabat pembuat komitmen', 'trim|required');
		$this->form_validation->set_rules('diskon_global', 'diskon global', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

		$this->form_validation->set_rules('skema_harga_id', 'skema_harga_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file skema_harga.php */
/* Location: ./application/controllers/skema_harga.php */
/* Please DO NOT modify this information : */