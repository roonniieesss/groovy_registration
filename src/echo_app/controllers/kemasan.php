<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kemasan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kemasan_model');
    }

    public function index()
    {
        $data['page'] = 'kemasan/kemasan_list';
        $data['title'] = 'List Kemasan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->kemasan_model->json();
    }

    public function read($id)
    {
        $row = $this->kemasan_model->get($id);
        if ($row) {
            $data = array(
				'nama_kemasan' => $row->nama_kemasan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'kemasan/kemasan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kemasan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('kemasan/create-action'),
		    'nama_kemasan' => set_value('nama_kemasan'),
		);
        $data['page'] = 'kemasan/kemasan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kemasan/create'));
        } else {
            $data = array(
				'nama_kemasan' => post('nama_kemasan',true),
		    );

            $this->kemasan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kemasan'));
        }
    }

    public function update($id)
    {
        $row = $this->kemasan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('kemasan/update-action'),
				'kemasan_id' => set_value('kemasan_id', $row->kemasan_id),
				'nama_kemasan' => set_value('nama_kemasan', $row->nama_kemasan),
			    );

            $data['page'] = 'kemasan/kemasan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kemasan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kemasan/update/'.post('kemasan_id', true)));
        } else {
            $data = array(
				'nama_kemasan' => post('nama_kemasan',true),
		    );

            $this->kemasan_model->update($data, post('kemasan_id', true));

            success('Update Record Success');
            redirect(base_url('kemasan/update/'.post('kemasan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kemasan_model->get($id);

        if ($row) {
            $this->kemasan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kemasan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kemasan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_kemasan', 'nama kemasan', 'trim|required');

		$this->form_validation->set_rules('kemasan_id', 'kemasan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file kemasan.php */
/* Location: ./application/controllers/kemasan.php */
/* Please DO NOT modify this information : */