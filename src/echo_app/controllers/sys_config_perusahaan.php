<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sys_config_perusahaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sys_config_perusahaan_model');
    }

    public function index()
    {
        $data['page'] = 'sys-config-perusahaan/sys_config_perusahaan_list';
        $data['title'] = 'List Sys Config Perusahaan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->sys_config_perusahaan_model->json();
    }

    public function read($id)
    {
        $row = $this->sys_config_perusahaan_model->get($id);
        if ($row) {
            $data = array(
				'value' => $row->value,
		    );
            $data['page'] = 'sys-config-perusahaan/sys_config_perusahaan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sys-config-perusahaan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('sys-config-perusahaan/create-action'),
		    'value' => set_value('value'),
		);
        $data['page'] = 'sys-config-perusahaan/sys_config_perusahaan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sys-config-perusahaan/create'));
        } else {
            $data = array(
				'value' => post('value',true),
		    );

            $this->sys_config_perusahaan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sys-config-perusahaan'));
        }
    }

    public function update($id)
    {
        $row = $this->sys_config_perusahaan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('sys-config-perusahaan/update-action'),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'value' => set_value('value', $row->value),
			    );

            $data['page'] = 'sys-config-perusahaan/sys_config_perusahaan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sys-config-perusahaan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sys-config-perusahaan/update/'.post('perusahaan_id', true)));
        } else {
            $data = array(
				'value' => post('value',true),
		    );

            $this->sys_config_perusahaan_model->update($data, post('perusahaan_id', true));

            success('Update Record Success');
            redirect(base_url('sys-config-perusahaan/update/'.post('perusahaan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sys_config_perusahaan_model->get($id);

        if ($row) {
            $this->sys_config_perusahaan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sys-config-perusahaan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sys-config-perusahaan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('value', 'value', 'trim|required');

		$this->form_validation->set_rules('perusahaan_id', 'perusahaan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file sys_config_perusahaan.php */
/* Location: ./application/controllers/sys_config_perusahaan.php */
/* Please DO NOT modify this information : */