<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_barang_model');
    }

    public function index()
    {
        $data['page']  = 'jenis-barang/jenis_barang_list';
        $data['title'] = 'List Jenis Barang';
        view('template', $data);
    }

    public function read($id)
    {
        $row = $this->jenis_barang_model->get($id);
        if ($row) {
            $data = array(
                'kategori_barang_id' => $row->kategori_barang_id,
                'satuan_id'          => $row->satuan_id,
                'kode_barang'        => $row->kode_barang,
                'kode_barcode'       => $row->kode_barcode,
                'nama_jenis_barang'  => $row->nama_jenis_barang,
                'spesifikasi'        => $row->spesifikasi,
                'warna'              => $row->warna,
                'ukuran'             => $row->ukuran,
                'harga'              => $row->harga,
                'batas_bawah_harga'  => $row->batas_bawah_harga,
                'klasifikasi_barang' => $row->klasifikasi_barang,
                'jenis_identifikasi' => $row->jenis_identifikasi,
                'dijual'             => $row->dijual,
                'pcf_stok'           => $row->pcf_stok,
                'pcf_stok_booked'    => $row->pcf_stok_booked,
                '__active'           => $row->__active,
                '__created'          => $row->__created,
                '__updated'          => $row->__updated,
                '__username'         => $row->__username,
            );
            $data['page'] = 'jenis-barang/jenis_barang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'             => 'Create',
            'action'             => base_url('jenis-barang/create-action'),
            'kategori_barang_id' => set_value('kategori_barang_id'),
            'satuan_id'          => set_value('satuan_id'),
            'kode_barang'        => set_value('kode_barang'),
            'kode_barcode'       => set_value('kode_barcode'),
            'nama_jenis_barang'  => set_value('nama_jenis_barang'),
            'spesifikasi'        => set_value('spesifikasi'),
            'warna'              => set_value('warna'),
            'ukuran'             => set_value('ukuran'),
            'harga'              => set_value('harga'),
            'batas_bawah_harga'  => set_value('batas_bawah_harga'),
            'klasifikasi_barang' => set_value('klasifikasi_barang'),
            'jenis_identifikasi' => set_value('jenis_identifikasi'),
            'dijual'             => set_value('dijual'),
            'pcf_stok'           => set_value('pcf_stok'),
            'pcf_stok_booked'    => set_value('pcf_stok_booked'),
        );
        $data['page'] = 'jenis-barang/jenis_barang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-barang/create'));
        } else {
            $data = array(
                'kategori_barang_id' => post('kategori_barang_id', true),
                'satuan_id'          => post('satuan_id', true),
                'kode_barang'        => post('kode_barang', true),
                'kode_barcode'       => post('kode_barcode', true),
                'nama_jenis_barang'  => post('nama_jenis_barang', true),
                'spesifikasi'        => post('spesifikasi', true),
                'warna'              => post('warna', true),
                'ukuran'             => post('ukuran', true),
                'harga'              => post('harga', true),
                'batas_bawah_harga'  => post('batas_bawah_harga', true),
                'klasifikasi_barang' => post('klasifikasi_barang', true),
                'jenis_identifikasi' => post('jenis_identifikasi', true),
                'dijual'             => post('dijual', true),
                'pcf_stok'           => post('pcf_stok', true),
                'pcf_stok_booked'    => post('pcf_stok_booked', true),
            );

            $this->jenis_barang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-barang'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_barang_model->get($id);

        if ($row) {
            $data = array(
                'button'             => 'Update',
                'action'             => base_url('jenis-barang/update-action'),
                'jenis_barang_id'    => set_value('jenis_barang_id', $row->jenis_barang_id),
                'kategori_barang_id' => set_value('kategori_barang_id', $row->kategori_barang_id),
                'satuan_id'          => set_value('satuan_id', $row->satuan_id),
                'kode_barang'        => set_value('kode_barang', $row->kode_barang),
                'kode_barcode'       => set_value('kode_barcode', $row->kode_barcode),
                'nama_jenis_barang'  => set_value('nama_jenis_barang', $row->nama_jenis_barang),
                'spesifikasi'        => set_value('spesifikasi', $row->spesifikasi),
                'warna'              => set_value('warna', $row->warna),
                'ukuran'             => set_value('ukuran', $row->ukuran),
                'harga'              => set_value('harga', $row->harga),
                'batas_bawah_harga'  => set_value('batas_bawah_harga', $row->batas_bawah_harga),
                'klasifikasi_barang' => set_value('klasifikasi_barang', $row->klasifikasi_barang),
                'jenis_identifikasi' => set_value('jenis_identifikasi', $row->jenis_identifikasi),
                'dijual'             => set_value('dijual', $row->dijual),
                'pcf_stok'           => set_value('pcf_stok', $row->pcf_stok),
                'pcf_stok_booked'    => set_value('pcf_stok_booked', $row->pcf_stok_booked),
            );

            $data['page'] = 'jenis-barang/jenis_barang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-barang/update/' . post('jenis_barang_id', true)));
        } else {
            $data = array(
                'kategori_barang_id' => post('kategori_barang_id', true),
                'satuan_id'          => post('satuan_id', true),
                'kode_barang'        => post('kode_barang', true),
                'kode_barcode'       => post('kode_barcode', true),
                'nama_jenis_barang'  => post('nama_jenis_barang', true),
                'spesifikasi'        => post('spesifikasi', true),
                'warna'              => post('warna', true),
                'ukuran'             => post('ukuran', true),
                'harga'              => post('harga', true),
                'batas_bawah_harga'  => post('batas_bawah_harga', true),
                'klasifikasi_barang' => post('klasifikasi_barang', true),
                'jenis_identifikasi' => post('jenis_identifikasi', true),
                'dijual'             => post('dijual', true),
                'pcf_stok'           => post('pcf_stok', true),
                'pcf_stok_booked'    => post('pcf_stok_booked', true),
            );

            $this->jenis_barang_model->update($data, post('jenis_barang_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-barang/update/' . post('jenis_barang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_barang_model->get($id);

        if ($row) {
            $this->jenis_barang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-barang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-barang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kategori_barang_id', 'kategori barang', 'trim|required|numeric');
        $this->form_validation->set_rules('satuan_id', 'satuan', 'trim|required|numeric');
        $this->form_validation->set_rules('kode_barang', 'kode barang', 'trim|required');
        $this->form_validation->set_rules('kode_barcode', 'kode barcode', 'trim|required');
        $this->form_validation->set_rules('nama_jenis_barang', 'nama jenis barang', 'trim|required');
        $this->form_validation->set_rules('spesifikasi', 'spesifikasi', 'trim|required');
        $this->form_validation->set_rules('warna', 'warna', 'trim|required');
        $this->form_validation->set_rules('ukuran', 'ukuran', 'trim|required');
        $this->form_validation->set_rules('harga', 'harga', 'trim|required|numeric');
        $this->form_validation->set_rules('batas_bawah_harga', 'batas bawah harga', 'trim|required|numeric');
        $this->form_validation->set_rules('klasifikasi_barang', 'klasifikasi barang', 'trim|required');
        $this->form_validation->set_rules('jenis_identifikasi', 'jenis entifikasi', 'trim|required');
        $this->form_validation->set_rules('dijual', 'dijual', 'trim|required');
        $this->form_validation->set_rules('pcf_stok', 'pcf stok', 'trim|required|numeric');
        $this->form_validation->set_rules('pcf_stok_booked', 'pcf stok booked', 'trim|required|numeric');

        $this->form_validation->set_rules('jenis_barang_id', 'jenis_barang_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function get_jenis_barang($token)
    {
		$data_barang = $this->jenis_barang_model->get_all();
		// dd($data_barang);
		echo $this->load->view('lookup/jenis_barang','',true);
    }

    public function json($type = '')
    {
		header('Content-Type: application/json');
		
		if($type == 'ajax'){
			echo $this->jenis_barang_model->ajax();
		}else{
			echo $this->jenis_barang_model->json();
		}

    }

}

/* End of file jenis_barang.php */
/* Location: ./application/controllers/jenis_barang.php */
/* Please DO NOT modify this information : */
