<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peminjaman_barang_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peminjaman_barang_detail_model');
    }

    public function index()
    {
        $data['page'] = 'peminjaman-barang-detail/peminjaman_barang_detail_list';
        $data['title'] = 'List Peminjaman Barang Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peminjaman_barang_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->peminjaman_barang_detail_model->get($id);
        if ($row) {
            $data = array(
				'peminjaman_barang_id' => $row->peminjaman_barang_id,
				'barang_id' => $row->barang_id,
				'transaksi_barang_keluar' => $row->transaksi_barang_keluar,
				'transaksi_barang_kembali' => $row->transaksi_barang_kembali,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peminjaman-barang-detail/peminjaman_barang_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peminjaman-barang-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peminjaman-barang-detail/create-action'),
		    'peminjaman_barang_id' => set_value('peminjaman_barang_id'),
		    'barang_id' => set_value('barang_id'),
		    'transaksi_barang_keluar' => set_value('transaksi_barang_keluar'),
		    'transaksi_barang_kembali' => set_value('transaksi_barang_kembali'),
		);
        $data['page'] = 'peminjaman-barang-detail/peminjaman_barang_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peminjaman-barang-detail/create'));
        } else {
            $data = array(
				'peminjaman_barang_id' => post('peminjaman_barang_id',true),
				'barang_id' => post('barang_id',true),
				'transaksi_barang_keluar' => post('transaksi_barang_keluar',true),
				'transaksi_barang_kembali' => post('transaksi_barang_kembali',true),
		    );

            $this->peminjaman_barang_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peminjaman-barang-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->peminjaman_barang_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peminjaman-barang-detail/update-action'),
				'peminjaman_barang_detail_id' => set_value('peminjaman_barang_detail_id', $row->peminjaman_barang_detail_id),
				'peminjaman_barang_id' => set_value('peminjaman_barang_id', $row->peminjaman_barang_id),
				'barang_id' => set_value('barang_id', $row->barang_id),
				'transaksi_barang_keluar' => set_value('transaksi_barang_keluar', $row->transaksi_barang_keluar),
				'transaksi_barang_kembali' => set_value('transaksi_barang_kembali', $row->transaksi_barang_kembali),
			    );

            $data['page'] = 'peminjaman-barang-detail/peminjaman_barang_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peminjaman-barang-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peminjaman-barang-detail/update/'.post('peminjaman_barang_detail_id', true)));
        } else {
            $data = array(
				'peminjaman_barang_id' => post('peminjaman_barang_id',true),
				'barang_id' => post('barang_id',true),
				'transaksi_barang_keluar' => post('transaksi_barang_keluar',true),
				'transaksi_barang_kembali' => post('transaksi_barang_kembali',true),
		    );

            $this->peminjaman_barang_detail_model->update($data, post('peminjaman_barang_detail_id', true));

            success('Update Record Success');
            redirect(base_url('peminjaman-barang-detail/update/'.post('peminjaman_barang_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peminjaman_barang_detail_model->get($id);

        if ($row) {
            $this->peminjaman_barang_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peminjaman-barang-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peminjaman-barang-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('peminjaman_barang_id', 'peminjaman barang', 'trim|required|numeric');
		$this->form_validation->set_rules('barang_id', 'barang', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_keluar', 'transaksi barang keluar', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_kembali', 'transaksi barang kembali', 'trim|required|numeric');

		$this->form_validation->set_rules('peminjaman_barang_detail_id', 'peminjaman_barang_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peminjaman_barang_detail.php */
/* Location: ./application/controllers/peminjaman_barang_detail.php */
/* Please DO NOT modify this information : */