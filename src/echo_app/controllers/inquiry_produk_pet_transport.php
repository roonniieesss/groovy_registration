<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class inquiry_produk_pet_transport extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('inquiry_produk_pet_transport_model');
    }

    public function index()
    {
        $data['page'] = 'inquiry-produk-pet-transport/inquiry_produk_pet_transport_list';
        $data['title'] = 'List Inquiry Produk Pet Transport';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->inquiry_produk_pet_transport_model->json();
    }

    public function read($id)
    {
        $row = $this->inquiry_produk_pet_transport_model->get($id);
        if ($row) {
            $data = array(
				'inquiry_produk_id' => $row->inquiry_produk_id,
				'asal_kota_id' => $row->asal_kota_id,
				'tujuan_kota_id' => $row->tujuan_kota_id,
				'moda_transportasi_id' => $row->moda_transportasi_id,
				'ras_hewan_id' => $row->ras_hewan_id,
				'nama_hewan' => $row->nama_hewan,
				'jenis_kelamin' => $row->jenis_kelamin,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'inquiry-produk-pet-transport/inquiry_produk_pet_transport_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry-produk-pet-transport'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('inquiry-produk-pet-transport/create-action'),
		    'inquiry_produk_id' => set_value('inquiry_produk_id'),
		    'asal_kota_id' => set_value('asal_kota_id'),
		    'tujuan_kota_id' => set_value('tujuan_kota_id'),
		    'moda_transportasi_id' => set_value('moda_transportasi_id'),
		    'ras_hewan_id' => set_value('ras_hewan_id'),
		    'nama_hewan' => set_value('nama_hewan'),
		    'jenis_kelamin' => set_value('jenis_kelamin'),
		);
        $data['page'] = 'inquiry-produk-pet-transport/inquiry_produk_pet_transport_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry-produk-pet-transport/create'));
        } else {
            $data = array(
				'inquiry_produk_id' => post('inquiry_produk_id',true),
				'asal_kota_id' => post('asal_kota_id',true),
				'tujuan_kota_id' => post('tujuan_kota_id',true),
				'moda_transportasi_id' => post('moda_transportasi_id',true),
				'ras_hewan_id' => post('ras_hewan_id',true),
				'nama_hewan' => post('nama_hewan',true),
				'jenis_kelamin' => post('jenis_kelamin',true),
		    );

            $this->inquiry_produk_pet_transport_model->insert($data);

            success('Create Record Success');
            redirect(base_url('inquiry-produk-pet-transport'));
        }
    }

    public function update($id)
    {
        $row = $this->inquiry_produk_pet_transport_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('inquiry-produk-pet-transport/update-action'),
				'inquiry_produk_pet_transport_id' => set_value('inquiry_produk_pet_transport_id', $row->inquiry_produk_pet_transport_id),
				'inquiry_produk_id' => set_value('inquiry_produk_id', $row->inquiry_produk_id),
				'asal_kota_id' => set_value('asal_kota_id', $row->asal_kota_id),
				'tujuan_kota_id' => set_value('tujuan_kota_id', $row->tujuan_kota_id),
				'moda_transportasi_id' => set_value('moda_transportasi_id', $row->moda_transportasi_id),
				'ras_hewan_id' => set_value('ras_hewan_id', $row->ras_hewan_id),
				'nama_hewan' => set_value('nama_hewan', $row->nama_hewan),
				'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
			    );

            $data['page'] = 'inquiry-produk-pet-transport/inquiry_produk_pet_transport_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry-produk-pet-transport'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry-produk-pet-transport/update/'.post('inquiry_produk_pet_transport_id', true)));
        } else {
            $data = array(
				'inquiry_produk_id' => post('inquiry_produk_id',true),
				'asal_kota_id' => post('asal_kota_id',true),
				'tujuan_kota_id' => post('tujuan_kota_id',true),
				'moda_transportasi_id' => post('moda_transportasi_id',true),
				'ras_hewan_id' => post('ras_hewan_id',true),
				'nama_hewan' => post('nama_hewan',true),
				'jenis_kelamin' => post('jenis_kelamin',true),
		    );

            $this->inquiry_produk_pet_transport_model->update($data, post('inquiry_produk_pet_transport_id', true));

            success('Update Record Success');
            redirect(base_url('inquiry-produk-pet-transport/update/'.post('inquiry_produk_pet_transport_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->inquiry_produk_pet_transport_model->get($id);

        if ($row) {
            $this->inquiry_produk_pet_transport_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('inquiry-produk-pet-transport'));
        } else {

            warning('Record Not Found');
            redirect(base_url('inquiry-produk-pet-transport'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('inquiry_produk_id', 'inquiry produk', 'trim|required|numeric');
		$this->form_validation->set_rules('asal_kota_id', 'asal kota', 'trim|required|numeric');
		$this->form_validation->set_rules('tujuan_kota_id', 'tujuan kota', 'trim|required|numeric');
		$this->form_validation->set_rules('moda_transportasi_id', 'moda transportasi', 'trim|required|numeric');
		$this->form_validation->set_rules('ras_hewan_id', 'ras hewan', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_hewan', 'nama hewan', 'trim|required');
		$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');

		$this->form_validation->set_rules('inquiry_produk_pet_transport_id', 'inquiry_produk_pet_transport_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file inquiry_produk_pet_transport.php */
/* Location: ./application/controllers/inquiry_produk_pet_transport.php */
/* Please DO NOT modify this information : */