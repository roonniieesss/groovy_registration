<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class supplier extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('supplier_model');
    }

    public function index()
    {
        $data['page'] = 'supplier/supplier_list';
        $data['title'] = 'List Supplier';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->supplier_model->json();
    }

    public function read($id)
    {
        $row = $this->supplier_model->get($id);
        if ($row) {
            $data = array(
				'nama_supplier' => $row->nama_supplier,
				'alamat_supplier' => $row->alamat_supplier,
				'npwp_supplier' => $row->npwp_supplier,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'supplier/supplier_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('supplier'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('supplier/create-action'),
		    'nama_supplier' => set_value('nama_supplier'),
		    'alamat_supplier' => set_value('alamat_supplier'),
		    'npwp_supplier' => set_value('npwp_supplier'),
		);
        $data['page'] = 'supplier/supplier_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('supplier/create'));
        } else {
            $data = array(
				'nama_supplier' => post('nama_supplier',true),
				'alamat_supplier' => post('alamat_supplier',true),
				'npwp_supplier' => post('npwp_supplier',true),
		    );

            $this->supplier_model->insert($data);

            success('Create Record Success');
            redirect(base_url('supplier'));
        }
    }

    public function update($id)
    {
        $row = $this->supplier_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('supplier/update-action'),
				'supplier_id' => set_value('supplier_id', $row->supplier_id),
				'nama_supplier' => set_value('nama_supplier', $row->nama_supplier),
				'alamat_supplier' => set_value('alamat_supplier', $row->alamat_supplier),
				'npwp_supplier' => set_value('npwp_supplier', $row->npwp_supplier),
			    );

            $data['page'] = 'supplier/supplier_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('supplier'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('supplier/update/'.post('supplier_id', true)));
        } else {
            $data = array(
				'nama_supplier' => post('nama_supplier',true),
				'alamat_supplier' => post('alamat_supplier',true),
				'npwp_supplier' => post('npwp_supplier',true),
		    );

            $this->supplier_model->update($data, post('supplier_id', true));

            success('Update Record Success');
            redirect(base_url('supplier/update/'.post('supplier_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->supplier_model->get($id);

        if ($row) {
            $this->supplier_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('supplier'));
        } else {

            warning('Record Not Found');
            redirect(base_url('supplier'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_supplier', 'nama supplier', 'trim|required');
		$this->form_validation->set_rules('alamat_supplier', 'alamat supplier', 'trim|required');
		$this->form_validation->set_rules('npwp_supplier', 'npwp supplier', 'trim|required');

		$this->form_validation->set_rules('supplier_id', 'supplier_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file supplier.php */
/* Location: ./application/controllers/supplier.php */
/* Please DO NOT modify this information : */