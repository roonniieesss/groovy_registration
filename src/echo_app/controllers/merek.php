<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class merek extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('merek_model');
    }

    public function index()
    {
        $data['page'] = 'merek/merek_list';
        $data['title'] = 'List Merek';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->merek_model->json();
    }

    public function read($id)
    {
        $row = $this->merek_model->get($id);
        if ($row) {
            $data = array(
				'nama_merek' => $row->nama_merek,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'merek/merek_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('merek'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('merek/create-action'),
		    'nama_merek' => set_value('nama_merek'),
		);
        $data['page'] = 'merek/merek_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('merek/create'));
        } else {
            $data = array(
				'nama_merek' => post('nama_merek',true),
		    );

            $this->merek_model->insert($data);

            success('Create Record Success');
            redirect(base_url('merek'));
        }
    }

    public function update($id)
    {
        $row = $this->merek_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('merek/update-action'),
				'merek_id' => set_value('merek_id', $row->merek_id),
				'nama_merek' => set_value('nama_merek', $row->nama_merek),
			    );

            $data['page'] = 'merek/merek_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('merek'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('merek/update/'.post('merek_id', true)));
        } else {
            $data = array(
				'nama_merek' => post('nama_merek',true),
		    );

            $this->merek_model->update($data, post('merek_id', true));

            success('Update Record Success');
            redirect(base_url('merek/update/'.post('merek_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->merek_model->get($id);

        if ($row) {
            $this->merek_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('merek'));
        } else {

            warning('Record Not Found');
            redirect(base_url('merek'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_merek', 'nama merek', 'trim|required');

		$this->form_validation->set_rules('merek_id', 'merek_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file merek.php */
/* Location: ./application/controllers/merek.php */
/* Please DO NOT modify this information : */