<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pindah_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pindah_aset_model');
    }

    public function index()
    {
        $data['page'] = 'pindah-aset/pindah_aset_list';
        $data['title'] = 'List Pindah Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pindah_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->pindah_aset_model->get($id);
        if ($row) {
            $data = array(
				'keperluan' => $row->keperluan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pindah-aset/pindah_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pindah-aset/create-action'),
		    'keperluan' => set_value('keperluan'),
		);
        $data['page'] = 'pindah-aset/pindah_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-aset/create'));
        } else {
            $data = array(
				'keperluan' => post('keperluan',true),
		    );

            $this->pindah_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pindah-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->pindah_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pindah-aset/update-action'),
				'pindah_aset_id' => set_value('pindah_aset_id', $row->pindah_aset_id),
				'keperluan' => set_value('keperluan', $row->keperluan),
			    );

            $data['page'] = 'pindah-aset/pindah_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-aset/update/'.post('pindah_aset_id', true)));
        } else {
            $data = array(
				'keperluan' => post('keperluan',true),
		    );

            $this->pindah_aset_model->update($data, post('pindah_aset_id', true));

            success('Update Record Success');
            redirect(base_url('pindah-aset/update/'.post('pindah_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pindah_aset_model->get($id);

        if ($row) {
            $this->pindah_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pindah-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pindah-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('keperluan', 'keperluan', 'trim|required');

		$this->form_validation->set_rules('pindah_aset_id', 'pindah_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pindah_aset.php */
/* Location: ./application/controllers/pindah_aset.php */
/* Please DO NOT modify this information : */