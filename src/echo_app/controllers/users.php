<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('users_model');
    }

    public function index()
    {
        $data['page'] = 'users/users_list';
        $data['title'] = 'List Users';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->users_model->json();
    }

    public function read($id)
    {
        $row = $this->users_model->get($id);
        if ($row) {
            $data = array(
				'username' => $row->username,
				'nama_users' => $row->nama_users,
				'email' => $row->email,
				'password' => $row->password,
				'status' => $row->status,
				'role' => $row->role,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'users/users_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('users'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('users/create-action'),
		    'username' => set_value('username'),
		    'nama_users' => set_value('nama_users'),
		    'email' => set_value('email'),
		    'password' => set_value('password'),
		    'status' => set_value('status'),
		    'role' => set_value('role'),
		);
        $data['page'] = 'users/users_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('users/create'));
        } else {
            $data = array(
				'username' => post('username',true),
				'nama_users' => post('nama_users',true),
				'email' => post('email',true),
				'password' => post('password',true),
				'status' => post('status',true),
				'role' => post('role',true),
		    );

            $this->users_model->insert($data);

            success('Create Record Success');
            redirect(base_url('users'));
        }
    }

    public function update($id)
    {
        $row = $this->users_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('users/update-action'),
				'users_id' => set_value('users_id', $row->users_id),
				'username' => set_value('username', $row->username),
				'nama_users' => set_value('nama_users', $row->nama_users),
				'email' => set_value('email', $row->email),
				'password' => set_value('password', $row->password),
				'status' => set_value('status', $row->status),
				'role' => set_value('role', $row->role),
			    );

            $data['page'] = 'users/users_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('users'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('users/update/'.post('users_id', true)));
        } else {
            $data = array(
				'username' => post('username',true),
				'nama_users' => post('nama_users',true),
				'email' => post('email',true),
				'password' => post('password',true),
				'status' => post('status',true),
				'role' => post('role',true),
		    );

            $this->users_model->update($data, post('users_id', true));

            success('Update Record Success');
            redirect(base_url('users/update/'.post('users_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->users_model->get($id);

        if ($row) {
            $this->users_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('users'));
        } else {

            warning('Record Not Found');
            redirect(base_url('users'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('nama_users', 'nama users', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('role', 'role', 'trim|required');

		$this->form_validation->set_rules('users_id', 'users_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file users.php */
/* Location: ./application/controllers/users.php */
/* Please DO NOT modify this information : */