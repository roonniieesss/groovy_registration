<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_layanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_layanan_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-layanan/jenis_layanan_list';
        $data['title'] = 'List Jenis Layanan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_layanan_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_layanan_model->get($id);
        if ($row) {
            $data = array(
				'jenis_barang_id' => $row->jenis_barang_id,
				'kategori_layanan_id' => $row->kategori_layanan_id,
				'pelaksana' => $row->pelaksana,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-layanan/jenis_layanan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-layanan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-layanan/create-action'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'kategori_layanan_id' => set_value('kategori_layanan_id'),
		    'pelaksana' => set_value('pelaksana'),
		);
        $data['page'] = 'jenis-layanan/jenis_layanan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-layanan/create'));
        } else {
            $data = array(
				'jenis_barang_id' => post('jenis_barang_id',true),
				'kategori_layanan_id' => post('kategori_layanan_id',true),
				'pelaksana' => post('pelaksana',true),
		    );

            $this->jenis_layanan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-layanan'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_layanan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-layanan/update-action'),
				'jenis_layanan_id' => set_value('jenis_layanan_id', $row->jenis_layanan_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'kategori_layanan_id' => set_value('kategori_layanan_id', $row->kategori_layanan_id),
				'pelaksana' => set_value('pelaksana', $row->pelaksana),
			    );

            $data['page'] = 'jenis-layanan/jenis_layanan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-layanan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-layanan/update/'.post('jenis_layanan_id', true)));
        } else {
            $data = array(
				'jenis_barang_id' => post('jenis_barang_id',true),
				'kategori_layanan_id' => post('kategori_layanan_id',true),
				'pelaksana' => post('pelaksana',true),
		    );

            $this->jenis_layanan_model->update($data, post('jenis_layanan_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-layanan/update/'.post('jenis_layanan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_layanan_model->get($id);

        if ($row) {
            $this->jenis_layanan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-layanan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-layanan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('kategori_layanan_id', 'kategori layanan', 'trim|required|numeric');
		$this->form_validation->set_rules('pelaksana', 'pelaksana', 'trim|required');

		$this->form_validation->set_rules('jenis_layanan_id', 'jenis_layanan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_layanan.php */
/* Location: ./application/controllers/jenis_layanan.php */
/* Please DO NOT modify this information : */