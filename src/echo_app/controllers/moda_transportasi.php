<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class moda_transportasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('moda_transportasi_model');
    }

    public function index()
    {
        $data['page'] = 'moda-transportasi/moda_transportasi_list';
        $data['title'] = 'List Moda Transportasi';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->moda_transportasi_model->json();
    }

    public function read($id)
    {
        $row = $this->moda_transportasi_model->get($id);
        if ($row) {
            $data = array(
				'nama_moda_transportasi' => $row->nama_moda_transportasi,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'moda-transportasi/moda_transportasi_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('moda-transportasi'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('moda-transportasi/create-action'),
		    'nama_moda_transportasi' => set_value('nama_moda_transportasi'),
		);
        $data['page'] = 'moda-transportasi/moda_transportasi_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('moda-transportasi/create'));
        } else {
            $data = array(
				'nama_moda_transportasi' => post('nama_moda_transportasi',true),
		    );

            $this->moda_transportasi_model->insert($data);

            success('Create Record Success');
            redirect(base_url('moda-transportasi'));
        }
    }

    public function update($id)
    {
        $row = $this->moda_transportasi_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('moda-transportasi/update-action'),
				'moda_transportasi_id' => set_value('moda_transportasi_id', $row->moda_transportasi_id),
				'nama_moda_transportasi' => set_value('nama_moda_transportasi', $row->nama_moda_transportasi),
			    );

            $data['page'] = 'moda-transportasi/moda_transportasi_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('moda-transportasi'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('moda-transportasi/update/'.post('moda_transportasi_id', true)));
        } else {
            $data = array(
				'nama_moda_transportasi' => post('nama_moda_transportasi',true),
		    );

            $this->moda_transportasi_model->update($data, post('moda_transportasi_id', true));

            success('Update Record Success');
            redirect(base_url('moda-transportasi/update/'.post('moda_transportasi_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->moda_transportasi_model->get($id);

        if ($row) {
            $this->moda_transportasi_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('moda-transportasi'));
        } else {

            warning('Record Not Found');
            redirect(base_url('moda-transportasi'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_moda_transportasi', 'nama moda transportasi', 'trim|required');

		$this->form_validation->set_rules('moda_transportasi_id', 'moda_transportasi_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file moda_transportasi.php */
/* Location: ./application/controllers/moda_transportasi.php */
/* Please DO NOT modify this information : */