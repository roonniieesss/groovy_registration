<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pindah_gudang_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pindah_gudang_detail_model');
    }

    public function index()
    {
        $data['page'] = 'pindah-gudang-detail/pindah_gudang_detail_list';
        $data['title'] = 'List Pindah Gudang Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pindah_gudang_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->pindah_gudang_detail_model->get($id);
        if ($row) {
            $data = array(
				'pindah_gudang_id' => $row->pindah_gudang_id,
				'barang_id' => $row->barang_id,
				'transaksi_barang_keluar' => $row->transaksi_barang_keluar,
				'transaksi_barang_masuk' => $row->transaksi_barang_masuk,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pindah-gudang-detail/pindah_gudang_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-gudang-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pindah-gudang-detail/create-action'),
		    'pindah_gudang_id' => set_value('pindah_gudang_id'),
		    'barang_id' => set_value('barang_id'),
		    'transaksi_barang_keluar' => set_value('transaksi_barang_keluar'),
		    'transaksi_barang_masuk' => set_value('transaksi_barang_masuk'),
		);
        $data['page'] = 'pindah-gudang-detail/pindah_gudang_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-gudang-detail/create'));
        } else {
            $data = array(
				'pindah_gudang_id' => post('pindah_gudang_id',true),
				'barang_id' => post('barang_id',true),
				'transaksi_barang_keluar' => post('transaksi_barang_keluar',true),
				'transaksi_barang_masuk' => post('transaksi_barang_masuk',true),
		    );

            $this->pindah_gudang_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pindah-gudang-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->pindah_gudang_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pindah-gudang-detail/update-action'),
				'pindah_gudang_detail_id' => set_value('pindah_gudang_detail_id', $row->pindah_gudang_detail_id),
				'pindah_gudang_id' => set_value('pindah_gudang_id', $row->pindah_gudang_id),
				'barang_id' => set_value('barang_id', $row->barang_id),
				'transaksi_barang_keluar' => set_value('transaksi_barang_keluar', $row->transaksi_barang_keluar),
				'transaksi_barang_masuk' => set_value('transaksi_barang_masuk', $row->transaksi_barang_masuk),
			    );

            $data['page'] = 'pindah-gudang-detail/pindah_gudang_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-gudang-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-gudang-detail/update/'.post('pindah_gudang_detail_id', true)));
        } else {
            $data = array(
				'pindah_gudang_id' => post('pindah_gudang_id',true),
				'barang_id' => post('barang_id',true),
				'transaksi_barang_keluar' => post('transaksi_barang_keluar',true),
				'transaksi_barang_masuk' => post('transaksi_barang_masuk',true),
		    );

            $this->pindah_gudang_detail_model->update($data, post('pindah_gudang_detail_id', true));

            success('Update Record Success');
            redirect(base_url('pindah-gudang-detail/update/'.post('pindah_gudang_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pindah_gudang_detail_model->get($id);

        if ($row) {
            $this->pindah_gudang_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pindah-gudang-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pindah-gudang-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pindah_gudang_id', 'pindah gudang', 'trim|required|numeric');
		$this->form_validation->set_rules('barang_id', 'barang', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_keluar', 'transaksi barang keluar', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_masuk', 'transaksi barang masuk', 'trim|required|numeric');

		$this->form_validation->set_rules('pindah_gudang_detail_id', 'pindah_gudang_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pindah_gudang_detail.php */
/* Location: ./application/controllers/pindah_gudang_detail.php */
/* Please DO NOT modify this information : */