<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_hewan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_hewan_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-hewan/jenis_hewan_list';
        $data['title'] = 'List Jenis Hewan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_hewan_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_hewan_model->get($id);
        if ($row) {
            $data = array(
				'nama_jenis_hewan' => $row->nama_jenis_hewan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-hewan/jenis_hewan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-hewan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-hewan/create-action'),
		    'nama_jenis_hewan' => set_value('nama_jenis_hewan'),
		);
        $data['page'] = 'jenis-hewan/jenis_hewan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-hewan/create'));
        } else {
            $data = array(
				'nama_jenis_hewan' => post('nama_jenis_hewan',true),
		    );

            $this->jenis_hewan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-hewan'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_hewan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-hewan/update-action'),
				'jenis_hewan_id' => set_value('jenis_hewan_id', $row->jenis_hewan_id),
				'nama_jenis_hewan' => set_value('nama_jenis_hewan', $row->nama_jenis_hewan),
			    );

            $data['page'] = 'jenis-hewan/jenis_hewan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-hewan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-hewan/update/'.post('jenis_hewan_id', true)));
        } else {
            $data = array(
				'nama_jenis_hewan' => post('nama_jenis_hewan',true),
		    );

            $this->jenis_hewan_model->update($data, post('jenis_hewan_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-hewan/update/'.post('jenis_hewan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_hewan_model->get($id);

        if ($row) {
            $this->jenis_hewan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-hewan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-hewan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_jenis_hewan', 'nama jenis hewan', 'trim|required');

		$this->form_validation->set_rules('jenis_hewan_id', 'jenis_hewan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_hewan.php */
/* Location: ./application/controllers/jenis_hewan.php */
/* Please DO NOT modify this information : */