<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peraturan_exim_ras_hewan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peraturan_exim_ras_hewan_model');
    }

    public function index()
    {
        $data['page'] = 'peraturan-exim-ras-hewan/peraturan_exim_ras_hewan_list';
        $data['title'] = 'List Peraturan Exim Ras Hewan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peraturan_exim_ras_hewan_model->json();
    }

    public function read($id)
    {
        $row = $this->peraturan_exim_ras_hewan_model->get($id);
        if ($row) {
            $data = array(
				'peraturan_exim_id' => $row->peraturan_exim_id,
				'ras_hewan_id' => $row->ras_hewan_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peraturan-exim-ras-hewan/peraturan_exim_ras_hewan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-ras-hewan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peraturan-exim-ras-hewan/create-action'),
		    'peraturan_exim_id' => set_value('peraturan_exim_id'),
		    'ras_hewan_id' => set_value('ras_hewan_id'),
		);
        $data['page'] = 'peraturan-exim-ras-hewan/peraturan_exim_ras_hewan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-ras-hewan/create'));
        } else {
            $data = array(
				'peraturan_exim_id' => post('peraturan_exim_id',true),
				'ras_hewan_id' => post('ras_hewan_id',true),
		    );

            $this->peraturan_exim_ras_hewan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peraturan-exim-ras-hewan'));
        }
    }

    public function update($id)
    {
        $row = $this->peraturan_exim_ras_hewan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peraturan-exim-ras-hewan/update-action'),
				'peraturan_exim_ras_hewan_id' => set_value('peraturan_exim_ras_hewan_id', $row->peraturan_exim_ras_hewan_id),
				'peraturan_exim_id' => set_value('peraturan_exim_id', $row->peraturan_exim_id),
				'ras_hewan_id' => set_value('ras_hewan_id', $row->ras_hewan_id),
			    );

            $data['page'] = 'peraturan-exim-ras-hewan/peraturan_exim_ras_hewan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-ras-hewan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-ras-hewan/update/'.post('peraturan_exim_ras_hewan_id', true)));
        } else {
            $data = array(
				'peraturan_exim_id' => post('peraturan_exim_id',true),
				'ras_hewan_id' => post('ras_hewan_id',true),
		    );

            $this->peraturan_exim_ras_hewan_model->update($data, post('peraturan_exim_ras_hewan_id', true));

            success('Update Record Success');
            redirect(base_url('peraturan-exim-ras-hewan/update/'.post('peraturan_exim_ras_hewan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peraturan_exim_ras_hewan_model->get($id);

        if ($row) {
            $this->peraturan_exim_ras_hewan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peraturan-exim-ras-hewan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peraturan-exim-ras-hewan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('peraturan_exim_id', 'peraturan exim', 'trim|required|numeric');
		$this->form_validation->set_rules('ras_hewan_id', 'ras hewan', 'trim|required|numeric');

		$this->form_validation->set_rules('peraturan_exim_ras_hewan_id', 'peraturan_exim_ras_hewan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peraturan_exim_ras_hewan.php */
/* Location: ./application/controllers/peraturan_exim_ras_hewan.php */
/* Please DO NOT modify this information : */