<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peraturan_exim extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peraturan_exim_model');
    }

    public function index()
    {
        $data['page'] = 'peraturan-exim/peraturan_exim_list';
        $data['title'] = 'List Peraturan Exim';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peraturan_exim_model->json();
    }

    public function read($id)
    {
        $row = $this->peraturan_exim_model->get($id);
        if ($row) {
            $data = array(
				'negara_id' => $row->negara_id,
				'arah' => $row->arah,
				'tanggal_berlaku' => $row->tanggal_berlaku,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peraturan-exim/peraturan_exim_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peraturan-exim/create-action'),
		    'negara_id' => set_value('negara_id'),
		    'arah' => set_value('arah'),
		    'tanggal_berlaku' => set_value('tanggal_berlaku'),
		);
        $data['page'] = 'peraturan-exim/peraturan_exim_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim/create'));
        } else {
            $data = array(
				'negara_id' => post('negara_id',true),
				'arah' => post('arah',true),
				'tanggal_berlaku' => post('tanggal_berlaku',true),
		    );

            $this->peraturan_exim_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peraturan-exim'));
        }
    }

    public function update($id)
    {
        $row = $this->peraturan_exim_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peraturan-exim/update-action'),
				'peraturan_exim_id' => set_value('peraturan_exim_id', $row->peraturan_exim_id),
				'negara_id' => set_value('negara_id', $row->negara_id),
				'arah' => set_value('arah', $row->arah),
				'tanggal_berlaku' => set_value('tanggal_berlaku', $row->tanggal_berlaku),
			    );

            $data['page'] = 'peraturan-exim/peraturan_exim_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim/update/'.post('peraturan_exim_id', true)));
        } else {
            $data = array(
				'negara_id' => post('negara_id',true),
				'arah' => post('arah',true),
				'tanggal_berlaku' => post('tanggal_berlaku',true),
		    );

            $this->peraturan_exim_model->update($data, post('peraturan_exim_id', true));

            success('Update Record Success');
            redirect(base_url('peraturan-exim/update/'.post('peraturan_exim_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peraturan_exim_model->get($id);

        if ($row) {
            $this->peraturan_exim_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peraturan-exim'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peraturan-exim'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('negara_id', 'negara', 'trim|required|numeric');
		$this->form_validation->set_rules('arah', 'arah', 'trim|required');
		$this->form_validation->set_rules('tanggal_berlaku', 'tanggal berlaku', 'trim|required');

		$this->form_validation->set_rules('peraturan_exim_id', 'peraturan_exim_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peraturan_exim.php */
/* Location: ./application/controllers/peraturan_exim.php */
/* Please DO NOT modify this information : */