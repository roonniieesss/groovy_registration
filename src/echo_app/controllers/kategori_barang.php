<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kategori_barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kategori_barang_model');
    }

    public function index()
    {
        $data['page']  = 'kategori-barang/kategori_barang_list';
        $data['title'] = 'List Kategori Barang';
        view('template', $data);
    }

    public function read($id)
    {
        $row = $this->kategori_barang_model->get($id);
        if ($row) {
            $data = array(
                'kategori_barang' => $row->kategori_barang,
                'dapat_dijual'    => $row->dapat_dijual,
                '__active'        => $row->__active,
                '__created'       => $row->__created,
                '__updated'       => $row->__updated,
                '__username'      => $row->__username,
            );
            $data['page'] = 'kategori-barang/kategori_barang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kategori-barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'          => 'Create',
            'action'          => base_url('kategori-barang/create-action'),
            'kategori_barang' => set_value('kategori_barang'),
            'dapat_dijual'    => set_value('dapat_dijual'),
        );
        $data['page'] = 'kategori-barang/kategori_barang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kategori-barang/create'));
        } else {
            $data = array(
                'kategori_barang' => post('kategori_barang', true),
                'dapat_dijual'    => post('dapat_dijual', true),
            );

            $this->kategori_barang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kategori-barang'));
        }
    }

    public function update($id)
    {
        $row = $this->kategori_barang_model->get($id);

        if ($row) {
            $data = array(
                'button'             => 'Update',
                'action'             => base_url('kategori-barang/update-action'),
                'kategori_barang_id' => set_value('kategori_barang_id', $row->kategori_barang_id),
                'kategori_barang'    => set_value('kategori_barang', $row->kategori_barang),
                'dapat_dijual'       => set_value('dapat_dijual', $row->dapat_dijual),
            );

            $data['page'] = 'kategori-barang/kategori_barang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kategori-barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kategori-barang/update/' . post('kategori_barang_id', true)));
        } else {
            $data = array(
                'kategori_barang' => post('kategori_barang', true),
                'dapat_dijual'    => post('dapat_dijual', true),
            );

            $this->kategori_barang_model->update($data, post('kategori_barang_id', true));

            success('Update Record Success');
            redirect(base_url('kategori-barang/update/' . post('kategori_barang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kategori_barang_model->get($id);

        if ($row) {
            $this->kategori_barang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kategori-barang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kategori-barang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kategori_barang', 'kategori barang', 'trim|required');
        $this->form_validation->set_rules('dapat_dijual', 'dapat dijual', 'trim|required');

        $this->form_validation->set_rules('kategori_barang_id', 'kategori_barang_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    // AJAX BASE
    public function json()
    {
        header('Content-Type: application/json');
        echo $this->kategori_barang_model->json();
    }

    public function get()
    {
        // header('Content-Type: application/json');
        // return $this->kategori_barang_model->get_all();

        if(is_post_request()){
            $mod = model('jenis_barang_model');
            
            $applied_where = array();
            $where = post('where');

            // Filter unwanted where fields
            foreach($where as $criteria){
                if(isset($mod->searchable[$criteria['field']]) && $criteria['keyword'] != ''){
                    $applied_where[$criteria['field']] = $criteria['keyword'];
                }
            }

            $cfg = new StdClass();
            $cfg->rowPerPage = 10;
            
            $mod->terapkanConfig($cfg);
            
            $applied_where['jb.__active'] = 1;

            $datasource = $mod->select($this->auth, $applied_where);
            
            echo json_encode($datasource);
        }else{
            echo 'INVALID METHOD';
        }
    }

}

/* End of file kategori_barang.php */
/* Location: ./application/controllers/kategori_barang.php */
/* Please DO NOT modify this information : */
