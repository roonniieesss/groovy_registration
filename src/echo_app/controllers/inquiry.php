<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class inquiry extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('inquiry_model');
        model('peraturan_exim_ras_hewan_model', 'aturan_ras_hewan');

    }

    public function index()
    {
        $data['page']  = 'inquiry/inquiry_list';
        $data['title'] = 'List Inquiry';
        view('template', $data);
    }

    public function read($id)
    {
        $row = $this->inquiry_model->getDetailInquiry($id);
        if ($row) {
            $data['inquiry'] = $row;
            $data['page']    = 'inquiry/inquiry_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry'));
        }
    }

    public function create()
    {
        $data = array(
            'button'       => 'Create',
            'action'       => base_url('inquiry/create-action'),
            'pelanggan_id' => set_value('pelanggan_id'),
            'no_inquiry'   => set_value('no_inquiry'),
        );
        $data['page'] = 'inquiry/inquiry_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry/create'));
        } else {
        $this->inquiry_model->insertInquiry();

        success('Create Record Success');
        redirect(base_url('inquiry'));
        }
    }

    public function update($id)
    {
        $row = $this->inquiry_model->get($id);

        if ($row) {
            $data = array(
                'button'       => 'Update',
                'action'       => base_url('inquiry/update-action'),
                'inquiry_id'   => set_value('inquiry_id', $row->inquiry_id),
                'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
                'no_inquiry'   => set_value('no_inquiry', $row->no_inquiry),
            );

            $data['page'] = 'inquiry/inquiry_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry/update/' . post('inquiry_id', true)));
        } else {
            $data = array(
                'pelanggan_id' => post('pelanggan_id', true),
                'no_inquiry'   => post('no_inquiry', true),
            );

            $this->inquiry_model->update($data, post('inquiry_id', true));

            success('Update Record Success');
            redirect(base_url('nquiry/update/' . post('inquiry_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->inquiry_model->get($id);

        if ($row) {
            $this->inquiry_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('inquiry'));
        } else {

            warning('Record Not Found');
            redirect(base_url('inquiry'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
        $this->form_validation->set_rules('no_inquiry', 'no inquiry', 'trim|required');

        $this->form_validation->set_rules('inquiry_id', 'inquiry_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    /**
     * Ajax Base
     */

    public function json()
    {
        // header('Content-Type: application/json');
        echo $this->inquiry_model->json();
    }

    public function get_peraturan_hewan()
    {
        $ras_hewan_id = post('ras_hewan_id');
        // header('Content-Type: application/json');
        echo $this->aturan_ras_hewan->get_peraturan_hewan($ras_hewan_id);
    }

}

/* End of file inquiry.php */
/* Location: ./application/controllers/inquiry.php */
/* Please DO NOT modify this information : */
