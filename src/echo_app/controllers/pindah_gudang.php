<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pindah_gudang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pindah_gudang_model');
    }

    public function index()
    {
        $data['page'] = 'pindah-gudang/pindah_gudang_list';
        $data['title'] = 'List Pindah Gudang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pindah_gudang_model->json();
    }

    public function read($id)
    {
        $row = $this->pindah_gudang_model->get($id);
        if ($row) {
            $data = array(
				'gudang_asal' => $row->gudang_asal,
				'gudang_tujuan' => $row->gudang_tujuan,
				'tanggal_kirim' => $row->tanggal_kirim,
				'tanggal_terima' => $row->tanggal_terima,
				'catatan_pengirim' => $row->catatan_pengirim,
				'catatan_penerima' => $row->catatan_penerima,
				'jenis' => $row->jenis,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pindah-gudang/pindah_gudang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-gudang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pindah-gudang/create-action'),
		    'gudang_asal' => set_value('gudang_asal'),
		    'gudang_tujuan' => set_value('gudang_tujuan'),
		    'tanggal_kirim' => set_value('tanggal_kirim'),
		    'tanggal_terima' => set_value('tanggal_terima'),
		    'catatan_pengirim' => set_value('catatan_pengirim'),
		    'catatan_penerima' => set_value('catatan_penerima'),
		    'jenis' => set_value('jenis'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'pindah-gudang/pindah_gudang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-gudang/create'));
        } else {
            $data = array(
				'gudang_asal' => post('gudang_asal',true),
				'gudang_tujuan' => post('gudang_tujuan',true),
				'tanggal_kirim' => post('tanggal_kirim',true),
				'tanggal_terima' => post('tanggal_terima',true),
				'catatan_pengirim' => post('catatan_pengirim',true),
				'catatan_penerima' => post('catatan_penerima',true),
				'jenis' => post('jenis',true),
				'status' => post('status',true),
		    );

            $this->pindah_gudang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pindah-gudang'));
        }
    }

    public function update($id)
    {
        $row = $this->pindah_gudang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pindah-gudang/update-action'),
				'pindah_gudang_id' => set_value('pindah_gudang_id', $row->pindah_gudang_id),
				'gudang_asal' => set_value('gudang_asal', $row->gudang_asal),
				'gudang_tujuan' => set_value('gudang_tujuan', $row->gudang_tujuan),
				'tanggal_kirim' => set_value('tanggal_kirim', $row->tanggal_kirim),
				'tanggal_terima' => set_value('tanggal_terima', $row->tanggal_terima),
				'catatan_pengirim' => set_value('catatan_pengirim', $row->catatan_pengirim),
				'catatan_penerima' => set_value('catatan_penerima', $row->catatan_penerima),
				'jenis' => set_value('jenis', $row->jenis),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'pindah-gudang/pindah_gudang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-gudang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-gudang/update/'.post('pindah_gudang_id', true)));
        } else {
            $data = array(
				'gudang_asal' => post('gudang_asal',true),
				'gudang_tujuan' => post('gudang_tujuan',true),
				'tanggal_kirim' => post('tanggal_kirim',true),
				'tanggal_terima' => post('tanggal_terima',true),
				'catatan_pengirim' => post('catatan_pengirim',true),
				'catatan_penerima' => post('catatan_penerima',true),
				'jenis' => post('jenis',true),
				'status' => post('status',true),
		    );

            $this->pindah_gudang_model->update($data, post('pindah_gudang_id', true));

            success('Update Record Success');
            redirect(base_url('pindah-gudang/update/'.post('pindah_gudang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pindah_gudang_model->get($id);

        if ($row) {
            $this->pindah_gudang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pindah-gudang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pindah-gudang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('gudang_asal', 'gudang asal', 'trim|required|numeric');
		$this->form_validation->set_rules('gudang_tujuan', 'gudang tujuan', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal_kirim', 'tanggal kirim', 'trim|required');
		$this->form_validation->set_rules('tanggal_terima', 'tanggal terima', 'trim|required');
		$this->form_validation->set_rules('catatan_pengirim', 'catatan pengirim', 'trim|required');
		$this->form_validation->set_rules('catatan_penerima', 'catatan penerima', 'trim|required');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('pindah_gudang_id', 'pindah_gudang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pindah_gudang.php */
/* Location: ./application/controllers/pindah_gudang.php */
/* Please DO NOT modify this information : */