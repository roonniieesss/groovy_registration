<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class paket_layanan_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('paket_layanan_detail_model');
    }

    public function index()
    {
        $data['page'] = 'paket-layanan-detail/paket_layanan_detail_list';
        $data['title'] = 'List Paket Layanan Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->paket_layanan_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->paket_layanan_detail_model->get($id);
        if ($row) {
            $data = array(
				'paket_layanan_id' => $row->paket_layanan_id,
				'jenis_layanan_id' => $row->jenis_layanan_id,
				'group_pilihan' => $row->group_pilihan,
				'qty' => $row->qty,
				'persentase_harga_custom' => $row->persentase_harga_custom,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'paket-layanan-detail/paket_layanan_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('paket-layanan-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('paket-layanan-detail/create-action'),
		    'paket_layanan_id' => set_value('paket_layanan_id'),
		    'jenis_layanan_id' => set_value('jenis_layanan_id'),
		    'group_pilihan' => set_value('group_pilihan'),
		    'qty' => set_value('qty'),
		    'persentase_harga_custom' => set_value('persentase_harga_custom'),
		);
        $data['page'] = 'paket-layanan-detail/paket_layanan_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('paket-layanan-detail/create'));
        } else {
            $data = array(
				'paket_layanan_id' => post('paket_layanan_id',true),
				'jenis_layanan_id' => post('jenis_layanan_id',true),
				'group_pilihan' => post('group_pilihan',true),
				'qty' => post('qty',true),
				'persentase_harga_custom' => post('persentase_harga_custom',true),
		    );

            $this->paket_layanan_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('paket-layanan-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->paket_layanan_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('paket-layanan-detail/update-action'),
				'paket_layanan_detail_id' => set_value('paket_layanan_detail_id', $row->paket_layanan_detail_id),
				'paket_layanan_id' => set_value('paket_layanan_id', $row->paket_layanan_id),
				'jenis_layanan_id' => set_value('jenis_layanan_id', $row->jenis_layanan_id),
				'group_pilihan' => set_value('group_pilihan', $row->group_pilihan),
				'qty' => set_value('qty', $row->qty),
				'persentase_harga_custom' => set_value('persentase_harga_custom', $row->persentase_harga_custom),
			    );

            $data['page'] = 'paket-layanan-detail/paket_layanan_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('paket-layanan-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('paket-layanan-detail/update/'.post('paket_layanan_detail_id', true)));
        } else {
            $data = array(
				'paket_layanan_id' => post('paket_layanan_id',true),
				'jenis_layanan_id' => post('jenis_layanan_id',true),
				'group_pilihan' => post('group_pilihan',true),
				'qty' => post('qty',true),
				'persentase_harga_custom' => post('persentase_harga_custom',true),
		    );

            $this->paket_layanan_detail_model->update($data, post('paket_layanan_detail_id', true));

            success('Update Record Success');
            redirect(base_url('paket-layanan-detail/update/'.post('paket_layanan_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->paket_layanan_detail_model->get($id);

        if ($row) {
            $this->paket_layanan_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('paket-layanan-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('paket-layanan-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('paket_layanan_id', 'paket layanan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_layanan_id', 'jenis layanan', 'trim|required|numeric');
		$this->form_validation->set_rules('group_pilihan', 'group pilihan', 'trim|required');
		$this->form_validation->set_rules('qty', 'qty', 'trim|required|numeric');
		$this->form_validation->set_rules('persentase_harga_custom', 'persentase harga custom', 'trim|required');

		$this->form_validation->set_rules('paket_layanan_detail_id', 'paket_layanan_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file paket_layanan_detail.php */
/* Location: ./application/controllers/paket_layanan_detail.php */
/* Please DO NOT modify this information : */