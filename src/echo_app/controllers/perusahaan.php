<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class perusahaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('perusahaan_model');
    }

    public function index()
    {
        $data['page'] = 'perusahaan/perusahaan_list';
        $data['title'] = 'List Perusahaan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->perusahaan_model->json();
    }

    public function read($id)
    {
        $row = $this->perusahaan_model->get($id);
        if ($row) {
            $data = array(
				'nama_perusahaan' => $row->nama_perusahaan,
				'alamat_perusahaan' => $row->alamat_perusahaan,
				'telp_perusahaan' => $row->telp_perusahaan,
				'fax_perusahaan' => $row->fax_perusahaan,
				'npwp' => $row->npwp,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'perusahaan/perusahaan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('perusahaan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('perusahaan/create-action'),
		    'nama_perusahaan' => set_value('nama_perusahaan'),
		    'alamat_perusahaan' => set_value('alamat_perusahaan'),
		    'telp_perusahaan' => set_value('telp_perusahaan'),
		    'fax_perusahaan' => set_value('fax_perusahaan'),
		    'npwp' => set_value('npwp'),
		);
        $data['page'] = 'perusahaan/perusahaan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('perusahaan/create'));
        } else {
            $data = array(
				'nama_perusahaan' => post('nama_perusahaan',true),
				'alamat_perusahaan' => post('alamat_perusahaan',true),
				'telp_perusahaan' => post('telp_perusahaan',true),
				'fax_perusahaan' => post('fax_perusahaan',true),
				'npwp' => post('npwp',true),
		    );

            $this->perusahaan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('perusahaan'));
        }
    }

    public function update($id)
    {
        $row = $this->perusahaan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('perusahaan/update-action'),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'nama_perusahaan' => set_value('nama_perusahaan', $row->nama_perusahaan),
				'alamat_perusahaan' => set_value('alamat_perusahaan', $row->alamat_perusahaan),
				'telp_perusahaan' => set_value('telp_perusahaan', $row->telp_perusahaan),
				'fax_perusahaan' => set_value('fax_perusahaan', $row->fax_perusahaan),
				'npwp' => set_value('npwp', $row->npwp),
			    );

            $data['page'] = 'perusahaan/perusahaan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('perusahaan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('perusahaan/update/'.post('perusahaan_id', true)));
        } else {
            $data = array(
				'nama_perusahaan' => post('nama_perusahaan',true),
				'alamat_perusahaan' => post('alamat_perusahaan',true),
				'telp_perusahaan' => post('telp_perusahaan',true),
				'fax_perusahaan' => post('fax_perusahaan',true),
				'npwp' => post('npwp',true),
		    );

            $this->perusahaan_model->update($data, post('perusahaan_id', true));

            success('Update Record Success');
            redirect(base_url('perusahaan/update/'.post('perusahaan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->perusahaan_model->get($id);

        if ($row) {
            $this->perusahaan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('perusahaan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('perusahaan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_perusahaan', 'nama perusahaan', 'trim|required');
		$this->form_validation->set_rules('alamat_perusahaan', 'alamat perusahaan', 'trim|required');
		$this->form_validation->set_rules('telp_perusahaan', 'telp perusahaan', 'trim|required');
		$this->form_validation->set_rules('fax_perusahaan', 'fax perusahaan', 'trim|required');
		$this->form_validation->set_rules('npwp', 'npwp', 'trim|required');

		$this->form_validation->set_rules('perusahaan_id', 'perusahaan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file perusahaan.php */
/* Location: ./application/controllers/perusahaan.php */
/* Please DO NOT modify this information : */