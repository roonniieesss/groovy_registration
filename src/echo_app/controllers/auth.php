<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // model('inquiry_model');
        // model('peraturan_exim_ras_hewan_model', 'aturan_ras_hewan');
        // model('auth_model');
    }
    
    function index()
    {                
        view('front/login');
    }

    function login(){
    	$user		=	$this->input->post('user');
		$pass		=	$this->input->post('pass');
		// dd(array($user,$pass));		
		if(post()){	
			model('auth_model');
			$hsl = $this->auth_model->login($user,$pass);
			// dd($hsl[0]->role);
			// dd($hsl);
			if($hsl !== 0){			
				$this->session->set_userdata(array(
					'status_login'=>'oke', 
					'user'=>$user,
					'role'=>$hsl[0]->role,
					'name'=>$hsl[0]->nama_users
				));
				redirect ('dashboard');
			}else{
				// warning('Incorrect Username Or Password');
				redirect('auth');
			}
		}
    }

    function logout(){
    	$this->session->sess_destroy();
    	redirect('auth');
    }

}
/* End of file inquiry.php */
/* Location: ./application/controllers/inquiry.php */
/* Please DO NOT modify this information : */
