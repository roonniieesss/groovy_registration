<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class buku_besar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('buku_besar_model');
    }

    public function index()
    {
        $data['page'] = 'buku-besar/buku_besar_list';
        $data['title'] = 'List Buku Besar';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->buku_besar_model->json();
    }

    public function read($id)
    {
        $row = $this->buku_besar_model->get($id);
        if ($row) {
            $data = array(
				'transaksi_keuangan_id' => $row->transaksi_keuangan_id,
				'akun_id' => $row->akun_id,
				'kurs' => $row->kurs,
				'nilai' => $row->nilai,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'buku-besar/buku_besar_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('buku-besar'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('buku-besar/create-action'),
		    'transaksi_keuangan_id' => set_value('transaksi_keuangan_id'),
		    'akun_id' => set_value('akun_id'),
		    'kurs' => set_value('kurs'),
		    'nilai' => set_value('nilai'),
		);
        $data['page'] = 'buku-besar/buku_besar_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('buku-besar/create'));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'akun_id' => post('akun_id',true),
				'kurs' => post('kurs',true),
				'nilai' => post('nilai',true),
		    );

            $this->buku_besar_model->insert($data);

            success('Create Record Success');
            redirect(base_url('buku-besar'));
        }
    }

    public function update($id)
    {
        $row = $this->buku_besar_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('buku-besar/update-action'),
				'buku_besar_id' => set_value('buku_besar_id', $row->buku_besar_id),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'akun_id' => set_value('akun_id', $row->akun_id),
				'kurs' => set_value('kurs', $row->kurs),
				'nilai' => set_value('nilai', $row->nilai),
			    );

            $data['page'] = 'buku-besar/buku_besar_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('buku-besar'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('buku-besar/update/'.post('buku_besar_id', true)));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'akun_id' => post('akun_id',true),
				'kurs' => post('kurs',true),
				'nilai' => post('nilai',true),
		    );

            $this->buku_besar_model->update($data, post('buku_besar_id', true));

            success('Update Record Success');
            redirect(base_url('buku-besar/update/'.post('buku_besar_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->buku_besar_model->get($id);

        if ($row) {
            $this->buku_besar_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('buku-besar'));
        } else {

            warning('Record Not Found');
            redirect(base_url('buku-besar'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi keuangan', 'trim|required|numeric');
		$this->form_validation->set_rules('akun_id', 'akun', 'trim|required|numeric');
		$this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
		$this->form_validation->set_rules('nilai', 'nilai', 'trim|required|numeric');

		$this->form_validation->set_rules('buku_besar_id', 'buku_besar_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file buku_besar.php */
/* Location: ./application/controllers/buku_besar.php */
/* Please DO NOT modify this information : */