<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class penerimaan_barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('penerimaan_barang_model');
    }

    public function index()
    {
        $data['page'] = 'penerimaan-barang/penerimaan_barang_list';
        $data['title'] = 'List Penerimaan Barang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->penerimaan_barang_model->json();
    }

    public function read($id)
    {
        $row = $this->penerimaan_barang_model->get($id);
        if ($row) {
            $data = array(
				'purchase_order_id' => $row->purchase_order_id,
				'gudang_id' => $row->gudang_id,
				'no_surat_jalan' => $row->no_surat_jalan,
				'tanggal_surat_jalan' => $row->tanggal_surat_jalan,
				'tanggal_terima' => $row->tanggal_terima,
				'nama_supir' => $row->nama_supir,
				'telp_supir' => $row->telp_supir,
				'catatan_gudang' => $row->catatan_gudang,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'penerimaan-barang/penerimaan_barang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('penerimaan-barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('penerimaan-barang/create-action'),
		    'purchase_order_id' => set_value('purchase_order_id'),
		    'gudang_id' => set_value('gudang_id'),
		    'no_surat_jalan' => set_value('no_surat_jalan'),
		    'tanggal_surat_jalan' => set_value('tanggal_surat_jalan'),
		    'tanggal_terima' => set_value('tanggal_terima'),
		    'nama_supir' => set_value('nama_supir'),
		    'telp_supir' => set_value('telp_supir'),
		    'catatan_gudang' => set_value('catatan_gudang'),
		);
        $data['page'] = 'penerimaan-barang/penerimaan_barang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penerimaan-barang/create'));
        } else {
            $data = array(
				'purchase_order_id' => post('purchase_order_id',true),
				'gudang_id' => post('gudang_id',true),
				'no_surat_jalan' => post('no_surat_jalan',true),
				'tanggal_surat_jalan' => post('tanggal_surat_jalan',true),
				'tanggal_terima' => post('tanggal_terima',true),
				'nama_supir' => post('nama_supir',true),
				'telp_supir' => post('telp_supir',true),
				'catatan_gudang' => post('catatan_gudang',true),
		    );

            $this->penerimaan_barang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('penerimaan-barang'));
        }
    }

    public function update($id)
    {
        $row = $this->penerimaan_barang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('penerimaan-barang/update-action'),
				'penerimaan_barang_id' => set_value('penerimaan_barang_id', $row->penerimaan_barang_id),
				'purchase_order_id' => set_value('purchase_order_id', $row->purchase_order_id),
				'gudang_id' => set_value('gudang_id', $row->gudang_id),
				'no_surat_jalan' => set_value('no_surat_jalan', $row->no_surat_jalan),
				'tanggal_surat_jalan' => set_value('tanggal_surat_jalan', $row->tanggal_surat_jalan),
				'tanggal_terima' => set_value('tanggal_terima', $row->tanggal_terima),
				'nama_supir' => set_value('nama_supir', $row->nama_supir),
				'telp_supir' => set_value('telp_supir', $row->telp_supir),
				'catatan_gudang' => set_value('catatan_gudang', $row->catatan_gudang),
			    );

            $data['page'] = 'penerimaan-barang/penerimaan_barang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('penerimaan-barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penerimaan-barang/update/'.post('penerimaan_barang_id', true)));
        } else {
            $data = array(
				'purchase_order_id' => post('purchase_order_id',true),
				'gudang_id' => post('gudang_id',true),
				'no_surat_jalan' => post('no_surat_jalan',true),
				'tanggal_surat_jalan' => post('tanggal_surat_jalan',true),
				'tanggal_terima' => post('tanggal_terima',true),
				'nama_supir' => post('nama_supir',true),
				'telp_supir' => post('telp_supir',true),
				'catatan_gudang' => post('catatan_gudang',true),
		    );

            $this->penerimaan_barang_model->update($data, post('penerimaan_barang_id', true));

            success('Update Record Success');
            redirect(base_url('penerimaan-barang/update/'.post('penerimaan_barang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->penerimaan_barang_model->get($id);

        if ($row) {
            $this->penerimaan_barang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('penerimaan-barang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('penerimaan-barang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('purchase_order_id', 'purchase order', 'trim|required|numeric');
		$this->form_validation->set_rules('gudang_id', 'gudang', 'trim|required|numeric');
		$this->form_validation->set_rules('no_surat_jalan', 'no surat jalan', 'trim|required');
		$this->form_validation->set_rules('tanggal_surat_jalan', 'tanggal surat jalan', 'trim|required');
		$this->form_validation->set_rules('tanggal_terima', 'tanggal terima', 'trim|required');
		$this->form_validation->set_rules('nama_supir', 'nama supir', 'trim|required');
		$this->form_validation->set_rules('telp_supir', 'telp supir', 'trim|required');
		$this->form_validation->set_rules('catatan_gudang', 'catatan gudang', 'trim|required');

		$this->form_validation->set_rules('penerimaan_barang_id', 'penerimaan_barang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file penerimaan_barang.php */
/* Location: ./application/controllers/penerimaan_barang.php */
/* Please DO NOT modify this information : */