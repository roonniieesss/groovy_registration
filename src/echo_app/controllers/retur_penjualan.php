<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class retur_penjualan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('retur_penjualan_model');
    }

    public function index()
    {
        $data['page'] = 'retur-penjualan/retur_penjualan_list';
        $data['title'] = 'List Retur Penjualan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->retur_penjualan_model->json();
    }

    public function read($id)
    {
        $row = $this->retur_penjualan_model->get($id);
        if ($row) {
            $data = array(
				'transaksi_keuangan_id' => $row->transaksi_keuangan_id,
				'no_retur_penjualan' => $row->no_retur_penjualan,
				'jenis_retur' => $row->jenis_retur,
				'status_barang' => $row->status_barang,
				'alasan_pengembalian' => $row->alasan_pengembalian,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'retur-penjualan/retur_penjualan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('retur-penjualan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('retur-penjualan/create-action'),
		    'transaksi_keuangan_id' => set_value('transaksi_keuangan_id'),
		    'no_retur_penjualan' => set_value('no_retur_penjualan'),
		    'jenis_retur' => set_value('jenis_retur'),
		    'status_barang' => set_value('status_barang'),
		    'alasan_pengembalian' => set_value('alasan_pengembalian'),
		);
        $data['page'] = 'retur-penjualan/retur_penjualan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-penjualan/create'));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'no_retur_penjualan' => post('no_retur_penjualan',true),
				'jenis_retur' => post('jenis_retur',true),
				'status_barang' => post('status_barang',true),
				'alasan_pengembalian' => post('alasan_pengembalian',true),
		    );

            $this->retur_penjualan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('retur-penjualan'));
        }
    }

    public function update($id)
    {
        $row = $this->retur_penjualan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('retur-penjualan/update-action'),
				'retur_penjualan_id' => set_value('retur_penjualan_id', $row->retur_penjualan_id),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'no_retur_penjualan' => set_value('no_retur_penjualan', $row->no_retur_penjualan),
				'jenis_retur' => set_value('jenis_retur', $row->jenis_retur),
				'status_barang' => set_value('status_barang', $row->status_barang),
				'alasan_pengembalian' => set_value('alasan_pengembalian', $row->alasan_pengembalian),
			    );

            $data['page'] = 'retur-penjualan/retur_penjualan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('retur-penjualan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-penjualan/update/'.post('retur_penjualan_id', true)));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'no_retur_penjualan' => post('no_retur_penjualan',true),
				'jenis_retur' => post('jenis_retur',true),
				'status_barang' => post('status_barang',true),
				'alasan_pengembalian' => post('alasan_pengembalian',true),
		    );

            $this->retur_penjualan_model->update($data, post('retur_penjualan_id', true));

            success('Update Record Success');
            redirect(base_url('retur-penjualan/update/'.post('retur_penjualan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->retur_penjualan_model->get($id);

        if ($row) {
            $this->retur_penjualan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('retur-penjualan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('retur-penjualan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi keuangan', 'trim|required|numeric');
		$this->form_validation->set_rules('no_retur_penjualan', 'no retur penjualan', 'trim|required');
		$this->form_validation->set_rules('jenis_retur', 'jenis retur', 'trim|required');
		$this->form_validation->set_rules('status_barang', 'status barang', 'trim|required');
		$this->form_validation->set_rules('alasan_pengembalian', 'alasan pengembalian', 'trim|required');

		$this->form_validation->set_rules('retur_penjualan_id', 'retur_penjualan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file retur_penjualan.php */
/* Location: ./application/controllers/retur_penjualan.php */
/* Please DO NOT modify this information : */