<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class penerimaan_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('penerimaan_aset_model');
    }

    public function index()
    {
        $data['page'] = 'penerimaan-aset/penerimaan_aset_list';
        $data['title'] = 'List Penerimaan Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->penerimaan_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->penerimaan_aset_model->get($id);
        if ($row) {
            $data = array(
				'no_surat_jalan' => $row->no_surat_jalan,
				'tanggal_surat_jalan' => $row->tanggal_surat_jalan,
				'nama_supir' => $row->nama_supir,
				'keterangan_penerimaan_aset' => $row->keterangan_penerimaan_aset,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'penerimaan-aset/penerimaan_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('penerimaan-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('penerimaan-aset/create-action'),
		    'no_surat_jalan' => set_value('no_surat_jalan'),
		    'tanggal_surat_jalan' => set_value('tanggal_surat_jalan'),
		    'nama_supir' => set_value('nama_supir'),
		    'keterangan_penerimaan_aset' => set_value('keterangan_penerimaan_aset'),
		);
        $data['page'] = 'penerimaan-aset/penerimaan_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penerimaan-aset/create'));
        } else {
            $data = array(
				'no_surat_jalan' => post('no_surat_jalan',true),
				'tanggal_surat_jalan' => post('tanggal_surat_jalan',true),
				'nama_supir' => post('nama_supir',true),
				'keterangan_penerimaan_aset' => post('keterangan_penerimaan_aset',true),
		    );

            $this->penerimaan_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('penerimaan-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->penerimaan_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('penerimaan-aset/update-action'),
				'penerimaan_aset_id' => set_value('penerimaan_aset_id', $row->penerimaan_aset_id),
				'no_surat_jalan' => set_value('no_surat_jalan', $row->no_surat_jalan),
				'tanggal_surat_jalan' => set_value('tanggal_surat_jalan', $row->tanggal_surat_jalan),
				'nama_supir' => set_value('nama_supir', $row->nama_supir),
				'keterangan_penerimaan_aset' => set_value('keterangan_penerimaan_aset', $row->keterangan_penerimaan_aset),
			    );

            $data['page'] = 'penerimaan-aset/penerimaan_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('penerimaan-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penerimaan-aset/update/'.post('penerimaan_aset_id', true)));
        } else {
            $data = array(
				'no_surat_jalan' => post('no_surat_jalan',true),
				'tanggal_surat_jalan' => post('tanggal_surat_jalan',true),
				'nama_supir' => post('nama_supir',true),
				'keterangan_penerimaan_aset' => post('keterangan_penerimaan_aset',true),
		    );

            $this->penerimaan_aset_model->update($data, post('penerimaan_aset_id', true));

            success('Update Record Success');
            redirect(base_url('penerimaan-aset/update/'.post('penerimaan_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->penerimaan_aset_model->get($id);

        if ($row) {
            $this->penerimaan_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('penerimaan-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('penerimaan-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('no_surat_jalan', 'no surat jalan', 'trim|required');
		$this->form_validation->set_rules('tanggal_surat_jalan', 'tanggal surat jalan', 'trim|required');
		$this->form_validation->set_rules('nama_supir', 'nama supir', 'trim|required');
		$this->form_validation->set_rules('keterangan_penerimaan_aset', 'keterangan penerimaan aset', 'trim|required');

		$this->form_validation->set_rules('penerimaan_aset_id', 'penerimaan_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file penerimaan_aset.php */
/* Location: ./application/controllers/penerimaan_aset.php */
/* Please DO NOT modify this information : */