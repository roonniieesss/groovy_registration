<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class penerimaan_barang_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('penerimaan_barang_detail_model');
    }

    public function index()
    {
        $data['page'] = 'penerimaan-barang-detail/penerimaan_barang_detail_list';
        $data['title'] = 'List Penerimaan Barang Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->penerimaan_barang_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->penerimaan_barang_detail_model->get($id);
        if ($row) {
            $data = array(
				'penerimaan_barang_id' => $row->penerimaan_barang_id,
				'transaksi_barang_id' => $row->transaksi_barang_id,
				'purchase_order_detail_id' => $row->purchase_order_detail_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'penerimaan-barang-detail/penerimaan_barang_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('penerimaan-barang-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('penerimaan-barang-detail/create-action'),
		    'penerimaan_barang_id' => set_value('penerimaan_barang_id'),
		    'transaksi_barang_id' => set_value('transaksi_barang_id'),
		    'purchase_order_detail_id' => set_value('purchase_order_detail_id'),
		);
        $data['page'] = 'penerimaan-barang-detail/penerimaan_barang_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penerimaan-barang-detail/create'));
        } else {
            $data = array(
				'penerimaan_barang_id' => post('penerimaan_barang_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
				'purchase_order_detail_id' => post('purchase_order_detail_id',true),
		    );

            $this->penerimaan_barang_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('penerimaan-barang-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->penerimaan_barang_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('penerimaan-barang-detail/update-action'),
				'penerimaan_barang_detail_id' => set_value('penerimaan_barang_detail_id', $row->penerimaan_barang_detail_id),
				'penerimaan_barang_id' => set_value('penerimaan_barang_id', $row->penerimaan_barang_id),
				'transaksi_barang_id' => set_value('transaksi_barang_id', $row->transaksi_barang_id),
				'purchase_order_detail_id' => set_value('purchase_order_detail_id', $row->purchase_order_detail_id),
			    );

            $data['page'] = 'penerimaan-barang-detail/penerimaan_barang_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('penerimaan-barang-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penerimaan-barang-detail/update/'.post('penerimaan_barang_detail_id', true)));
        } else {
            $data = array(
				'penerimaan_barang_id' => post('penerimaan_barang_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
				'purchase_order_detail_id' => post('purchase_order_detail_id',true),
		    );

            $this->penerimaan_barang_detail_model->update($data, post('penerimaan_barang_detail_id', true));

            success('Update Record Success');
            redirect(base_url('penerimaan-barang-detail/update/'.post('penerimaan_barang_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->penerimaan_barang_detail_model->get($id);

        if ($row) {
            $this->penerimaan_barang_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('penerimaan-barang-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('penerimaan-barang-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('penerimaan_barang_id', 'penerimaan barang', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_id', 'transaksi barang', 'trim|required|numeric');
		$this->form_validation->set_rules('purchase_order_detail_id', 'purchase order detail', 'trim|required|numeric');

		$this->form_validation->set_rules('penerimaan_barang_detail_id', 'penerimaan_barang_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file penerimaan_barang_detail.php */
/* Location: ./application/controllers/penerimaan_barang_detail.php */
/* Please DO NOT modify this information : */