<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class satuan_barang_delivery_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('satuan_barang_delivery_order_model');
    }

    public function index()
    {
        $data['page'] = 'satuan-barang-delivery-order/satuan_barang_delivery_order_list';
        $data['title'] = 'List Satuan Barang Delivery Order';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->satuan_barang_delivery_order_model->json();
    }

    public function read($id)
    {
        $row = $this->satuan_barang_delivery_order_model->get($id);
        if ($row) {
            $data = array(
				'delivery_order_detail_id' => $row->delivery_order_detail_id,
				'transaksi_barang_id' => $row->transaksi_barang_id,
				'diretur' => $row->diretur,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'satuan-barang-delivery-order/satuan_barang_delivery_order_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('satuan-barang-delivery-order'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('satuan-barang-delivery-order/create-action'),
		    'delivery_order_detail_id' => set_value('delivery_order_detail_id'),
		    'transaksi_barang_id' => set_value('transaksi_barang_id'),
		    'diretur' => set_value('diretur'),
		);
        $data['page'] = 'satuan-barang-delivery-order/satuan_barang_delivery_order_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('satuan-barang-delivery-order/create'));
        } else {
            $data = array(
				'delivery_order_detail_id' => post('delivery_order_detail_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
				'diretur' => post('diretur',true),
		    );

            $this->satuan_barang_delivery_order_model->insert($data);

            success('Create Record Success');
            redirect(base_url('satuan-barang-delivery-order'));
        }
    }

    public function update($id)
    {
        $row = $this->satuan_barang_delivery_order_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('satuan-barang-delivery-order/update-action'),
				'satuan_barang_delivery_order_id' => set_value('satuan_barang_delivery_order_id', $row->satuan_barang_delivery_order_id),
				'delivery_order_detail_id' => set_value('delivery_order_detail_id', $row->delivery_order_detail_id),
				'transaksi_barang_id' => set_value('transaksi_barang_id', $row->transaksi_barang_id),
				'diretur' => set_value('diretur', $row->diretur),
			    );

            $data['page'] = 'satuan-barang-delivery-order/satuan_barang_delivery_order_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('satuan-barang-delivery-order'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('satuan-barang-delivery-order/update/'.post('satuan_barang_delivery_order_id', true)));
        } else {
            $data = array(
				'delivery_order_detail_id' => post('delivery_order_detail_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
				'diretur' => post('diretur',true),
		    );

            $this->satuan_barang_delivery_order_model->update($data, post('satuan_barang_delivery_order_id', true));

            success('Update Record Success');
            redirect(base_url('satuan-barang-delivery-order/update/'.post('satuan_barang_delivery_order_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->satuan_barang_delivery_order_model->get($id);

        if ($row) {
            $this->satuan_barang_delivery_order_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('satuan-barang-delivery-order'));
        } else {

            warning('Record Not Found');
            redirect(base_url('satuan-barang-delivery-order'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('delivery_order_detail_id', 'delivery order detail', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_id', 'transaksi barang', 'trim|required|numeric');
		$this->form_validation->set_rules('diretur', 'diretur', 'trim|required');

		$this->form_validation->set_rules('satuan_barang_delivery_order_id', 'satuan_barang_delivery_order_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file satuan_barang_delivery_order.php */
/* Location: ./application/controllers/satuan_barang_delivery_order.php */
/* Please DO NOT modify this information : */