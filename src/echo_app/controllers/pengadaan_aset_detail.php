<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pengadaan_aset_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pengadaan_aset_detail_model');
    }

    public function index()
    {
        $data['page'] = 'pengadaan-aset-detail/pengadaan_aset_detail_list';
        $data['title'] = 'List Pengadaan Aset Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pengadaan_aset_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->pengadaan_aset_detail_model->get($id);
        if ($row) {
            $data = array(
				'pengadaan_aset_id' => $row->pengadaan_aset_id,
				'jenis_aset_id' => $row->jenis_aset_id,
				'jumlah' => $row->jumlah,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pengadaan-aset-detail/pengadaan_aset_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pengadaan-aset-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pengadaan-aset-detail/create-action'),
		    'pengadaan_aset_id' => set_value('pengadaan_aset_id'),
		    'jenis_aset_id' => set_value('jenis_aset_id'),
		    'jumlah' => set_value('jumlah'),
		);
        $data['page'] = 'pengadaan-aset-detail/pengadaan_aset_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pengadaan-aset-detail/create'));
        } else {
            $data = array(
				'pengadaan_aset_id' => post('pengadaan_aset_id',true),
				'jenis_aset_id' => post('jenis_aset_id',true),
				'jumlah' => post('jumlah',true),
		    );

            $this->pengadaan_aset_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pengadaan-aset-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->pengadaan_aset_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pengadaan-aset-detail/update-action'),
				'pengadaan_aset_detail_id' => set_value('pengadaan_aset_detail_id', $row->pengadaan_aset_detail_id),
				'pengadaan_aset_id' => set_value('pengadaan_aset_id', $row->pengadaan_aset_id),
				'jenis_aset_id' => set_value('jenis_aset_id', $row->jenis_aset_id),
				'jumlah' => set_value('jumlah', $row->jumlah),
			    );

            $data['page'] = 'pengadaan-aset-detail/pengadaan_aset_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pengadaan-aset-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pengadaan-aset-detail/update/'.post('pengadaan_aset_detail_id', true)));
        } else {
            $data = array(
				'pengadaan_aset_id' => post('pengadaan_aset_id',true),
				'jenis_aset_id' => post('jenis_aset_id',true),
				'jumlah' => post('jumlah',true),
		    );

            $this->pengadaan_aset_detail_model->update($data, post('pengadaan_aset_detail_id', true));

            success('Update Record Success');
            redirect(base_url('pengadaan-aset-detail/update/'.post('pengadaan_aset_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pengadaan_aset_detail_model->get($id);

        if ($row) {
            $this->pengadaan_aset_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pengadaan-aset-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pengadaan-aset-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pengadaan_aset_id', 'pengadaan aset', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_aset_id', 'jenis aset', 'trim|required|numeric');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'trim|required|numeric');

		$this->form_validation->set_rules('pengadaan_aset_detail_id', 'pengadaan_aset_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pengadaan_aset_detail.php */
/* Location: ./application/controllers/pengadaan_aset_detail.php */
/* Please DO NOT modify this information : */