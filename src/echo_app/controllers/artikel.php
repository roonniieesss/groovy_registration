<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class artikel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('artikel_model');
    }

    public function index()
    {
        $data['page'] = 'artikel/artikel_list';
        $data['title'] = 'List Artikel';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->artikel_model->json();
    }

    public function read($id)
    {
        $row = $this->artikel_model->get($id);
        if ($row) {
            $data = array(
				'waktu_terbit' => $row->waktu_terbit,
				'waktu_akhir_tayang' => $row->waktu_akhir_tayang,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'artikel/artikel_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('artikel'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('artikel/create-action'),
		    'waktu_terbit' => set_value('waktu_terbit'),
		    'waktu_akhir_tayang' => set_value('waktu_akhir_tayang'),
		);
        $data['page'] = 'artikel/artikel_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('artikel/create'));
        } else {
            $data = array(
				'waktu_terbit' => post('waktu_terbit',true),
				'waktu_akhir_tayang' => post('waktu_akhir_tayang',true),
		    );

            $this->artikel_model->insert($data);

            success('Create Record Success');
            redirect(base_url('artikel'));
        }
    }

    public function update($id)
    {
        $row = $this->artikel_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('artikel/update-action'),
				'artikel_id' => set_value('artikel_id', $row->artikel_id),
				'waktu_terbit' => set_value('waktu_terbit', $row->waktu_terbit),
				'waktu_akhir_tayang' => set_value('waktu_akhir_tayang', $row->waktu_akhir_tayang),
			    );

            $data['page'] = 'artikel/artikel_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('artikel'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('artikel/update/'.post('artikel_id', true)));
        } else {
            $data = array(
				'waktu_terbit' => post('waktu_terbit',true),
				'waktu_akhir_tayang' => post('waktu_akhir_tayang',true),
		    );

            $this->artikel_model->update($data, post('artikel_id', true));

            success('Update Record Success');
            redirect(base_url('artikel/update/'.post('artikel_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->artikel_model->get($id);

        if ($row) {
            $this->artikel_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('artikel'));
        } else {

            warning('Record Not Found');
            redirect(base_url('artikel'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('waktu_terbit', 'waktu terbit', 'trim|required');
		$this->form_validation->set_rules('waktu_akhir_tayang', 'waktu akhir tayang', 'trim|required');

		$this->form_validation->set_rules('artikel_id', 'artikel_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file artikel.php */
/* Location: ./application/controllers/artikel.php */
/* Please DO NOT modify this information : */