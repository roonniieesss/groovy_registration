<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sub_lokasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sub_lokasi_model');
    }

    public function index()
    {
        $data['page'] = 'sub-lokasi/sub_lokasi_list';
        $data['title'] = 'List Sub Lokasi';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->sub_lokasi_model->json();
    }

    public function read($id)
    {
        $row = $this->sub_lokasi_model->get($id);
        if ($row) {
            $data = array(
				'lokasi_id' => $row->lokasi_id,
				'nama_sub_lokasi' => $row->nama_sub_lokasi,
				'filename_denah' => $row->filename_denah,
				'width' => $row->width,
				'height' => $row->height,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'sub-lokasi/sub_lokasi_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sub-lokasi'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('sub-lokasi/create-action'),
		    'lokasi_id' => set_value('lokasi_id'),
		    'nama_sub_lokasi' => set_value('nama_sub_lokasi'),
		    'filename_denah' => set_value('filename_denah'),
		    'width' => set_value('width'),
		    'height' => set_value('height'),
		);
        $data['page'] = 'sub-lokasi/sub_lokasi_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sub-lokasi/create'));
        } else {
            $data = array(
				'lokasi_id' => post('lokasi_id',true),
				'nama_sub_lokasi' => post('nama_sub_lokasi',true),
				'filename_denah' => post('filename_denah',true),
				'width' => post('width',true),
				'height' => post('height',true),
		    );

            $this->sub_lokasi_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sub-lokasi'));
        }
    }

    public function update($id)
    {
        $row = $this->sub_lokasi_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('sub-lokasi/update-action'),
				'sub_lokasi_id' => set_value('sub_lokasi_id', $row->sub_lokasi_id),
				'lokasi_id' => set_value('lokasi_id', $row->lokasi_id),
				'nama_sub_lokasi' => set_value('nama_sub_lokasi', $row->nama_sub_lokasi),
				'filename_denah' => set_value('filename_denah', $row->filename_denah),
				'width' => set_value('width', $row->width),
				'height' => set_value('height', $row->height),
			    );

            $data['page'] = 'sub-lokasi/sub_lokasi_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sub-lokasi'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sub-lokasi/update/'.post('sub_lokasi_id', true)));
        } else {
            $data = array(
				'lokasi_id' => post('lokasi_id',true),
				'nama_sub_lokasi' => post('nama_sub_lokasi',true),
				'filename_denah' => post('filename_denah',true),
				'width' => post('width',true),
				'height' => post('height',true),
		    );

            $this->sub_lokasi_model->update($data, post('sub_lokasi_id', true));

            success('Update Record Success');
            redirect(base_url('sub-lokasi/update/'.post('sub_lokasi_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sub_lokasi_model->get($id);

        if ($row) {
            $this->sub_lokasi_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sub-lokasi'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sub-lokasi'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('lokasi_id', 'lokasi', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_sub_lokasi', 'nama sub lokasi', 'trim|required');
		$this->form_validation->set_rules('filename_denah', 'filename denah', 'trim|required');
		$this->form_validation->set_rules('width', 'wth', 'trim|required|numeric');
		$this->form_validation->set_rules('height', 'height', 'trim|required|numeric');

		$this->form_validation->set_rules('sub_lokasi_id', 'sub_lokasi_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file sub_lokasi.php */
/* Location: ./application/controllers/sub_lokasi.php */
/* Please DO NOT modify this information : */