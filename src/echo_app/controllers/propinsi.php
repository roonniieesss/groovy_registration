<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class propinsi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('propinsi_model');
    }

    public function index()
    {
        $data['page'] = 'propinsi/propinsi_list';
        $data['title'] = 'List Propinsi';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->propinsi_model->json();
    }

    public function read($id)
    {
        $row = $this->propinsi_model->get($id);
        if ($row) {
            $data = array(
				'negara_id' => $row->negara_id,
				'nama_propinsi' => $row->nama_propinsi,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'propinsi/propinsi_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('propinsi'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('propinsi/create-action'),
		    'negara_id' => set_value('negara_id'),
		    'nama_propinsi' => set_value('nama_propinsi'),
		);
        $data['page'] = 'propinsi/propinsi_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('propinsi/create'));
        } else {
            $data = array(
				'negara_id' => post('negara_id',true),
				'nama_propinsi' => post('nama_propinsi',true),
		    );

            $this->propinsi_model->insert($data);

            success('Create Record Success');
            redirect(base_url('propinsi'));
        }
    }

    public function update($id)
    {
        $row = $this->propinsi_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('propinsi/update-action'),
				'propinsi_id' => set_value('propinsi_id', $row->propinsi_id),
				'negara_id' => set_value('negara_id', $row->negara_id),
				'nama_propinsi' => set_value('nama_propinsi', $row->nama_propinsi),
			    );

            $data['page'] = 'propinsi/propinsi_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('propinsi'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('propinsi/update/'.post('propinsi_id', true)));
        } else {
            $data = array(
				'negara_id' => post('negara_id',true),
				'nama_propinsi' => post('nama_propinsi',true),
		    );

            $this->propinsi_model->update($data, post('propinsi_id', true));

            success('Update Record Success');
            redirect(base_url('propinsi/update/'.post('propinsi_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->propinsi_model->get($id);

        if ($row) {
            $this->propinsi_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('propinsi'));
        } else {

            warning('Record Not Found');
            redirect(base_url('propinsi'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('negara_id', 'negara', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_propinsi', 'nama propinsi', 'trim|required');

		$this->form_validation->set_rules('propinsi_id', 'propinsi_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file propinsi.php */
/* Location: ./application/controllers/propinsi.php */
/* Please DO NOT modify this information : */