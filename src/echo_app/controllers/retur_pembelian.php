<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class retur_pembelian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('retur_pembelian_model');
    }

    public function index()
    {
        $data['page'] = 'retur-pembelian/retur_pembelian_list';
        $data['title'] = 'List Retur Pembelian';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->retur_pembelian_model->json();
    }

    public function read($id)
    {
        $row = $this->retur_pembelian_model->get($id);
        if ($row) {
            $data = array(
				'transaksi_keuangan_id' => $row->transaksi_keuangan_id,
				'no_retur_pembelian' => $row->no_retur_pembelian,
				'jenis_retur' => $row->jenis_retur,
				'status_barang' => $row->status_barang,
				'alasan_pengembalian' => $row->alasan_pengembalian,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'retur-pembelian/retur_pembelian_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('retur-pembelian'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('retur-pembelian/create-action'),
		    'transaksi_keuangan_id' => set_value('transaksi_keuangan_id'),
		    'no_retur_pembelian' => set_value('no_retur_pembelian'),
		    'jenis_retur' => set_value('jenis_retur'),
		    'status_barang' => set_value('status_barang'),
		    'alasan_pengembalian' => set_value('alasan_pengembalian'),
		);
        $data['page'] = 'retur-pembelian/retur_pembelian_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-pembelian/create'));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'no_retur_pembelian' => post('no_retur_pembelian',true),
				'jenis_retur' => post('jenis_retur',true),
				'status_barang' => post('status_barang',true),
				'alasan_pengembalian' => post('alasan_pengembalian',true),
		    );

            $this->retur_pembelian_model->insert($data);

            success('Create Record Success');
            redirect(base_url('retur-pembelian'));
        }
    }

    public function update($id)
    {
        $row = $this->retur_pembelian_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('retur-pembelian/update-action'),
				'retur_pembelian_id' => set_value('retur_pembelian_id', $row->retur_pembelian_id),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'no_retur_pembelian' => set_value('no_retur_pembelian', $row->no_retur_pembelian),
				'jenis_retur' => set_value('jenis_retur', $row->jenis_retur),
				'status_barang' => set_value('status_barang', $row->status_barang),
				'alasan_pengembalian' => set_value('alasan_pengembalian', $row->alasan_pengembalian),
			    );

            $data['page'] = 'retur-pembelian/retur_pembelian_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('retur-pembelian'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('retur-pembelian/update/'.post('retur_pembelian_id', true)));
        } else {
            $data = array(
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'no_retur_pembelian' => post('no_retur_pembelian',true),
				'jenis_retur' => post('jenis_retur',true),
				'status_barang' => post('status_barang',true),
				'alasan_pengembalian' => post('alasan_pengembalian',true),
		    );

            $this->retur_pembelian_model->update($data, post('retur_pembelian_id', true));

            success('Update Record Success');
            redirect(base_url('retur-pembelian/update/'.post('retur_pembelian_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->retur_pembelian_model->get($id);

        if ($row) {
            $this->retur_pembelian_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('retur-pembelian'));
        } else {

            warning('Record Not Found');
            redirect(base_url('retur-pembelian'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi keuangan', 'trim|required|numeric');
		$this->form_validation->set_rules('no_retur_pembelian', 'no retur pembelian', 'trim|required');
		$this->form_validation->set_rules('jenis_retur', 'jenis retur', 'trim|required');
		$this->form_validation->set_rules('status_barang', 'status barang', 'trim|required');
		$this->form_validation->set_rules('alasan_pengembalian', 'alasan pengembalian', 'trim|required');

		$this->form_validation->set_rules('retur_pembelian_id', 'retur_pembelian_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file retur_pembelian.php */
/* Location: ./application/controllers/retur_pembelian.php */
/* Please DO NOT modify this information : */