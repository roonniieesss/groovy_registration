<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class inquiry_komentar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('inquiry_komentar_model');
    }

    public function index()
    {
        $data['page'] = 'inquiry-komentar/inquiry_komentar_list';
        $data['title'] = 'List Inquiry Komentar';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->inquiry_komentar_model->json();
    }

    public function read($id)
    {
        $row = $this->inquiry_komentar_model->get($id);
        if ($row) {
            $data = array(
				'inquiry_id' => $row->inquiry_id,
				'komentar' => $row->komentar,
				'penulis' => $row->penulis,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'inquiry-komentar/inquiry_komentar_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry-komentar'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('inquiry-komentar/create-action'),
		    'inquiry_id' => set_value('inquiry_id'),
		    'komentar' => set_value('komentar'),
		    'penulis' => set_value('penulis'),
		);
        $data['page'] = 'inquiry-komentar/inquiry_komentar_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry-komentar/create'));
        } else {
            $data = array(
				'inquiry_id' => post('inquiry_id',true),
				'komentar' => post('komentar',true),
				'penulis' => post('penulis',true),
		    );

            $this->inquiry_komentar_model->insert($data);

            success('Create Record Success');
            redirect(base_url('inquiry-komentar'));
        }
    }

    public function update($id)
    {
        $row = $this->inquiry_komentar_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('inquiry-komentar/update-action'),
				'inquiry_komentar_id' => set_value('inquiry_komentar_id', $row->inquiry_komentar_id),
				'inquiry_id' => set_value('inquiry_id', $row->inquiry_id),
				'komentar' => set_value('komentar', $row->komentar),
				'penulis' => set_value('penulis', $row->penulis),
			    );

            $data['page'] = 'inquiry-komentar/inquiry_komentar_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('inquiry-komentar'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('inquiry-komentar/update/'.post('inquiry_komentar_id', true)));
        } else {
            $data = array(
				'inquiry_id' => post('inquiry_id',true),
				'komentar' => post('komentar',true),
				'penulis' => post('penulis',true),
		    );

            $this->inquiry_komentar_model->update($data, post('inquiry_komentar_id', true));

            success('Update Record Success');
            redirect(base_url('inquiry-komentar/update/'.post('inquiry_komentar_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->inquiry_komentar_model->get($id);

        if ($row) {
            $this->inquiry_komentar_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('inquiry-komentar'));
        } else {

            warning('Record Not Found');
            redirect(base_url('inquiry-komentar'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('inquiry_id', 'inquiry', 'trim|required|numeric');
		$this->form_validation->set_rules('komentar', 'komentar', 'trim|required');
		$this->form_validation->set_rules('penulis', 'penulis', 'trim|required');

		$this->form_validation->set_rules('inquiry_komentar_id', 'inquiry_komentar_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file inquiry_komentar.php */
/* Location: ./application/controllers/inquiry_komentar.php */
/* Please DO NOT modify this information : */