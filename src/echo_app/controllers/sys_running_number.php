<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sys_running_number extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sys_running_number_model');
    }

    public function index()
    {
        $data['page'] = 'sys-running-number/sys_running_number_list';
        $data['title'] = 'List Sys Running Number';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->sys_running_number_model->json();
    }

    public function read($id)
    {
        $row = $this->sys_running_number_model->get($id);
        if ($row) {
            $data = array(
				'format' => $row->format,
				'next_id' => $row->next_id,
				'period' => $row->period,
				'last_retrieve' => $row->last_retrieve,
				'zero_pad_length' => $row->zero_pad_length,
		    );
            $data['page'] = 'sys-running-number/sys_running_number_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sys-running-number'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('sys-running-number/create-action'),
		    'format' => set_value('format'),
		    'next_id' => set_value('next_id'),
		    'period' => set_value('period'),
		    'last_retrieve' => set_value('last_retrieve'),
		    'zero_pad_length' => set_value('zero_pad_length'),
		);
        $data['page'] = 'sys-running-number/sys_running_number_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sys-running-number/create'));
        } else {
            $data = array(
				'format' => post('format',true),
				'next_id' => post('next_id',true),
				'period' => post('period',true),
				'last_retrieve' => post('last_retrieve',true),
				'zero_pad_length' => post('zero_pad_length',true),
		    );

            $this->sys_running_number_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sys-running-number'));
        }
    }

    public function update($id)
    {
        $row = $this->sys_running_number_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('sys-running-number/update-action'),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'format' => set_value('format', $row->format),
				'next_id' => set_value('next_id', $row->next_id),
				'period' => set_value('period', $row->period),
				'last_retrieve' => set_value('last_retrieve', $row->last_retrieve),
				'zero_pad_length' => set_value('zero_pad_length', $row->zero_pad_length),
			    );

            $data['page'] = 'sys-running-number/sys_running_number_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sys-running-number'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sys-running-number/update/'.post('perusahaan_id', true)));
        } else {
            $data = array(
				'format' => post('format',true),
				'next_id' => post('next_id',true),
				'period' => post('period',true),
				'last_retrieve' => post('last_retrieve',true),
				'zero_pad_length' => post('zero_pad_length',true),
		    );

            $this->sys_running_number_model->update($data, post('perusahaan_id', true));

            success('Update Record Success');
            redirect(base_url('sys-running-number/update/'.post('perusahaan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sys_running_number_model->get($id);

        if ($row) {
            $this->sys_running_number_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sys-running-number'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sys-running-number'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('format', 'format', 'trim|required');
		$this->form_validation->set_rules('next_id', 'next', 'trim|required|numeric');
		$this->form_validation->set_rules('period', 'period', 'trim|required|numeric');
		$this->form_validation->set_rules('last_retrieve', 'last retrieve', 'trim|required');
		$this->form_validation->set_rules('zero_pad_length', 'zero pad length', 'trim|required|numeric');

		$this->form_validation->set_rules('perusahaan_id', 'perusahaan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file sys_running_number.php */
/* Location: ./application/controllers/sys_running_number.php */
/* Please DO NOT modify this information : */