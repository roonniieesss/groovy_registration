<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class transaksi_keuangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('transaksi_keuangan_model');
    }

    public function index()
    {
        $data['page'] = 'transaksi-keuangan/transaksi_keuangan_list';
        $data['title'] = 'List Transaksi Keuangan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->transaksi_keuangan_model->json();
    }

    public function read($id)
    {
        $row = $this->transaksi_keuangan_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'tanggal_buku' => $row->tanggal_buku,
				'keterangan' => $row->keterangan,
				'no_cek' => $row->no_cek,
				'tanggal_terima_cek' => $row->tanggal_terima_cek,
				'tanggal_cair_cek' => $row->tanggal_cair_cek,
				'status' => $row->status,
				'keterangan_status' => $row->keterangan_status,
				'jenis' => $row->jenis,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'transaksi-keuangan/transaksi_keuangan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('transaksi-keuangan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('transaksi-keuangan/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'tanggal_buku' => set_value('tanggal_buku'),
		    'keterangan' => set_value('keterangan'),
		    'no_cek' => set_value('no_cek'),
		    'tanggal_terima_cek' => set_value('tanggal_terima_cek'),
		    'tanggal_cair_cek' => set_value('tanggal_cair_cek'),
		    'status' => set_value('status'),
		    'keterangan_status' => set_value('keterangan_status'),
		    'jenis' => set_value('jenis'),
		);
        $data['page'] = 'transaksi-keuangan/transaksi_keuangan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transaksi-keuangan/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'tanggal_buku' => post('tanggal_buku',true),
				'keterangan' => post('keterangan',true),
				'no_cek' => post('no_cek',true),
				'tanggal_terima_cek' => post('tanggal_terima_cek',true),
				'tanggal_cair_cek' => post('tanggal_cair_cek',true),
				'status' => post('status',true),
				'keterangan_status' => post('keterangan_status',true),
				'jenis' => post('jenis',true),
		    );

            $this->transaksi_keuangan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('transaksi-keuangan'));
        }
    }

    public function update($id)
    {
        $row = $this->transaksi_keuangan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('transaksi-keuangan/update-action'),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'tanggal_buku' => set_value('tanggal_buku', $row->tanggal_buku),
				'keterangan' => set_value('keterangan', $row->keterangan),
				'no_cek' => set_value('no_cek', $row->no_cek),
				'tanggal_terima_cek' => set_value('tanggal_terima_cek', $row->tanggal_terima_cek),
				'tanggal_cair_cek' => set_value('tanggal_cair_cek', $row->tanggal_cair_cek),
				'status' => set_value('status', $row->status),
				'keterangan_status' => set_value('keterangan_status', $row->keterangan_status),
				'jenis' => set_value('jenis', $row->jenis),
			    );

            $data['page'] = 'transaksi-keuangan/transaksi_keuangan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('transaksi-keuangan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transaksi-keuangan/update/'.post('transaksi_keuangan_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'tanggal_buku' => post('tanggal_buku',true),
				'keterangan' => post('keterangan',true),
				'no_cek' => post('no_cek',true),
				'tanggal_terima_cek' => post('tanggal_terima_cek',true),
				'tanggal_cair_cek' => post('tanggal_cair_cek',true),
				'status' => post('status',true),
				'keterangan_status' => post('keterangan_status',true),
				'jenis' => post('jenis',true),
		    );

            $this->transaksi_keuangan_model->update($data, post('transaksi_keuangan_id', true));

            success('Update Record Success');
            redirect(base_url('transaksi-keuangan/update/'.post('transaksi_keuangan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->transaksi_keuangan_model->get($id);

        if ($row) {
            $this->transaksi_keuangan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('transaksi-keuangan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('transaksi-keuangan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal_buku', 'tanggal buku', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
		$this->form_validation->set_rules('no_cek', 'no cek', 'trim|required');
		$this->form_validation->set_rules('tanggal_terima_cek', 'tanggal terima cek', 'trim|required');
		$this->form_validation->set_rules('tanggal_cair_cek', 'tanggal cair cek', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('keterangan_status', 'keterangan status', 'trim|required');
		$this->form_validation->set_rules('jenis', 'jenis', 'trim|required');

		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi_keuangan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file transaksi_keuangan.php */
/* Location: ./application/controllers/transaksi_keuangan.php */
/* Please DO NOT modify this information : */