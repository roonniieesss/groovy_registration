<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class apkir_aset_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('apkir_aset_detail_model');
    }

    public function index()
    {
        $data['page'] = 'apkir-aset-detail/apkir_aset_detail_list';
        $data['title'] = 'List Apkir Aset Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->apkir_aset_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->apkir_aset_detail_model->get($id);
        if ($row) {
            $data = array(
				'apkir_aset_id' => $row->apkir_aset_id,
				'aset_id' => $row->aset_id,
				'penempatan_aset_id' => $row->penempatan_aset_id,
				'alasan_apkir' => $row->alasan_apkir,
				'keterangan_alasan_apkir' => $row->keterangan_alasan_apkir,
				'sudah_diapkir' => $row->sudah_diapkir,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'apkir-aset-detail/apkir_aset_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('apkir-aset-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('apkir-aset-detail/create-action'),
		    'apkir_aset_id' => set_value('apkir_aset_id'),
		    'aset_id' => set_value('aset_id'),
		    'penempatan_aset_id' => set_value('penempatan_aset_id'),
		    'alasan_apkir' => set_value('alasan_apkir'),
		    'keterangan_alasan_apkir' => set_value('keterangan_alasan_apkir'),
		    'sudah_diapkir' => set_value('sudah_diapkir'),
		);
        $data['page'] = 'apkir-aset-detail/apkir_aset_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('apkir-aset-detail/create'));
        } else {
            $data = array(
				'apkir_aset_id' => post('apkir_aset_id',true),
				'aset_id' => post('aset_id',true),
				'penempatan_aset_id' => post('penempatan_aset_id',true),
				'alasan_apkir' => post('alasan_apkir',true),
				'keterangan_alasan_apkir' => post('keterangan_alasan_apkir',true),
				'sudah_diapkir' => post('sudah_diapkir',true),
		    );

            $this->apkir_aset_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('apkir-aset-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->apkir_aset_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('apkir-aset-detail/update-action'),
				'apkir_aset_detail_id' => set_value('apkir_aset_detail_id', $row->apkir_aset_detail_id),
				'apkir_aset_id' => set_value('apkir_aset_id', $row->apkir_aset_id),
				'aset_id' => set_value('aset_id', $row->aset_id),
				'penempatan_aset_id' => set_value('penempatan_aset_id', $row->penempatan_aset_id),
				'alasan_apkir' => set_value('alasan_apkir', $row->alasan_apkir),
				'keterangan_alasan_apkir' => set_value('keterangan_alasan_apkir', $row->keterangan_alasan_apkir),
				'sudah_diapkir' => set_value('sudah_diapkir', $row->sudah_diapkir),
			    );

            $data['page'] = 'apkir-aset-detail/apkir_aset_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('apkir-aset-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('apkir-aset-detail/update/'.post('apkir_aset_detail_id', true)));
        } else {
            $data = array(
				'apkir_aset_id' => post('apkir_aset_id',true),
				'aset_id' => post('aset_id',true),
				'penempatan_aset_id' => post('penempatan_aset_id',true),
				'alasan_apkir' => post('alasan_apkir',true),
				'keterangan_alasan_apkir' => post('keterangan_alasan_apkir',true),
				'sudah_diapkir' => post('sudah_diapkir',true),
		    );

            $this->apkir_aset_detail_model->update($data, post('apkir_aset_detail_id', true));

            success('Update Record Success');
            redirect(base_url('apkir-aset-detail/update/'.post('apkir_aset_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->apkir_aset_detail_model->get($id);

        if ($row) {
            $this->apkir_aset_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('apkir-aset-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('apkir-aset-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('apkir_aset_id', 'apkir aset', 'trim|required|numeric');
		$this->form_validation->set_rules('aset_id', 'aset', 'trim|required|numeric');
		$this->form_validation->set_rules('penempatan_aset_id', 'penempatan aset', 'trim|required|numeric');
		$this->form_validation->set_rules('alasan_apkir', 'alasan apkir', 'trim|required');
		$this->form_validation->set_rules('keterangan_alasan_apkir', 'keterangan alasan apkir', 'trim|required');
		$this->form_validation->set_rules('sudah_diapkir', 'sudah diapkir', 'trim|required');

		$this->form_validation->set_rules('apkir_aset_detail_id', 'apkir_aset_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file apkir_aset_detail.php */
/* Location: ./application/controllers/apkir_aset_detail.php */
/* Please DO NOT modify this information : */