<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class apkir_aset extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('apkir_aset_model');
    }

    public function index()
    {
        $data['page'] = 'apkir-aset/apkir_aset_list';
        $data['title'] = 'List Apkir Aset';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->apkir_aset_model->json();
    }

    public function read($id)
    {
        $row = $this->apkir_aset_model->get($id);
        if ($row) {
            $data = array(
				'nama_penanggung_jawab' => $row->nama_penanggung_jawab,
				'berita_acara_apkir' => $row->berita_acara_apkir,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'apkir-aset/apkir_aset_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('apkir-aset'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('apkir-aset/create-action'),
		    'nama_penanggung_jawab' => set_value('nama_penanggung_jawab'),
		    'berita_acara_apkir' => set_value('berita_acara_apkir'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'apkir-aset/apkir_aset_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('apkir-aset/create'));
        } else {
            $data = array(
				'nama_penanggung_jawab' => post('nama_penanggung_jawab',true),
				'berita_acara_apkir' => post('berita_acara_apkir',true),
				'status' => post('status',true),
		    );

            $this->apkir_aset_model->insert($data);

            success('Create Record Success');
            redirect(base_url('apkir-aset'));
        }
    }

    public function update($id)
    {
        $row = $this->apkir_aset_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('apkir-aset/update-action'),
				'apkir_aset_id' => set_value('apkir_aset_id', $row->apkir_aset_id),
				'nama_penanggung_jawab' => set_value('nama_penanggung_jawab', $row->nama_penanggung_jawab),
				'berita_acara_apkir' => set_value('berita_acara_apkir', $row->berita_acara_apkir),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'apkir-aset/apkir_aset_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('apkir-aset'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('apkir-aset/update/'.post('apkir_aset_id', true)));
        } else {
            $data = array(
				'nama_penanggung_jawab' => post('nama_penanggung_jawab',true),
				'berita_acara_apkir' => post('berita_acara_apkir',true),
				'status' => post('status',true),
		    );

            $this->apkir_aset_model->update($data, post('apkir_aset_id', true));

            success('Update Record Success');
            redirect(base_url('apkir-aset/update/'.post('apkir_aset_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->apkir_aset_model->get($id);

        if ($row) {
            $this->apkir_aset_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('apkir-aset'));
        } else {

            warning('Record Not Found');
            redirect(base_url('apkir-aset'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_penanggung_jawab', 'nama penanggung jawab', 'trim|required');
		$this->form_validation->set_rules('berita_acara_apkir', 'berita acara apkir', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('apkir_aset_id', 'apkir_aset_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file apkir_aset.php */
/* Location: ./application/controllers/apkir_aset.php */
/* Please DO NOT modify this information : */