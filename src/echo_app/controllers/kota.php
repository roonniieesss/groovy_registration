<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kota extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kota_model');
    }

    public function index()
    {
        $data['page'] = 'kota/kota_list';
        $data['title'] = 'List Kota';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->kota_model->json();
    }

    public function read($id)
    {
        $row = $this->kota_model->get($id);
        if ($row) {
            $data = array(
				'propinsi_id' => $row->propinsi_id,
				'nama_kota' => $row->nama_kota,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'kota/kota_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kota'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('kota/create-action'),
		    'propinsi_id' => set_value('propinsi_id'),
		    'nama_kota' => set_value('nama_kota'),
		);
        $data['page'] = 'kota/kota_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kota/create'));
        } else {
            $data = array(
				'propinsi_id' => post('propinsi_id',true),
				'nama_kota' => post('nama_kota',true),
		    );

            $this->kota_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kota'));
        }
    }

    public function update($id)
    {
        $row = $this->kota_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('kota/update-action'),
				'kota_id' => set_value('kota_id', $row->kota_id),
				'propinsi_id' => set_value('propinsi_id', $row->propinsi_id),
				'nama_kota' => set_value('nama_kota', $row->nama_kota),
			    );

            $data['page'] = 'kota/kota_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kota'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kota/update/'.post('kota_id', true)));
        } else {
            $data = array(
				'propinsi_id' => post('propinsi_id',true),
				'nama_kota' => post('nama_kota',true),
		    );

            $this->kota_model->update($data, post('kota_id', true));

            success('Update Record Success');
            redirect(base_url('kota/update/'.post('kota_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kota_model->get($id);

        if ($row) {
            $this->kota_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kota'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kota'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('propinsi_id', 'propinsi', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_kota', 'nama kota', 'trim|required');

		$this->form_validation->set_rules('kota_id', 'kota_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file kota.php */
/* Location: ./application/controllers/kota.php */
/* Please DO NOT modify this information : */