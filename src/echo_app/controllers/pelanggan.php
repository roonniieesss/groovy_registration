<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'pelanggan/pelanggan_list';
        $data['title'] = 'List Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'nama_pelanggan' => $row->nama_pelanggan,
				'tanggal_lahir' => $row->tanggal_lahir,
				'jenis_kelamin' => $row->jenis_kelamin,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pelanggan/pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pelanggan/create-action'),
		    'nama_pelanggan' => set_value('nama_pelanggan'),
		    'tanggal_lahir' => set_value('tanggal_lahir'),
		    'jenis_kelamin' => set_value('jenis_kelamin'),
		);
        $data['page'] = 'pelanggan/pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pelanggan/create'));
        } else {
            $data = array(
				'nama_pelanggan' => post('nama_pelanggan',true),
				'tanggal_lahir' => post('tanggal_lahir',true),
				'jenis_kelamin' => post('jenis_kelamin',true),
		    );

            $this->pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pelanggan/update-action'),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'nama_pelanggan' => set_value('nama_pelanggan', $row->nama_pelanggan),
				'tanggal_lahir' => set_value('tanggal_lahir', $row->tanggal_lahir),
				'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
			    );

            $data['page'] = 'pelanggan/pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pelanggan/update/'.post('pelanggan_id', true)));
        } else {
            $data = array(
				'nama_pelanggan' => post('nama_pelanggan',true),
				'tanggal_lahir' => post('tanggal_lahir',true),
				'jenis_kelamin' => post('jenis_kelamin',true),
		    );

            $this->pelanggan_model->update($data, post('pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('pelanggan/update/'.post('pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pelanggan_model->get($id);

        if ($row) {
            $this->pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_pelanggan', 'nama pelanggan', 'trim|required');
		$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
		$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');

		$this->form_validation->set_rules('pelanggan_id', 'pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pelanggan.php */
/* Location: ./application/controllers/pelanggan.php */
/* Please DO NOT modify this information : */