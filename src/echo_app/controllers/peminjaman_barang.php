<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peminjaman_barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peminjaman_barang_model');
    }

    public function index()
    {
        $data['page'] = 'peminjaman-barang/peminjaman_barang_list';
        $data['title'] = 'List Peminjaman Barang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peminjaman_barang_model->json();
    }

    public function read($id)
    {
        $row = $this->peminjaman_barang_model->get($id);
        if ($row) {
            $data = array(
				'no_peminjaman_barang' => $row->no_peminjaman_barang,
				'tanggal_pinjam' => $row->tanggal_pinjam,
				'tanggal_kembali' => $row->tanggal_kembali,
				'nama_peminjam' => $row->nama_peminjam,
				'alamat_peminjam' => $row->alamat_peminjam,
				'keperluan' => $row->keperluan,
				'telp_peminjam' => $row->telp_peminjam,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peminjaman-barang/peminjaman_barang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peminjaman-barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peminjaman-barang/create-action'),
		    'no_peminjaman_barang' => set_value('no_peminjaman_barang'),
		    'tanggal_pinjam' => set_value('tanggal_pinjam'),
		    'tanggal_kembali' => set_value('tanggal_kembali'),
		    'nama_peminjam' => set_value('nama_peminjam'),
		    'alamat_peminjam' => set_value('alamat_peminjam'),
		    'keperluan' => set_value('keperluan'),
		    'telp_peminjam' => set_value('telp_peminjam'),
		);
        $data['page'] = 'peminjaman-barang/peminjaman_barang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peminjaman-barang/create'));
        } else {
            $data = array(
				'no_peminjaman_barang' => post('no_peminjaman_barang',true),
				'tanggal_pinjam' => post('tanggal_pinjam',true),
				'tanggal_kembali' => post('tanggal_kembali',true),
				'nama_peminjam' => post('nama_peminjam',true),
				'alamat_peminjam' => post('alamat_peminjam',true),
				'keperluan' => post('keperluan',true),
				'telp_peminjam' => post('telp_peminjam',true),
		    );

            $this->peminjaman_barang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peminjaman-barang'));
        }
    }

    public function update($id)
    {
        $row = $this->peminjaman_barang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peminjaman-barang/update-action'),
				'peminjaman_barang_id' => set_value('peminjaman_barang_id', $row->peminjaman_barang_id),
				'no_peminjaman_barang' => set_value('no_peminjaman_barang', $row->no_peminjaman_barang),
				'tanggal_pinjam' => set_value('tanggal_pinjam', $row->tanggal_pinjam),
				'tanggal_kembali' => set_value('tanggal_kembali', $row->tanggal_kembali),
				'nama_peminjam' => set_value('nama_peminjam', $row->nama_peminjam),
				'alamat_peminjam' => set_value('alamat_peminjam', $row->alamat_peminjam),
				'keperluan' => set_value('keperluan', $row->keperluan),
				'telp_peminjam' => set_value('telp_peminjam', $row->telp_peminjam),
			    );

            $data['page'] = 'peminjaman-barang/peminjaman_barang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peminjaman-barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peminjaman-barang/update/'.post('peminjaman_barang_id', true)));
        } else {
            $data = array(
				'no_peminjaman_barang' => post('no_peminjaman_barang',true),
				'tanggal_pinjam' => post('tanggal_pinjam',true),
				'tanggal_kembali' => post('tanggal_kembali',true),
				'nama_peminjam' => post('nama_peminjam',true),
				'alamat_peminjam' => post('alamat_peminjam',true),
				'keperluan' => post('keperluan',true),
				'telp_peminjam' => post('telp_peminjam',true),
		    );

            $this->peminjaman_barang_model->update($data, post('peminjaman_barang_id', true));

            success('Update Record Success');
            redirect(base_url('peminjaman-barang/update/'.post('peminjaman_barang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peminjaman_barang_model->get($id);

        if ($row) {
            $this->peminjaman_barang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peminjaman-barang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peminjaman-barang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('no_peminjaman_barang', 'no peminjaman barang', 'trim|required');
		$this->form_validation->set_rules('tanggal_pinjam', 'tanggal pinjam', 'trim|required');
		$this->form_validation->set_rules('tanggal_kembali', 'tanggal kembali', 'trim|required');
		$this->form_validation->set_rules('nama_peminjam', 'nama peminjam', 'trim|required');
		$this->form_validation->set_rules('alamat_peminjam', 'alamat peminjam', 'trim|required');
		$this->form_validation->set_rules('keperluan', 'keperluan', 'trim|required');
		$this->form_validation->set_rules('telp_peminjam', 'telp peminjam', 'trim|required');

		$this->form_validation->set_rules('peminjaman_barang_id', 'peminjaman_barang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peminjaman_barang.php */
/* Location: ./application/controllers/peminjaman_barang.php */
/* Please DO NOT modify this information : */