<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class transfer_stock extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('transfer_stock_model');
    }

    public function index()
    {
        $data['page'] = 'transfer-stock/transfer_stock_list';
        $data['title'] = 'List Transfer Stock';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->transfer_stock_model->json();
    }

    public function read($id)
    {
        $row = $this->transfer_stock_model->get($id);
        if ($row) {
            $data = array(
				'gudang_asal' => $row->gudang_asal,
				'gudang_tujuan' => $row->gudang_tujuan,
				'tanggal_transfer' => $row->tanggal_transfer,
				'keperluan' => $row->keperluan,
				'catatan_pengiriman' => $row->catatan_pengiriman,
				'catatan_penerimaan' => $row->catatan_penerimaan,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'transfer-stock/transfer_stock_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('transfer-stock'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('transfer-stock/create-action'),
		    'gudang_asal' => set_value('gudang_asal'),
		    'gudang_tujuan' => set_value('gudang_tujuan'),
		    'tanggal_transfer' => set_value('tanggal_transfer'),
		    'keperluan' => set_value('keperluan'),
		    'catatan_pengiriman' => set_value('catatan_pengiriman'),
		    'catatan_penerimaan' => set_value('catatan_penerimaan'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'transfer-stock/transfer_stock_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transfer-stock/create'));
        } else {
            $data = array(
				'gudang_asal' => post('gudang_asal',true),
				'gudang_tujuan' => post('gudang_tujuan',true),
				'tanggal_transfer' => post('tanggal_transfer',true),
				'keperluan' => post('keperluan',true),
				'catatan_pengiriman' => post('catatan_pengiriman',true),
				'catatan_penerimaan' => post('catatan_penerimaan',true),
				'status' => post('status',true),
		    );

            $this->transfer_stock_model->insert($data);

            success('Create Record Success');
            redirect(base_url('transfer-stock'));
        }
    }

    public function update($id)
    {
        $row = $this->transfer_stock_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('transfer-stock/update-action'),
				'transfer_stock_id' => set_value('transfer_stock_id', $row->transfer_stock_id),
				'gudang_asal' => set_value('gudang_asal', $row->gudang_asal),
				'gudang_tujuan' => set_value('gudang_tujuan', $row->gudang_tujuan),
				'tanggal_transfer' => set_value('tanggal_transfer', $row->tanggal_transfer),
				'keperluan' => set_value('keperluan', $row->keperluan),
				'catatan_pengiriman' => set_value('catatan_pengiriman', $row->catatan_pengiriman),
				'catatan_penerimaan' => set_value('catatan_penerimaan', $row->catatan_penerimaan),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'transfer-stock/transfer_stock_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('transfer-stock'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transfer-stock/update/'.post('transfer_stock_id', true)));
        } else {
            $data = array(
				'gudang_asal' => post('gudang_asal',true),
				'gudang_tujuan' => post('gudang_tujuan',true),
				'tanggal_transfer' => post('tanggal_transfer',true),
				'keperluan' => post('keperluan',true),
				'catatan_pengiriman' => post('catatan_pengiriman',true),
				'catatan_penerimaan' => post('catatan_penerimaan',true),
				'status' => post('status',true),
		    );

            $this->transfer_stock_model->update($data, post('transfer_stock_id', true));

            success('Update Record Success');
            redirect(base_url('transfer-stock/update/'.post('transfer_stock_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->transfer_stock_model->get($id);

        if ($row) {
            $this->transfer_stock_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('transfer-stock'));
        } else {

            warning('Record Not Found');
            redirect(base_url('transfer-stock'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('gudang_asal', 'gudang asal', 'trim|required|numeric');
		$this->form_validation->set_rules('gudang_tujuan', 'gudang tujuan', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal_transfer', 'tanggal transfer', 'trim|required');
		$this->form_validation->set_rules('keperluan', 'keperluan', 'trim|required');
		$this->form_validation->set_rules('catatan_pengiriman', 'catatan pengiriman', 'trim|required');
		$this->form_validation->set_rules('catatan_penerimaan', 'catatan penerimaan', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('transfer_stock_id', 'transfer_stock_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file transfer_stock.php */
/* Location: ./application/controllers/transfer_stock.php */
/* Please DO NOT modify this information : */