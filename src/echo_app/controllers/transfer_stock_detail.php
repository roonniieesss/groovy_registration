<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class transfer_stock_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('transfer_stock_detail_model');
    }

    public function index()
    {
        $data['page'] = 'transfer-stock-detail/transfer_stock_detail_list';
        $data['title'] = 'List Transfer Stock Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->transfer_stock_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->transfer_stock_detail_model->get($id);
        if ($row) {
            $data = array(
				'transfer_stock_id' => $row->transfer_stock_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'qty' => $row->qty,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'transfer-stock-detail/transfer_stock_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('transfer-stock-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('transfer-stock-detail/create-action'),
		    'transfer_stock_id' => set_value('transfer_stock_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'qty' => set_value('qty'),
		);
        $data['page'] = 'transfer-stock-detail/transfer_stock_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transfer-stock-detail/create'));
        } else {
            $data = array(
				'transfer_stock_id' => post('transfer_stock_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'qty' => post('qty',true),
		    );

            $this->transfer_stock_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('transfer-stock-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->transfer_stock_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('transfer-stock-detail/update-action'),
				'transfer_stock_detail_id' => set_value('transfer_stock_detail_id', $row->transfer_stock_detail_id),
				'transfer_stock_id' => set_value('transfer_stock_id', $row->transfer_stock_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'qty' => set_value('qty', $row->qty),
			    );

            $data['page'] = 'transfer-stock-detail/transfer_stock_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('transfer-stock-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('transfer-stock-detail/update/'.post('transfer_stock_detail_id', true)));
        } else {
            $data = array(
				'transfer_stock_id' => post('transfer_stock_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'qty' => post('qty',true),
		    );

            $this->transfer_stock_detail_model->update($data, post('transfer_stock_detail_id', true));

            success('Update Record Success');
            redirect(base_url('transfer-stock-detail/update/'.post('transfer_stock_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->transfer_stock_detail_model->get($id);

        if ($row) {
            $this->transfer_stock_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('transfer-stock-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('transfer-stock-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('transfer_stock_id', 'transfer stock', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('qty', 'qty', 'trim|required|numeric');

		$this->form_validation->set_rules('transfer_stock_detail_id', 'transfer_stock_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file transfer_stock_detail.php */
/* Location: ./application/controllers/transfer_stock_detail.php */
/* Please DO NOT modify this information : */