<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class akun extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('akun_model');
    }

    public function index()
    {
        $data['page'] = 'akun/akun_list';
        $data['title'] = 'List Akun';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->akun_model->json();
    }

    public function read($id)
    {
        $row = $this->akun_model->get($id);
        if ($row) {
            $data = array(
				'nama_akun' => $row->nama_akun,
				'kelompok' => $row->kelompok,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'akun/akun_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('akun'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('akun/create-action'),
		    'nama_akun' => set_value('nama_akun'),
		    'kelompok' => set_value('kelompok'),
		);
        $data['page'] = 'akun/akun_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('akun/create'));
        } else {
            $data = array(
				'nama_akun' => post('nama_akun',true),
				'kelompok' => post('kelompok',true),
		    );

            $this->akun_model->insert($data);

            success('Create Record Success');
            redirect(base_url('akun'));
        }
    }

    public function update($id)
    {
        $row = $this->akun_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('akun/update-action'),
				'akun_id' => set_value('akun_id', $row->akun_id),
				'nama_akun' => set_value('nama_akun', $row->nama_akun),
				'kelompok' => set_value('kelompok', $row->kelompok),
			    );

            $data['page'] = 'akun/akun_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('akun'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('akun/update/'.post('akun_id', true)));
        } else {
            $data = array(
				'nama_akun' => post('nama_akun',true),
				'kelompok' => post('kelompok',true),
		    );

            $this->akun_model->update($data, post('akun_id', true));

            success('Update Record Success');
            redirect(base_url('akun/update/'.post('akun_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->akun_model->get($id);

        if ($row) {
            $this->akun_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('akun'));
        } else {

            warning('Record Not Found');
            redirect(base_url('akun'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_akun', 'nama akun', 'trim|required');
		$this->form_validation->set_rules('kelompok', 'kelompok', 'trim|required');

		$this->form_validation->set_rules('akun_id', 'akun_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file akun.php */
/* Location: ./application/controllers/akun.php */
/* Please DO NOT modify this information : */