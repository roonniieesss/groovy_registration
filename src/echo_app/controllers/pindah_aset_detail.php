<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class pindah_aset_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('pindah_aset_detail_model');
    }

    public function index()
    {
        $data['page'] = 'pindah-aset-detail/pindah_aset_detail_list';
        $data['title'] = 'List Pindah Aset Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->pindah_aset_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->pindah_aset_detail_model->get($id);
        if ($row) {
            $data = array(
				'pindah_aset_id' => $row->pindah_aset_id,
				'penempatan_aset_asal' => $row->penempatan_aset_asal,
				'penempatan_aset_tujuan' => $row->penempatan_aset_tujuan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'pindah-aset-detail/pindah_aset_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-aset-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('pindah-aset-detail/create-action'),
		    'pindah_aset_id' => set_value('pindah_aset_id'),
		    'penempatan_aset_asal' => set_value('penempatan_aset_asal'),
		    'penempatan_aset_tujuan' => set_value('penempatan_aset_tujuan'),
		);
        $data['page'] = 'pindah-aset-detail/pindah_aset_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-aset-detail/create'));
        } else {
            $data = array(
				'pindah_aset_id' => post('pindah_aset_id',true),
				'penempatan_aset_asal' => post('penempatan_aset_asal',true),
				'penempatan_aset_tujuan' => post('penempatan_aset_tujuan',true),
		    );

            $this->pindah_aset_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('pindah-aset-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->pindah_aset_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('pindah-aset-detail/update-action'),
				'pindah_aset_detail_id' => set_value('pindah_aset_detail_id', $row->pindah_aset_detail_id),
				'pindah_aset_id' => set_value('pindah_aset_id', $row->pindah_aset_id),
				'penempatan_aset_asal' => set_value('penempatan_aset_asal', $row->penempatan_aset_asal),
				'penempatan_aset_tujuan' => set_value('penempatan_aset_tujuan', $row->penempatan_aset_tujuan),
			    );

            $data['page'] = 'pindah-aset-detail/pindah_aset_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('pindah-aset-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('pindah-aset-detail/update/'.post('pindah_aset_detail_id', true)));
        } else {
            $data = array(
				'pindah_aset_id' => post('pindah_aset_id',true),
				'penempatan_aset_asal' => post('penempatan_aset_asal',true),
				'penempatan_aset_tujuan' => post('penempatan_aset_tujuan',true),
		    );

            $this->pindah_aset_detail_model->update($data, post('pindah_aset_detail_id', true));

            success('Update Record Success');
            redirect(base_url('pindah-aset-detail/update/'.post('pindah_aset_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->pindah_aset_detail_model->get($id);

        if ($row) {
            $this->pindah_aset_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('pindah-aset-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('pindah-aset-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pindah_aset_id', 'pindah aset', 'trim|required|numeric');
		$this->form_validation->set_rules('penempatan_aset_asal', 'penempatan aset asal', 'trim|required|numeric');
		$this->form_validation->set_rules('penempatan_aset_tujuan', 'penempatan aset tujuan', 'trim|required|numeric');

		$this->form_validation->set_rules('pindah_aset_detail_id', 'pindah_aset_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file pindah_aset_detail.php */
/* Location: ./application/controllers/pindah_aset_detail.php */
/* Please DO NOT modify this information : */