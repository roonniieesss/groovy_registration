<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kupon extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kupon_model');
    }

    public function index()
    {
        $data['page'] = 'kupon/kupon_list';
        $data['title'] = 'List Kupon';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->kupon_model->json();
    }

    public function read($id)
    {
        $row = $this->kupon_model->get($id);
        if ($row) {
            $data = array(
				'kode_kupon' => $row->kode_kupon,
				'kuota' => $row->kuota,
				'kuota_per_pelanggan' => $row->kuota_per_pelanggan,
				'tanggal_habis_berlaku' => $row->tanggal_habis_berlaku,
				'nilai' => $row->nilai,
				'tipe_nilai' => $row->tipe_nilai,
				'nilai_diskon_maks' => $row->nilai_diskon_maks,
				'min_pembelian' => $row->min_pembelian,
				'pembatasan_jenis_barang' => $row->pembatasan_jenis_barang,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'kupon/kupon_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kupon'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('kupon/create-action'),
		    'kode_kupon' => set_value('kode_kupon'),
		    'kuota' => set_value('kuota'),
		    'kuota_per_pelanggan' => set_value('kuota_per_pelanggan'),
		    'tanggal_habis_berlaku' => set_value('tanggal_habis_berlaku'),
		    'nilai' => set_value('nilai'),
		    'tipe_nilai' => set_value('tipe_nilai'),
		    'nilai_diskon_maks' => set_value('nilai_diskon_maks'),
		    'min_pembelian' => set_value('min_pembelian'),
		    'pembatasan_jenis_barang' => set_value('pembatasan_jenis_barang'),
		);
        $data['page'] = 'kupon/kupon_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kupon/create'));
        } else {
            $data = array(
				'kode_kupon' => post('kode_kupon',true),
				'kuota' => post('kuota',true),
				'kuota_per_pelanggan' => post('kuota_per_pelanggan',true),
				'tanggal_habis_berlaku' => post('tanggal_habis_berlaku',true),
				'nilai' => post('nilai',true),
				'tipe_nilai' => post('tipe_nilai',true),
				'nilai_diskon_maks' => post('nilai_diskon_maks',true),
				'min_pembelian' => post('min_pembelian',true),
				'pembatasan_jenis_barang' => post('pembatasan_jenis_barang',true),
		    );

            $this->kupon_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kupon'));
        }
    }

    public function update($id)
    {
        $row = $this->kupon_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('kupon/update-action'),
				'kupon_id' => set_value('kupon_id', $row->kupon_id),
				'kode_kupon' => set_value('kode_kupon', $row->kode_kupon),
				'kuota' => set_value('kuota', $row->kuota),
				'kuota_per_pelanggan' => set_value('kuota_per_pelanggan', $row->kuota_per_pelanggan),
				'tanggal_habis_berlaku' => set_value('tanggal_habis_berlaku', $row->tanggal_habis_berlaku),
				'nilai' => set_value('nilai', $row->nilai),
				'tipe_nilai' => set_value('tipe_nilai', $row->tipe_nilai),
				'nilai_diskon_maks' => set_value('nilai_diskon_maks', $row->nilai_diskon_maks),
				'min_pembelian' => set_value('min_pembelian', $row->min_pembelian),
				'pembatasan_jenis_barang' => set_value('pembatasan_jenis_barang', $row->pembatasan_jenis_barang),
			    );

            $data['page'] = 'kupon/kupon_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kupon'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kupon/update/'.post('kupon_id', true)));
        } else {
            $data = array(
				'kode_kupon' => post('kode_kupon',true),
				'kuota' => post('kuota',true),
				'kuota_per_pelanggan' => post('kuota_per_pelanggan',true),
				'tanggal_habis_berlaku' => post('tanggal_habis_berlaku',true),
				'nilai' => post('nilai',true),
				'tipe_nilai' => post('tipe_nilai',true),
				'nilai_diskon_maks' => post('nilai_diskon_maks',true),
				'min_pembelian' => post('min_pembelian',true),
				'pembatasan_jenis_barang' => post('pembatasan_jenis_barang',true),
		    );

            $this->kupon_model->update($data, post('kupon_id', true));

            success('Update Record Success');
            redirect(base_url('kupon/update/'.post('kupon_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kupon_model->get($id);

        if ($row) {
            $this->kupon_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kupon'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kupon'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('kode_kupon', 'kode kupon', 'trim|required');
		$this->form_validation->set_rules('kuota', 'kuota', 'trim|required|numeric');
		$this->form_validation->set_rules('kuota_per_pelanggan', 'kuota per pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('tanggal_habis_berlaku', 'tanggal habis berlaku', 'trim|required');
		$this->form_validation->set_rules('nilai', 'nilai', 'trim|required|numeric');
		$this->form_validation->set_rules('tipe_nilai', 'tipe nilai', 'trim|required');
		$this->form_validation->set_rules('nilai_diskon_maks', 'nilai diskon maks', 'trim|required|numeric');
		$this->form_validation->set_rules('min_pembelian', 'min pembelian', 'trim|required|numeric');
		$this->form_validation->set_rules('pembatasan_jenis_barang', 'pembatasan jenis barang', 'trim|required');

		$this->form_validation->set_rules('kupon_id', 'kupon_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file kupon.php */
/* Location: ./application/controllers/kupon.php */
/* Please DO NOT modify this information : */