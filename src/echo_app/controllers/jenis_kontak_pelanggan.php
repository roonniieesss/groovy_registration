<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_kontak_pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('jenis_kontak_pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'jenis-kontak-pelanggan/jenis_kontak_pelanggan_list';
        $data['title'] = 'List Jenis Kontak Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->jenis_kontak_pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->jenis_kontak_pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'nama_jenis_kontak_pelanggan' => $row->nama_jenis_kontak_pelanggan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'jenis-kontak-pelanggan/jenis_kontak_pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-kontak-pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('jenis-kontak-pelanggan/create-action'),
		    'nama_jenis_kontak_pelanggan' => set_value('nama_jenis_kontak_pelanggan'),
		);
        $data['page'] = 'jenis-kontak-pelanggan/jenis_kontak_pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-kontak-pelanggan/create'));
        } else {
            $data = array(
				'nama_jenis_kontak_pelanggan' => post('nama_jenis_kontak_pelanggan',true),
		    );

            $this->jenis_kontak_pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('jenis-kontak-pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->jenis_kontak_pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('jenis-kontak-pelanggan/update-action'),
				'jenis_kontak_pelanggan_id' => set_value('jenis_kontak_pelanggan_id', $row->jenis_kontak_pelanggan_id),
				'nama_jenis_kontak_pelanggan' => set_value('nama_jenis_kontak_pelanggan', $row->nama_jenis_kontak_pelanggan),
			    );

            $data['page'] = 'jenis-kontak-pelanggan/jenis_kontak_pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('jenis-kontak-pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('jenis-kontak-pelanggan/update/'.post('jenis_kontak_pelanggan_id', true)));
        } else {
            $data = array(
				'nama_jenis_kontak_pelanggan' => post('nama_jenis_kontak_pelanggan',true),
		    );

            $this->jenis_kontak_pelanggan_model->update($data, post('jenis_kontak_pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('jenis-kontak-pelanggan/update/'.post('jenis_kontak_pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->jenis_kontak_pelanggan_model->get($id);

        if ($row) {
            $this->jenis_kontak_pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('jenis-kontak-pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('jenis-kontak-pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_jenis_kontak_pelanggan', 'nama jenis kontak pelanggan', 'trim|required');

		$this->form_validation->set_rules('jenis_kontak_pelanggan_id', 'jenis_kontak_pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file jenis_kontak_pelanggan.php */
/* Location: ./application/controllers/jenis_kontak_pelanggan.php */
/* Please DO NOT modify this information : */