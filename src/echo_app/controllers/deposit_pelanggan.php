<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class deposit_pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('deposit_pelanggan_model');
    }

    public function index()
    {
        $data['page'] = 'deposit-pelanggan/deposit_pelanggan_list';
        $data['title'] = 'List Deposit Pelanggan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->deposit_pelanggan_model->json();
    }

    public function read($id)
    {
        $row = $this->deposit_pelanggan_model->get($id);
        if ($row) {
            $data = array(
				'pelanggan_id' => $row->pelanggan_id,
				'transaksi_keuangan_id' => $row->transaksi_keuangan_id,
				'kurs' => $row->kurs,
				'nilai' => $row->nilai,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'deposit-pelanggan/deposit_pelanggan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('deposit-pelanggan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('deposit-pelanggan/create-action'),
		    'pelanggan_id' => set_value('pelanggan_id'),
		    'transaksi_keuangan_id' => set_value('transaksi_keuangan_id'),
		    'kurs' => set_value('kurs'),
		    'nilai' => set_value('nilai'),
		);
        $data['page'] = 'deposit-pelanggan/deposit_pelanggan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('deposit-pelanggan/create'));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'kurs' => post('kurs',true),
				'nilai' => post('nilai',true),
		    );

            $this->deposit_pelanggan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('deposit-pelanggan'));
        }
    }

    public function update($id)
    {
        $row = $this->deposit_pelanggan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('deposit-pelanggan/update-action'),
				'deposit_pelanggan_id' => set_value('deposit_pelanggan_id', $row->deposit_pelanggan_id),
				'pelanggan_id' => set_value('pelanggan_id', $row->pelanggan_id),
				'transaksi_keuangan_id' => set_value('transaksi_keuangan_id', $row->transaksi_keuangan_id),
				'kurs' => set_value('kurs', $row->kurs),
				'nilai' => set_value('nilai', $row->nilai),
			    );

            $data['page'] = 'deposit-pelanggan/deposit_pelanggan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('deposit-pelanggan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('deposit-pelanggan/update/'.post('deposit_pelanggan_id', true)));
        } else {
            $data = array(
				'pelanggan_id' => post('pelanggan_id',true),
				'transaksi_keuangan_id' => post('transaksi_keuangan_id',true),
				'kurs' => post('kurs',true),
				'nilai' => post('nilai',true),
		    );

            $this->deposit_pelanggan_model->update($data, post('deposit_pelanggan_id', true));

            success('Update Record Success');
            redirect(base_url('deposit-pelanggan/update/'.post('deposit_pelanggan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->deposit_pelanggan_model->get($id);

        if ($row) {
            $this->deposit_pelanggan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('deposit-pelanggan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('deposit-pelanggan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('pelanggan_id', 'pelanggan', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_keuangan_id', 'transaksi keuangan', 'trim|required|numeric');
		$this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
		$this->form_validation->set_rules('nilai', 'nilai', 'trim|required|numeric');

		$this->form_validation->set_rules('deposit_pelanggan_id', 'deposit_pelanggan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file deposit_pelanggan.php */
/* Location: ./application/controllers/deposit_pelanggan.php */
/* Please DO NOT modify this information : */