<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class tagihan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('tagihan_model');
    }

    public function index()
    {
        $data['page'] = 'tagihan/tagihan_list';
        $data['title'] = 'List Tagihan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->tagihan_model->json();
    }

    public function read($id)
    {
        $row = $this->tagihan_model->get($id);
        if ($row) {
            $data = array(
				'purchase_order_id' => $row->purchase_order_id,
				'no_invoice_supplier' => $row->no_invoice_supplier,
				'tanggal_invoice_supplier' => $row->tanggal_invoice_supplier,
				'jatuh_tempo' => $row->jatuh_tempo,
				'total_tagihan' => $row->total_tagihan,
				'kurs' => $row->kurs,
				'status' => $row->status,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'tagihan/tagihan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('tagihan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('tagihan/create-action'),
		    'purchase_order_id' => set_value('purchase_order_id'),
		    'no_invoice_supplier' => set_value('no_invoice_supplier'),
		    'tanggal_invoice_supplier' => set_value('tanggal_invoice_supplier'),
		    'jatuh_tempo' => set_value('jatuh_tempo'),
		    'total_tagihan' => set_value('total_tagihan'),
		    'kurs' => set_value('kurs'),
		    'status' => set_value('status'),
		);
        $data['page'] = 'tagihan/tagihan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('tagihan/create'));
        } else {
            $data = array(
				'purchase_order_id' => post('purchase_order_id',true),
				'no_invoice_supplier' => post('no_invoice_supplier',true),
				'tanggal_invoice_supplier' => post('tanggal_invoice_supplier',true),
				'jatuh_tempo' => post('jatuh_tempo',true),
				'total_tagihan' => post('total_tagihan',true),
				'kurs' => post('kurs',true),
				'status' => post('status',true),
		    );

            $this->tagihan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('tagihan'));
        }
    }

    public function update($id)
    {
        $row = $this->tagihan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('tagihan/update-action'),
				'tagihan_id' => set_value('tagihan_id', $row->tagihan_id),
				'purchase_order_id' => set_value('purchase_order_id', $row->purchase_order_id),
				'no_invoice_supplier' => set_value('no_invoice_supplier', $row->no_invoice_supplier),
				'tanggal_invoice_supplier' => set_value('tanggal_invoice_supplier', $row->tanggal_invoice_supplier),
				'jatuh_tempo' => set_value('jatuh_tempo', $row->jatuh_tempo),
				'total_tagihan' => set_value('total_tagihan', $row->total_tagihan),
				'kurs' => set_value('kurs', $row->kurs),
				'status' => set_value('status', $row->status),
			    );

            $data['page'] = 'tagihan/tagihan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('tagihan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('tagihan/update/'.post('tagihan_id', true)));
        } else {
            $data = array(
				'purchase_order_id' => post('purchase_order_id',true),
				'no_invoice_supplier' => post('no_invoice_supplier',true),
				'tanggal_invoice_supplier' => post('tanggal_invoice_supplier',true),
				'jatuh_tempo' => post('jatuh_tempo',true),
				'total_tagihan' => post('total_tagihan',true),
				'kurs' => post('kurs',true),
				'status' => post('status',true),
		    );

            $this->tagihan_model->update($data, post('tagihan_id', true));

            success('Update Record Success');
            redirect(base_url('tagihan/update/'.post('tagihan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->tagihan_model->get($id);

        if ($row) {
            $this->tagihan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('tagihan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('tagihan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('purchase_order_id', 'purchase order', 'trim|required|numeric');
		$this->form_validation->set_rules('no_invoice_supplier', 'no invoice supplier', 'trim|required');
		$this->form_validation->set_rules('tanggal_invoice_supplier', 'tanggal invoice supplier', 'trim|required');
		$this->form_validation->set_rules('jatuh_tempo', 'jatuh tempo', 'trim|required');
		$this->form_validation->set_rules('total_tagihan', 'total tagihan', 'trim|required|numeric');
		$this->form_validation->set_rules('kurs', 'kurs', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('tagihan_id', 'tagihan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file tagihan.php */
/* Location: ./application/controllers/tagihan.php */
/* Please DO NOT modify this information : */