<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class peraturan_exim_jenis_dokumen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('peraturan_exim_jenis_dokumen_model');
    }

    public function index()
    {
        $data['page'] = 'peraturan-exim-jenis-dokumen/peraturan_exim_jenis_dokumen_list';
        $data['title'] = 'List Peraturan Exim Jenis Dokumen';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->peraturan_exim_jenis_dokumen_model->json();
    }

    public function read($id)
    {
        $row = $this->peraturan_exim_jenis_dokumen_model->get($id);
        if ($row) {
            $data = array(
				'peraturan_exim_ras_hewan_id' => $row->peraturan_exim_ras_hewan_id,
				'jenis_dokumen_id' => $row->jenis_dokumen_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'peraturan-exim-jenis-dokumen/peraturan_exim_jenis_dokumen_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-jenis-dokumen'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('peraturan-exim-jenis-dokumen/create-action'),
		    'peraturan_exim_ras_hewan_id' => set_value('peraturan_exim_ras_hewan_id'),
		    'jenis_dokumen_id' => set_value('jenis_dokumen_id'),
		);
        $data['page'] = 'peraturan-exim-jenis-dokumen/peraturan_exim_jenis_dokumen_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-jenis-dokumen/create'));
        } else {
            $data = array(
				'peraturan_exim_ras_hewan_id' => post('peraturan_exim_ras_hewan_id',true),
				'jenis_dokumen_id' => post('jenis_dokumen_id',true),
		    );

            $this->peraturan_exim_jenis_dokumen_model->insert($data);

            success('Create Record Success');
            redirect(base_url('peraturan-exim-jenis-dokumen'));
        }
    }

    public function update($id)
    {
        $row = $this->peraturan_exim_jenis_dokumen_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('peraturan-exim-jenis-dokumen/update-action'),
				'peraturan_exim_jenis_dokumen_id' => set_value('peraturan_exim_jenis_dokumen_id', $row->peraturan_exim_jenis_dokumen_id),
				'peraturan_exim_ras_hewan_id' => set_value('peraturan_exim_ras_hewan_id', $row->peraturan_exim_ras_hewan_id),
				'jenis_dokumen_id' => set_value('jenis_dokumen_id', $row->jenis_dokumen_id),
			    );

            $data['page'] = 'peraturan-exim-jenis-dokumen/peraturan_exim_jenis_dokumen_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('peraturan-exim-jenis-dokumen'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('peraturan-exim-jenis-dokumen/update/'.post('peraturan_exim_jenis_dokumen_id', true)));
        } else {
            $data = array(
				'peraturan_exim_ras_hewan_id' => post('peraturan_exim_ras_hewan_id',true),
				'jenis_dokumen_id' => post('jenis_dokumen_id',true),
		    );

            $this->peraturan_exim_jenis_dokumen_model->update($data, post('peraturan_exim_jenis_dokumen_id', true));

            success('Update Record Success');
            redirect(base_url('peraturan-exim-jenis-dokumen/update/'.post('peraturan_exim_jenis_dokumen_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->peraturan_exim_jenis_dokumen_model->get($id);

        if ($row) {
            $this->peraturan_exim_jenis_dokumen_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('peraturan-exim-jenis-dokumen'));
        } else {

            warning('Record Not Found');
            redirect(base_url('peraturan-exim-jenis-dokumen'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('peraturan_exim_ras_hewan_id', 'peraturan exim ras hewan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_dokumen_id', 'jenis dokumen', 'trim|required|numeric');

		$this->form_validation->set_rules('peraturan_exim_jenis_dokumen_id', 'peraturan_exim_jenis_dokumen_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file peraturan_exim_jenis_dokumen.php */
/* Location: ./application/controllers/peraturan_exim_jenis_dokumen.php */
/* Please DO NOT modify this information : */