<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kategori_layanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('kategori_layanan_model');
    }

    public function index()
    {
        $data['page'] = 'kategori-layanan/kategori_layanan_list';
        $data['title'] = 'List Kategori Layanan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->kategori_layanan_model->json();
    }

    public function read($id)
    {
        $row = $this->kategori_layanan_model->get($id);
        if ($row) {
            $data = array(
				'parent_kategori_layanan_id' => $row->parent_kategori_layanan_id,
				'nama_kategori_layanan' => $row->nama_kategori_layanan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'kategori-layanan/kategori_layanan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('kategori-layanan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('kategori-layanan/create-action'),
		    'parent_kategori_layanan_id' => set_value('parent_kategori_layanan_id'),
		    'nama_kategori_layanan' => set_value('nama_kategori_layanan'),
		);
        $data['page'] = 'kategori-layanan/kategori_layanan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kategori-layanan/create'));
        } else {
            $data = array(
				'parent_kategori_layanan_id' => post('parent_kategori_layanan_id',true),
				'nama_kategori_layanan' => post('nama_kategori_layanan',true),
		    );

            $this->kategori_layanan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('kategori-layanan'));
        }
    }

    public function update($id)
    {
        $row = $this->kategori_layanan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('kategori-layanan/update-action'),
				'kategori_layanan_id' => set_value('kategori_layanan_id', $row->kategori_layanan_id),
				'parent_kategori_layanan_id' => set_value('parent_kategori_layanan_id', $row->parent_kategori_layanan_id),
				'nama_kategori_layanan' => set_value('nama_kategori_layanan', $row->nama_kategori_layanan),
			    );

            $data['page'] = 'kategori-layanan/kategori_layanan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('kategori-layanan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('kategori-layanan/update/'.post('kategori_layanan_id', true)));
        } else {
            $data = array(
				'parent_kategori_layanan_id' => post('parent_kategori_layanan_id',true),
				'nama_kategori_layanan' => post('nama_kategori_layanan',true),
		    );

            $this->kategori_layanan_model->update($data, post('kategori_layanan_id', true));

            success('Update Record Success');
            redirect(base_url('kategori-layanan/update/'.post('kategori_layanan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->kategori_layanan_model->get($id);

        if ($row) {
            $this->kategori_layanan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('kategori-layanan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('kategori-layanan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('parent_kategori_layanan_id', 'parent kategori layanan', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_kategori_layanan', 'nama kategori layanan', 'trim|required');

		$this->form_validation->set_rules('kategori_layanan_id', 'kategori_layanan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file kategori_layanan.php */
/* Location: ./application/controllers/kategori_layanan.php */
/* Please DO NOT modify this information : */