<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class batal_inquiry extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('batal_inquiry_model');
    }

    public function index()
    {
        $data['page'] = 'batal-inquiry/batal_inquiry_list';
        $data['title'] = 'List Batal Inquiry';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->batal_inquiry_model->json();
    }

    public function read($id)
    {
        $row = $this->batal_inquiry_model->get($id);
        if ($row) {
            $data = array(
				'inquiry_id' => $row->inquiry_id,
				'alasan_batal_inquiry_id' => $row->alasan_batal_inquiry_id,
				'keterangan_alasan' => $row->keterangan_alasan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'batal-inquiry/batal_inquiry_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('batal-inquiry'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('batal-inquiry/create-action'),
		    'inquiry_id' => set_value('inquiry_id'),
		    'alasan_batal_inquiry_id' => set_value('alasan_batal_inquiry_id'),
		    'keterangan_alasan' => set_value('keterangan_alasan'),
		);
        $data['page'] = 'batal-inquiry/batal_inquiry_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('batal-inquiry/create'));
        } else {
            $data = array(
				'inquiry_id' => post('inquiry_id',true),
				'alasan_batal_inquiry_id' => post('alasan_batal_inquiry_id',true),
				'keterangan_alasan' => post('keterangan_alasan',true),
		    );

            $this->batal_inquiry_model->insert($data);

            success('Create Record Success');
            redirect(base_url('batal-inquiry'));
        }
    }

    public function update($id)
    {
        $row = $this->batal_inquiry_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('batal-inquiry/update-action'),
				'inquiry_id' => set_value('inquiry_id', $row->inquiry_id),
				'inquiry_id' => set_value('inquiry_id', $row->inquiry_id),
				'alasan_batal_inquiry_id' => set_value('alasan_batal_inquiry_id', $row->alasan_batal_inquiry_id),
				'keterangan_alasan' => set_value('keterangan_alasan', $row->keterangan_alasan),
			    );

            $data['page'] = 'batal-inquiry/batal_inquiry_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('batal-inquiry'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('batal-inquiry/update/'.post('', true)));
        } else {
            $data = array(
				'inquiry_id' => post('inquiry_id',true),
				'alasan_batal_inquiry_id' => post('alasan_batal_inquiry_id',true),
				'keterangan_alasan' => post('keterangan_alasan',true),
		    );

            $this->batal_inquiry_model->update($data, post('', true));

            success('Update Record Success');
            redirect(base_url('batal-inquiry/update/'.post('', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->batal_inquiry_model->get($id);

        if ($row) {
            $this->batal_inquiry_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('batal-inquiry'));
        } else {

            warning('Record Not Found');
            redirect(base_url('batal-inquiry'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('inquiry_id', 'inquiry', 'trim|required|numeric');
		$this->form_validation->set_rules('alasan_batal_inquiry_id', 'alasan batal inquiry', 'trim|required|numeric');
		$this->form_validation->set_rules('keterangan_alasan', 'keterangan alasan', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file batal_inquiry.php */
/* Location: ./application/controllers/batal_inquiry.php */
/* Please DO NOT modify this information : */