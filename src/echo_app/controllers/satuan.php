<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class satuan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('satuan_model');
    }

    public function index()
    {
        $data['page'] = 'satuan/satuan_list';
        $data['title'] = 'List Satuan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->satuan_model->json();
    }

    public function read($id)
    {
        $row = $this->satuan_model->get($id);
        if ($row) {
            $data = array(
				'nama_satuan' => $row->nama_satuan,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'satuan/satuan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('satuan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('satuan/create-action'),
		    'nama_satuan' => set_value('nama_satuan'),
		);
        $data['page'] = 'satuan/satuan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('satuan/create'));
        } else {
            $data = array(
				'nama_satuan' => post('nama_satuan',true),
		    );

            $this->satuan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('satuan'));
        }
    }

    public function update($id)
    {
        $row = $this->satuan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('satuan/update-action'),
				'satuan_id' => set_value('satuan_id', $row->satuan_id),
				'nama_satuan' => set_value('nama_satuan', $row->nama_satuan),
			    );

            $data['page'] = 'satuan/satuan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('satuan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('satuan/update/'.post('satuan_id', true)));
        } else {
            $data = array(
				'nama_satuan' => post('nama_satuan',true),
		    );

            $this->satuan_model->update($data, post('satuan_id', true));

            success('Update Record Success');
            redirect(base_url('satuan/update/'.post('satuan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->satuan_model->get($id);

        if ($row) {
            $this->satuan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('satuan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('satuan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('nama_satuan', 'nama satuan', 'trim|required');

		$this->form_validation->set_rules('satuan_id', 'satuan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file satuan.php */
/* Location: ./application/controllers/satuan.php */
/* Please DO NOT modify this information : */