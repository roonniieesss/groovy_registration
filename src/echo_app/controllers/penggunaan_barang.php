<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class penggunaan_barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('penggunaan_barang_model');
    }

    public function index()
    {
        $data['page'] = 'penggunaan-barang/penggunaan_barang_list';
        $data['title'] = 'List Penggunaan Barang';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->penggunaan_barang_model->json();
    }

    public function read($id)
    {
        $row = $this->penggunaan_barang_model->get($id);
        if ($row) {
            $data = array(
				'work_order_detail_id' => $row->work_order_detail_id,
				'transaksi_barang_id' => $row->transaksi_barang_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'penggunaan-barang/penggunaan_barang_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('penggunaan-barang'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('penggunaan-barang/create-action'),
		    'work_order_detail_id' => set_value('work_order_detail_id'),
		    'transaksi_barang_id' => set_value('transaksi_barang_id'),
		);
        $data['page'] = 'penggunaan-barang/penggunaan_barang_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penggunaan-barang/create'));
        } else {
            $data = array(
				'work_order_detail_id' => post('work_order_detail_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
		    );

            $this->penggunaan_barang_model->insert($data);

            success('Create Record Success');
            redirect(base_url('penggunaan-barang'));
        }
    }

    public function update($id)
    {
        $row = $this->penggunaan_barang_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('penggunaan-barang/update-action'),
				'penggunaan_barang_id' => set_value('penggunaan_barang_id', $row->penggunaan_barang_id),
				'work_order_detail_id' => set_value('work_order_detail_id', $row->work_order_detail_id),
				'transaksi_barang_id' => set_value('transaksi_barang_id', $row->transaksi_barang_id),
			    );

            $data['page'] = 'penggunaan-barang/penggunaan_barang_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('penggunaan-barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('penggunaan-barang/update/'.post('penggunaan_barang_id', true)));
        } else {
            $data = array(
				'work_order_detail_id' => post('work_order_detail_id',true),
				'transaksi_barang_id' => post('transaksi_barang_id',true),
		    );

            $this->penggunaan_barang_model->update($data, post('penggunaan_barang_id', true));

            success('Update Record Success');
            redirect(base_url('penggunaan-barang/update/'.post('penggunaan_barang_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->penggunaan_barang_model->get($id);

        if ($row) {
            $this->penggunaan_barang_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('penggunaan-barang'));
        } else {

            warning('Record Not Found');
            redirect(base_url('penggunaan-barang'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('work_order_detail_id', 'work order detail', 'trim|required|numeric');
		$this->form_validation->set_rules('transaksi_barang_id', 'transaksi barang', 'trim|required|numeric');

		$this->form_validation->set_rules('penggunaan_barang_id', 'penggunaan_barang_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file penggunaan_barang.php */
/* Location: ./application/controllers/penggunaan_barang.php */
/* Please DO NOT modify this information : */