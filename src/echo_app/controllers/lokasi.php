<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class lokasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('lokasi_model');
    }

    public function index()
    {
        $data['page'] = 'lokasi/lokasi_list';
        $data['title'] = 'List Lokasi';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->lokasi_model->json();
    }

    public function read($id)
    {
        $row = $this->lokasi_model->get($id);
        if ($row) {
            $data = array(
				'perusahaan_id' => $row->perusahaan_id,
				'nama_lokasi' => $row->nama_lokasi,
				'jenis_lokasi' => $row->jenis_lokasi,
				'longitude' => $row->longitude,
				'latitude' => $row->latitude,
				'hak_akses' => $row->hak_akses,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'lokasi/lokasi_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('lokasi'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('lokasi/create-action'),
		    'perusahaan_id' => set_value('perusahaan_id'),
		    'nama_lokasi' => set_value('nama_lokasi'),
		    'jenis_lokasi' => set_value('jenis_lokasi'),
		    'longitude' => set_value('longitude'),
		    'latitude' => set_value('latitude'),
		    'hak_akses' => set_value('hak_akses'),
		);
        $data['page'] = 'lokasi/lokasi_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('lokasi/create'));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'nama_lokasi' => post('nama_lokasi',true),
				'jenis_lokasi' => post('jenis_lokasi',true),
				'longitude' => post('longitude',true),
				'latitude' => post('latitude',true),
				'hak_akses' => post('hak_akses',true),
		    );

            $this->lokasi_model->insert($data);

            success('Create Record Success');
            redirect(base_url('lokasi'));
        }
    }

    public function update($id)
    {
        $row = $this->lokasi_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('lokasi/update-action'),
				'lokasi_id' => set_value('lokasi_id', $row->lokasi_id),
				'perusahaan_id' => set_value('perusahaan_id', $row->perusahaan_id),
				'nama_lokasi' => set_value('nama_lokasi', $row->nama_lokasi),
				'jenis_lokasi' => set_value('jenis_lokasi', $row->jenis_lokasi),
				'longitude' => set_value('longitude', $row->longitude),
				'latitude' => set_value('latitude', $row->latitude),
				'hak_akses' => set_value('hak_akses', $row->hak_akses),
			    );

            $data['page'] = 'lokasi/lokasi_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('lokasi'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('lokasi/update/'.post('lokasi_id', true)));
        } else {
            $data = array(
				'perusahaan_id' => post('perusahaan_id',true),
				'nama_lokasi' => post('nama_lokasi',true),
				'jenis_lokasi' => post('jenis_lokasi',true),
				'longitude' => post('longitude',true),
				'latitude' => post('latitude',true),
				'hak_akses' => post('hak_akses',true),
		    );

            $this->lokasi_model->update($data, post('lokasi_id', true));

            success('Update Record Success');
            redirect(base_url('lokasi/update/'.post('lokasi_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->lokasi_model->get($id);

        if ($row) {
            $this->lokasi_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('lokasi'));
        } else {

            warning('Record Not Found');
            redirect(base_url('lokasi'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('perusahaan_id', 'perusahaan', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_lokasi', 'nama lokasi', 'trim|required');
		$this->form_validation->set_rules('jenis_lokasi', 'jenis lokasi', 'trim|required');
		$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
		$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
		$this->form_validation->set_rules('hak_akses', 'hak akses', 'trim|required');

		$this->form_validation->set_rules('lokasi_id', 'lokasi_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file lokasi.php */
/* Location: ./application/controllers/lokasi.php */
/* Please DO NOT modify this information : */