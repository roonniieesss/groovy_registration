<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sales_order_item extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('sales_order_item_model');
    }

    public function index()
    {
        $data['page'] = 'sales-order-item/sales_order_item_list';
        $data['title'] = 'List Sales Order Item';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->sales_order_item_model->json();
    }

    public function read($id)
    {
        $row = $this->sales_order_item_model->get($id);
        if ($row) {
            $data = array(
				'sales_order_detail_id' => $row->sales_order_detail_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'sales-order-item/sales_order_item_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('sales-order-item'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('sales-order-item/create-action'),
		    'sales_order_detail_id' => set_value('sales_order_detail_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		);
        $data['page'] = 'sales-order-item/sales_order_item_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sales-order-item/create'));
        } else {
            $data = array(
				'sales_order_detail_id' => post('sales_order_detail_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
		    );

            $this->sales_order_item_model->insert($data);

            success('Create Record Success');
            redirect(base_url('sales-order-item'));
        }
    }

    public function update($id)
    {
        $row = $this->sales_order_item_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('sales-order-item/update-action'),
				'sales_order_item_id' => set_value('sales_order_item_id', $row->sales_order_item_id),
				'sales_order_detail_id' => set_value('sales_order_detail_id', $row->sales_order_detail_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
			    );

            $data['page'] = 'sales-order-item/sales_order_item_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('sales-order-item'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('sales-order-item/update/'.post('sales_order_item_id', true)));
        } else {
            $data = array(
				'sales_order_detail_id' => post('sales_order_detail_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
		    );

            $this->sales_order_item_model->update($data, post('sales_order_item_id', true));

            success('Update Record Success');
            redirect(base_url('sales-order-item/update/'.post('sales_order_item_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->sales_order_item_model->get($id);

        if ($row) {
            $this->sales_order_item_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('sales-order-item'));
        } else {

            warning('Record Not Found');
            redirect(base_url('sales-order-item'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('sales_order_detail_id', 'sales order detail', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');

		$this->form_validation->set_rules('sales_order_item_id', 'sales_order_item_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file sales_order_item.php */
/* Location: ./application/controllers/sales_order_item.php */
/* Please DO NOT modify this information : */