<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class skema_harga_detail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('skema_harga_detail_model');
    }

    public function index()
    {
        $data['page'] = 'skema-harga-detail/skema_harga_detail_list';
        $data['title'] = 'List Skema Harga Detail';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->skema_harga_detail_model->json();
    }

    public function read($id)
    {
        $row = $this->skema_harga_detail_model->get($id);
        if ($row) {
            $data = array(
				'skema_harga_id' => $row->skema_harga_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'harga' => $row->harga,
				'diskon' => $row->diskon,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
		    );
            $data['page'] = 'skema-harga-detail/skema_harga_detail_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('skema-harga-detail'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('skema-harga-detail/create-action'),
		    'skema_harga_id' => set_value('skema_harga_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'harga' => set_value('harga'),
		    'diskon' => set_value('diskon'),
		);
        $data['page'] = 'skema-harga-detail/skema_harga_detail_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('skema-harga-detail/create'));
        } else {
            $data = array(
				'skema_harga_id' => post('skema_harga_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'harga' => post('harga',true),
				'diskon' => post('diskon',true),
		    );

            $this->skema_harga_detail_model->insert($data);

            success('Create Record Success');
            redirect(base_url('skema-harga-detail'));
        }
    }

    public function update($id)
    {
        $row = $this->skema_harga_detail_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('skema-harga-detail/update-action'),
				'skema_harga_detail_id' => set_value('skema_harga_detail_id', $row->skema_harga_detail_id),
				'skema_harga_id' => set_value('skema_harga_id', $row->skema_harga_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'harga' => set_value('harga', $row->harga),
				'diskon' => set_value('diskon', $row->diskon),
			    );

            $data['page'] = 'skema-harga-detail/skema_harga_detail_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('skema-harga-detail'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('skema-harga-detail/update/'.post('skema_harga_detail_id', true)));
        } else {
            $data = array(
				'skema_harga_id' => post('skema_harga_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'harga' => post('harga',true),
				'diskon' => post('diskon',true),
		    );

            $this->skema_harga_detail_model->update($data, post('skema_harga_detail_id', true));

            success('Update Record Success');
            redirect(base_url('skema-harga-detail/update/'.post('skema_harga_detail_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->skema_harga_detail_model->get($id);

        if ($row) {
            $this->skema_harga_detail_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('skema-harga-detail'));
        } else {

            warning('Record Not Found');
            redirect(base_url('skema-harga-detail'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('skema_harga_id', 'skema harga', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required|numeric');
		$this->form_validation->set_rules('diskon', 'diskon', 'trim|required|numeric');

		$this->form_validation->set_rules('skema_harga_detail_id', 'skema_harga_detail_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file skema_harga_detail.php */
/* Location: ./application/controllers/skema_harga_detail.php */
/* Please DO NOT modify this information : */