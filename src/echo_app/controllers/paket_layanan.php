<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class paket_layanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        model('paket_layanan_model');
    }

    public function index()
    {
        $data['page'] = 'paket-layanan/paket_layanan_list';
        $data['title'] = 'List Paket Layanan';
        view('template', $data);
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->paket_layanan_model->json();
    }

    public function read($id)
    {
        $row = $this->paket_layanan_model->get($id);
        if ($row) {
            $data = array(
				'kategori_layanan_id' => $row->kategori_layanan_id,
				'jenis_barang_id' => $row->jenis_barang_id,
				'nama_paket_layanan' => $row->nama_paket_layanan,
				'dapat_dicustom' => $row->dapat_dicustom,
				'rumus_pembulatan_custom' => $row->rumus_pembulatan_custom,
				'kelipatan_pembulatan_custom' => $row->kelipatan_pembulatan_custom,
				'__active' => $row->__active,
				'__created' => $row->__created,
				'__updated' => $row->__updated,
				'__username' => $row->__username,
				'paket_layanan_id1' => $row->paket_layanan_id1,
		    );
            $data['page'] = 'paket-layanan/paket_layanan_read';
            view('template', $data);
        } else {
            warning('Record Not Found');
            redirect(base_url('paket-layanan'));
        }
    }

    public function create()
    {
        $data = array(
            'button'         => 'Create',
            'action'         => base_url('paket-layanan/create-action'),
		    'kategori_layanan_id' => set_value('kategori_layanan_id'),
		    'jenis_barang_id' => set_value('jenis_barang_id'),
		    'nama_paket_layanan' => set_value('nama_paket_layanan'),
		    'dapat_dicustom' => set_value('dapat_dicustom'),
		    'rumus_pembulatan_custom' => set_value('rumus_pembulatan_custom'),
		    'kelipatan_pembulatan_custom' => set_value('kelipatan_pembulatan_custom'),
		    'paket_layanan_id1' => set_value('paket_layanan_id1'),
		);
        $data['page'] = 'paket-layanan/paket_layanan_form';
        view('template', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('paket-layanan/create'));
        } else {
            $data = array(
				'kategori_layanan_id' => post('kategori_layanan_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'nama_paket_layanan' => post('nama_paket_layanan',true),
				'dapat_dicustom' => post('dapat_dicustom',true),
				'rumus_pembulatan_custom' => post('rumus_pembulatan_custom',true),
				'kelipatan_pembulatan_custom' => post('kelipatan_pembulatan_custom',true),
				'paket_layanan_id1' => post('paket_layanan_id1',true),
		    );

            $this->paket_layanan_model->insert($data);

            success('Create Record Success');
            redirect(base_url('paket-layanan'));
        }
    }

    public function update($id)
    {
        $row = $this->paket_layanan_model->get($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => base_url('paket-layanan/update-action'),
				'paket_layanan_id' => set_value('paket_layanan_id', $row->paket_layanan_id),
				'kategori_layanan_id' => set_value('kategori_layanan_id', $row->kategori_layanan_id),
				'jenis_barang_id' => set_value('jenis_barang_id', $row->jenis_barang_id),
				'nama_paket_layanan' => set_value('nama_paket_layanan', $row->nama_paket_layanan),
				'dapat_dicustom' => set_value('dapat_dicustom', $row->dapat_dicustom),
				'rumus_pembulatan_custom' => set_value('rumus_pembulatan_custom', $row->rumus_pembulatan_custom),
				'kelipatan_pembulatan_custom' => set_value('kelipatan_pembulatan_custom', $row->kelipatan_pembulatan_custom),
				'paket_layanan_id1' => set_value('paket_layanan_id1', $row->paket_layanan_id1),
			    );

            $data['page'] = 'paket-layanan/paket_layanan_form';
            view('template', $data);

        } else {
            warning('Record Not Found');
            redirect(base_url('paket-layanan'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            error(validation_errors());
            redirect(base_url('paket-layanan/update/'.post('paket_layanan_id', true)));
        } else {
            $data = array(
				'kategori_layanan_id' => post('kategori_layanan_id',true),
				'jenis_barang_id' => post('jenis_barang_id',true),
				'nama_paket_layanan' => post('nama_paket_layanan',true),
				'dapat_dicustom' => post('dapat_dicustom',true),
				'rumus_pembulatan_custom' => post('rumus_pembulatan_custom',true),
				'kelipatan_pembulatan_custom' => post('kelipatan_pembulatan_custom',true),
				'paket_layanan_id1' => post('paket_layanan_id1',true),
		    );

            $this->paket_layanan_model->update($data, post('paket_layanan_id', true));

            success('Update Record Success');
            redirect(base_url('paket-layanan/update/'.post('paket_layanan_id', true)));
        }
    }

    public function delete($id)
    {
        $row = $this->paket_layanan_model->get($id);

        if ($row) {
            $this->paket_layanan_model->delete($id);

            success('Delete Record Success');
            redirect(base_url('paket-layanan'));
        } else {

            warning('Record Not Found');
            redirect(base_url('paket-layanan'));
        }
    }

    public function _rules()
    {
		$this->form_validation->set_rules('kategori_layanan_id', 'kategori layanan', 'trim|required|numeric');
		$this->form_validation->set_rules('jenis_barang_id', 'jenis barang', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_paket_layanan', 'nama paket layanan', 'trim|required');
		$this->form_validation->set_rules('dapat_dicustom', 'dapat dicustom', 'trim|required');
		$this->form_validation->set_rules('rumus_pembulatan_custom', 'rumus pembulatan custom', 'trim|required');
		$this->form_validation->set_rules('kelipatan_pembulatan_custom', 'kelipatan pembulatan custom', 'trim|required|numeric');
		$this->form_validation->set_rules('paket_layanan_id1', 'paket layanan 1', 'trim|required|numeric');

		$this->form_validation->set_rules('paket_layanan_id', 'paket_layanan_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file paket_layanan.php */
/* Location: ./application/controllers/paket_layanan.php */
/* Please DO NOT modify this information : */