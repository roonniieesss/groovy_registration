<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class kontak_pelanggan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'kontak_pelanggan';
    public $primary = 'kontak_pelanggan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('kontak_pelanggan_id,pelanggan_id,jenis_kontak_pelanggan_id,kontak,kontak_utama');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('kontak-pelanggan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('kontak-pelanggan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('kontak-pelanggan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kontak_pelanggan_id');

            return $this->datatables->generate();
        }
    }
/* End of file kontak_pelanggan_model.php */
/* Location: ./application/models/kontak_pelanggan_model.php */
/* Please DO NOT modify this information : */