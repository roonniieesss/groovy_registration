<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class artikel_konten_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'artikel_konten';
    public $primary = 'artikel_konten_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('artikel_konten_id,artikel_id,kode_bahasa,judul,isi,ringkasan,tag');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('artikel-konten/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('artikel-konten/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('artikel-konten/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'artikel_konten_id');

            return $this->datatables->generate();
        }
    }
/* End of file artikel_konten_model.php */
/* Location: ./application/models/artikel_konten_model.php */
/* Please DO NOT modify this information : */