<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pindah_gudang_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'pindah_gudang';
    public $primary = 'pindah_gudang_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('pindah_gudang_id,gudang_asal,gudang_tujuan,tanggal_kirim,tanggal_terima,catatan_pengirim,catatan_penerima,jenis,status');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('pindah-gudang/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('pindah-gudang/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('pindah-gudang/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'pindah_gudang_id');

            return $this->datatables->generate();
        }
    }
/* End of file pindah_gudang_model.php */
/* Location: ./application/models/pindah_gudang_model.php */
/* Please DO NOT modify this information : */