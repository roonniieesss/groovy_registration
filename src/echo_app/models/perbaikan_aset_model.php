<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class perbaikan_aset_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'perbaikan_aset';
    public $primary = 'perbaikan_aset_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('perbaikan_aset_id,aset_id,aset_id_pengganti,penempatan_aset_keluar,penempatan_aset_masuk,pelaksana_perbaikan,kontak_pelaksana_perbaikan,tanggal_perbaikan,tanggal_selesai,alasan_perbaikan,catatan_hasil_perbaikan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('perbaikan-aset/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('perbaikan-aset/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('perbaikan-aset/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'perbaikan_aset_id');

            return $this->datatables->generate();
        }
    }
/* End of file perbaikan_aset_model.php */
/* Location: ./application/models/perbaikan_aset_model.php */
/* Please DO NOT modify this information : */