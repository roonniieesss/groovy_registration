<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class retur_penjualan_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'retur_penjualan_detail';
    public $primary = 'retur_penjualan_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('retur_penjualan_detail_id,retur_penjualan_id,satuan_barang_delivery_order_id');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('retur-penjualan-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('retur-penjualan-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('retur-penjualan-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'retur_penjualan_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file retur_penjualan_detail_model.php */
/* Location: ./application/models/retur_penjualan_detail_model.php */
/* Please DO NOT modify this information : */