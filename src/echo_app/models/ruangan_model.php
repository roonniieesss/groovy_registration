<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ruangan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'ruangan';
    public $primary = 'ruangan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('ruangan_id,sub_lokasi_id,nama_ruangan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('ruangan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('ruangan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('ruangan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'ruangan_id');

            return $this->datatables->generate();
        }
    }
/* End of file ruangan_model.php */
/* Location: ./application/models/ruangan_model.php */
/* Please DO NOT modify this information : */