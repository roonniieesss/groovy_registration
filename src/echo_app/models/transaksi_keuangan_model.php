<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transaksi_keuangan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'transaksi_keuangan';
    public $primary = 'transaksi_keuangan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('transaksi_keuangan_id,perusahaan_id,tanggal_buku,keterangan,no_cek,tanggal_terima_cek,tanggal_cair_cek,status,keterangan_status,jenis');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('transaksi-keuangan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('transaksi-keuangan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('transaksi-keuangan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'transaksi_keuangan_id');

            return $this->datatables->generate();
        }
    }
/* End of file transaksi_keuangan_model.php */
/* Location: ./application/models/transaksi_keuangan_model.php */
/* Please DO NOT modify this information : */