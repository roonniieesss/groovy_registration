<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class work_order_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'work_order_detail';
    public $primary = 'work_order_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('work_order_detail_id,work_order_id,sales_order_item_id,alat_produksi_id,jenis_sample,waktu_ambil_sample,barang_id,status,alasan_batal');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('work-order-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('work-order-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('work-order-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'work_order_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file work_order_detail_model.php */
/* Location: ./application/models/work_order_detail_model.php */
/* Please DO NOT modify this information : */