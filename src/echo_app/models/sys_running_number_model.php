<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sys_running_number_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'sys_running_number';
    public $primary = 'perusahaan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('perusahaan_id,format,next_id,period,last_retrieve,zero_pad_length');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('sys-running-number/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('sys-running-number/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('sys-running-number/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'perusahaan_id');

            return $this->datatables->generate();
        }
    }
/* End of file sys_running_number_model.php */
/* Location: ./application/models/sys_running_number_model.php */
/* Please DO NOT modify this information : */