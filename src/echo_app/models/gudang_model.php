<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class gudang_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'gudang';
    public $primary = 'gudang_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('gudang_id,perusahaan_id,nama_gudang,nama_pic,alamat,telepon,fax,kapasitas_kirim_harian,kapasitas_do_harian');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('gudang/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('gudang/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('gudang/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'gudang_id');

            return $this->datatables->generate();
        }
    }
/* End of file gudang_model.php */
/* Location: ./application/models/gudang_model.php */
/* Please DO NOT modify this information : */