<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ras_hewan_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'ras_hewan';
    public $primary    = 'ras_hewan_id';
    public $order      = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('ras_hewan_id,nama_jenis_hewan,nama_ras_hewan');

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('jenis_hewan', $this->table.'.jenis_hewan_id = jenis_hewan.jenis_hewan_id');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('ras-hewan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('ras-hewan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('ras-hewan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'ras_hewan_id');

        return $this->datatables->generate();
    }
}
/* End of file ras_hewan_model.php */
/* Location: ./application/models/ras_hewan_model.php */
/* Please DO NOT modify this information : */
