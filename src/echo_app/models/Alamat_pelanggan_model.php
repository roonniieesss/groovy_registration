<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class alamat_pelanggan_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'alamat_pelanggan';
    public $primary    = 'alamat_pelanggan_id';
    public $order      = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('alamat_pelanggan_id,nama_pelanggan,nama_kota,label,alamat,alamat_utama');

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('pelanggan', $this->table.'.pelanggan_id = pelanggan.pelanggan_id');
        $this->datatables->join('kota', $this->table.'.kota_id = kota.kota_id');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('alamat-pelanggan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('alamat-pelanggan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('alamat-pelanggan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'alamat_pelanggan_id');

        return $this->datatables->generate();
    }
}
/* End of file alamat_pelanggan_model.php */
/* Location: ./application/models/alamat_pelanggan_model.php */
/* Please DO NOT modify this information : */
