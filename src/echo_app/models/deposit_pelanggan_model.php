<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class deposit_pelanggan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'deposit_pelanggan';
    public $primary = 'deposit_pelanggan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('deposit_pelanggan_id,pelanggan_id,transaksi_keuangan_id,kurs,nilai');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('deposit-pelanggan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('deposit-pelanggan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('deposit-pelanggan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'deposit_pelanggan_id');

            return $this->datatables->generate();
        }
    }
/* End of file deposit_pelanggan_model.php */
/* Location: ./application/models/deposit_pelanggan_model.php */
/* Please DO NOT modify this information : */