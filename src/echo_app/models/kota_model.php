<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class kota_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'kota';
    public $primary    = 'kota_id';
    public $order      = 'DESC';

    public function __construct()
    {
        $this->has_one['propinsi'] = array('Propinsi_model', 'propinsi_id', 'propinsi_id');
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('kota_id,nama_propinsi,nama_kota');

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('propinsi', $this->table.'.propinsi_id = propinsi.propinsi_id');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('kota/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('kota/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('kota/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kota_id');

        return $this->datatables->generate();
    }
}
/* End of file kota_model.php */
/* Location: ./application/models/kota_model.php */
/* Please DO NOT modify this information : */
