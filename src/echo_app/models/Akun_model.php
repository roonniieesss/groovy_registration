<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class akun_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'akun';
    public $primary = 'akun_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('akun_id,nama_akun,kelompok');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('akun/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('akun/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('akun/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'akun_id');

            return $this->datatables->generate();
        }
    }
/* End of file akun_model.php */
/* Location: ./application/models/akun_model.php */
/* Please DO NOT modify this information : */