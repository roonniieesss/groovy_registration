<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class alasan_batal_inquiry_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'alasan_batal_inquiry';
    public $primary = 'alasan_batal_inquiry_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('alasan_batal_inquiry_id,alasan_batal_inquiry');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('alasan-batal-inquiry/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('alasan-batal-inquiry/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('alasan-batal-inquiry/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'alasan_batal_inquiry_id');

            return $this->datatables->generate();
        }
    }
/* End of file alasan_batal_inquiry_model.php */
/* Location: ./application/models/alasan_batal_inquiry_model.php */
/* Please DO NOT modify this information : */