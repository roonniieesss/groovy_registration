<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class artikel_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'artikel';
    public $primary = 'artikel_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('artikel_id,waktu_terbit,waktu_akhir_tayang');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('artikel/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('artikel/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('artikel/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'artikel_id');

            return $this->datatables->generate();
        }
    }
/* End of file artikel_model.php */
/* Location: ./application/models/artikel_model.php */
/* Please DO NOT modify this information : */