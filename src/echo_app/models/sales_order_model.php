<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class sales_order_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'sales_order';
    public $primary    = 'sales_order_id';
    public $order      = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('
        sales_order_id, perusahaan_id, nama_pelanggan, skema_harga_id, no_sales_order,
        permintaan_awal_kirim, permintaan_akhir_kirim, jumlah_pengiriman,
        default_jumlah_pengiriman, nama_penerima, alamat_penerima, telp_penerima, fax_penerima,
        no_po_pelanggan,tanggal_po_pelanggan,lampiran_po,kurs,total_penjualan,total_ppn,
        catatan_penjualan,status,alasan_batal');

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('pelanggan', $this->table . '.pelanggan_id = pelanggan.pelanggan_id');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('sales-order/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('sales-order/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('sales-order/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'sales_order_id');

        return $this->datatables->generate();
    }

    public function getSalesNumber()
    {
        //get all last data base on date
        $keyword     = 'no_sales_order';
        $getLastData = $this->db
            ->like($keyword, date('d/m/y'))
            ->order_by($this->primary, 'desc')
            ->get($this->table)->row();

        $newNumber = '001';
        if (!empty($getLastData)) {
            $lastNumber = explode('-', $getLastData->$keyword);
            $number     = $lastNumber[2] * 1;
            $newNumber  = str_pad($number + 1, 3, '0', STR_PAD_LEFT);
        }
        return "SO-" . date('d/m/y') . '-' . $newNumber;
    }
}
/* End of file sales_order_model.php */
/* Location: ./application/models/sales_order_model.php */
/* Please DO NOT modify this information : */
