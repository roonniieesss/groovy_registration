<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jenis_layanan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'jenis_layanan';
    public $primary = 'jenis_layanan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('jenis_layanan_id,jenis_barang_id,kategori_layanan_id,pelaksana');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('jenis-layanan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('jenis-layanan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('jenis-layanan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'jenis_layanan_id');

            return $this->datatables->generate();
        }
    }
/* End of file jenis_layanan_model.php */
/* Location: ./application/models/jenis_layanan_model.php */
/* Please DO NOT modify this information : */