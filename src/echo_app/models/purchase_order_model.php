<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class purchase_order_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'purchase_order';
    public $primary = 'purchase_order_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('purchase_order_id,perusahaan_id,supplier_id,no_purchase_order,permintaan_tanggal_kirim,nama_penerima,alamat_penerima,telp_penerima,fax_penerima,kurs,nilai_tukar,diskon,biaya_lainnya,total_pembelian,no_so_supplier,tanggal_so_supplier,lampiran_so,term_pembayaran,catatan,status');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('purchase-order/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('purchase-order/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('purchase-order/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'purchase_order_id');

            return $this->datatables->generate();
        }
    }
/* End of file purchase_order_model.php */
/* Location: ./application/models/purchase_order_model.php */
/* Please DO NOT modify this information : */