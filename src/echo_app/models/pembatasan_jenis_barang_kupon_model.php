<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pembatasan_jenis_barang_kupon_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'pembatasan_jenis_barang_kupon';
    public $primary = 'pembatasan_jenis_barang_kupon_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('pembatasan_jenis_barang_kupon_id,kupon_id,jenis_barang_id');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('pembatasan-jenis-barang-kupon/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('pembatasan-jenis-barang-kupon/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('pembatasan-jenis-barang-kupon/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'pembatasan_jenis_barang_kupon_id');

            return $this->datatables->generate();
        }
    }
/* End of file pembatasan_jenis_barang_kupon_model.php */
/* Location: ./application/models/pembatasan_jenis_barang_kupon_model.php */
/* Please DO NOT modify this information : */