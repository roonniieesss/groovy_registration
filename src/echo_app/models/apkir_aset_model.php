<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class apkir_aset_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'apkir_aset';
    public $primary = 'apkir_aset_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('apkir_aset_id,nama_penanggung_jawab,berita_acara_apkir,status');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('apkir-aset/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('apkir-aset/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('apkir-aset/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'apkir_aset_id');

            return $this->datatables->generate();
        }
    }
/* End of file apkir_aset_model.php */
/* Location: ./application/models/apkir_aset_model.php */
/* Please DO NOT modify this information : */