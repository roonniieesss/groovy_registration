<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jenis_kontak_pelanggan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'jenis_kontak_pelanggan';
    public $primary = 'jenis_kontak_pelanggan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('jenis_kontak_pelanggan_id,nama_jenis_kontak_pelanggan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('jenis-kontak-pelanggan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('jenis-kontak-pelanggan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('jenis-kontak-pelanggan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'jenis_kontak_pelanggan_id');

            return $this->datatables->generate();
        }
    }
/* End of file jenis_kontak_pelanggan_model.php */
/* Location: ./application/models/jenis_kontak_pelanggan_model.php */
/* Please DO NOT modify this information : */