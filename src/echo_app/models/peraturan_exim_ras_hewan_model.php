<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class peraturan_exim_ras_hewan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'peraturan_exim_ras_hewan';
    public $primary = 'peraturan_exim_ras_hewan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('peraturan_exim_ras_hewan_id,peraturan_exim_id,ras_hewan_id');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('peraturan-exim-ras-hewan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('peraturan-exim-ras-hewan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('peraturan-exim-ras-hewan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'peraturan_exim_ras_hewan_id');

            return $this->datatables->generate();
        }
    }
/* End of file peraturan_exim_ras_hewan_model.php */
/* Location: ./application/models/peraturan_exim_ras_hewan_model.php */
/* Please DO NOT modify this information : */