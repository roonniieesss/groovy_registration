<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sys_config_perusahaan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'sys_config_perusahaan';
    public $primary = 'perusahaan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('perusahaan_id,value');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('sys-config-perusahaan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('sys-config-perusahaan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('sys-config-perusahaan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'perusahaan_id');

            return $this->datatables->generate();
        }
    }
/* End of file sys_config_perusahaan_model.php */
/* Location: ./application/models/sys_config_perusahaan_model.php */
/* Please DO NOT modify this information : */