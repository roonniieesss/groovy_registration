<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jenis_segmen_pelanggan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'jenis_segmen_pelanggan';
    public $primary = 'jenis_segmen_pelanggan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('jenis_segmen_pelanggan_id,jenis_segmen_pelanggan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('jenis-segmen-pelanggan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('jenis-segmen-pelanggan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('jenis-segmen-pelanggan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'jenis_segmen_pelanggan_id');

            return $this->datatables->generate();
        }
    }
/* End of file jenis_segmen_pelanggan_model.php */
/* Location: ./application/models/jenis_segmen_pelanggan_model.php */
/* Please DO NOT modify this information : */