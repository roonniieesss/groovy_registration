<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class penerimaan_barang_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'penerimaan_barang';
    public $primary = 'penerimaan_barang_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('penerimaan_barang_id,purchase_order_id,gudang_id,no_surat_jalan,tanggal_surat_jalan,tanggal_terima,nama_supir,telp_supir,catatan_gudang');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('penerimaan-barang/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('penerimaan-barang/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('penerimaan-barang/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'penerimaan_barang_id');

            return $this->datatables->generate();
        }
    }
/* End of file penerimaan_barang_model.php */
/* Location: ./application/models/penerimaan_barang_model.php */
/* Please DO NOT modify this information : */