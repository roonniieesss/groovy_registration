<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class satuan_barang_delivery_order_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'satuan_barang_delivery_order';
    public $primary = 'satuan_barang_delivery_order_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('satuan_barang_delivery_order_id,delivery_order_detail_id,transaksi_barang_id,diretur');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('satuan-barang-delivery-order/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('satuan-barang-delivery-order/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('satuan-barang-delivery-order/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'satuan_barang_delivery_order_id');

            return $this->datatables->generate();
        }
    }
/* End of file satuan_barang_delivery_order_model.php */
/* Location: ./application/models/satuan_barang_delivery_order_model.php */
/* Please DO NOT modify this information : */