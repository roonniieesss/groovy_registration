<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class buku_besar_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'buku_besar';
    public $primary = 'buku_besar_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('buku_besar_id,transaksi_keuangan_id,akun_id,kurs,nilai');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('buku-besar/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('buku-besar/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('buku-besar/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'buku_besar_id');

            return $this->datatables->generate();
        }
    }
/* End of file buku_besar_model.php */
/* Location: ./application/models/buku_besar_model.php */
/* Please DO NOT modify this information : */