<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class alat_produksi_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'alat_produksi';
    public $primary = 'alat_produksi_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('alat_produksi_id,nama_alat,merek,tipe');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('alat-produksi/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('alat-produksi/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('alat-produksi/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'alat_produksi_id');

            return $this->datatables->generate();
        }
    }
/* End of file alat_produksi_model.php */
/* Location: ./application/models/alat_produksi_model.php */
/* Please DO NOT modify this information : */