<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pindah_aset_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'pindah_aset';
    public $primary = 'pindah_aset_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('pindah_aset_id,keperluan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('pindah-aset/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('pindah-aset/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('pindah-aset/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'pindah_aset_id');

            return $this->datatables->generate();
        }
    }
/* End of file pindah_aset_model.php */
/* Location: ./application/models/pindah_aset_model.php */
/* Please DO NOT modify this information : */