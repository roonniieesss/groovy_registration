<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pecah_kemasan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'pecah_kemasan';
    public $primary = 'pecah_kemasan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('pecah_kemasan_id,lokasi_bongkar,keterangan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('pecah-kemasan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('pecah-kemasan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('pecah-kemasan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'pecah_kemasan_id');

            return $this->datatables->generate();
        }
    }
/* End of file pecah_kemasan_model.php */
/* Location: ./application/models/pecah_kemasan_model.php */
/* Please DO NOT modify this information : */