<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class invoice_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'invoice_detail';
    public $primary = 'invoice_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('invoice_detail_id,sales_order_id,invoice_id');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('invoice-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('invoice-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('invoice-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'invoice_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file invoice_detail_model.php */
/* Location: ./application/models/invoice_detail_model.php */
/* Please DO NOT modify this information : */