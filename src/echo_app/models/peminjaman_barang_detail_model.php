<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class peminjaman_barang_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'peminjaman_barang_detail';
    public $primary = 'peminjaman_barang_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('peminjaman_barang_detail_id,peminjaman_barang_id,barang_id,transaksi_barang_keluar,transaksi_barang_kembali');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('peminjaman-barang-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('peminjaman-barang-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('peminjaman-barang-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'peminjaman_barang_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file peminjaman_barang_detail_model.php */
/* Location: ./application/models/peminjaman_barang_detail_model.php */
/* Please DO NOT modify this information : */