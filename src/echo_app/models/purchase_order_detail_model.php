<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class purchase_order_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'purchase_order_detail';
    public $primary = 'purchase_order_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('purchase_order_detail_id,purchase_order_id,jenis_barang_id,gudang_id,qty,harga_satuan,satuan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('purchase-order-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('purchase-order-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('purchase-order-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'purchase_order_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file purchase_order_detail_model.php */
/* Location: ./application/models/purchase_order_detail_model.php */
/* Please DO NOT modify this information : */