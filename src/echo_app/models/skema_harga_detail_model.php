<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class skema_harga_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'skema_harga_detail';
    public $primary = 'skema_harga_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('skema_harga_detail_id,skema_harga_id,jenis_barang_id,harga,diskon');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('skema-harga-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('skema-harga-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('skema-harga-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'skema_harga_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file skema_harga_detail_model.php */
/* Location: ./application/models/skema_harga_detail_model.php */
/* Please DO NOT modify this information : */