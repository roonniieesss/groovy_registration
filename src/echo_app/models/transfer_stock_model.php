<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transfer_stock_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'transfer_stock';
    public $primary = 'transfer_stock_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('transfer_stock_id,gudang_asal,gudang_tujuan,tanggal_transfer,keperluan,catatan_pengiriman,catatan_penerimaan,status');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('transfer-stock/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('transfer-stock/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('transfer-stock/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'transfer_stock_id');

            return $this->datatables->generate();
        }
    }
/* End of file transfer_stock_model.php */
/* Location: ./application/models/transfer_stock_model.php */
/* Please DO NOT modify this information : */