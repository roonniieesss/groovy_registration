<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pengadaan_aset_detail_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'pengadaan_aset_detail';
    public $primary = 'pengadaan_aset_detail_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('pengadaan_aset_detail_id,pengadaan_aset_id,jenis_aset_id,jumlah');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('pengadaan-aset-detail/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('pengadaan-aset-detail/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('pengadaan-aset-detail/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'pengadaan_aset_detail_id');

            return $this->datatables->generate();
        }
    }
/* End of file pengadaan_aset_detail_model.php */
/* Location: ./application/models/pengadaan_aset_detail_model.php */
/* Please DO NOT modify this information : */