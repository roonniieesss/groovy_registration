<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class tagihan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'tagihan';
    public $primary = 'tagihan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('tagihan_id,purchase_order_id,no_invoice_supplier,tanggal_invoice_supplier,jatuh_tempo,total_tagihan,kurs,status');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('tagihan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('tagihan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('tagihan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'tagihan_id');

            return $this->datatables->generate();
        }
    }
/* End of file tagihan_model.php */
/* Location: ./application/models/tagihan_model.php */
/* Please DO NOT modify this information : */