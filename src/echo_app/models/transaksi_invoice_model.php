<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transaksi_invoice_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'transaksi_invoice';
    public $primary = 'transaksi_invoice_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('transaksi_invoice_id,transaksi_keuangan_id,invoice_id,jenis');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('transaksi-invoice/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('transaksi-invoice/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('transaksi-invoice/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'transaksi_invoice_id');

            return $this->datatables->generate();
        }
    }
/* End of file transaksi_invoice_model.php */
/* Location: ./application/models/transaksi_invoice_model.php */
/* Please DO NOT modify this information : */