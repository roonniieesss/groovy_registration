<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class penggunaan_barang_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'penggunaan_barang';
    public $primary = 'penggunaan_barang_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('penggunaan_barang_id,work_order_detail_id,transaksi_barang_id');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('penggunaan-barang/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('penggunaan-barang/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('penggunaan-barang/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'penggunaan_barang_id');

            return $this->datatables->generate();
        }
    }
/* End of file penggunaan_barang_model.php */
/* Location: ./application/models/penggunaan_barang_model.php */
/* Please DO NOT modify this information : */