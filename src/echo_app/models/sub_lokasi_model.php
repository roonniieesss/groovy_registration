<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sub_lokasi_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'sub_lokasi';
    public $primary = 'sub_lokasi_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('sub_lokasi_id,lokasi_id,nama_sub_lokasi,filename_denah,width,height');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('sub-lokasi/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('sub-lokasi/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('sub-lokasi/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'sub_lokasi_id');

            return $this->datatables->generate();
        }
    }
/* End of file sub_lokasi_model.php */
/* Location: ./application/models/sub_lokasi_model.php */
/* Please DO NOT modify this information : */