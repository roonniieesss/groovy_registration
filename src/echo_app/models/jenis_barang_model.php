<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class jenis_barang_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'jenis_barang';
    public $primary    = 'jenis_barang_id';
    public $order      = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('jenis_barang_id,kategori_barang_id,satuan_id,kode_barang,kode_barcode,nama_jenis_barang,spesifikasi,warna,ukuran,harga,batas_bawah_harga,klasifikasi_barang,jenis_identifikasi,dijual,pcf_stok,pcf_stok_booked');

        $this->datatables->from($this->table);

        //add this line for join
        //$this->datatables->join('table2', $this->table.'.field = table2.field');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('jenis-barang/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('jenis-barang/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('jenis-barang/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'jenis_barang_id');

        return $this->datatables->generate();
    }

    public function ajax()
    {
        library('datatables');

        $this->datatables->select('jenis_barang_id,kategori_barang,satuan_id,kode_barang,kode_barcode,nama_jenis_barang,spesifikasi,warna,ukuran,harga,batas_bawah_harga,klasifikasi_barang,jenis_identifikasi,dijual,pcf_stok,pcf_stok_booked');

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('kategori_barang', $this->table.'.kategori_barang_id = kategori_barang.kategori_barang_id');

        //generate
        $this->datatables->add_column('action','<a href="#$1" class="btn btn-info waves-effect">Pilih</a>', 'jenis_barang_id');

        return $this->datatables->generate();
    }
}
/* End of file jenis_barang_model.php */
/* Location: ./application/models/jenis_barang_model.php */
/* Please DO NOT modify this information : */
