<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class penerimaan_aset_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'penerimaan_aset';
    public $primary = 'penerimaan_aset_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('penerimaan_aset_id,no_surat_jalan,tanggal_surat_jalan,nama_supir,keterangan_penerimaan_aset');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('penerimaan-aset/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('penerimaan-aset/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('penerimaan-aset/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'penerimaan_aset_id');

            return $this->datatables->generate();
        }
    }
/* End of file penerimaan_aset_model.php */
/* Location: ./application/models/penerimaan_aset_model.php */
/* Please DO NOT modify this information : */