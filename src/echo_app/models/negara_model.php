<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class negara_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'negara';
    public $primary = 'negara_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('negara_id,kode_negara,kode_bahasa,nama_negara');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('negara/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('negara/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('negara/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'negara_id');

            return $this->datatables->generate();
        }
    }
/* End of file negara_model.php */
/* Location: ./application/models/negara_model.php */
/* Please DO NOT modify this information : */