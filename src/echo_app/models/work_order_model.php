<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class work_order_model extends MY_Model
{
    public $table = 'work_order'; //untuk get ketika pilih tombol read
    public $primary = 'work_order_id';
    public $order = 'DESC';
    public $timestamps = false;    

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');
            $this->datatables->select('work_order.work_order_id,work_order.*');
            $this->datatables->from($this->table);

            //generate  
            $this->datatables->add_column('action',
                anchor(base_url('work_order1/baca_wo/$1'), 'Read', 'class="btn btn-default waves-effect"'), 'work_order_id');

            return $this->datatables->generate();
            // return $this->db->get('work_order');
        }

    function baca($id){ //as read
        // $table = "work_order"; //untuk get ketika pilih tombol read
        // $primary = "work_order_id";
        // $this->datatables->select('so.*,soi.*,p.*, jb1.*, pl.*,pn.nama_perusahaan')
        //         ->from('sales_order_item as soi')                
        //         ->join('sales_order_detail as sod', 'sod.sales_order_detail_id = soi.sales_order_detail_id')
        //         ->join('sales_order as so','so.sales_order_id = sod.sales_order_id')
        //         ->join('perusahaan as pn', 'pn.perusahaan_id = so.perusahaan_id')   
        //         ->join('pelanggan as p','p.pelanggan_id = so.pelanggan_id')
        //         ->join('jenis_barang as jb1','jb1.jenis_barang_id = sod.jenis_barang_id')
        //         ->join('paket_layanan as pl','pl.jenis_barang_id = jb1.jenis_barang_id')
        //         ->join('jenis_barang as jb2','jb2.jenis_barang_id = soi.jenis_barang_id')                
        //         ->join('work_order_detail as wod','wod.sales_order_item_id = soi.sales_order_item_id');
        //         // ->get()->result();
        //     return $this->datatables->generate();   
        $q = "SELECT so.*,soi.*,p.*, jb1.*, pl.*,wod.*,wo.*, pn.nama_perusahaan FROM sales_order_item soi JOIN sales_order_detail sod ON sod.sales_order_detail_id = soi.sales_order_detail_id JOIN sales_order so ON so.sales_order_id = sod.sales_order_id JOIN perusahaan pn ON pn.perusahaan_id = so.perusahaan_id JOIN pelanggan p ON p.pelanggan_id = so.pelanggan_id JOIN jenis_barang jb1 ON jb1.jenis_barang_id = sod.jenis_barang_id JOIN paket_layanan pl ON pl.jenis_barang_id = jb1.jenis_barang_id JOIN work_order_detail wod ON wod.sales_order_item_id = soi.sales_order_item_id JOIN work_order wo ON wo.work_order_id = wod.work_order_id"; 
        return $this->db->query($q);       
    }

    function baca_wo($id){ //as read
     $q = "SELECT so.sales_order_id, so.no_sales_order, so.__created,
     soi.sales_order_item_id,
     p.nama_pelanggan,
     jb1.kode_barang, jb1.nama_jenis_barang,
     pl.nama_paket_layanan,           
     wo.*,
     wod.work_order_detail_id
     FROM sales_order_item soi JOIN sales_order_detail sod ON sod.sales_order_detail_id = soi.sales_order_detail_id JOIN sales_order so ON so.sales_order_id = sod.sales_order_id JOIN perusahaan pn ON pn.perusahaan_id = so.perusahaan_id JOIN pelanggan p ON p.pelanggan_id = so.pelanggan_id JOIN jenis_barang jb1 ON jb1.jenis_barang_id = sod.jenis_barang_id JOIN paket_layanan pl ON pl.jenis_barang_id = jb1.jenis_barang_id JOIN work_order_detail wod ON wod.sales_order_item_id = soi.sales_order_item_id JOIN work_order wo ON wo.work_order_id = wod.work_order_id where wo.work_order_id = $id"; 
     return $this->db->query($q); 
    }

    function baca_wo_json($id){
        library('datatables');
        $this->datatables->select('so.sales_order_id, so.no_sales_order, so.__created,
                                   soi.sales_order_item_id,
                                   p.nama_pelanggan,
                                   jb1.kode_barang, jb1.nama_jenis_barang,
                                   pl.nama_paket_layanan,           
                                   wo.*,
                                   wod.work_order_detail_id
                                ')
                            ->join('work_order wo', 'wo.work_order_id = wod.work_order_id')
                            ->join('sales_order_item soi','soi.sales_order_item_id = wod.sales_order_item_id')
                            ->join('sales_order_detail sod','sod.sales_order_detail_id = soi.sales_order_detail_id')
                            ->join('sales_order so','so.sales_order_id = sod.sales_order_id')
                            ->join('pelanggan p','p.pelanggan_id = so.pelanggan_id')

                            ->join('jenis_barang jb1','jb1.jenis_barang_id = sod.jenis_barang_id')                        
                            ->join('paket_layanan pl', 'pl.jenis_barang_id = jb1.jenis_barang_id')

                            ->join('jenis_barang jb2', 'jb2.jenis_barang_id = soi.jenis_barang_id')
                            ->join('jenis_layanan jl', 'jl.jenis_barang_id = jb2.jenis_barang_id');

        //generate                          
        $this->datatables->add_column('action',
                        anchor(base_url('work-order1/baca/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                        anchor(base_url('work-order1/create/$1'), 'Create Work Order', 'class="btn btn-info waves-effect"'), 'sales_order_id');
        return $this->datatables->generate();
    }

    function listview_wo(){
        library('datatables');
        $this->datatables->select('so.sales_order_id, so.no_sales_order, so.__created,
                        soi.sales_order_item_id,
                        p.nama_pelanggan,
                        jb1.kode_barang, jb1.nama_jenis_barang,
                        pl.nama_paket_layanan
                        ')
                        ->from('sales_order_item as soi')
                        ->join('sales_order_detail as sod', 'sod.sales_order_detail_id = soi.sales_order_detail_id')
                        ->join('sales_order as so','so.sales_order_id = sod.sales_order_id')
                        ->join('pelanggan as p','p.pelanggan_id = so.pelanggan_id')
                        ->join('jenis_barang as jb1','jb1.jenis_barang_id = sod.jenis_barang_id')
                        ->join('paket_layanan as pl','pl.jenis_barang_id = jb1.jenis_barang_id')
                        ->join('jenis_barang as jb2','jb2.jenis_barang_id = soi.jenis_barang_id')
                        // ->join('jenis_layanan','jenis_layanan.jenis_barang_id = jenis_barang.jenis_barang_id');
                        ->join('work_order_detail as wod','wod.sales_order_item_id = soi.sales_order_item_id');        
                   // $this->datatables->add_column('action',anchor(base_url('work-order1/read/$1'), 'Read', 'class="btn btn-default waves-effect"'));
        //-R
        //generate                          
        $this->datatables->add_column('action',
                        anchor(base_url('work-order1/baca/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                        anchor(base_url('work-order1/create/$1'), 'Create Work Order', 'class="btn btn-info waves-effect"'), 'sales_order_id');

        return $this->datatables->generate();
    }
}
/* End of file work_order_model.php */
/* Location: ./application/models/work_order_model.php */
/* Please DO NOT modify this information : */

/*
 $this->datatables->select('so.sales_order_id, so.no_sales_order, so.__created,
                        soi.sales_order_item_id,
                        p.nama_pelanggan,
                        jb1.kode_barang, jb1.nama_jenis_barang,
                        pl.nama_paket_layanan
                        ')
->join('work_order wo', 'wo.work_order_id = wod.work_order_id')
                       ->join('sales_order_item soi','soi.sales_order_item_id = wod.sales_order_item_id')
                       ->join('sales_order_detail sod','sod.sales_order_detail_id = soi.sales_order_detail_id')
                       ->join('sales_order so','so.sales_order_id = sod.sales_order_id')
                       ->join('pelanggan p','p.pelanggan_id = so.pelanggan_id')

                       ->join('jenis_barang jb1','jb1.jenis_barang_id = sod.jenis_barang_id')                        
                       ->join('paket_layanan pl', 'pl.jenis_barang_id = jb1.jenis_barang_id')

                       ->join('jenis_barang jb2', 'jb2.jenis_barang_id = soi.jenis_barang_id')
                       ->join('jenis_layanan jl', 'jl.jenis_barang_id = jb2.jenis_barang_id');


*/