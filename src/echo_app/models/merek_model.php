<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class merek_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'merek';
    public $primary = 'merek_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('merek_id,nama_merek');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('merek/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('merek/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('merek/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'merek_id');

            return $this->datatables->generate();
        }
    }
/* End of file merek_model.php */
/* Location: ./application/models/merek_model.php */
/* Please DO NOT modify this information : */