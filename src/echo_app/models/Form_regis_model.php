<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class form_regis_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'inquiry';
    public $primary    = 'inquiry_id';
    public $order      = 'DESC';

    public function __construct()
    {
        $this->has_one['inquiry_produk'] = 'Inquiry_produk_model';
        $this->has_one['pelanggan']      = array('Pelanggan_model', 'pelanggan_id', 'pelanggan_id');
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('inquiry.inquiry_id, no_inquiry, inquiry.__active,
            nama_pelanggan, CONCAT(asal_kota.nama_kota," - ", tujuan_kota.nama_kota) AS destinasi,
            nama_jenis_barang as nama_layanan, qty', false);

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('pelanggan', $this->table . '.pelanggan_id = pelanggan.pelanggan_id');
        $this->datatables->join('inquiry_produk', 'inquiry.inquiry_id = inquiry_produk.inquiry_id');
        $this->datatables->join('jenis_barang', 'inquiry_produk.jenis_barang_id = jenis_barang.jenis_barang_id');
        $this->datatables->join('inquiry_produk_pet_transport ippt', 'inquiry_produk.inquiry_produk_id = ippt.inquiry_produk_id');
        $this->datatables->join('moda_transportasi', 'moda_transportasi.moda_transportasi_id = ippt.moda_transportasi_id');
        $this->datatables->join('kota as asal_kota', 'asal_kota.kota_id = ippt.asal_kota_id');
        $this->datatables->join('kota as tujuan_kota', 'tujuan_kota.kota_id = ippt.tujuan_kota_id');
        $this->datatables->group_by('inquiry.inquiry_id');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('inquiry/read/$1'), 'Detail', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('sales-order/create-from-inq/$1'), 'Buat Order', 'class="btn btn-info waves-effect"') . ' ' //.
            // anchor(base_url('inquiry/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' //.
            // anchor(base_url('inquiry/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"')
            , 'inquiry_id');

        return $this->datatables->generate();
    }

    public function getDetailInquiry($inquiry_id)
    {

        $this->db->where('inquiry.inquiry_id', $inquiry_id);

        $this->db->select('
            inquiry.inquiry_id, no_inquiry, inquiry.__created, pelanggan.pelanggan_id,
            nama_pelanggan, CONCAT(asal_kota.nama_kota," - ", tujuan_kota.nama_kota) AS destinasi,
            nama_moda_transportasi as transport, qty,
            ippt.jenis_kelamin,nama_hewan, nama_ras_hewan, dob, ippt.warna,berat,
            nama_jenis_barang as nama_layanan', false
        );

        $this->db->from($this->table);
        $this->db->join('pelanggan', $this->table . '.pelanggan_id = pelanggan.pelanggan_id');
        $this->db->join('inquiry_produk', 'inquiry.inquiry_id = inquiry_produk.inquiry_id');
        $this->db->join('jenis_barang', 'jenis_barang.jenis_barang_id = inquiry_produk.jenis_barang_id');
        $this->db->join('inquiry_produk_pet_transport ippt', 'inquiry_produk.inquiry_produk_id = ippt.inquiry_produk_id');
        $this->db->join('moda_transportasi', 'moda_transportasi.moda_transportasi_id = ippt.moda_transportasi_id');
        $this->db->join('kota as asal_kota', 'asal_kota.kota_id = ippt.asal_kota_id');
        $this->db->join('kota as tujuan_kota', 'tujuan_kota.kota_id = ippt.tujuan_kota_id');
        $this->db->join('ras_hewan', 'ras_hewan.ras_hewan_id = ippt.ras_hewan_id');

        return $this->db->get()->result();
    }

    public function insertInquiry()
    {
        model('kota_model');
        model('pelanggan_model');
        model('alamat_pelanggan_model');
        model('inquiry_produk_model');
        model('inquiry_produk_pet_transport_model');

        //get inquiry pet
        $petdata = (object) post('inquirypet');

        //get negara by kota        
        $kota = $this->kota_model->with('propinsi')->get($petdata->tujuan_kota_id);        

        $this->db->trans_start();

        //insert pelanggan
        $pelanggan_id = empty(post('pelanggan_id')) ? post('pelanggan_id') : null;
        if (empty($pelanggan_id)) {
            $dataPelanggan = [
                'nama_pelanggan' => post('nama_pelanggan'),
                'tanggal_lahir'  => !empty(post('tanggal_lahir')) ? post('tanggal_lahir') : null,
                'jenis_kelamin'  => !empty(post('jenis_kelamin')) ? post('jenis_kelamin') : null,
                '__active'       => 1
            ];
            $pelanggan_id = $this->pelanggan_model->insert($dataPelanggan);
        }
        
        //insert alamat pelanggan
        $dataAlamatPelanggan = [
            'pelanggan_id' => $pelanggan_id,
            'negara_id'    => $kota->propinsi->negara_id,
            'kota_id'      => $kota->kota_id,
            'label'        => 'Alamat Utama',
            'alamat'       => post('alamat'),            
            'kode_pos'     => post('kode_pos'),
            'alamat_utama' => 1,
            '__active'     => 1
        ];
        $this->alamat_pelanggan_model->insert($dataAlamatPelanggan);

        //insert inquiry
        $dataInquiry = [
            'pelanggan_id' => $pelanggan_id,
            'no_inquiry'   => $this->getlastnumer(),
            '__active'     => 1,
        ];
        $inquiry_id = $this->insert($dataInquiry);
        
        //insert inquiry produk
        $dataInquiryProduk = [
            'inquiry_id'      => $inquiry_id,
            'jenis_barang_id' => $petdata->jenis_barang_id,
            'qty'             => count($petdata->nama_hewan),
            'catatan_pesanan' => post('catatan'),
            '__active'        => 1,

        ];

        $inquiry_produk_model_id = $this->inquiry_produk_model->insert($dataInquiryProduk);

        //insert inquiry produk pet transport
        foreach ($petdata->nama_hewan as $key => $value) {
            $dataInquiryProdukPet = [
                'inquiry_produk_id'    => $inquiry_produk_model_id,
                'asal_kota_id'         => $petdata->asal_kota_id,
                'tujuan_kota_id'       => $petdata->tujuan_kota_id,
                'moda_transportasi_id' => $petdata->moda_transportasi_id,
                'ras_hewan_id'         => $petdata->ras_hewan_id[$key],
                'nama_hewan'           => $petdata->nama_hewan[$key],
                'jenis_kelamin'        => !empty($petdata->jenis_kelamin[$key]) ? $petdata->jenis_kelamin[$key] : null,
                'ukuran_kandang'       => $petdata->ukuran_kandang[$key],
                'nama_penerima'        => post('nama_penerima'),
                'telp_penerima'        => post('notelp_penerima'),
                'idcard_penerima'      => post('idcard_penerima'),
                'alamat_pengiriman'    => post('alamat_pengiriman'),                                            
                'dob'                  => !empty($petdata->dob[$key]) ? $petdata->dob[$key] : null,
                'warna'                => $petdata->warna_hewan[$key],
                'berat'                => $petdata->berat[$key],
                'microchips'           => $petdata->microchip[$key],
                'maskapai'             => post('maskapai'),
                '__active'             => 1,
            ];
            $pelanggan_id = $this->inquiry_produk_pet_transport_model->insert($dataInquiryProdukPet);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            // generate an error... or use the log_message() function to log your error
            warning('Error Insert Data');
            redirect(base_url('inquiry'));
        }

    }

    public function getlastnumer()
    {
        //get all inquiry base on date
        $keyword     = 'no_inquiry';
        $getLastData = $this->db
            ->like($keyword, date('d/m/y'))
            ->order_by($this->primary, 'desc')
            ->get($this->table)->row();

        $newNumber = '001';
        if (!empty($getLastData)) {
            $lastNumber = explode('-', $getLastData->$keyword);
            $number     = $lastNumber[2] * 1;
            $newNumber  = str_pad($number + 1, 3, '0', STR_PAD_LEFT);
        }
        return "INQ-" . date('d/m/y') . '-' . $newNumber;
    }
}
/* End of file inquiry_model.php */
/* Location: ./application/models/inquiry_model.php */
/* Please DO NOT modify this information : */
