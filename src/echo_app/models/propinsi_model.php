<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class propinsi_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'propinsi';
    public $primary    = 'propinsi_id';
    public $order      = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('propinsi_id,nama_propinsi,nama_negara');

        $this->datatables->from($this->table);

        //add this line for join
        $this->datatables->join('negara', $this->table . '.negara_id = negara.negara_id');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('propinsi/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('propinsi/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('propinsi/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'propinsi_id');

        return $this->datatables->generate();
    }
}
/* End of file propinsi_model.php */
/* Location: ./application/models/propinsi_model.php */
/* Please DO NOT modify this information : */
