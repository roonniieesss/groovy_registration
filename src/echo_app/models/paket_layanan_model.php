<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class paket_layanan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'paket_layanan';
    public $primary = 'paket_layanan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('paket_layanan_id,kategori_layanan_id,jenis_barang_id,nama_paket_layanan,dapat_dicustom,rumus_pembulatan_custom,kelipatan_pembulatan_custom,paket_layanan_id1');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('paket-layanan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('paket-layanan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('paket-layanan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'paket_layanan_id');

            return $this->datatables->generate();
        }
    }
/* End of file paket_layanan_model.php */
/* Location: ./application/models/paket_layanan_model.php */
/* Please DO NOT modify this information : */