<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class retur_pembelian_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'retur_pembelian';
    public $primary = 'retur_pembelian_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('retur_pembelian_id,transaksi_keuangan_id,no_retur_pembelian,jenis_retur,status_barang,alasan_pengembalian');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('retur-pembelian/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('retur-pembelian/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('retur-pembelian/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'retur_pembelian_id');

            return $this->datatables->generate();
        }
    }
/* End of file retur_pembelian_model.php */
/* Location: ./application/models/retur_pembelian_model.php */
/* Please DO NOT modify this information : */