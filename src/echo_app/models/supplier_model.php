<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class supplier_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'supplier';
    public $primary = 'supplier_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('supplier_id,nama_supplier,alamat_supplier,npwp_supplier');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('supplier/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('supplier/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('supplier/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'supplier_id');

            return $this->datatables->generate();
        }
    }
/* End of file supplier_model.php */
/* Location: ./application/models/supplier_model.php */
/* Please DO NOT modify this information : */