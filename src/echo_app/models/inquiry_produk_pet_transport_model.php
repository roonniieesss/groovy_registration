<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class inquiry_produk_pet_transport_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'inquiry_produk_pet_transport';
    public $primary = 'inquiry_produk_pet_transport_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('inquiry_produk_pet_transport_id,inquiry_produk_id,asal_kota_id,tujuan_kota_id,moda_transportasi_id,ras_hewan_id,nama_hewan,jenis_kelamin');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('inquiry-produk-pet-transport/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('inquiry-produk-pet-transport/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('inquiry-produk-pet-transport/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'inquiry_produk_pet_transport_id');

            return $this->datatables->generate();
        }
    }
/* End of file inquiry_produk_pet_transport_model.php */
/* Location: ./application/models/inquiry_produk_pet_transport_model.php */
/* Please DO NOT modify this information : */