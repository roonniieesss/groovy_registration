<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class acl_gudang_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'acl_gudang';
    public $primary = 'acl_gudang_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('acl_gudang_id,users_id,gudang_id,kadaluarsa');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('acl-gudang/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('acl-gudang/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('acl-gudang/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'acl_gudang_id');

            return $this->datatables->generate();
        }
    }
/* End of file acl_gudang_model.php */
/* Location: ./application/models/acl_gudang_model.php */
/* Please DO NOT modify this information : */