<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class peraturan_exim_konten_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'peraturan_exim_konten';
    public $primary = 'peraturan_exim_konten_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('peraturan_exim_konten_id,peraturan_exim_id,kode_bahasa,media_type,media_filename,isi_peraturan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('peraturan-exim-konten/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('peraturan-exim-konten/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('peraturan-exim-konten/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'peraturan_exim_konten_id');

            return $this->datatables->generate();
        }
    }
/* End of file peraturan_exim_konten_model.php */
/* Location: ./application/models/peraturan_exim_konten_model.php */
/* Please DO NOT modify this information : */