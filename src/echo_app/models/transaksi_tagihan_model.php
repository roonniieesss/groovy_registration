<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transaksi_tagihan_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'transaksi_tagihan';
    public $primary = 'transaksi_tagihan_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('transaksi_tagihan_id,transaksi_keuangan_id,tagihan_id,term,rate,net_pembayaran,catatan_pembayaran,jenis');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('transaksi-tagihan/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('transaksi-tagihan/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('transaksi-tagihan/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'transaksi_tagihan_id');

            return $this->datatables->generate();
        }
    }
/* End of file transaksi_tagihan_model.php */
/* Location: ./application/models/transaksi_tagihan_model.php */
/* Please DO NOT modify this information : */