<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class kategori_aset_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'kategori_aset';
    public $primary = 'kategori_aset_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('kategori_aset_id,nama_kategori_aset');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('kategori-aset/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('kategori-aset/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('kategori-aset/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kategori_aset_id');

            return $this->datatables->generate();
        }
    }
/* End of file kategori_aset_model.php */
/* Location: ./application/models/kategori_aset_model.php */
/* Please DO NOT modify this information : */