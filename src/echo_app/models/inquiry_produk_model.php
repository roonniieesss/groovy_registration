<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class inquiry_produk_model extends MY_Model
{
    public $timestamps = false;
    public $table      = 'inquiry_produk';
    public $primary    = 'inquiry_produk_id';
    public $order      = 'DESC';

    public function __construct()
    {
        $this->has_one['jenis_barang'] = ['Jenis_barang_model', 'jenis_barang_id', 'jenis_barang_id'];
        parent::__construct();
    }

    //datatables
    public function json()
    {
        library('datatables');

        $this->datatables->select('inquiry_produk_id,inquiry_id,jenis_barang_id,qty,catatan_pesanan');

        $this->datatables->from($this->table);

        //add this line for join
        //$this->datatables->join('table2', $this->table.'.field = table2.field');

        //generate
        $this->datatables->add_column('action',
            anchor(base_url('inquiry-produk/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
            anchor(base_url('inquiry-produk/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
            anchor(base_url('inquiry-produk/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'inquiry_produk_id');

        return $this->datatables->generate();
    }
}
/* End of file inquiry_produk_model.php */
/* Location: ./application/models/inquiry_produk_model.php */
/* Please DO NOT modify this information : */
