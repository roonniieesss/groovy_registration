<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class moda_transportasi_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'moda_transportasi';
    public $primary = 'moda_transportasi_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('moda_transportasi_id,nama_moda_transportasi');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('moda-transportasi/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('moda-transportasi/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('moda-transportasi/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'moda_transportasi_id');

            return $this->datatables->generate();
        }
    }
/* End of file moda_transportasi_model.php */
/* Location: ./application/models/moda_transportasi_model.php */
/* Please DO NOT modify this information : */