<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sales_order_item_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'sales_order_item';
    public $primary = 'sales_order_item_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('sales_order_item_id,sales_order_detail_id,jenis_barang_id');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('sales-order-item/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('sales-order-item/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('sales-order-item/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'sales_order_item_id');

            return $this->datatables->generate();
        }
    }
/* End of file sales_order_item_model.php */
/* Location: ./application/models/sales_order_item_model.php */
/* Please DO NOT modify this information : */