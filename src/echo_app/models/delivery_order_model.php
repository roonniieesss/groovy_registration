<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class delivery_order_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'delivery_order';
    public $primary = 'delivery_order_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('delivery_order_id,gudang_id,jadwal_pengiriman_id,no_surat_jalan,rencana_tanggal_kirim,tanggal_kirim,tanggal_diterima,nama_supir,telp_supir,nama_penerima,alamat_penerima,telp_penerima,fax_penerima,metode_kirim,catatan,catatan_gudang,status,keterangan_status');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('delivery-order/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('delivery-order/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('delivery-order/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'delivery_order_id');

            return $this->datatables->generate();
        }
    }
/* End of file delivery_order_model.php */
/* Location: ./application/models/delivery_order_model.php */
/* Please DO NOT modify this information : */