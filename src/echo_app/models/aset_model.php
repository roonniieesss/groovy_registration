<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class aset_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'aset';
    public $primary = 'aset_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('aset_id,jenis_aset_id,pengadaan_aset_detail_id,penerimaan_aset_id,no_aset,tanggal_perolehan,harga_perolehan,penyusutan_tahunan,serial_number,habis_garansi,aset_dipakai,qty');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('aset/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('aset/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('aset/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'aset_id');

            return $this->datatables->generate();
        }
    }
/* End of file aset_model.php */
/* Location: ./application/models/aset_model.php */
/* Please DO NOT modify this information : */