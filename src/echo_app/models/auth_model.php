<?php
  
class auth_model extends CI_Model{

	/* Get Login Data untuk otorisasi user */
	public function login($user,$pass){		
		$chek =	$this->db->get_where('users',array('username'=>$user,'password'=>$pass));
		
		if ($chek->num_rows() > 0)
		{
			return $chek->result();
		}
		else 
		{
			return 0;
		}
	}
}
