<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class batal_inquiry_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'batal_inquiry';
    public $primary = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('inquiry_id,inquiry_id,alasan_batal_inquiry_id,keterangan_alasan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('batal-inquiry/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('batal-inquiry/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('batal-inquiry/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), '');

            return $this->datatables->generate();
        }
    }
/* End of file batal_inquiry_model.php */
/* Location: ./application/models/batal_inquiry_model.php */
/* Please DO NOT modify this information : */