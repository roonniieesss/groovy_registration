<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class metode_pembayaran_model extends MY_Model
{
    public $timestamps = false;
    public $table = 'metode_pembayaran';
    public $primary = 'metode_pembayaran_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    //datatables
        function json() {
            library('datatables');

            $this->datatables->select('metode_pembayaran_id,akun_id,nama_metode_pembayaran,nama_bank,nomor_rekening,pemilik_rekening,kurs,jenis,pembayaran_invoice,pembayaran_tagihan');

            $this->datatables->from($this->table);

            //add this line for join
            //$this->datatables->join('table2', $this->table.'.field = table2.field');

            //generate
            $this->datatables->add_column('action',
                anchor(base_url('metode-pembayaran/read/$1'), 'Read', 'class="btn btn-default waves-effect"') . ' ' .
                anchor(base_url('metode-pembayaran/update/$1'), 'Update', 'class="btn btn-info waves-effect"') . ' ' .
                anchor(base_url('metode-pembayaran/delete/$1'), 'Delete', 'class="btn btn-warning waves-effect" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'metode_pembayaran_id');

            return $this->datatables->generate();
        }
    }
/* End of file metode_pembayaran_model.php */
/* Location: ./application/models/metode_pembayaran_model.php */
/* Please DO NOT modify this information : */