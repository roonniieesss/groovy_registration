<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
            <a href="index.html">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>


        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu("master-data", 1)?> ">
                <i class="material-icons">widgets</i>
                <span>Master Data</span>
            </a>
            <ul class="ml-menu">

                <!-- lokasi    -->
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu(['master-data/negara', 'master-data/propinsi', 'master-data/kota'])?>">
                        <span>Lokasi</span>
                    </a>
                    <ul class="ml-menu">

                        <li <?php activemenu("negara")?>>
                            <a href="<?php echo base_url("master-data/negara"); ?>">
                                <span>Negara</span>
                            </a>
                        </li>

                        <li <?php activemenu("propinsi")?>>
                            <a href="<?php echo base_url("master-data/propinsi"); ?>">
                                <span>Propinsi</span>
                            </a>
                        </li>

                        <li <?php activemenu("kota")?>>
                            <a href="<?php echo base_url("master-data/kota"); ?>">
                                <span>Kota</span>
                            </a>
                        </li>

                    </ul>

                </li>


                <!-- pelanggan -->
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu(['master-data/jenis-segmen-pelanggan', 'master-data/segmen-pelanggan', 'master-data/jenis-kontak-pelanggan'])?>">
                        <span>Pelanggan</span>
                    </a>
                    <ul class="ml-menu">

                        <li <?php activemenu("master-data/jenis-segmen-pelanggan")?>>
                            <a href="<?php echo base_url("master-data/jenis-segmen-pelanggan"); ?>">
                                <span>Jenis Segmen Pelanggan</span>
                            </a>
                        </li>

                        <li <?php activemenu("master-data/segmen-pelanggan")?>>
                            <a href="<?php echo base_url("master-data/segmen-pelanggan"); ?>">
                                <span>Segmen Pelanggan</span>
                            </a>
                        </li>

                        <li <?php activemenu("master-data/jenis-kontak-pelanggan")?>>
                            <a href="<?php echo base_url("master-data/jenis-kontak-pelanggan"); ?>">
                                <span>Jenis Kontak Pelanggan</span>
                            </a>
                        </li>

                    </ul>

                </li>

                <!-- Hewan -->
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu(['master-data/jenis-hewan', 'master-data/ras-hewan'])?>">
                        <span>Hewan</span>
                    </a>
                    <ul class="ml-menu">

                        <li <?php activemenu("master-data/jenis-hewan")?>>
                            <a href="<?php echo base_url("master-data/jenis-hewan"); ?>">
                                <span>Jenis Hewan</span>
                            </a>
                        </li>

                        <li <?php activemenu("master-data/ras-hewan")?>>
                            <a href="<?php echo base_url("master-data/ras-hewan"); ?>">
                                <span>Ras Hewan</span>
                            </a>
                        </li>

                    </ul>

                </li>

                <!-- Layanan / barang -->
                <li>
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu("barang", 1)?> ">
                        <span>Data Layanan</span>
                    </a>
                    <ul class="ml-menu">

                        <li <?php activemenu("master-data/barang")?>>
                            <a href="<?php echo base_url("master-data/barang"); ?>">
                                <span>Daftar Layanan</span>
                            </a>
                        </li>

                        <li <?php activemenu("master-data/kategori-barang")?>>
                            <a href="<?php echo base_url("master-data/kategori-barang"); ?>">
                                <span>Kategori Layanan</span>
                            </a>
                        </li>

                        <li <?php activemenu("master-data/jenis-barang")?>>
                            <a href="<?php echo base_url("master-data/jenis-barang"); ?>">
                                <span>Jenis Layanan</span>
                            </a>
                        </li>


                        <!-- <li <?php activemenu("peminjaman-barang")?>>
                            <a href="<?php echo base_url("peminjaman-barang"); ?>">
                                <span>Peminjaman Layanan</span>
                            </a>
                        </li>


                        <li <?php activemenu("peminjaman-barang-detail")?>>
                            <a href="<?php echo base_url("peminjaman-barang-detail"); ?>">
                                <span>Peminjaman Barang Detail</span>
                            </a>
                        </li>


                        <li <?php activemenu("penerimaan-barang")?>>
                            <a href="<?php echo base_url("penerimaan-barang"); ?>">
                                <span>Penerimaan Barang</span>
                            </a>
                        </li>


                        <li <?php activemenu("penerimaan-barang-detail")?>>
                            <a href="<?php echo base_url("penerimaan-barang-detail"); ?>">
                                <span>Penerimaan Barang Detail</span>
                            </a>
                        </li>-->


                    </ul>
                </li>

                <li <?php activemenu("master-data/kemasan")?>>
                    <a href="<?php echo base_url("master-data/kemasan"); ?>">
                        <span>Kemasan</span>
                    </a>
                </li>


                <li <?php activemenu("master-data/paket-layanan-detail")?>>
                    <a href="<?php echo base_url("master-data/paket-layanan-detail"); ?>">
                        <span>Paket Layanan Detail</span>
                    </a>
                </li>

                <li <?php activemenu("master-data/moda-transportasi")?>>
                    <a href="<?php echo base_url("master-data/moda-transportasi"); ?>">
                        <span>Moda Transportasi</span>
                    </a>
                </li>

                <li <?php activemenu("master-data/satuan")?>>
                    <a href="<?php echo base_url("master-data/satuan"); ?>">
                        <span>Satuan</span>
                    </a>
                </li>


            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu("pelanggan", 1)?> ">
                <i class="material-icons">widgets</i>
                <span>Data Pelanggan</span>
            </a>
            <ul class="ml-menu">

                <li <?php activemenu("pelanggan")?>>
                    <a href="<?php echo base_url("pelanggan"); ?>">
                        <span>Pelanggan</span>
                    </a>
                </li>


                <li <?php activemenu("kontak-pelanggan")?>>
                    <a href="<?php echo base_url("kontak-pelanggan"); ?>">
                        <span>Kontak Pelanggan</span>
                    </a>
                </li>


                <li <?php activemenu("alamat-pelanggan")?>>
                    <a href="<?php echo base_url("alamat-pelanggan"); ?>">
                        <span>Alamat Pelanggan</span>
                    </a>
                </li>


            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu('inquiry', 1)?> ">
                <i class="material-icons">widgets</i>
                <span>Data Inquiry</span>
            </a>
            <ul class="ml-menu">
                <li <?php activemenu("inquiry/create")?>>
                    <a href="<?php echo base_url("inquiry/create"); ?>">
                        <span>Buat Inquiry</span>
                    </a>
                </li>

                <li <?php activemenu("inquiry")?>>
                    <a href="<?php echo base_url("inquiry"); ?>">
                        <span>Daftar Inquiry</span>
                    </a>
                </li>

            </ul>
        </li>


        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu("sales-order", 1)?> ">
                <i class="material-icons">widgets</i>
                <span>Data Sales Order</span>
            </a>
            <ul class="ml-menu">

                <li <?php activemenu("sales-order")?>>
                    <a href="<?php echo base_url("sales-order"); ?>">
                        <span>Sales Order</span>
                    </a>
                </li>

                <li <?php activemenu("sales-order-detail")?>>
                    <a href="<?php echo base_url("sales-order-detail"); ?>">
                        <span>Sales Order Detail</span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu(['invoice','akun','metode-pembayaran','skema-harga','transaksi-keuangan'])?> ">
                <i class="material-icons">widgets</i>
                <span>Finance</span>
            </a>
            <ul class="ml-menu">

                <li <?php activemenu("invoice")?>>
                    <a href="<?php echo base_url("invoice"); ?>">
                        <span>Invoice</span>
                    </a>
                </li>

                <li <?php activemenu("akun")?>>
                    <a href="<?php echo base_url("akun"); ?>">
                        <span>Akun Pembayaran</span>
                    </a>
                </li>

                <li <?php activemenu("metode-pembayaran")?>>
                    <a href="<?php echo base_url("metode-pembayaran"); ?>">
                        <span>Metode Pembayaran</span>
                    </a>
                </li>

                <li <?php activemenu("skema-harga")?>>
                    <a href="<?php echo base_url("skema-harga"); ?>">
                        <span>Skema Harga</span>
                    </a>
                </li>

                <li <?php activemenu("transaksi-keuangan")?>>
                    <a href="<?php echo base_url("transaksi-keuangan"); ?>">
                        <span>Transaksi Keuangan</span>
                    </a>
                </li>


            </ul>
        </li>

        <li>        
            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block <?php togglemenu(['work_order1']) ?>">
                <i class="material-icons">widgets</i>
                <span>Work Order</span>
            </a>
            <ul class="ml-menu">
                <li <?php activemenu("work_order1") ?>>
                    <a href="<?php echo base_url("work_order1"); ?>">
                        <span>Riwayat Work Order</span>
                    </a>
                </li>
                <li <?php activemenu("work_order1/listview") ?>>
                    <a href="<?php echo base_url("work_order1/listview"); ?>">
                        <span>Buat Work Order</span>
                    </a>
                </li>              
            </ul>

        </li>

        <!-- <li <?php activemenu("acl-gudang")?>>
            <a href="<?php echo base_url("acl-gudang"); ?>">
                <i class="material-icons">view_list</i>
                <span>Acl Gudang</span>
            </a>
        </li>


        <li <?php activemenu("acl-perusahaan")?>>
            <a href="<?php echo base_url("acl-perusahaan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Acl Perusahaan</span>
            </a>
        </li>

        <li <?php activemenu("alat-produksi")?>>
            <a href="<?php echo base_url("alat-produksi"); ?>">
                <i class="material-icons">view_list</i>
                <span>Alat Produksi</span>
            </a>
        </li>


        <li <?php activemenu("apkir-aset")?>>
            <a href="<?php echo base_url("apkir-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Apkir Aset</span>
            </a>
        </li>


        <li <?php activemenu("apkir-aset-detail")?>>
            <a href="<?php echo base_url("apkir-aset-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Apkir Aset Detail</span>
            </a>
        </li>


        <!-- WEB -->
        <!-- <li <?php activemenu("artikel")?>>
            <a href="<?php echo base_url("artikel"); ?>">
                <i class="material-icons">view_list</i>
                <span>Artikel</span>
            </a>
        </li>

        <li <?php activemenu("artikel-konten")?>>
            <a href="<?php echo base_url("artikel-konten"); ?>">
                <i class="material-icons">view_list</i>
                <span>Artikel Konten</span>
            </a>
        </li> -->
        <!-- END WEB -->


        <!-- <li <?php activemenu("aset")?>>
            <a href="<?php echo base_url("aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Aset</span>
            </a>
        </li> -->










        <!-- <li <?php activemenu("bahasa")?>>
            <a href="<?php echo base_url("bahasa"); ?>">
                <i class="material-icons">view_list</i>
                <span>Bahasa</span>
            </a>
        </li>

        <li <?php activemenu("buku-besar")?>>
            <a href="<?php echo base_url("buku-besar"); ?>">
                <i class="material-icons">view_list</i>
                <span>Buku Besar</span>
            </a>
        </li>


        <li <?php activemenu("delivery-order")?>>
            <a href="<?php echo base_url("delivery-order"); ?>">
                <i class="material-icons">view_list</i>
                <span>Delivery Order</span>
            </a>
        </li>


        <li <?php activemenu("delivery-order-detail")?>>
            <a href="<?php echo base_url("delivery-order-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Delivery Order Detail</span>
            </a>
        </li>


        <li <?php activemenu("deposit-pelanggan")?>>
            <a href="<?php echo base_url("deposit-pelanggan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Deposit Pelanggan</span>
            </a>
        </li>


        <li <?php activemenu("gudang")?>>
            <a href="<?php echo base_url("gudang"); ?>">
                <i class="material-icons">view_list</i>
                <span>Gudang</span>
            </a>
        </li>


        <li <?php activemenu("hasil-pecah-kemasan")?>>
            <a href="<?php echo base_url("hasil-pecah-kemasan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Hasil Pecah Kemasan</span>
            </a>
        </li>

        <li <?php activemenu("jenis-aset")?>>
            <a href="<?php echo base_url("jenis-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Jenis Aset</span>
            </a>
        </li>



        <li <?php activemenu("jenis-dokumen")?>>
            <a href="<?php echo base_url("jenis-dokumen"); ?>">
                <i class="material-icons">view_list</i>
                <span>Jenis Dokumen</span>
            </a>
        </li>


        <li <?php activemenu("jenis-layanan")?>>
            <a href="<?php echo base_url("jenis-layanan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Jenis Layanan</span>
            </a>
        </li>

        <li <?php activemenu("jenis-vaksin")?>>
            <a href="<?php echo base_url("jenis-vaksin"); ?>">
                <i class="material-icons">view_list</i>
                <span>Jenis Vaksin</span>
            </a>
        </li>


        <li <?php activemenu("kategori-aset")?>>
            <a href="<?php echo base_url("kategori-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Kategori Aset</span>
            </a>
        </li>



        <li <?php activemenu("kategori-layanan")?>>
            <a href="<?php echo base_url("kategori-layanan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Kategori Layanan</span>
            </a>
        </li>


        <li <?php activemenu("kemasan")?>>
            <a href="<?php echo base_url("kemasan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Kemasan</span>
            </a>
        </li>


        <li <?php activemenu("kupon")?>>
            <a href="<?php echo base_url("kupon"); ?>">
                <i class="material-icons">view_list</i>
                <span>Kupon</span>
            </a>
        </li>


        <li <?php activemenu("lokasi")?>>
            <a href="<?php echo base_url("lokasi"); ?>">
                <i class="material-icons">view_list</i>
                <span>Lokasi</span>
            </a>
        </li>


        <li <?php activemenu("merek")?>>
            <a href="<?php echo base_url("merek"); ?>">
                <i class="material-icons">view_list</i>
                <span>Merek</span>
            </a>
        </li>


        <li <?php activemenu("metode-pembayaran")?>>
            <a href="<?php echo base_url("metode-pembayaran"); ?>">
                <i class="material-icons">view_list</i>
                <span>Metode Pembayaran</span>
            </a>
        </li>

        <li <?php activemenu("negara")?>>
            <a href="<?php echo base_url("negara"); ?>">
                <i class="material-icons">view_list</i>
                <span>Negara</span>
            </a>
        </li>


        <li <?php activemenu("paket-layanan")?>>
            <a href="<?php echo base_url("paket-layanan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Paket Layanan</span>
            </a>
        </li>


        <li <?php activemenu("paket-layanan-detail")?>>
            <a href="<?php echo base_url("paket-layanan-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Paket Layanan Detail</span>
            </a>
        </li>


        <li <?php activemenu("pecah-kemasan")?>>
            <a href="<?php echo base_url("pecah-kemasan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pecah Kemasan</span>
            </a>
        </li>


        <li <?php activemenu("pecah-kemasan-detail")?>>
            <a href="<?php echo base_url("pecah-kemasan-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pecah Kemasan Detail</span>
            </a>
        </li>

        <li <?php activemenu("pembatasan-jenis-barang-kupon")?>>
            <a href="<?php echo base_url("pembatasan-jenis-barang-kupon"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pembatasan Jenis Barang Kupon</span>
            </a>
        </li>



        <li <?php activemenu("penempatan-aset")?>>
            <a href="<?php echo base_url("penempatan-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Penempatan Aset</span>
            </a>
        </li>


        <li <?php activemenu("penerimaan-aset")?>>
            <a href="<?php echo base_url("penerimaan-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Penerimaan Aset</span>
            </a>
        </li>


        <li <?php activemenu("pengadaan-aset")?>>
            <a href="<?php echo base_url("pengadaan-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pengadaan Aset</span>
            </a>
        </li>


        <li <?php activemenu("pengadaan-aset-detail")?>>
            <a href="<?php echo base_url("pengadaan-aset-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pengadaan Aset Detail</span>
            </a>
        </li>


        <li <?php activemenu("penggunaan-barang")?>>
            <a href="<?php echo base_url("penggunaan-barang"); ?>">
                <i class="material-icons">view_list</i>
                <span>Penggunaan Barang</span>
            </a>
        </li>


        <li <?php activemenu("peraturan-exim")?>>
            <a href="<?php echo base_url("peraturan-exim"); ?>">
                <i class="material-icons">view_list</i>
                <span>Peraturan Exim</span>
            </a>
        </li>


        <li <?php activemenu("peraturan-exim-attachment")?>>
            <a href="<?php echo base_url("peraturan-exim-attachment"); ?>">
                <i class="material-icons">view_list</i>
                <span>Peraturan Exim Attachment</span>
            </a>
        </li>


        <li <?php activemenu("peraturan-exim-jenis-dokumen")?>>
            <a href="<?php echo base_url("peraturan-exim-jenis-dokumen"); ?>">
                <i class="material-icons">view_list</i>
                <span>Peraturan Exim Jenis Dokumen</span>
            </a>
        </li>


        <li <?php activemenu("peraturan-exim-konten")?>>
            <a href="<?php echo base_url("peraturan-exim-konten"); ?>">
                <i class="material-icons">view_list</i>
                <span>Peraturan Exim Konten</span>
            </a>
        </li>


        <li <?php activemenu("peraturan-exim-ras-hewan")?>>
            <a href="<?php echo base_url("peraturan-exim-ras-hewan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Peraturan Exim Ras Hewan</span>
            </a>
        </li>


        <li <?php activemenu("peraturan-exim-vaksin")?>>
            <a href="<?php echo base_url("peraturan-exim-vaksin"); ?>">
                <i class="material-icons">view_list</i>
                <span>Peraturan Exim Vaksin</span>
            </a>
        </li>


        <li <?php activemenu("perbaikan-aset")?>>
            <a href="<?php echo base_url("perbaikan-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Perbaikan Aset</span>
            </a>
        </li>


        <li <?php activemenu("perusahaan")?>>
            <a href="<?php echo base_url("perusahaan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Perusahaan</span>
            </a>
        </li>


        <li <?php activemenu("pindah-aset")?>>
            <a href="<?php echo base_url("pindah-aset"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pindah Aset</span>
            </a>
        </li>


        <li <?php activemenu("pindah-aset-detail")?>>
            <a href="<?php echo base_url("pindah-aset-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pindah Aset Detail</span>
            </a>
        </li>


        <li <?php activemenu("pindah-gudang")?>>
            <a href="<?php echo base_url("pindah-gudang"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pindah Gudang</span>
            </a>
        </li>


        <li <?php activemenu("pindah-gudang-detail")?>>
            <a href="<?php echo base_url("pindah-gudang-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Pindah Gudang Detail</span>
            </a>
        </li>

        <li <?php activemenu("purchase-order")?>>
            <a href="<?php echo base_url("purchase-order"); ?>">
                <i class="material-icons">view_list</i>
                <span>Purchase Order</span>
            </a>
        </li>


        <li <?php activemenu("purchase-order-detail")?>>
            <a href="<?php echo base_url("purchase-order-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Purchase Order Detail</span>
            </a>
        </li>

        <li <?php activemenu("retur-pembelian")?>>
            <a href="<?php echo base_url("retur-pembelian"); ?>">
                <i class="material-icons">view_list</i>
                <span>Retur Pembelian</span>
            </a>
        </li>


        <li <?php activemenu("retur-pembelian-detail")?>>
            <a href="<?php echo base_url("retur-pembelian-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Retur Pembelian Detail</span>
            </a>
        </li>


        <li <?php activemenu("retur-penjualan")?>>
            <a href="<?php echo base_url("retur-penjualan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Retur Penjualan</span>
            </a>
        </li>


        <li <?php activemenu("retur-penjualan-detail")?>>
            <a href="<?php echo base_url("retur-penjualan-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Retur Penjualan Detail</span>
            </a>
        </li>


        <li <?php activemenu("ruangan")?>>
            <a href="<?php echo base_url("ruangan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Ruangan</span>
            </a>
        </li>




        <li <?php activemenu("sales-order-item")?>>
            <a href="<?php echo base_url("sales-order-item"); ?>">
                <i class="material-icons">view_list</i>
                <span>Sales Order Item</span>
            </a>
        </li>

        <li <?php activemenu("satuan-barang-delivery-order")?>>
            <a href="<?php echo base_url("satuan-barang-delivery-order"); ?>">
                <i class="material-icons">view_list</i>
                <span>Satuan Barang Delivery Order</span>
            </a>
        </li>


        <li <?php activemenu("satuan-barang-transfer-stock")?>>
            <a href="<?php echo base_url("satuan-barang-transfer-stock"); ?>">
                <i class="material-icons">view_list</i>
                <span>Satuan Barang Transfer Stock</span>
            </a>
        </li>




        <li <?php activemenu("skema-harga-detail")?>>
            <a href="<?php echo base_url("skema-harga-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Skema Harga Detail</span>
            </a>
        </li>

        <li <?php activemenu("supplier")?>>
            <a href="<?php echo base_url("supplier"); ?>">
                <i class="material-icons">view_list</i>
                <span>Supplier</span>
            </a>
        </li>


        <li <?php activemenu("sys-config")?>>
            <a href="<?php echo base_url("sys-config"); ?>">
                <i class="material-icons">view_list</i>
                <span>Sys Config</span>
            </a>
        </li>


        <li <?php activemenu("sys-config-perusahaan")?>>
            <a href="<?php echo base_url("sys-config-perusahaan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Sys Config Perusahaan</span>
            </a>
        </li>


        <li <?php activemenu("sys-running-number")?>>
            <a href="<?php echo base_url("sys-running-number"); ?>">
                <i class="material-icons">view_list</i>
                <span>Sys Running Number</span>
            </a>
        </li>


        <li <?php activemenu("tagihan")?>>
            <a href="<?php echo base_url("tagihan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Tagihan</span>
            </a>
        </li>


        <li <?php activemenu("transaksi-invoice")?>>
            <a href="<?php echo base_url("transaksi-invoice"); ?>">
                <i class="material-icons">view_list</i>
                <span>Transaksi Invoice</span>
            </a>
        </li>


        


        <li <?php activemenu("transaksi-tagihan")?>>
            <a href="<?php echo base_url("transaksi-tagihan"); ?>">
                <i class="material-icons">view_list</i>
                <span>Transaksi Tagihan</span>
            </a>
        </li>


        <li <?php activemenu("transfer-stock")?>>
            <a href="<?php echo base_url("transfer-stock"); ?>">
                <i class="material-icons">view_list</i>
                <span>Transfer Stock</span>
            </a>
        </li>


        <li <?php activemenu("transfer-stock-detail")?>>
            <a href="<?php echo base_url("transfer-stock-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Transfer Stock Detail</span>
            </a>
        </li>


        <li <?php activemenu("users")?>>
            <a href="<?php echo base_url("users"); ?>">
                <i class="material-icons">view_list</i>
                <span>Users</span>
            </a>
        </li>


        <li <?php activemenu("work-order")?>>
            <a href="<?php echo base_url("work-order"); ?>">
                <i class="material-icons">view_list</i>
                <span>Work Order</span>
            </a>
        </li>


        <li <?php activemenu("work-order-detail")?>>
            <a href="<?php echo base_url("work-order-detail"); ?>">
                <i class="material-icons">view_list</i>
                <span>Work Order Detail</span>
            </a>
        </li>-->






    </ul>
</div>
<!-- #Menu -->