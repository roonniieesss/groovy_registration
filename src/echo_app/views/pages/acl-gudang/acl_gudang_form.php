
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Form <?php echo $button ?> Acl Gudang
                </h2>
            </div>

            <?php echo $this->session->flashdata("message") ?: ""; ?>

            <div class="body">
                <form class="form-horizontal" action="<?php echo $action; ?>" method="post">
                    <input type="hidden" name="acl_gudang_id" value="<?php echo $acl_gudang_id; ?>" />

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Users">Users <?php echo form_error("Users") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="users_id" id="users_id" placeholder="Input Users" value="<?php echo $users_id; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Gudang">Gudang <?php echo form_error("Gudang") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="gudang_id" id="gudang_id" placeholder="Input Gudang" value="<?php echo $gudang_id; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Kadaluarsa">Kadaluarsa <?php echo form_error("Kadaluarsa") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="kadaluarsa" id="kadaluarsa" placeholder="Input Kadaluarsa" value="<?php echo $kadaluarsa; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?php echo $button ?></button>
                            <a href="<?php echo base_url("acl-gudang") ?>" class="btn btn-default m-t-15 waves-effect">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
