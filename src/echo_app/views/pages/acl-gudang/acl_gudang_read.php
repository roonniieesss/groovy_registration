
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detail Acl Gudang
                </h2>
            </div>

            <div class="body">
                    <input type="hidden" name="acl_gudang_id" value="<?php echo $acl_gudang_id; ?>" />
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Users">Users <?php echo form_error("Users") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $users_id; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Gudang">Gudang <?php echo form_error("Gudang") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $gudang_id; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Kadaluarsa">Kadaluarsa <?php echo form_error("Kadaluarsa") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $kadaluarsa; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Active">Active <?php echo form_error("Active") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__active; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Created">Created <?php echo form_error("Created") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__created; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Updated">Updated <?php echo form_error("Updated") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__updated; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Username">Username <?php echo form_error("Username") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__username; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <a href="<?php echo base_url("acl-gudang") ?>" class="btn btn-default m-t-15 waves-effect">Cancel</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
