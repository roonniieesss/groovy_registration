
<link rel="stylesheet" href="<?php echo base_url("tpl_assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css") ?>"/>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">

                <?php echo $this->session->flashdata("message") ?: ""; ?>

                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-9">
                        <h2 style="margin-top:0px"><?php echo $title; ?></h2>
                    </div>

                    <div class="col-md-3 text-right">
                        <?php echo anchor(base_url("alamat-pelanggan/create"), "Create Alamat Pelanggan", 'class="btn btn-primary btn-lg btn-block"'); ?>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="mytable">
                        <thead>
                            <tr>
                                <th width="80px">No</th>
								 <th>Pelanggan</th>
								 <th>Negara</th>
								 <th>Kota</th>
								 <th>Label</th>
								 <th>Alamat</th>
								 <th>Kode Pos</th>
								 <th>Nama Penerima</th>
								 <th>Alamat Utama</th>
                                <th width="200px">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var myAjax = {"url": "alamat-pelanggan/json", "type": "POST"};
    var myColumns = [
        {
            "data": "alamat_pelanggan_id",
            "orderable": false
        },
			{"data": "pelanggan_id"},
			{"data": "negara_id"},
			{"data": "kota_id"},
			{"data": "label"},
			{"data": "alamat"},
			{"data": "kode_pos"},
			{"data": "nama_penerima"},
			{"data": "alamat_utama"},
        {
            "data" : "action",
            "orderable": false,
            "className" : "text-center"
        }
    ];
</script>


<?php view("components/datatable");?>
