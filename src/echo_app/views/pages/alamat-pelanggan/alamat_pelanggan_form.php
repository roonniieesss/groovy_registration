
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Form <?php echo $button ?> Alamat Pelanggan
                </h2>
            </div>

            <?php echo $this->session->flashdata("message") ?: ""; ?>

            <div class="body">
                <form class="form-horizontal" action="<?php echo $action; ?>" method="post">
                    <input type="hidden" name="alamat_pelanggan_id" value="<?php echo $alamat_pelanggan_id; ?>" />

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Pelanggan">Pelanggan <?php echo form_error("Pelanggan") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="pelanggan_id" id="pelanggan_id" placeholder="Input Pelanggan" value="<?php echo $pelanggan_id; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Negara">Negara <?php echo form_error("Negara") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="negara_id" id="negara_id" placeholder="Input Negara" value="<?php echo $negara_id; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Kota">Kota <?php echo form_error("Kota") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="kota_id" id="kota_id" placeholder="Input Kota" value="<?php echo $kota_id; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Label">Label <?php echo form_error("Label") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="label" id="label" placeholder="Input Label" value="<?php echo $label; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Alamat">Alamat <?php echo form_error("Alamat") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="alamat" id="alamat" placeholder="Input Alamat" value="<?php echo $alamat; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Kode Pos">Kode Pos <?php echo form_error("Kode Pos") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="kode_pos" id="kode_pos" placeholder="Input Kode Pos" value="<?php echo $kode_pos; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Nama Penerima">Nama Penerima <?php echo form_error("Nama Penerima") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="nama_penerima" id="nama_penerima" placeholder="Input Nama Penerima" value="<?php echo $nama_penerima; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Alamat Utama">Alamat Utama <?php echo form_error("Alamat Utama") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="alamat_utama" id="alamat_utama" placeholder="Input Alamat Utama" value="<?php echo $alamat_utama; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?php echo $button ?></button>
                            <a href="<?php echo base_url("alamat-pelanggan") ?>" class="btn btn-default m-t-15 waves-effect">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
