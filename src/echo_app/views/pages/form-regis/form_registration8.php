<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Groovy-CRM</title>
  <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" />
</head>
<body>
    <br/>
    <div class="container box">
        <br/>
        <h2 align="center">Groovy Registration</h2><br />
        <?php echo $this->session->flashdata('notif');?>             
        <?php echo form_open('form_regis/insert', array('enctype'=>'multipart/form-data','id'=>'register_form'));?>
        <ul class="nav nav-tabs">
           <li class="nav-item">
                <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_destination">Destination</a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive_tab1" id="list_pet_information" style="border:1px solid #ccc">Pet Information</a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive_tab1" id="list_customer_information" style="border:1px solid #ccc">Customer Informations</a>
            </li>
        </ul>
    
        <div class="tab-content" style="margin-top:16px;">
            <div class="tab-pane active" id="destination_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Destinations</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Kota Asal</label>
                            <select name="kota_asal" required='true' id="kota-asal-sel" class="form-control">
                                <option required='true' value='0' disabled='disabled' selected/>Kota Asal</option>
                                <?php
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_kota_asal" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Kota Tujuan</label>
                            <select name="kota_tujuan" required='true' id="kota-tujuan-sel" class="form-control">
                                <?php       
                                echo "<option required='true' value='0' disabled='disabled' selected/>Kota Tujuan</option>";                                    
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>  
                           <span id="err_msg_kota_tujuan" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Moda Transportasi</label>
                            <select name="moda_transportasi" required='true' id="moda-transportasi-sel" class="form-control">
                                <option required='true' value='0' disabled='disabled' selected/>Moda Trasnportasi</option>
                                <?php                                                                            
                                foreach ($transport as $t) {
                                    echo "<option required='true' value='$t->moda_transportasi_id'>$t->nama_moda_transportasi</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_moda_transportasi" class="text-danger"></span>
                        </div>
                        <div align="center">
                           <button type="button" name="btn_login_details" id="btn_destination_details" class="btn btn-info btn-lg">Next</button>
                        </div>
                    </div>  <!-- panel-body -->
                </div>
            </div>
            <div class="tab-pane fade" id="pet_information_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill Pet Information</div>
                    <div class="panel-body">
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <!-- inquiry -->
                                <input type="hidden" id="inquiry_id" name="inquiry_id" class="form-control" value="<?php echo $inquiry_id; ?>" >

                                <!-- inquiry dan inquiry produk -->
                                <input type="hidden" id="no_inquiry" name="no_inquiry" class="form-control" value="<?php echo $no_inquiry; ?>" >

                                <!-- inquiry produk -->
                                <input type="hidden" id="inquiry_produk_id" name="inquiry_produk_id" class="form-control" value="<?php echo $inquiry_produk_id; ?>" >
                                <input type="hidden" id="jenis_barang_id" name="jenis_barang_id" class="form-control" value="<?php echo $jenis_barang_id;     ?>" >

                                <!-- inquiry produk pet transport -->
                                 <input type="hidden" id="inquiry_produk_pet_transport_id" name="inquiry_produk_pet_transport_id" class="form-control" value="<?php echo $inquiry_produk_pet_transport_id;     ?>" >

                                <label>Jumlah Hewan</label>
                                    <input type="text" name="qty" id="qty" class="form-control" />
                                    <span id="err_msg_qty_hewan" class="text-danger"></span>
                            </div>
                        </div>                       
                        <div id="namjen">
                            <div class="col-md-6" id="class-nama-hewan">
                                <div class="form-group">
                                   <label>Nama Hewan</label>
                                   <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" />
                                   <span id="err_msg_nama_hewan" class="text-danger"></span>
                                </div>                            
                            </div>
                            <div class="col-md-6" id="class-jenis-hewan">
                                <div class="form-group" id="det-pet-1">
                                    <label>Jenis Hewan</label>
                                    <select name="jenis_hewan" id="jenis_hewan_sel" required='true' class="form-control">
                                        <!-- <div id="isi-hewan"></div> -->
                                    </select>
                                    <span id="err_msg_jenis_hewan" class="text-danger"></span>
                                </div>
                            </div>
                        </div>                     
                        <div class="col-md-12" id="rule-pet-herder">
                            <div class="panel panel-default">
                                <div class="panel-heading">Rule Information Herder</div> 
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Dokumen Vaksin Hewan</label>
                                        <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                                    </div>
                                    <div class="form-group">
                                        <label>Catatan</label>
                                        <textarea name="catatan_pesananan" id="catatan_pesananan" class="form-control"/></textarea>
                                        <span id="err_msg_catatan_pesanan" class="text-danger"></span>
                                    </div>
                                </div>                           
                            </div>                          
                        </div>
                        <div class="col-md-12" id="rule-pet-anggora">
                            <div class="panel panel-default">
                                <div class="panel-heading">Rule Information Anggora</div> 
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Dokumen Vaksin Hewan</label>
                                        <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                                    </div>
                                    <div class="form-group">
                                        <label>Catatan</label>
                                        <textarea name="catatan_pesananan" id="catatan_pesananan" class="form-control"/></textarea>
                                    </div>
                                </div>                           
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div align="center">
                               <button type="button" name="previous_btn_pet_details" id="previous_btn_pet_details" class="btn btn-default btn-lg">Previous</button>
                               <button type="button" name="btn_pet_details" id="btn_pet_details" class="btn btn-info btn-lg">Next</button>
                            </div>
                        </div>
                     <br/>
                    </div>
                </div> 
            </div>           
            <div class="tab-pane fade" id="pelanggan_information_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill Customer Information</div>
                    <div class="panel-body">
                        <div class="form-group">                            
                            <label>Nama Pelanggan</label>
                            <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" />
                            <span id="err_msg_nama_pelanggan" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                           <label>Tanggal Lahir</label>
                           <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" />
                           <span id="err_msg_tgl_lahir" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <label class="radio-inline">
                                <input type="radio" name="jenis_kelamin" value="PRIA" checked> Pria
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="jenis_kelamin" value="WANITA"> Wanita
                            </label>
                            <span id="err_msg_jenis_kelamin" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                           <label>Kontak</label>
                           <input type="text" name="kontak" id="kontak" class="form-control" />
                           <span id="err_msg_kontak" class="text-danger"></span>
                        </div>                        
                        <div class="form-group">
                            <input type="hidden" id="kontak_pelanggan_id" name="kontak_pelanggan_id"class="form-control" value="<?php echo $kontak_pel_id;  ?>" >
                            <input type="hidden" id="pelanggan_id" name="pelanggan_id" class="form-control" value="<?php echo $id; ?>" >
                            <label>Jenis Kontak</label>
                            <select name="jenis_kontak_pelanggan_id" required='true' class="form-control">
                                <option value="0" disabled selected>Jenis Kontak</option>
                                <?php                                                           
                                foreach ($kontak as $k) {
                                    echo "<option required='true' value='$k->jenis_kontak_pelanggan_id'>$k->nama_jenis_kontak_pelanggan</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_jenis_kontak" class="text-danger"></span>
                        </div>
                        <br />
                        <div align="center">
                           <button type="button" name="previous_btn_cust_details" id="previous_btn_cust_details" class="btn btn-default btn-lg">Previous</button>
                           <button type="submit" name="submit" id="Register" class="btn btn-info btn-lg">Register</button>
                        </div>
                        <br/>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>

    <script type="text/javascript">
        // Rule Information
        $("#rule-pet-anggora").hide();
        $("#rule-pet-herder").hide();

        $("#class-nama-hewan").hide();
        $("#class-jenis-hewan").hide();

        $("#qty").change(function(){
            var qty = $(this).val();
            var html = '';
            var html2 = '';

            if($(this).val() != ""){
                html='<div class="col-md-12" id="class-nama-hewan">'
                + '<div class="form-group">'
                + '<label>Nama Hewan</label>'
                + '<input type="text" name="nama_hewan" id="nama_hewan" class="form-control" />'
                + '<span id="err_msg_nama_hewan" class="text-danger"></span>'
                + '</div>'
                + '</div>'
                + '<div class="col-md-12" id="class-jenis-hewan">'
                + '<div class="form-group" id="det-pet-1">'
                + '<label>Jenis Hewan</label>'
                + '<select name="jenis_hewan" id="jenis_hewan_sel" required="true" class="form-control">'
                + '<option value="0" disabled="disabled" selected>Jenis Hewan</option>'          
                + '</select>'
                + '<span id="err_msg_jenis_hewan" class="text-danger"></span>'
                + '</div></div>'
                + '<div class="col-md-12" id="class-ras-hewan">'
                + '<div class="form-group" id="det-pet-1">'
                + '<label>Ras Hewan</label>'
                + '<select name="ras_hewan" id="jenis_ras_sel" required="true" class="ras-hewan form-control">'
                + '<option value="0" disabled="disabled" selected>Jenis Hewan</option>'          
                + '</select>'
                + '<span id="err_msg_ras_hewan" class="text-danger"></span>'
                + '</div></div>'
                + '<div class="col-md-12">'
                + '<div class="form-group">'
                + '<label> Jenis Kelamin  </label>'                
                + '<label class="radio-inline">'
                + '<input type="radio" name="jenis_kelamin_pet" value="Jantan" checked> Jantan </label>'
                + '<label class="radio-inline">'
                + '<input type="radio" name="jenis_kelamin_pet" value="Wanita"> Betina </label>'
                + '<span id="err_msg_jenis_kelamin_pet" class="text-danger"></span>'
                + '</select>'
                + '</div></div>';
                $('#namjen').html("");
                $('#catatan_pesananan').html("");
                for (var i = 1; i<=qty; i++) {
                    $('#namjen').append(html);                    
                }

                // select jenis hewan
                $("#jenis_hewan_sel").change(function(){
                    // alert($("#jenis_hewan").val());
                    if($(this).val() == "1"){
                        $('#rule-pet-anggora').show();
                        $('#rule-pet-herder').hide();
                    }else if($(this).val() == "2"){
                        $('#rule-pet-herder').show();
                        $('#rule-pet-anggora').hide();              
                    }

                    if ($(this).val()=="") {
                        $('#all-rule-pet').hide();
                    }
                });
            }
            
            if ($(this).val() == "") {
                $('#namjen').html(""); // Mengisi sebuah record didalam tag penuh.
                $('#namjen').append(html); // Menambahkan sebuah record didalam tag bagian bawah.              
            }

            $('#jenis_hewan_sel').change(function(){
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url();?>form-regis/get_ras_hewan",
                    method : "POST",
                    data : {id: id},
                    async : false,
                    dataType : 'json',
                    success: function(a){
                        var html = '';
                        var i;
                        for(i=0; i<a.length; i++){
                            html += '<option value='+a[i].ras_hewan_id+'>'+a[i].nama_ras_hewan+'</option>';
                        }
                        $('.ras-hewan').html(html);                
                    }
                });
            });

            $.ajax({
                url:"<?php echo base_url('/form-regis/get_data_jenis_hewan'); ?>",
                dataType:"json",
                success: function( a){
                    // a = JSON.parse(a);
                    // console.log(a);
                    // console.log(typeof((a))); //menampilkan tipe dari a
                    var  opt = "";
                    for(var i = 0; i<a.length; i++){                                
                        opt += '<option value='+a[i]['jenis_hewan_id']+'>'+a[i]['nama_jenis_hewan']+ '</option>';
                    }                
                    $("select[name=jenis_hewan]").append(opt);
                    // $("div#isi-hewan").html(opt);
                } //success
            });
        });
    </script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/main.js')?>"></script>
</body>
</html>