<!DOCTYPE html>
<html>
<head>
    <title>Groovy-CRM</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>

    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>

    <script src="<?php echo base_url('assets/js/bootstrapValidator.min.js')?>"> </script>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />

    <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" />

    <link href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" rel="stylesheet" />
    
    <!-- Bootstrap DatePicker Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" />

</head>

<?php echo $this->session->flashdata("message") ?: ""; ?>
<body>
    <div class="container box">
        <div class="row form-group">
            <div class="col-xs-12" id="#kepala">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li class="active"><a href="#step-1">
                        <h4 class="list-group-item-heading">Step 1</h4>
                        <p class="list-group-item-text">Transport and Destination Details</p>
                    </a></li>
                    <li class="disabled"><a href="#step-2">
                        <h4 class="list-group-item-heading">Step 2</h4>
                        <p class="list-group-item-text">Pet Informations Details</p>
                    </a></li>
                    <li class="disabled"><a href="#step-3">
                        <h4 class="list-group-item-heading">Step 3</h4>
                        <p class="list-group-item-text">Customer Informations Details</p>
                    </a></li>
                </ul>
            </div>
        </div>
        
        <form action="<?php echo $action; ?>" method="post" autocomplete="off">
            
            <!-- MASTER DATA TUJUAN -->
            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label class="pull-left">Kota/Airport Asal *</label>

                                                <?php dropdown(['kota as Kota Asal', 'inquirypet[asal_kota_id]'], ['kota_id', 'nama_kota'], $asal_kota, 'class="form-control" id="asal_kota" required');
                                                ?>
                                                
                                                <span id="err_msg_kota_asal" class="text-danger pull-right"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label class="pull-left">Kota/Airport Tujuan *</label>

                                                <?php dropdown(['kota as Kota Tujuan', 'inquirypet[tujuan_kota_id]'], ['kota_id', 'nama_kota'], $tujuan_kota, 'class="form-control" id="tujuan_kota" ');?> 

                                               <span id="err_msg_kota_tujuan" class="text-danger pull-right"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label class="pull-left">Jenis Layanan *</label>

                                                <?php dropdown(['jenis_barang as jenis_layanan', 'inquirypet[jenis_barang_id]'], ['jenis_barang_id', 'nama_jenis_barang'], $jenis_barang_id, 'class="form-control" id="jenis_barang_id" ');?>

                                               <span id="err_msg_jenis_layanan" class="text-danger pull-right"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label class="pull-left">Moda Transportasi *</label>
                                                <?php dropdown(['moda_transportasi', 'inquirypet[moda_transportasi_id]'], ['moda_transportasi_id', 'nama_moda_transportasi'], $moda_transportasi_id, 'class="form-control" id="moda_transportasi_id" ');
                                                ?>
                                                <span id="err_msg_moda_transportasi" class="text-danger pull-right"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group hidden maskapai">
                                            <div class="form-line">
                                                <label class="pull-left">Maskapai *</label>
                                                <input type="text" name="inquirypet[maskapai]" id="maskapai" class="form-control" placeholder="Maskapai Penerbangan" />
                                                <span id="err_msg_maskapai" class="text-danger pull-right"></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a id="activate-step-2" class="btn btn-info btn-lg pull-right">Next</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MASTER DATA HEWAN -->
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <div class="panel-body cari_rule">                        
                            <div class="panel panel-default">
                                <div class="panel-body">                               
                                    <div class="col-md-6">                                       
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>Nama Hewan *</label>
                                                <input type="text" name="inquirypet[nama_hewan][]" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                                                <span id="err_msg_nama_hewan" class="text-danger"></span>
                                            </div>
                                        </div>                            
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>DOB *</label>
                                                <input type="text" name="inquirypet[dob][]" id="dob" class="form-control date" placeholder="Date of Birth" />
                                                <span id="err_msg_dob_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>Warna Hewan *</label>
                                                <input type="text" name="inquirypet[warna_hewan][]" class="form-control" placeholder="Input Warna Hewan" />
                                                <span id="err_msg_warna_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>Jenis Kelamin *</label>
                                                <select name="inquirypet[jenis_kelamin][]" class="form-control" id="jenis_kelamin_hewan">
                                                    <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                                    <option value="JANTAN">Jantan</option>
                                                    <option value="BETINA">Betina</option>
                                                </select>
                                                <span id="err_msg_jenis_kelamin_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>Berat (kg) *</label>
                                                <input type="number" name="inquirypet[berat][]" class="form-control" placeholder="Berat Hewan" />
                                                <span id="err_msg_berat_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>Microchip *</label>
                                                <input type="text" name="inquirypet[microchip][]" class="form-control" placeholder="Input Microchip" />
                                                <span id="err_msg_microchip_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>Ukuran Kandang *</label>
                                                <input type="text" name="inquirypet[ukuran_kandang][]" id="ukuran_kandang" class="form-control" placeholder="Input Ukuran Kandang" />
                                                <span id="err_msg_ukuran_kandang" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>                                

                                    <div class="col-md-6">
                                        <div class="form-group" id="det-pet-1">
                                            <div class="form-line">
                                                <label>Ras Hewan *</label>
                                                <?php dropdown(['ras_hewan', 'inquirypet[ras_hewan_id][]'], ['ras_hewan_id', 'nama_ras_hewan'], $ras_hewan_id, 'class="form-control" id="ras_hewan_id"');
                                                ?>
                                                <span id="err_msg_ras_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>                                                                    
                                    
                                </div>       
                            </div> <!-- panel panel-default -->
                            <div class="show-rule-hewan"></div>
                            <div class="hewan-baru"></div>                                                                          
                            
                            <div class="col-md-12">                       
                                <div class="form-group">
                                    <a class="btn btn-info btn-lg btn-block" id="addhewan">Tambah Hewan</a>
                                </div>
                            </div>         
                        </div> <!-- panel-body -->

                        <a href="#step-1" name="previous_1" id="previous_1" class="btn btn-warning btn-lg pull-left">Previous</a>
                        <a id="activate-step-3" class="btn btn-info btn-lg pull-right">Next</a>                    
                    </div>                                                        
                </div>
            </div>

            <!-- MASTER DATA PELANGGAN -->
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="page-header">
                                        <h3>Data Pelanggan</h3>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">                              
                                                <label>Nama Pelanggan</label>
                                                <input type="text" name="nama_pelanggan" class="form-control" placeholder="Input Nama Pelanggan" />
                                                <span id="err_msg_nama_pelanggan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>                                    

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                               <label>Tanggal Lahir</label>
                                               <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control date" />
                                               <span id="err_msg_tgl_lahir" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                                <label>Jenis Kelamin</label>
                                                <select name="jenis_kelamin" class="form-control">
                                                    <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                                    <option value="Pria">Pria</option>
                                                    <option value="Wanita">Wanita</option>
                                                </select>
                                                <span id="err_msg_jenis_kelamin" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>                        
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                                <label>Kode POS</label>
                                                <input type="text" name="kode_pos" id="kode_pos" class="form-control" placeholder="Input Kode POS" />
                                                <span id="err_msg_kode_pos" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-6">                       
                                        <div class="form-group">
                                            <div class="form-line">                         
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="email" placeholder="Input Email" />
                                                <span id="err_msg_email" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                               <label>No Telepon</label>
                                               <input type="text" name="kontak" class="form-control" placeholder="Input Nomor Telepon" />
                                               <span id="err_msg_kontak" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                               <label>No Pasport</label>
                                               <input type="text" name="pasport" class="form-control" placeholder="Input Nomor Pasport" />
                                               <span id="err_msg_pasport" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                                <label>Kota</label>
                                                <?php dropdown('kota', ['kota_id', 'nama_kota'], $kota_id, 'class="form-control" id="kota_id"');?>                                                    
                                                <span id="err_msg_jenis_kontak" class="text-danger"></span>
                                                <input type="hidden" name="get-negara" class="get-negara"/>
                                            </div>                               
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                               <label>Alamat Pelanggan</label>
                                               <textarea type="text" name="alamat" id="alamat" class="form-control" placeholder="Input Alamat Pelanggan"></textarea>
                                               <span id="err_msg_alamat" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                               <label>Catatan Pesanan</label>
                                               <textarea type="text" name="catatan" class="form-control" placeholder="Input Catatan Pesanan"></textarea>
                                               <span id="err_msg_catatan_pesanan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="page-header col-md-12">
                                        <h3>Data Pengiriman</h3>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">                              
                                                <label>Nama Penerima</label>
                                                <input type="text" name="nama_penerima" class="form-control" placeholder="Input Nama Penerima" />
                                                <span id="err_msg_nama_penerima" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">                              
                                                <label for="idcard">ID CARD</label>
                                                <input type="text" class="form-control" autocomplete="off" name="idcard_penerima" id="idcard_penerima" placeholder="Input ID Card/KTP Penerima" value="<?php echo $idcard_penerima; ?>"  />
                                                <span id="err_msg_idcard_penerima" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">                              
                                                <label for="notelp">No Telepon Penerima</label>
                                                <input type="text" class="form-control" autocomplete="off" name="notelp_penerima" id="notelp_penerima" placeholder="Input No Telepon Penerima" />
                                                <span id="err_msg_notelp_penerima" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line"> 
                                               <label>Alamat Penerima</label>
                                               <textarea type="text" name="alamat_pengiriman" id="alamat_pengiriman" class="form-control" placeholder="Input Alamat Pengiriman"></textarea>
                                               <span id="err_msg_alamat_pengiriman" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>             

                                    <input type="hidden" class="form-control" name="label" value="Default Alamat" />
                                    <input type="hidden" class="form-control" name="alamat_utama" value="1" />

                                    <div class="col-md-12">
                                        <a href="#step-2" name="previous_2" id="previous_2" class="btn btn-warning btn-lg pull-left">Previous</a>
                                        <button type="submit" name="submit" id="register" class="btn btn-info btn-lg pull-right">Register</button>
                                    </div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <!-- u/ copy hewan -->
    <div class="panel-body copyHewan hidden">                        
        <div class="panel panel-default">
            <div class="panel-body">                               
                <div class="col-md-6">                                       
                    <div class="form-group">
                        <div class="form-line">
                            <label>Nama Hewan *</label>
                            <input type="text" name="inquirypet[nama_hewan][]" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                            <span id="err_msg_nama_hewan" class="text-danger"></span>
                        </div>
                    </div>                            
                </div>
                
                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>DOB *</label>
                            <input type="text" name="inquirypet[dob][]" id="dob" class="form-control date" placeholder="Date of Birth" />
                            <span id="err_msg_dob_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>Warna Hewan *</label>
                            <input type="text" name="inquirypet[warna_hewan][]" class="form-control" placeholder="Input Warna Hewan" />
                            <span id="err_msg_warna_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>Jenis Kelamin *</label>
                            <select name="inquirypet[jenis_kelamin][]" class="form-control" id="jenis_kelamin_hewan">
                                <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                <option value="JANTAN">Jantan</option>
                                <option value="BETINA">Betina</option>
                            </select>
                            <span id="err_msg_jenis_kelamin_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>Berat (kg) *</label>
                            <input type="number" name="inquirypet[berat][]" class="form-control" placeholder="Berat Hewan" />
                            <span id="err_msg_berat_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>Microchip *</label>
                            <input type="text" name="inquirypet[microchip][]" class="form-control" placeholder="Input Microchip" />
                            <span id="err_msg_microchip_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>Ukuran Kandang *</label>
                            <input type="text" name="inquirypet[ukuran_kandang][]" id="ukuran_kandang" class="form-control" placeholder="Input Ukuran Kandang" />
                            <span id="err_msg_ukuran_kandang" class="text-danger"></span>
                        </div>
                    </div>
                </div>                                

                <div class="col-md-6">
                    <div class="form-group" id="det-pet-1">
                        <div class="form-line">
                            <label>Ras Hewan *</label>
                            <?php dropdown(['ras_hewan', 'inquirypet[ras_hewan_id][]'], ['ras_hewan_id', 'nama_ras_hewan'], $ras_hewan_id, 'class="form-control" id="ras_hewan_id"');
                            ?>
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>
            </div>                                
        </div> <!-- panel derfault -->                                                                            
    </div> <!-- panel-body -->
    

    <!-- RULE PET HEWAN 1 -->
    <div class="col-md-12 master-hewan hidden" id="rule_pet_korean_jindo">
        <div class="panel panel-default">
            <div class="panel-heading">Rule Pet Information Korean Jindo</div> 
            <div class="panel-body">

                <div class="form-group">
                    <label>Dokumen Vaksin Hewan</label>
                    <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                </div>
            
            </div>                           
        </div>               
    </div>

    <!-- RULE PET HEWAN 2 -->
    <div class="col-md-12 master-hewan hidden" id="rule_pet_siberian_husky">
        <div class="panel panel-default">
            <div class="panel-heading">Rule Pet Information Siberian Husky</div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Dokumen Vaksin Hewan</label>
                    <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                </div>                
            </div>                           
        </div>                          
    </div>

    <!-- RULE PET HEWAN 3 -->
    <div class="col-md-12 master-hewan hidden" id="rule_pet_persian">
        <div class="panel panel-default">
            <div class="panel-heading">Rule Pet Information Persian</div> 
            <div class="panel-body">
                <div class="form-group">
                    <label>Dokumen Vaksin Hewan</label>
                    <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                </div>               
            </div>                           
        </div>                          
    </div>

</body>
</html>

<!-- Utama -->
<script src="<?php echo base_url('assets/js/main8d.js')?>"></script>

<!-- Add Pet / Show Rules / Setting Calender -->
<script src="<?php echo base_url('assets/js/all.js')?>"></script>
<!-- Moment Plugin Js -->
<script src="<?php echo base_url(); ?>assets/plugins/momentjs/moment.js"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

<script src="<?php echo base_url('assets/js/bootstrap-select.min.js')?>"></script>
