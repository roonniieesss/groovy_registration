<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Groovy-CRM</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" />

</head>
<body>
    <br />
    <div class="container box">
        <br/>
        <h2 align="center">Multi Step Registration Form Using JQuery Bootstrap in PHP</h2><br />
         <!-- <?php echo $message; ?> -->
         <form method="post" id="register_form">
            <ul class="nav nav-tabs">
               <li class="nav-item">
                  <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_login_details">Login Details</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link inactive_tab1" id="list_personal_details" style="border:1px solid #ccc">Personal Details</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link inactive_tab1" id="list_contact_details" style="border:1px solid #ccc">Contact Details</a>
              </li>
            </ul>
            <div class="progress">
                 <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="tab-content" style="margin-top:16px;">
                <div class="tab-pane active" id="login_details">
                    <div class="panel panel-default">
                           <div class="panel-heading">Login Details</div>
                               <div class="panel-body">
                                    <div class="form-group">
                                        <label>Enter Email Address</label>
                                        <input type="text" name="email" id="email" class="form-control" />
                                        <span id="error_email" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Enter Password</label>
                                        <input type="password" name="password" id="password" class="form-control" />
                                        <span id="error_password" class="text-danger"></span>
                                    </div>
                                    <br/>
                                    <div align="center">
                                        <button type="button" name="btn_login_details" id="btn_login_details" class="btn btn-info btn-lg">Next</button>
                                    </div>
                                    <br />
                                </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="personal_details">
                    <div class="panel panel-default">
                         <div class="panel-heading">Fill Personal Details</div>
                         <div class="panel-body">
                                <div class="form-group">
                                    <label>Enter First Name</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" />
                                    <span id="error_first_name" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Enter Last Name</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" />
                                    <span id="error_last_name" class="text-danger"></span>
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <label class="radio-inline">
                                      <input type="radio" name="gender" value="male" checked> Male
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="gender" value="female"> Female
                                    </label>
                                </div>
                              <br />
                              <div align="center">
                                 <button type="button" name="previous_btn_personal_details" id="previous_btn_personal_details" class="btn btn-default btn-lg">Previous</button>
                                 <button type="button" name="btn_personal_details" id="btn_personal_details" class="btn btn-info btn-lg">Next</button>
                             </div>
                             <br />
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact_details">
                    <div class="panel panel-default">
                        <div class="panel-heading">Fill Contact Details</div>
                        <div class="panel-body">
                            <div class="form-group">
                               <label>Enter Address</label>
                               <textarea name="address" id="address" class="form-control"></textarea>
                               <span id="error_address" class="text-danger"></span>
                            </div>
                            <div class="form-group">
                               <label>Enter Mobile No.</label>
                               <input type="text" name="mobile_no" id="mobile_no" class="form-control" />
                               <span id="error_mobile_no" class="text-danger"></span>
                            </div>
                            <br />
                            <div align="center">
                               <button type="button" name="previous_btn_contact_details" id="previous_btn_contact_details" class="btn btn-default btn-lg">Previous</button>
                               <button type="button" name="btn_contact_details" id="btn_contact_details" class="btn btn-success btn-lg">Register</button>
                            </div>
                            <br />
                       </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>

<script type="text/javascript" src="<?php echo base_url('assets/js/main-ms2.js')?>"></script>


<!-- <div class="tab-pane fade" id="">
                            <div class="panel panel-default">
                                <div class="panel-heading">Rule Pet Information</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        fafasdf
                                    </div>
                                </div>
                            </div>
                        </div> -->


                        <!-- perulangan form -->                   
                       <!--  <?php if (isset($_POST['qty'])){ $jml = $_POST['qty']; for ($a=1;$a<=$jml;$a++) { ?>
                        <br/>
                        <div class="col-md-12">
                            <div class="panel panel-default" id="rule-kucing">
                                <div class="panel-heading">Rule Information</div> 
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Dokumen Vaksin Hewan</label>
                                        <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                                    </div>
                                </div>                           
                            </div>
                        </div>
                        <?php }} ?> -->

<!-- $("#qty").keyup(function(){
      var qty = $(this).val();

      if($(this).val() > 0){
         html='<div class="col-md-12">'
            + '<div class="panel panel-default" id="rule-kucing">'
            + '<div class="panel-heading">Rule Information</div>'
            + '<div class="panel-body">'
            + '<div class="form-group">'
            + '<label>Dokumen Vaksin Hewan</label>'
            + '<input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>';

         for (var i = 1; i<=qty; i++) {
            $('#rule-pet').append(html);
            // alert(html);
         }
      }
      else
      {
         $('#rule-pet').hide(html);
      }
   }); -->

   <!-- if($(this).val() > 0){
         html='<div class="col-md-6" id="class-nama-hewan">'
            + '<div class="form-group">'
            + '<label>Nama Hewan</label>'
            + '<input type="text" name="nama_hewan" id="nama_hewan" class="form-control" />'
            + '<span id="err_msg_nama_hewan" class="text-danger"></span>'
            + '</div></div>'
            + '<div class="col-md-6" id="class-jenis-hewan">'
            + '<div class="form-group" id="det-pet-1">'
            + '<label>Jenis Hewan</label>'
            + '<select name="jenis_hewan" id="jenis_hewan_sel" required="true" class="form-control">'
            + '<?php echo ?>'
            + '<option required="true" value="0" disabled selected/>Jenis Hewan</option>'
            + 'foreach ($hewan as $h) { echo'
            + '<option required value=$h->jenis_hewan_id>$h->nama_jenis_hewan</option>'
            + '}?>'
            + '</select>'
            + '<span id="err_msg_jenis_hewan" class="text-danger"></span>'
            + '</div>';
         for (var i = 1; i<=qty; i++) {
            $('#rule-pet').append(html);
            // alert(html);
         }
      }
      else
      {
         $('#rule-pet').hide(html);
      } -->


      <!-- $("#qty").change(function(){
          var qty = $(this).val();
          
        alert(qty);
        $.ajax({
            url:"<?php echo base_url('form-regis/index'); ?>", 

            success: function(a){
                if($(this).val() > 0){
                 html='<div class="col-md-6" id="class-nama-hewan">'
                 + '<div class="form-group">'
                 + '<label>Nama Hewan</label>'
                 + '<input type="text" name="nama_hewan" id="nama_hewan" class="form-control" />'
                 + '<span id="err_msg_nama_hewan" class="text-danger"></span>'
                 + '</div></div>'
                 + '<div class="col-md-6" id="class-jenis-hewan">'
                 + '<div class="form-group" id="det-pet-1">'
                 + '<label>Jenis Hewan</label>'
                 + '<select name="jenis_hewan" id="jenis_hewan_sel" required="true" class="form-control">'
                 + <?php foreach ($hewan as $h) { echo ?>                 
                 + '<option required value=$h->jenis_hewan_id>$h->nama_jenis_hewan</option>'
                 + <?php } ?>
                 + '</select>'
                 + '<span id="err_msg_jenis_hewan" class="text-danger"></span>'
                 + '</div>';
                    for (var i = 1; i<=qty; i++) {
                        $('#namjen').append(html);
                        // alert(html);
                    }
                }
            else
            {
             $('#namjen').hide();
            }

            } //success
        });
          
        }); -->


        <!-- 8c -->
        <div class="col-md-12 hidden" id="rule-pet-anggora">
            <div class="panel panel-default">
                <div class="panel-heading">Rule Information Anggora</div> 
                <div class="panel-body">
                    <div class="form-group">
                        <label>Dokumen Vaksin Hewan</label>
                        <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                    </div>
                    <div class="form-group">
                        <label>Catatan</label>
                        <textarea name="catatan_pesananan" id="catatan_pesananan" class="form-control"></textarea>
                    </div>
                </div>                           
            </div>
        </div>
        <div class="col-md-12" id="rule-pet-herder">
            <div class="panel panel-default">
                <div class="panel-heading">Rule Information Herder</div> 
                <div class="panel-body">
                    <div class="form-group">
                        <label>Dokumen Vaksin Hewan</label>
                        <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                    </div>
                    <div class="form-group">
                        <label>Catatan</label>
                        <textarea name="catatan_pesananan" id="catatan_pesananan" class="form-control"></textarea>
                        <span id="err_msg_catatan_pesanan" class="text-danger"></span>
                    </div>
                </div>                           
            </div>                          
        </div>

        <div class="entry hidden">
            <div class="col-md-12" class="panel-heading">
                <div class="form-group">
                    <!-- inquiry -->
                    <input type="hidden" id="inquiry_id" name="inquiry_id" class="form-control" value="<?php echo $inquiry_id; ?>" >

                    <!-- inquiry dan inquiry produk -->
                    <input type="hidden" id="no_inquiry" name="no_inquiry" class="form-control" value="<?php echo $no_inquiry; ?>" >

                    <!-- inquiry produk -->
                    <input type="hidden" id="inquiry_produk_id" name="inquiry_produk_id" class="form-control" value="<?php echo $inquiry_produk_id; ?>" >
                    <input type="hidden" id="jenis_barang_id" name="jenis_barang_id" class="form-control" value="<?php echo $jenis_barang_id;     ?>" >

                    <!-- inquiry produk pet transport -->
                     <input type="hidden" id="inquiry_produk_pet_transport_id" name="inquiry_produk_pet_transport_id" class="form-control" value="<?php echo $inquiry_produk_pet_transport_id;     ?>" >
                    
                </div>
            </div>
            <div id="namjen">
                <div class="col-md-4">
                    <div class="form-group">
                       <label>Nama Hewan</label>
                       <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                       <span id="err_msg_nama_hewan" class="text-danger"></span>
                    </div>                            
                </div>
                <div class="col-md-4">
                    <div class="form-group" id="det-pet-1">
                        <label>Jenis Hewan</label>
                        <select name="jenis_hewan" id="jenis_hewan_sel" required='true' class="form-control">
                            <option value="0" disabled selected>Jenis Hewan</option>
                            <?php                                                           
                            foreach ($jenis_hewan as $jh) {
                                echo "<option required='true' value='$jh->jenis_hewan_id'>$jh->nama_jenis_hewan</option>";
                            }
                            ?>
                        </select>
                        <span id="err_msg_jenis_hewan" class="text-danger"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group" id="det-pet-1">
                        <label>Ras Hewan</label>
                        <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                            <option value="0" disabled selected>Ras Hewan</option>
                        </select>
                        <span id="err_msg_ras_hewan" class="text-danger"></span>
                    </div>
                </div>
            </div>            
        </div> <!-- entry -->


        <script type="text/javascript">
            // Rule Information
            $("#rule-pet-anggora").hide();
            $("#rule-pet-herder").hide();
            $(document).ready(function(){
                $(document).on( "click", "a#addhewan", function() {
                    var confirm = window.confirm('Yakin menambah data hewan??');
                    if(confirm == true){
                        var form_hewan = $('.entry').clone();
                        $(form_hewan).appendTo('.hewan-baru');                    
                    }                
                });

                $(document).on('change','select[id=jenis_hewan_sel]', function(e){
                    var id=$(this).val();
                    $.ajax({
                        url : "<?php echo base_url();?>form-regis/get_ras_hewan",
                        method : "POST",
                        data : {id: id},
                        async : false,
                        dataType : 'json',
                        success: function(a){
                            var html = '';
                            for(var i=0; i<a.length; i++){                                                        
                                html += '<option value='+a[i].ras_hewan_id+'>'+a[i].nama_ras_hewan+'</option>';                            
                            }
                            html1 = '<option value="0" disabled selected>Ras Hewan</option>';
                            $("select[id=ras_hewan_sel]").html(html1);                        
                            $(html).appendTo("select[id=ras_hewan_sel]");
                        }
                    });
                });

                // select jenis hewan
                $(document).on('change','#ras_hewan_sel', function(){        
                    var formRule = '';

                    if($(this).val() == "1"){                    
                        formRule = $('#rule-pet-anggora').clone();
                        formRule.appendTo('show-rule-hewan');
                        $('#rule-pet-herder').hide();
                    }else if($(this).val() == "2"){
                        formRule = $('#rule-pet-anggora').clone();
                        $('#rule-pet-herder').appendTo('show-rule-hewan');
                        $('#rule-pet-anggora').hide();              
                    }

                    if ($(this).val()=="") {
                        $('#all-rule-pet').hide();
                    }
                });        

                $('#kota-asal-pel-sel').change(function(){
                    var id=$(this).val();
                    $.ajax({
                        url : "<?php echo base_url();?>form-regis/get_negara_id",
                        method : "POST",
                        data : {id: id},
                        async : false,
                        dataType : 'json',
                        success: function(b){                       
                            var html = '';                        
                            html = b[0].negara_id;
                            $('.get-negara').val(html);
                        }
                    });            
                });
            });
        </script>
        <!-- 8c -->