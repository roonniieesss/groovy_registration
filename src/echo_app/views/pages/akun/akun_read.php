
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detail Akun
                </h2>
            </div>

            <div class="body">
                    <input type="hidden" name="akun_id" value="<?php echo $akun_id; ?>" />
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Nama Akun">Nama Akun <?php echo form_error("Nama Akun") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $nama_akun; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Kelompok">Kelompok <?php echo form_error("Kelompok") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $kelompok; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Active">Active <?php echo form_error("Active") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__active; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Created">Created <?php echo form_error("Created") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__created; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Updated">Updated <?php echo form_error("Updated") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__updated; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Username">Username <?php echo form_error("Username") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <?php echo $__username; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <a href="<?php echo base_url("akun") ?>" class="btn btn-default m-t-15 waves-effect">Cancel</a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
