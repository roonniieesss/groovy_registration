
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Form <?php echo $button ?> Akun
                </h2>
            </div>

            <?php echo $this->session->flashdata("message") ?: ""; ?>

            <div class="body">
                <form class="form-horizontal" action="<?php echo $action; ?>" method="post">
                    <input type="hidden" name="akun_id" value="<?php echo $akun_id; ?>" />

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Nama Akun">Nama Akun <?php echo form_error("Nama Akun") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="nama_akun" id="nama_akun" placeholder="Input Nama Akun" value="<?php echo $nama_akun; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="Kelompok">Kelompok <?php echo form_error("Kelompok") ?></label>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" name="kelompok" id="kelompok" placeholder="Input Kelompok" value="<?php echo $kelompok; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row clearfix">
                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect"><?php echo $button ?></button>
                            <a href="<?php echo base_url("akun") ?>" class="btn btn-default m-t-15 waves-effect">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
