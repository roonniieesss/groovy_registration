  <!-- Top Bar -->
  <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar-collapse"
                    aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo getenv('APP_NAME'); ?></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                    <!-- Notifications -->
                    <?php view('components/notifications');?>
                    <!-- #END# Notifications -->

                    <!-- Tasks -->
                    <?php view('components/tasks');?>
                    <!-- #END# Tasks -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->