<section class="content">
    <div class="container-fluid">
        <?php view('pages/' . $page, isset($content) ? $content : '');?>
    </div>
</section>