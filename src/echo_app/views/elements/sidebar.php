<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">

        <!-- User Info -->
        <?php view('components/userinfo');?>
        <!-- #User Info -->

        <!-- Menu -->
        <?php view('components/menus');?>
        <!-- #Menu -->

        <!-- Footer -->
        <?php view('components/legal');?>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->

</section>