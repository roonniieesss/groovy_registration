<?php

view('elements/header');

view('elements/navbar');

view('elements/sidebar');

view('elements/content', $page);

view('elements/footer');
