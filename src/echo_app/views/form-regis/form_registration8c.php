<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Groovy-CRM</title>
  <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrapValidator.min.js')?>"> </script>
  <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" rel="stylesheet" />
</head>
<body>
    <br />
    <div class="container box">
        <br />
        <h2 align="center">Groovy Registration</h2><br />
        <?php echo $this->session->flashdata('notif');?>             
        <?php echo form_open('form_regis/insert', array('enctype'=>'multipart/form-data','id'=>'register_form'));?>
        <ul class="nav nav-tabs">
           <li class="nav-item">
                <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_destination">Destination</a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive_tab1" id="list_pet_information" style="border:1px solid #ccc">Pet Information</a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive_tab1" id="list_customer_information" style="border:1px solid #ccc">Customer Informations</a>
            </li>
        </ul>
        <div class="tab-content" style="margin-top:16px;">
            <div class="tab-pane active" id="destination_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Destinations</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Kota/Airport Asal *</label>
                            <select name="kota_asal" required='true' id="kota-asal-sel" class="form-control selectpicker show-tick" data-live-search="true">
                                <option required='true' value='0' disabled='disabled' selected>Kota/Airport Asal *</option>
                                <?php
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>                               
                            </select>
                            <span id="err_msg_kota_asal" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Kota/Airport Tujuan *</label>
                            <select name="kota_tujuan" required='true' id="kota-tujuan-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                                <option required='true' value='0' disabled='disabled' selected>Kota/Airport Tujuan *</option>
                                <?php                                                                   
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>  
                           <span id="err_msg_kota_tujuan" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Jenis Layanan *</label>
                            <select name="jenis_layanan" required='true' id="jenis-layanan-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                                <option required='true' value='0' disabled='disabled' selected>Jenis Layanan *</option>
                                <?php                                                                   
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>  
                           <span id="err_msg_kota_tujuan" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Moda Transportasi</label>
                            <select name="moda_transportasi" required='true' id="moda-transportasi-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                                <option required='true' value='0' disabled='disabled' selected>Moda Trasnportasi</option>
                                <?php                                                                            
                                foreach ($transport as $t) {
                                    echo "<option required='true' value='$t->moda_transportasi_id'>$t->nama_moda_transportasi</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_moda_transportasi" class="text-danger"></span>
                        </div>
                        <div align="right">
                           <button type="button" name="btn_login_details" id="btn_destination_details" class="btn btn-info btn-lg">Next</button>
                        </div>
                    </div>  <!-- panel-body -->
                </div> <!-- panel panel-default -->
            </div> <!-- #destination-details -->

            <!-- PET INFORMATION -->
            <div class="tab-pane fade" id="pet_information_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill Pet Information </div>                    
                    <!-- utama -->
                    <div class="panel-body controls">
                        <form role="form" autocomplete="on" class="form-pet-information">                                                
                            <div class="entry">
                                <div class="col-md-12" class="panel-heading">
                                    <div class="form-group">
                                        <!-- inquiry -->
                                        <input type="hidden" id="inquiry_id" name="inquiry_id" class="form-control" value="<?php echo $inquiry_id; ?>" >

                                        <!-- inquiry dan inquiry produk -->
                                        <input type="hidden" id="no_inquiry" name="no_inquiry" class="form-control" value="<?php echo $no_inquiry; ?>" >

                                        <!-- inquiry produk -->
                                        <input type="hidden" id="inquiry_produk_id" name="inquiry_produk_id" class="form-control" value="<?php echo $inquiry_produk_id; ?>" >
                                        <input type="hidden" id="jenis_barang_id" name="jenis_barang_id" class="form-control" value="<?php echo $jenis_barang_id;     ?>" >

                                        <!-- inquiry produk pet transport -->
                                         <input type="hidden" id="inquiry_produk_pet_transport_id" name="inquiry_produk_pet_transport_id" class="form-control" value="<?php echo $inquiry_produk_pet_transport_id;     ?>" >
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-body">                
                                        <div id="namjen">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <label>Nama Hewan *</label>
                                                   <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                                                   <span id="err_msg_nama_hewan" class="text-danger"></span>
                                                </div>                            
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>BOD *</label>
                                                    <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                                                        <option value="0" disabled selected>Ras Hewan</option>
                                                    </select>
                                                    <span id="err_msg_ras_hewan" class="text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>Warna Hewan *</label>
                                                    <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                                                    <span id="err_msg_ras_hewan" class="text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>Jenis Kelamin *</label>
                                                    <select name="inquirypet[jenis_kelamin][]" class="form-control" id="ras_hewan_id">
                                                        <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                                        <option value="JANTAN">Jantan</option>
                                                        <option value="BETINA">Betina</option>
                                                    </select>
                                                    <span id="err_msg_ras_hewan" class="text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>Berat (gr) *</label>
                                                    <select name="inquirypet[jenis_kelamin][]" class="form-control" id="ras_hewan_id">
                                                        <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                                        <option value="JANTAN">Jantan</option>
                                                        <option value="BETINA">Betina</option>
                                                    </select>
                                                    <span id="err_msg_ras_hewan" class="text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>Microchip *</label>
                                                    <select name="inquirypet[jenis_kelamin][]" class="form-control" id="ras_hewan_id">
                                                        <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                                        <option value="JANTAN">Jantan</option>
                                                        <option value="BETINA">Betina</option>
                                                    </select>
                                                    <span id="err_msg_ras_hewan" class="text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>Jenis Hewan *</label>
                                                    <select name="jenis_hewan" id="jenis_hewan_sel" required='true' class="form-control">
                                                        <option value="0" disabled selected>Jenis Hewan</option>
                                                        <?php                                                           
                                                        foreach ($jenis_hewan as $jh) {
                                                            echo "<option required='true' value='$jh->jenis_hewan_id'>$jh->nama_jenis_hewan</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                    <span id="err_msg_jenis_hewan" class="text-danger"></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group" id="det-pet-1">
                                                    <label>Ras Hewan *</label>
                                                    <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                                                        <option value="0" disabled selected>Ras Hewan</option>
                                                    </select>
                                                    <span id="err_msg_ras_hewan" class="text-danger"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                                <div class="panel-body hewan-baru"></div>                           
                                <div class="show-rule-hewan"></div>
                                
                            </div> <!-- entry -->

                            <div class="col-md-12">                             
                                <div class="form-group">
                                    <a class="btn btn-info btn-lg btn-block" id="addhewan">Tambah Hewan</a>
                                </div>
                            </div>                          
                        </form>
                        <div class="col-md-12">
                            <br><br><br>                                                
                            <button type="button" name="previous_btn_pet_details" id="previous_btn_pet_details" class="btn btn-default btn-lg pull-left">Previous</button>
                            <button type="button" name="btn_pet_details" id="btn_pet_details" class="btn btn-info btn-lg pull-right">Next</button>
                        </div>                                
                    </div> <!-- panel-body -->
                </div> <!-- panel panel-default -->
            </div> <!-- pet_informations_details -->
            <!-- //PET INFORMATION -->

            <!-- PELANGGAN -->
            <div class="tab-pane fade" id="pelanggan_information_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill Customer Information</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <!-- alamat_pelanggan_id -->
                                <input type="hidden" name="alamat_pelanggan_id" value="<?php echo $alamat_pelanggan_id ?>"/>                  
                                <label>Nama Pelanggan</label>
                                <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" />
                                <span id="err_msg_nama_pelanggan" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label>Tanggal Lahir</label>
                               <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" />
                               <span id="err_msg_tgl_lahir" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <label class="radio-inline">
                                    <input type="radio" name="jenis_kelamin" value="PRIA" checked> Pria
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="jenis_kelamin" value="WANITA"> Wanita
                                </label>
                                <span id="err_msg_jenis_kelamin" class="text-danger"></span>
                            </div>
                        </div>                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kota Asal</label>
                                <select name="kota_asal_pel" id="kota-asal-pel-sel" required="true" class="form-control selectpicker show-tick" data-live-search="true">
                                    <option value="0" disabled selected>Kota Asal</option>
                                    <?php                                                           
                                    foreach ($kota as $k) {
                                        echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                    }
                                    ?>
                                </select>                                
                                <span id="err_msg_jenis_kontak" class="text-danger"></span>
                                <input type="hidden" name="get-negara" class="get-negara"/>                               
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kode POS</label>
                                <input type="text" name="kode_pos" id="kode_pos" class="form-control" placeholder="Kode POS" />
                                <span id="err_msg_kode_pos" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Penerima</label>
                                <input type="text" name="nama_penerima" id="nama_penerima" class="form-control" placeholder="Nama Penerima" />
                                <span id="err_msg_nama_penerima" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Label</label>
                                <input type="text" name="label" id="label" class="form-control" placeholder="Label" />
                                <span id="err_msg_nama_penerima" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label>Kontak</label>
                               <input type="text" name="kontak" id="kontak" class="form-control" placeholder="Kontak" />
                               <span id="err_msg_kontak" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">                       
                            <div class="form-group">
                                <!-- kontak_pelanggan_id -->
                                <input type="hidden" id="kontak_pelanggan_id" name="kontak_pelanggan_id"class="form-control" value="<?php echo $kontak_pel_id;  ?>" >
                                <!-- pelanggan_id -->
                                <input type="hidden" id="pelanggan_id" name="pelanggan_id" class="form-control" value="<?php echo $id; ?>" >
                                <label>Jenis Kontak</label>
                                <select name="jenis_kontak_pelanggan_id" required='true' class="form-control">
                                    <option value="0" disabled selected>Jenis Kontak</option>
                                    <?php                                                           
                                    foreach ($kontak as $k) {
                                        echo "<option required='true' value='$k->jenis_kontak_pelanggan_id'>$k->nama_jenis_kontak_pelanggan</option>";
                                    }
                                    ?>
                                </select>
                                <span id="err_msg_jenis_kontak" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label>Alamat</label>
                               <textarea type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat"></textarea>
                               <span id="err_msg_alamat" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" name="previous_btn_cust_details" id="previous_btn_cust_details" class="btn btn-default btn-lg pull-left">Previous</button>
                            <button type="submit" name="submit" id="Register" class="btn btn-info btn-lg pull-right">Register</button>
                        </div>
                        <br/>
                    </div>
                </div> 
            </div>

            <!-- //PELANGGAN -->

        </div>  
        <!-- //TAB-CONTENT -->
        <?php echo form_close(); ?>

    </div>
    <!-- //CONTAINER BOX -->

    <div class="panel panel-default copyHewan">
        <div class="panel-body">
            <div class="entryCopy">
                <div class="col-md-12" class="panel-heading">
                    <div class="form-group">
                        <!-- inquiry -->
                        <input type="hidden" id="inquiry_id" name="inquiry_id" class="form-control" value="<?php echo $inquiry_id; ?>" >

                        <!-- inquiry dan inquiry produk -->
                        <input type="hidden" id="no_inquiry" name="no_inquiry" class="form-control" value="<?php echo $no_inquiry; ?>" >

                        <!-- inquiry produk -->
                        <input type="hidden" id="inquiry_produk_id" name="inquiry_produk_id" class="form-control" value="<?php echo $inquiry_produk_id; ?>" >
                        <input type="hidden" id="jenis_barang_id" name="jenis_barang_id" class="form-control" value="<?php echo $jenis_barang_id;     ?>" >

                        <!-- inquiry produk pet transport -->
                         <input type="hidden" id="inquiry_produk_pet_transport_id" name="inquiry_produk_pet_transport_id" class="form-control" value="<?php echo $inquiry_produk_pet_transport_id;     ?>" >
                    </div>
                </div>                   
                <div id="namjen">
                    <div class="col-md-6">
                        <div class="form-group">
                           <label>Nama Hewan *</label>
                           <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                           <span id="err_msg_nama_hewan" class="text-danger"></span>
                        </div>                            
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>BOD *</label>
                            <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                                <option value="0" disabled selected>Ras Hewan</option>
                            </select>
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>Warna Hewan *</label>
                            <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>Jenis Kelamin *</label>
                            <select name="inquirypet[jenis_kelamin][]" class="form-control" id="ras_hewan_id">
                                <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                <option value="JANTAN">Jantan</option>
                                <option value="BETINA">Betina</option>
                            </select>
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>Berat (gr) *</label>
                            <select name="inquirypet[jenis_kelamin][]" class="form-control" id="ras_hewan_id">
                                <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                <option value="JANTAN">Jantan</option>
                                <option value="BETINA">Betina</option>
                            </select>
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>Microchip *</label>
                            <select name="inquirypet[jenis_kelamin][]" class="form-control" id="ras_hewan_id">
                                <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                <option value="JANTAN">Jantan</option>
                                <option value="BETINA">Betina</option>
                            </select>
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>Jenis Hewan *</label>
                            <select name="jenis_hewan" id="jenis_hewan_sel" required='true' class="form-control">
                                <option value="0" disabled selected>Jenis Hewan</option>
                                <?php                                                           
                                foreach ($jenis_hewan as $jh) {
                                    echo "<option required='true' value='$jh->jenis_hewan_id'>$jh->nama_jenis_hewan</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_jenis_hewan" class="text-danger"></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group" id="det-pet-1">
                            <label>Ras Hewan *</label>
                            <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                                <option value="0" disabled selected>Ras Hewan</option>
                            </select>
                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">                             
                    <div class="form-group">
                        <a class="btn btn-danger btn-lg btn-block" id="deleteHewan">Hapus Hewan</a>
                    </div>
                </div>
            </div> <!-- entryCopy -->
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on( "click", "a#addhewan", function() {
                var confirm = window.confirm('Yakin menambah data hewan??');
                if(confirm == true){
                    var form_hewan = $('.copyHewan').html();
                    $(form_hewan).insertBefore('.hewan-baru');                    
                }                
            });
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
            });
        });
    </script>
    <script src="<?php echo base_url('assets/js/main8b.js')?>"></script>
    <!-- js untuk bootstrap datetimepicker -->
    <script src="<?php echo base_url('assets/js/bootstrap-select.min.js')?>"></script>
</body>
</html>