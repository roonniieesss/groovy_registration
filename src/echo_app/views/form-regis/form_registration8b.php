<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Groovy-CRM</title>
  <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrapValidator.min.js')?>"> </script>
  <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" rel="stylesheet" />
</head>
<body>
    <br />
    <div class="container box">
        <br />
        <h2 align="center">Groovy Registration</h2><br />
        <?php echo $this->session->flashdata('notif');?>             
        <?php echo form_open('form_regis/insert', array('enctype'=>'multipart/form-data','id'=>'register_form'));?>
        <ul class="nav nav-tabs">
           <li class="nav-item">
                <a class="nav-link active_tab1" style="border:1px solid #ccc" id="list_destination">Destination</a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive_tab1" id="list_pet_information" style="border:1px solid #ccc">Pet Information</a>
            </li>
            <li class="nav-item">
                <a class="nav-link inactive_tab1" id="list_customer_information" style="border:1px solid #ccc">Customer Informations</a>
            </li>
        </ul>
        <div class="tab-content" style="margin-top:16px;">
            <div class="tab-pane active" id="destination_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Destinations</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Kota/Airport Asal *</label>
                            <select name="kota_asal" required='true' id="kota-asal-sel" class="form-control selectpicker show-tick" data-live-search="true">
                                <option required='true' value='0' disabled='disabled' selected>Kota/Airport Asal *</option>
                                <?php
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>                               
                            </select>
                            <span id="err_msg_kota_asal" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Kota/Airport Tujuan *</label>
                            <select name="kota_tujuan" required='true' id="kota-tujuan-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                                <option required='true' value='0' disabled='disabled' selected>Kota/Airport Tujuan *</option>
                                <?php                                                                   
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>  
                           <span id="err_msg_kota_tujuan" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Jenis Layanan *</label>
                            <select name="jenis_layanan" required='true' id="jenis-layanan-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                                <option required='true' value='0' disabled='disabled' selected>Jenis Layanan *</option>
                                <?php                                                                   
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>  
                           <span id="err_msg_kota_tujuan" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label>Moda Transportasi</label>
                            <select name="moda_transportasi" required='true' id="moda-transportasi-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                                <option required='true' value='0' disabled='disabled' selected>Moda Trasnportasi</option>
                                <?php                                                                            
                                foreach ($transport as $t) {
                                    echo "<option required='true' value='$t->moda_transportasi_id'>$t->nama_moda_transportasi</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_moda_transportasi" class="text-danger"></span>
                        </div>
                        <div align="right">
                           <button type="button" name="btn_login_details" id="btn_destination_details" class="btn btn-info btn-lg">Next</button>
                        </div>
                    </div>  <!-- panel-body -->
                </div> <!-- panel panel-default -->
            </div> <!-- #destination-details -->
            <div class="tab-pane fade" id="pet_information_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill Pet Information </div>                    
                    <!-- utama -->
                    <div class="panel-body controls">
                        <form role="form" autocomplete="off" class="form-pet-information">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="fa fa-plus"></i>
                                        Add Pet <input type="number" name="add_pet" class="pull-right" id="add-pet" placeholder="Add Your Pet">
                                    </div>
                                </div>
                            </div>                                
                            <div class="entry">
                                <div class="col-md-12" class="panel-heading">
                                    <div class="form-group">
                                        <!-- inquiry -->
                                        <input type="hidden" id="inquiry_id" name="inquiry_id" class="form-control" value="<?php echo $inquiry_id; ?>" >

                                        <!-- inquiry dan inquiry produk -->
                                        <input type="hidden" id="no_inquiry" name="no_inquiry" class="form-control" value="<?php echo $no_inquiry; ?>" >

                                        <!-- inquiry produk -->
                                        <input type="hidden" id="inquiry_produk_id" name="inquiry_produk_id" class="form-control" value="<?php echo $inquiry_produk_id; ?>" >
                                        <input type="hidden" id="jenis_barang_id" name="jenis_barang_id" class="form-control" value="<?php echo $jenis_barang_id;     ?>" >

                                        <!-- inquiry produk pet transport -->
                                         <input type="hidden" id="inquiry_produk_pet_transport_id" name="inquiry_produk_pet_transport_id" class="form-control" value="<?php echo $inquiry_produk_pet_transport_id;     ?>" >

                                        <!-- <label>Jumlah Hewan</label> -->
                                    </div>
                                </div>                   
                                <div id="namjen">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                           <label>Nama Hewan</label>
                                           <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                                           <span id="err_msg_nama_hewan" class="text-danger"></span>
                                        </div>                            
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" id="det-pet-1">
                                            <label>Jenis Hewan</label>
                                            <select name="jenis_hewan" id="jenis_hewan_sel" required='true' class="form-control">
                                                <option value="0" disabled selected>Jenis Hewan</option>
                                                <?php                                                           
                                                foreach ($jenis_hewan as $jh) {
                                                    echo "<option required='true' value='$jh->jenis_hewan_id'>$jh->nama_jenis_hewan</option>";
                                                }
                                                ?>
                                            </select>
                                            <span id="err_msg_jenis_hewan" class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" id="det-pet-1">
                                            <label>Ras Hewan</label>
                                            <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                                                <option value="0" disabled selected>Ras Hewan</option>
                                            </select>
                                            <span id="err_msg_ras_hewan" class="text-danger"></span>
                                        </div>
                                    </div>
                                </div>                     
                                <div class="col-md-12" id="rule-pet-herder">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Rule Information Herder</div> 
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Dokumen Vaksin Hewan</label>
                                                <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                                            </div>
                                            <div class="form-group">
                                                <label>Catatan</label>
                                                <textarea name="catatan_pesananan" id="catatan_pesananan" class="form-control"></textarea>
                                                <span id="err_msg_catatan_pesanan" class="text-danger"></span>
                                            </div>
                                        </div>                           
                                    </div>                          
                                </div>
                                <div class="col-md-12" id="rule-pet-anggora">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Rule Information Anggora</div> 
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Dokumen Vaksin Hewan</label>
                                                <input type="file" class="form-control" id="dokumen_vaksin" name="dokumen_vaksin">
                                            </div>
                                            <div class="form-group">
                                                <label>Catatan</label>
                                                <textarea name="catatan_pesananan" id="catatan_pesananan" class="form-control"></textarea>
                                            </div>
                                        </div>                           
                                    </div>
                                </div>
                            </div> <!-- entry -->
                        </form>
                        <div class="col-md-12">
                            <br><br><br>                                                
                            <button type="button" name="previous_btn_pet_details" id="previous_btn_pet_details" class="btn btn-default btn-lg pull-left">Previous</button>
                            <button type="button" name="btn_pet_details" id="btn_pet_details" class="btn btn-info btn-lg pull-right">Next</button>
                        </div>                                
                    </div> <!-- panel-body -->
                </div> <!-- panel panel-default -->
            </div> <!-- pet_informations_details -->   
            <div class="tab-pane fade" id="pelanggan_information_details">
                <div class="panel panel-default">
                    <div class="panel-heading">Fill Customer Information</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <!-- alamat_pelanggan_id -->
                                <input type="hidden" name="alamat_pelanggan_id" value="<?php echo $alamat_pelanggan_id ?>"/>                  
                                <label>Nama Pelanggan</label>
                                <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" />
                                <span id="err_msg_nama_pelanggan" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label>Tanggal Lahir</label>
                               <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" />
                               <span id="err_msg_tgl_lahir" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <label class="radio-inline">
                                    <input type="radio" name="jenis_kelamin" value="PRIA" checked> Pria
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="jenis_kelamin" value="WANITA"> Wanita
                                </label>
                                <span id="err_msg_jenis_kelamin" class="text-danger"></span>
                            </div>
                        </div>                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kota Asal</label>
                                <select name="kota_asal_pel" id="kota-asal-pel-sel" required="true" class="form-control selectpicker show-tick" data-live-search="true">
                                    <option value="0" disabled selected>Kota Asal</option>
                                    <?php                                                           
                                    foreach ($kota as $k) {
                                        echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                    }
                                    ?>
                                </select>                                
                                <span id="err_msg_jenis_kontak" class="text-danger"></span>
                                <input type="hidden" name="get-negara" class="get-negara"/>                               
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kode POS</label>
                                <input type="text" name="kode_pos" id="kode_pos" class="form-control" placeholder="Kode POS" />
                                <span id="err_msg_kode_pos" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Penerima</label>
                                <input type="text" name="nama_penerima" id="nama_penerima" class="form-control" placeholder="Nama Penerima" />
                                <span id="err_msg_nama_penerima" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Label</label>
                                <input type="text" name="label" id="label" class="form-control" placeholder="Label" />
                                <span id="err_msg_nama_penerima" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label>Kontak</label>
                               <input type="text" name="kontak" id="kontak" class="form-control" placeholder="Kontak" />
                               <span id="err_msg_kontak" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">                       
                            <div class="form-group">
                                <!-- kontak_pelanggan_id -->
                                <input type="hidden" id="kontak_pelanggan_id" name="kontak_pelanggan_id"class="form-control" value="<?php echo $kontak_pel_id;  ?>" >
                                <!-- pelanggan_id -->
                                <input type="hidden" id="pelanggan_id" name="pelanggan_id" class="form-control" value="<?php echo $id; ?>" >
                                <label>Jenis Kontak</label>
                                <select name="jenis_kontak_pelanggan_id" required='true' class="form-control">
                                    <option value="0" disabled selected>Jenis Kontak</option>
                                    <?php                                                           
                                    foreach ($kontak as $k) {
                                        echo "<option required='true' value='$k->jenis_kontak_pelanggan_id'>$k->nama_jenis_kontak_pelanggan</option>";
                                    }
                                    ?>
                                </select>
                                <span id="err_msg_jenis_kontak" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                               <label>Alamat</label>
                               <textarea type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat"></textarea>
                               <span id="err_msg_alamat" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" name="previous_btn_cust_details" id="previous_btn_cust_details" class="btn btn-default btn-lg pull-left">Previous</button>
                            <button type="submit" name="submit" id="Register" class="btn btn-info btn-lg pull-right">Register</button>
                        </div>
                        <br/>
                    </div>
                </div> 
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>

    <script type="text/javascript">
        // Rule Information
        $("#rule-pet-anggora").hide();
        $("#rule-pet-herder").hide();

        $(document).ready(function(){            
            $(document).on('change','select[id=jenis_hewan_sel]', function(e){
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url();?>form-regis/get_ras_hewan",
                    method : "POST",
                    data : {id: id},
                    async : false,
                    dataType : 'json',
                    success: function(a){
                        var html = '';
                        for(var i=0; i<a.length; i++){                                                        
                            html += '<option value='+a[i].ras_hewan_id+'>'+a[i].nama_ras_hewan+'</option>';                            
                        }
                        html1 = '<option value="0" disabled selected>Ras Hewan</option>';
                        $("select[id=ras_hewan_sel]").html(html1);                        
                        $(html).appendTo("select[id=ras_hewan_sel]");
                    }
                });
            });

            // select jenis hewan
            $("#ras_hewan_sel").change(function(){
                // alert($("#jenis_hewan").val());
                if($(this).val() == "1"){
                    $('#rule-pet-anggora').show();
                    $('#rule-pet-herder').hide();
                }else if($(this).val() == "2"){
                    $('#rule-pet-herder').show();
                    $('#rule-pet-anggora').hide();              
                }

                if ($(this).val()=="") {
                    $('#all-rule-pet').hide();
                }
            });        

            $('#kota-asal-pel-sel').change(function(){
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url();?>form-regis/get_negara_id",
                    method : "POST",
                    data : {id: id},
                    async : false,
                    dataType : 'json',
                    success: function(b){                       
                        var html = '';                        
                        html = b[0].negara_id;
                        $('.get-negara').val(html);
                    }
                });            
            });

            $("#add-pet").keyup(function(){
                var qty = $(this).val();
                var intRegex = $(this).val().replace(/[^0-9]/g, '');
                
                if(qty != "" && qty == intRegex){
                    for (var i = 1; i<=qty; i++) {
                        // curForm = $("#namjen").clone();
                        // curForm.appendTo(".entry").attr("id","namjen2");
                        $("#namjen").clone().appendTo(".entry").attr("id","namjen"+i);
                        $("#namjen"+i).find("select[id=jenis_hewan_sel]").attr("id","jenis_hewan_sel"+i);
                        $("#namjen"+i).find("select[id=ras_hewan_sel]").attr("id","ras_hewan_sel"+i);                        
                    }
                }
                if(qty == "" ){
                    // var najmen2 = $(".entry").find("#namjen2");
                    // $(".entry").find("#namjen2").remove();
                    $(".entry").children().not('#namjen').not('#add-pet').remove();

                }
            });           

        }); //document ready function

    </script>
    <script src="<?php echo base_url('assets/js/main8b.js')?>"></script>
    <!-- js untuk bootstrap datetimepicker -->
    <script src="<?php echo base_url('assets/js/bootstrap-select.min.js')?>"></script>
</body>
</html>