<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Groovy-CRM</title>
<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/bootstrapValidator.min.js')?>"> </script>
<link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" rel="stylesheet" />
<!------ Include the above in your HEAD tag ---------->

<div class="container box">
	<div class="row form-group">
        <div class="col-xs-12" id="#kepala">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                <li class="active"><a href="#step-1">
                    <h4 class="list-group-item-heading">Step 1</h4>
                    <p class="list-group-item-text">First step description</p>
                </a></li>
                <li class="disabled"><a href="#step-2">
                    <h4 class="list-group-item-heading">Step 2</h4>
                    <p class="list-group-item-text">Second step description</p>
                </a></li>
                <li class="disabled"><a href="#step-3">
                    <h4 class="list-group-item-heading">Step 3</h4>
                    <p class="list-group-item-text">Third step description</p>
                </a></li>
            </ul>
        </div>
	</div>
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12 well text-center">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="pull-left">Kota/Airport Asal *</label>
                        <select name="kota_asal" required='true' id="kota-asal-sel" class="form-control selectpicker show-tick" data-live-search="true" data-live-search-style="startsWith">
                            <option required='true' value='0' disabled='disabled' selected>Kota/Airport Asal *</option>
                            <?php
                            foreach ($kota as $k) {
                                echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                            }
                            ?>                               
                        </select>
                        <span id="err_msg_kota_asal" class="text-danger pull-right"></span>
                    </div>
                    <div class="form-group">
                        <label class="pull-left">Kota/Airport Tujuan *</label>
                        <select name="kota_tujuan" required='true' id="kota-tujuan-sel" class="form-control">
                            <option required='true' value='0' disabled='disabled' selected>Kota/Airport Tujuan *</option>
                            <?php                                                                   
                            foreach ($kota as $k) {
                                echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                            }
                            ?>
                        </select>  
                       <span id="err_msg_kota_tujuan" class="text-danger pull-right"></span>
                    </div>
                    <div class="form-group">
                        <label class="pull-left">Jenis Layanan *</label>
                        <select name="jenis_layanan" required='true' id="jenis-layanan-sel" class="form-control show-tick">
                            <option required='true' value='0' disabled='disabled' selected>Jenis Layanan *</option>
                            <?php                                                                   
                            foreach ($kota as $k) {
                                echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                            }
                            ?>
                        </select>  
                       <span id="err_msg_jenis_layanan" class="text-danger pull-right"></span>
                    </div>
                    <div class="form-group">
                        <label class="pull-left">Moda Transportasi *</label>
                        <select name="moda_transportasi" required='true' id="moda-transportasi-sel" class="form-control">
                            <option required='true' value='0' disabled='disabled' selected>Moda Trasnportasi</option>
                            <?php                                                                            
                            foreach ($transport as $t) {
                                echo "<option required='true' value='$t->moda_transportasi_id'>$t->nama_moda_transportasi</option>";
                            }
                            ?>
                        </select>
                        <span id="err_msg_moda_transportasi" class="text-danger pull-right"></span>
                    </div>
                </div>
                <button id="activate-step-2" class="btn btn-info btn-lg pull-right">Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="panel-body controls">
                    <form role="form" autocomplete="on" class="form-pet-information">                                                
                        <div class="entry">
                            <div class="col-md-12" class="panel-heading">
                                <div class="form-group">
                                    <!-- inquiry -->
                                    <input type="hidden" id="inquiry_id" name="inquiry_id" class="form-control" value="<?php echo $inquiry_id; ?>" >

                                    <!-- inquiry dan inquiry produk -->
                                    <input type="hidden" id="no_inquiry" name="no_inquiry" class="form-control" value="<?php echo $no_inquiry; ?>" >

                                    <!-- inquiry produk -->
                                    <input type="hidden" id="inquiry_produk_id" name="inquiry_produk_id" class="form-control" value="<?php echo $inquiry_produk_id; ?>" >
                                    <input type="hidden" id="jenis_barang_id" name="jenis_barang_id" class="form-control" value="<?php echo $jenis_barang_id;     ?>" >

                                    <!-- inquiry produk pet transport -->
                                     <input type="hidden" id="inquiry_produk_pet_transport_id" name="inquiry_produk_pet_transport_id" class="form-control" value="<?php echo $inquiry_produk_pet_transport_id;     ?>" >
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">                
                                    <div id="namjen">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                               <label>Nama Hewan *</label>
                                               <input type="text" name="nama_hewan" id="nama_hewan" class="form-control" placeholder="Nama Hewan" />
                                               <span id="err_msg_nama_hewan" class="text-danger"></span>
                                            </div>                            
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="form-group" id="det-pet-1">
                                                <label>DOB *</label>
                                                <input type="date" name="dob" id="dob" class="form-control datepicker" placeholder="Date of Birth" />
                                                <span id="err_msg_dob_hewan" class="text-danger"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group" id="det-pet-1">
                                                <label>Warna Hewan *</label>
                                                <input type="text" name="warna_hewan" id="warna_hewan" class="form-control" placeholder="Nama Hewan" />
                                                <span id="err_msg_warna_hewan" class="text-danger"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group" id="det-pet-1">
                                                <label>Jenis Kelamin *</label>
                                                <select name="inquirypet[jenis_kelamin][]" class="form-control" id="jenis_kelamin_hewan">
                                                    <option value="" selected="selected">Pilih Jenis Kelamin</option>
                                                    <option value="JANTAN">Jantan</option>
                                                    <option value="BETINA">Betina</option>
                                                </select>
                                                <span id="err_msg_jenis_kelamin_hewan" class="text-danger"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group" id="det-pet-1">
                                                <label>Berat (gr) *</label>
                                                <input type="number" name="berat_hewan" id="berat_hewan" class="form-control" placeholder="Berat Hewan" />
                                                <span id="err_msg_berat_hewan" class="text-danger"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group" id="det-pet-1">
                                                <label>Microchip *</label>
                                                <input type="text" name="microchip" id="microchip" class="form-control" placeholder="Nama Hewan" />
                                                <span id="err_msg_microchip_hewan" class="text-danger"></span>
                                            </div>
                                        </div>                                    

                                        <div class="col-md-12">
                                            <div class="form-group" id="det-pet-1">
                                                <label>Ras Hewan *</label>
                                                <select name="ras_hewan" id="ras_hewan_sel" required='true' class="form-control">
                                                    <option value="0" disabled selected>Ras Hewan</option>
                                                </select>
                                                <span id="err_msg_ras_hewan" class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="panel-body hewan-baru"></div>                           
                            <div class="show-rule-hewan"></div>
                            
                        </div> <!-- entry -->

                        <div class="col-md-12">                             
                            <div class="form-group">
                                <a class="btn btn-info btn-lg btn-block" id="addhewan">Tambah Hewan</a>
                            </div>
                        </div>                          
                    </form>                                            
                </div> <!-- panel-body -->
                <a href="#step-1" name="previous_1" id="previous_1" class="btn btn-warning btn-lg pull-left">Previous</a>         
                <button id="activate-step-3" class="btn btn-info btn-lg pull-right">Next</button>
            </div>
        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="form-group"> 
                            <!-- alamat_pelanggan_id -->
                            <input type="hidden" name="alamat_pelanggan_id" value="<?php echo $alamat_pelanggan_id ?>"/>                  
                            <label>Nama Pelanggan</label>
                            <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" />
                            <span id="err_msg_nama_pelanggan" class="text-danger"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           <label>Tanggal Lahir</label>
                           <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" />
                           <span id="err_msg_tgl_lahir" class="text-danger"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <label class="radio-inline">
                                <input type="radio" name="jenis_kelamin" value="PRIA" checked> Pria
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="jenis_kelamin" value="WANITA"> Wanita
                            </label>
                            <span id="err_msg_jenis_kelamin" class="text-danger"></span>
                        </div>
                    </div>                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kota Asal</label>
                            <select name="kota_asal_pel" id="kota-asal-pel-sel" required="true" class="form-control selectpicker show-tick" data-live-search="true">
                                <option value="0" disabled selected>Kota Asal</option>
                                <?php                                                           
                                foreach ($kota as $k) {
                                    echo "<option required='true' value='$k->kota_id'>$k->nama_kota</option>";
                                }
                                ?>
                            </select>                                
                            <span id="err_msg_jenis_kontak" class="text-danger"></span>
                            <input type="hidden" name="get-negara" class="get-negara"/>                               
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kode POS</label>
                            <input type="text" name="kode_pos" id="kode_pos" class="form-control" placeholder="Kode POS" />
                            <span id="err_msg_kode_pos" class="text-danger"></span>
                        </div>
                    </div>                                
                    <div class="col-md-12">
                        <div class="form-group">
                           <label>Kontak</label>
                           <input type="text" name="kontak" id="kontak" class="form-control" placeholder="Kontak" />
                           <span id="err_msg_kontak" class="text-danger"></span>
                        </div>
                    </div>
                    <div class="col-md-12">                       
                        <div class="form-group">
                            <!-- kontak_pelanggan_id -->
                            <input type="hidden" id="kontak_pelanggan_id" name="kontak_pelanggan_id"class="form-control" value="<?php echo $kontak_pel_id;  ?>" >
                            <!-- pelanggan_id -->
                            <input type="hidden" id="pelanggan_id" name="pelanggan_id" class="form-control" value="<?php echo $id; ?>" >
                            <label>Jenis Kontak</label>
                            <select name="jenis_kontak_pelanggan_id" required='true' class="form-control">
                                <option value="0" disabled selected>Jenis Kontak</option>
                                <?php                                                           
                                foreach ($kontak as $k) {
                                    echo "<option required='true' value='$k->jenis_kontak_pelanggan_id'>$k->nama_jenis_kontak_pelanggan</option>";
                                }
                                ?>
                            </select>
                            <span id="err_msg_jenis_kontak" class="text-danger"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                           <label>Alamat</label>
                           <textarea type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat"></textarea>
                           <span id="err_msg_alamat" class="text-danger"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a href="#step-2" name="previous_2" id="previous_2" class="btn btn-warning btn-lg pull-left">Previous</a>
                        <button type="submit" name="submit" id="Register" class="btn btn-info btn-lg pull-right">Register</button>
                    </div>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
                
    });
</script>
<script src="<?php echo base_url('assets/js/main8d.js')?>"></script>
<!-- js untuk bootstrap datetimepicker -->
<script src="<?php echo base_url('assets/js/bootstrap-select.min.js')?>"></script>
